This is a repository for *Assimilate*, a framework for rapid prototyping of data assimilation algorithms. 

If you are interested in contributing, please contact nadrey@gmail.com

NOTE: Development is still in early stages. At the moment, all C++ code is contained in header files for ease of implementation.