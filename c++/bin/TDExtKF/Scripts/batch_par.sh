#!/bin/bash
#$ -t 1-100
#$ -N nM_sweep
#$ -cwd
#$ -j y
#$ -S /bin/bash
#$ -o ./output
#$ -e ./error
#$ -q batch.q
# -M drey@physics.ucsd.edu
# -m beas
nD=$1;
dTau=10;
nTau=1;
root_dir="/home/drey/Desktop/Data/Lorenz96/mSweep";
for nM in {0..15}; do
  if [ ! -f "$data_dir/nM=$nM" ]; then
    ./run_TDExtKF.exe $SGE_TASK_ID $nD $(($nM*$nTau)) $dTau;
    data_dir="$root_dir/nD=$nD/data/run_$SGE_TASK_ID";
    mv "$data_dir/data" "$data_dir/nM=$nM";
  fi
done
