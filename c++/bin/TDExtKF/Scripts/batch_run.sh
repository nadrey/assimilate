#!/bin/bash

#dims=( 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20);
dims=( 6 7 8 9 11 12 13 14 16 17 18 19);
export dTau=10;
export root_dir="/home/drey/Desktop/Data/Lorenz96/mSweep";

for nD in "${dims[@]}"; do
  for ic in {1..100}; do
    ./nM_sweep.sh $ic $nD;
  done
done
