#!/bin/bash
nD=$1;
dTau=1;
nTau=10;
root_dir="/home/drey/Desktop/Data/Lorenz96/mSweep";
for iRun in {1..100}; do
  for nM in {0..15}; do
    fName="$root_dir/nD=$nD/data/run_$iRun/nM=$nM";
    if [ ! -f $fName ]; then
      echo "run_$iRun/nM=$nM";
    fi
  done
done
