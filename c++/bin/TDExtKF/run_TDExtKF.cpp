/*****************************************************
 *
 *****************************************************/

// My includes
#include "TDExtKF.h"
#include "TrueExperimentData.h"
#include "Lorenz96.h"

////////////////////////////////////////////////////////////////
/// typedefs
typedef Lorenz96 Model;
typedef TrueExperimentData<Model> Data;
typedef TDExtKF<Model> Filter;

////////////////////////////////////////////////////////////////
// gateway function
int main(int argc, char *argv[]) {
  // Input arguments
  std::cout << std::endl;
  std::stringstream ss;
  ss << argv[1]; uint jobID;  ss >> jobID;
  ss.clear(); ss.str("");
  std::cout << "Job ID: " << jobID << std::endl;
  ss << argv[2]; uint nD; ss >> nD;
  ss.clear(); ss.str("");
  std::cout << "nD: " << nD << std::endl;
  ss << argv[3]; uint nTau; ss >> nTau;
  ss.clear(); ss.str("");
  std::cout << "nTau: " << nTau << std::endl;
  ss << argv[4]; uint dTau; ss >> dTau;
  ss.clear(); ss.str("");
  std::cout << "dTau: " << dTau << std::endl;

  // Initialize random seed:
  init_rand();

  // Initialize model
  Model model(nD);

  // Data directory
  std::string dataDir = "/home/drey/Desktop/Data/Lorenz96/mSweep";
  ss << "/nD=" << nD << "/data";
  dataDir = dataDir + ss.str();
  ss.clear(); ss.str("");

  // Job directory
  ss << "/run_" << jobID;
  std::string jobDir = dataDir + ss.str();
  ss.clear(); ss.str("");

  // Load/set data
  Data data(model);
  std::string fName;
  fName = jobDir + "/yMeas";
  data.load(fName.c_str());

  // Load/set initial conditions
  fName = jobDir + "/x0Est";
  std::ifstream file(fName.c_str(),std::ios::in|std::ios::binary);
  if(!file.is_open()) {
    const char *msg = "Could not open file ";
    std::cout << msg << fName << std::endl;
    return 1;
  }
  RealVec ICs(model.nD());
  char *raw = reinterpret_cast<char*>(&ICs.front());
  file.read(raw,ICs.size()*sizeof(Real));
  file.close();
  data.setICs(&ICs.front(),NULL);
  /*
  for(uint i = 0; i < ICs.size(); ++i) {
    std::cout << ICs[i] << "  ";
  }
  std::cout << std::endl;
  */

  // Time delay indices
  IntVec tauIdxs;
  for(uint iTau = 0; iTau <= nTau; ++iTau) {
    tauIdxs.push_back(iTau*dTau);
  }

  // Create filter
  Filter filter(data,tauIdxs);
  filter.run();

  // Save data
  fName = jobDir + "/data";
  data.write(fName.c_str());

  return 0;
}

