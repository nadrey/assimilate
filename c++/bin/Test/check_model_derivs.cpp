// C++ includes
#include <iostream>

// My includes
#include "FiniteDiffDerivs.h"
#include "SparseTensor.h"

// Models
#include "Lorenz96.h"
#include "Lorenz63.h"
#include "NaKLModel.h"
#include "ColpittsModel.h"
#include "TestModel.h"

////////////////////////////////////////////////////////////////
/// typedefs
typedef TestModel Model;
  
////////////////////////////////////////////////////////////////
///
template <class Model>
struct Functor {
  Functor(Model &model) :
    _model (model) {
  }
  void operator()(const Real *xin, Real *xout)
    { _model.f_eval(0.,xin,xout); }
  uint num_argin() const
    { return _model.num_states(); }
  uint num_argout() const
    { return _model.num_states(); }
    
  Model &_model;
};

////////////////////////////////////////////////////////////////
///
int main(int argc, char *argv[]) {
  // Initialize random seed
  init_rand();
  
  // Initialize model
  Model model;
  
  // Initialize functor
  typedef Functor<Model> Functor;
  Functor functor(model);

  // Random initial conditions for model
  Index nD = model.num_states();
  RealVec x(nD);
  model.random_state(&x.front());
  Real t = 0.;

  // Initializations
  SparseIndexSorter sorter;
  Centered_Finite_Diff<Functor> cfd(functor);

  // Model Jacobian
  IndexVec df_sparse_idxs[2];
  model.df_sparse_idxs(df_sparse_idxs);
  IndexVec df_order(df_sparse_idxs[0].size());
  sorter.sort(df_sparse_idxs,2,df_order);
  RealVec df(nD*nD),
          df_sparse(df_sparse_idxs[0].size()),
          df_approx(nD*nD);
  cfd.df(&x.front(),&df_approx.front());
  model.df_eval(t,&x.front(),&df.front());
  model.df_sparse_eval(t,&x.front(),&df_sparse.front());
  calc_RMSE_df(nD,nD,df_sparse_idxs,df_order,df_sparse,df_approx);

  // Model Hessian
  IndexVec d2f_sparse_idxs[3];
  model.d2f_sparse_idxs(d2f_sparse_idxs);
  IndexVec d2f_order(d2f_sparse_idxs[0].size());
  sorter.sort(d2f_sparse_idxs,3,d2f_order);
  RealVec d2f(nD*nD*nD),
          d2f_sparse(d2f_sparse_idxs[0].size()),
          d2f_approx(nD*nD*nD);
  cfd.d2f(&x.front(),&d2f_approx.front());
  model.d2f_sparse_eval(t,&x.front(),&d2f_sparse.front());
  calc_RMSE_d2f(nD,nD,d2f_sparse_idxs,d2f_order,d2f_sparse,d2f_approx);

  // Model 3rd derivative
  IndexVec d3f_sparse_idxs[4];
  model.d3f_sparse_idxs(d3f_sparse_idxs);
  IndexVec d3f_order(d3f_sparse_idxs[0].size());
  sorter.sort(d3f_sparse_idxs,4,d3f_order);
  RealVec d3f(nD*nD*nD*nD),
          d3f_sparse(d3f_sparse_idxs[0].size()),
          d3f_approx(nD*nD*nD*nD);
  cfd.d3f(&x.front(),&d3f_approx.front());
  model.d3f_sparse_eval(t,&x.front(),&d3f_sparse.front());
  calc_RMSE_d3f(nD,nD,d3f_sparse_idxs,d3f_order,d3f_sparse,d3f_approx);

  return 0;
};
