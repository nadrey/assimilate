// C++ includes
#include <iostream>

// My includes
#include "FiniteDiffDerivs.h"
#include "TwinExperimentData.h"

// Models
#include "Lorenz96.h"
#include "Lorenz63.h"
#include "NaKLModel.h"
#include "ColpittsModel.h"
#include "TestModel.h"

// Actions
#include "A0_4DVar_Strong.h"
#include "A0_4DVar_Weak.h"
#include "A0_4DVar_Weak_Ld.h"
#include "A0_4DVar_Weak_Ld_ELx.h"
#include "A0_4DVar_Weak_Hxp.h"

////////////////////////////////////////////////////////////////
/// typedefs
typedef Lorenz96 Model;
typedef A0_4DVar_Weak Action;

////////////////////////////////////////////////////////////////
///
template <class A>
class FunctorF {
public:
  FunctorF(A &action) :
  _action (action) {
  }
  void operator()(const Real *xin, Real *xout) {
    _action.f_eval(xin,xout[0]);
  }
  Index num_argin() const
    { return _action.num_variables(); }
  Index num_argout() const
    { return 1; }
    
  A &_action;
};

////////////////////////////////////////////////////////////////
///
template <class A>
class FunctorG {
public:
  FunctorG(A &action) :
  _action (action) {
  }
  void operator()(const Real *xin, Real *xout)
    { _action.g_eval(xin,xout); }
  Index num_argin() const
    { return _action.num_variables(); }
  Index num_argout() const
    { return _action.num_constraints(); }
    
  A &_action;
};

////////////////////////////////////////////////////////////////
///
void check_derivs_f(EstimationAction &action) {
  std::cout << "Checking objective function:" << std::endl;
  
  // Initialize functor
  typedef FunctorF<EstimationAction> Functor;
  Functor functor(action);

  // Path to compute around
  const Index num_argin  (functor.num_argin()),
              num_argout (functor.num_argout());
  RealVec x(num_argin,0.);
  action.initial_path(&x.front());

  // Compute finite differences
  std::cout << "Computing finite differences...";
  Centered_Finite_Diff<Functor> fd(functor);
  RealVec df_approx(num_argout*num_argin),
          d2f_approx(num_argout*num_argin*num_argin);
  fd.df(&x.front(),&df_approx.front());
  fd.d2f(&x.front(),&d2f_approx.front());
  std::cout << " done" << std::endl;

  // Get sparse structure
  IndexVec df_idx_vec[2], d2f_idx_vec[3];
  df_idx_vec[1].resize(num_argin);
  for(Index i = 0; i < num_argin; ++i) {
    df_idx_vec[1][i] = i;
  }
  action.d2f_sparse_idxs(&d2f_idx_vec[1]);
  Index num_sparse_df = df_idx_vec[1].size(),
        num_sparse_d2f = d2f_idx_vec[1].size();

  // Dummy indices so we can reuse same RMSE method
  df_idx_vec[0].resize(num_sparse_df,0);
  d2f_idx_vec[0].resize(num_sparse_d2f,0);
  
  // Sort sparse indices
  IndexVec df_idx_order, d2f_idx_order;
  SparseIndexSorter sorter;
  sorter.sort(df_idx_vec,2,df_idx_order);
  sorter.sort(d2f_idx_vec,3,d2f_idx_order);
    
  // Compute analytic derivatives
  std::cout << "Computing derivatives...";
  RealVec df_sparse(num_sparse_df),
          d2f_sparse(num_sparse_d2f);
  action.df_eval(&x.front(),&df_sparse.front());
  action.d2f_sparse_eval(&x.front(),&d2f_sparse.front());
  std::cout << " done" << std::endl;

  // Calculate RMSE
  calc_RMSE_df(num_argin,num_argout,df_idx_vec,df_idx_order,df_sparse,df_approx);
  calc_RMSE_d2f(num_argin,num_argout,d2f_idx_vec,d2f_idx_order,d2f_sparse,d2f_approx);
}

////////////////////////////////////////////////////////////////
///
void check_derivs_g(EstimationAction &action) {
  std::cout << "Checking constraints:" << std::endl;
  
  // Initialize functor
  typedef FunctorG<EstimationAction> Functor;
  Functor functor(action);
  
  // Path to compute around
  const Index num_argin  (functor.num_argin()),
              num_argout (functor.num_argout());
  RealVec x(num_argin,0.);
  action.initial_path(&x.front());

  // Compute finite differences
  std::cout << "Computing finite differences...";
  Centered_Finite_Diff<Functor> cfd(functor);
  RealVec dg_approx(num_argout*num_argin),
          d2g_approx(num_argout*num_argin*num_argin);
  cfd.df(&x.front(),&dg_approx.front());
  cfd.d2f(&x.front(),&d2g_approx.front());
  std::cout << " done" << std::endl;

  // Get sparse structure
  IndexVec dg_idx_vec[2], d2g_idx_vec[3];
  action.dg_sparse_idxs(dg_idx_vec);
  action.d2g_sparse_idxs(d2g_idx_vec);
  Index num_sparse_dg = dg_idx_vec[0].size(),
        num_sparse_d2g = d2g_idx_vec[0].size();

  // Sort sparse indices
  IndexVec dg_idx_order, d2g_idx_order;
  SparseIndexSorter sorter;
  sorter.sort(dg_idx_vec,2,dg_idx_order);
  sorter.sort(d2g_idx_vec,3,d2g_idx_order);

  // Compute analytic derivatives
  std::cout << "Computing derivatives...";
  RealVec dg_sparse(num_sparse_dg),
          d2g_sparse(num_sparse_d2g);
  action.dg_sparse_eval(&x.front(),&dg_sparse.front());
  action.d2g_sparse_eval(&x.front(),&d2g_sparse.front());
  std::cout << " done" << std::endl;
  
  // Calculate RMSE
  calc_RMSE_df(num_argin,num_argout,dg_idx_vec,dg_idx_order,dg_sparse,dg_approx);
  calc_RMSE_d2f(num_argin,num_argout,d2g_idx_vec,d2g_idx_order,d2g_sparse,d2g_approx);
}

////////////////////////////////////////////////////////////////
///
template <class Model>
struct FunctorM {
  FunctorM(Model &model) :
    _model (model) {
  }
  void operator()(const Real *xin, Real *xout)
    { _model.f_eval(0.,xin,xout); }
  Index num_argin() const
    { return _model.num_states(); }
  Index num_argout() const
    { return _model.num_states(); }

  Model &_model;
};


////////////////////////////////////////////////////////////////
///
int main(int argc, char *argv[]) {
  // Initialize random seed:
  init_rand();

  // Initialize model
  Model model;
  const Index nD = model.num_states();

  // Initialize time domain
  const Index nN = 10;
  const Real dt = 1.e-2;
  TimeDomain time_domain(nN,dt);

  // Initialize observation operator
  Index  nL = nD;
  IndexVec meas_idxs;
  for(Index iL = 0; iL < nL; ++iL) {
    meas_idxs.push_back(iL);
  }
  ProjectionObsModel obs_model;
  obs_model.set_observed_state_idxs(idxs);

  // Twin data initialization
  TwinExperimentData::InitialData initial_data;

  // Rm, Rf
  RealVec Rm_diag(meas_idxs.size(),1.e0),
          Rf_diag(nD,1.e-4);

  // Initialize the action
  Action action;
  action.init_twin_data(model,time_domain,obs_model,initial_data);
  action.set_Rm_diag(Rm_diag);
  action.set_Rf_diag(Rf_diag);

  // Check objective function derivatives
  check_derivs_f(action);
  std::cout << std::endl;
  
  // Check constraint derivatives
  check_derivs_g(action);
  
  return 0;
};
