/*****************************************************
 *
 *****************************************************/

// IpOPT includes
#include "IpIpoptApplication.hpp"

// My includes
#include "IpOptInterface.h"
#include "TwinExperimentData.h"

// Models
#include "Lorenz96.h"
#include "Lorenz63.h"

// Actions
#include "A0_4DVar_Strong.h"
#include "A0_4DVar_Weak.h"
#include "A0_4DVar_Weak_Ld_ELx.h"
#include "A0_4DVar_Weak_Hxp.h"

////////////////////////////////////////////////////////////////
/// typedefs
typedef Lorenz96 Model;
typedef A0_4DVar_Weak Action;

////////////////////////////////////////////////////////////////
/// convenience function for Rf ~ alpha^beta annealing
Ipopt::ApplicationReturnStatus
  anneal_Rf(Ipopt::SmartPtr<Ipopt::IpoptApplication> &app,
            Ipopt::SmartPtr<Ipopt::TNLP> &myNLP,
            EstimationAction &action,
            const RealVec &Rf_diag,
            const Real  alpha,
            const Index beta_min,
            const Index beta_max) {
  // Perform annealing
  Ipopt::ApplicationReturnStatus status = Ipopt::Internal_Error;
  const Index nD = Rf_diag.size();
  RealVec tmp_Rf_diag = Rf_diag;
  for(Index beta = beta_min; beta <= beta_max; ++beta) {
    const Real sf = std::pow(alpha,static_cast<Real>(beta));
    for(Index iD = 0; iD < nD; ++iD) {
      tmp_Rf_diag[iD] = Rf_diag[iD]*sf;
    }
    action.set_Rf_diag(tmp_Rf_diag);
    status = app->OptimizeTNLP(myNLP);
  }
  return status;
}

///////////////////////////////////////////////////////////////////////
/// convenience function to set Ipopt app. options
void set_ipopt_app_options(Ipopt::IpoptApplication *app) {
  app->Options()->SetStringValue("linear_solver", "ma57");
  app->Options()->SetStringValue("print_user_options","yes");
  app->Options()->SetNumericValue("tol", 1e-9);
  //app->Options()->SetIntegerValue("print_frequency_iter",100);
  //app->Options()->SetStringValue("output_file", "ipopt.out");

  // Barrier parameter annealing
  app->Options()->SetStringValue("mu_strategy", "adaptive");
  app->Options()->SetStringValue("adaptive_mu_globalization", "never-monotone-mode");

  // Scaling
  app->Options()->SetStringValue("nlp_scaling_method","none");
  //app->Options()->SetNumericValue("obj_scaling_factor",1.e-4);

  // Derivative test options
  //app->Options()->SetStringValue("derivative_test", "second-order");
  //app->Options()->SetNumericValue("derivative_test_perturbation",1.e-6);
  //app->Options()->SetStringValue("derivative_test_print_all","yes");
  //
  // IpOpt by default performs derivative checking at a random
  // perturbation from the user-specified initial condition
  //
  // http://www.coin-or.org/Ipopt/documentation/
  //   node53.html#opt:derivative_test_first_index
  //
  // This so because the initial conditions are often special
  // cases. To override this behavior and ensure IpOpt uses
  // the user-defined initial condition set the perturbation
  // radius to zero
  //app->Options()->SetNumericValue("point_perturbation_radius",0.);
}

////////////////////////////////////////////////////////////////
// gateway function
int main(int argc, char *argv[]) {

  std::stringstream ss;

  // Initialize random seed:
  init_rand();

  // Initialize model
  Model model;
  const Index nD = model.num_states();

  // Initialize time domain
  const Index nN = 10;
  const Real dt = 0.01;
  TimeDomain time_domain(nN,dt);

  // Initialize observation operator
  Index  nL = 5;
  IndexVec meas_idxs;
  for(Index iL = 0; iL < nL; ++iL) {
    meas_idxs.push_back(iL);
  }
  ProjectionObsModel obs_model;
  obs_model.set_observed_state_idxs(meas_idxs);

  // True data initialization
  TrueExperimentData::InitialData initial_data;
  const Index nNp1 = time_domain.num_time_points();
  RealVec &obs = initial_data._observations;
  obs.resize(nL*nNp1);
  read_column_data("L96_D10_L=5.dat",&obs.front(),nNp1,nL);

  // Rm, Rf
  RealVec Rm_diag(meas_idxs.size(),1.e0),
          Rf_diag(nD,1.e0);

  // Initialize the action
  Action action;
  action.init_true_data(model,time_domain,obs_model,initial_data);
  action.set_Rm_diag(Rm_diag);
  action.set_Rf_diag(Rf_diag);

  // Write input data
  std::cout << "Action type: " << action.classname() << std::endl;

  // Create IP opt interface to nonlinear problem
  Ipopt::SmartPtr<Ipopt::TNLP> myNLP = new IpOptInterface(action);

  // Create the IpOpt app. and set options
  Ipopt::SmartPtr<Ipopt::IpoptApplication> app = IpoptApplicationFactory();
  set_ipopt_app_options(GetRawPtr(app));

  // Initialize the IP Opt app
  Ipopt::ApplicationReturnStatus status;
  status = app->Initialize();
  if (status != Ipopt::Solve_Succeeded) {
    std::cout << "\n\n*** Error during initialization!" << std::endl;
  }

  // Ask Ipopt to solve the problem
  //status = app->OptimizeTNLP(myNLP);
  // Anneal Rf
  status = anneal_Rf(app,myNLP,action,Rf_diag,10.,-2,3);

  // Check solution status
  if(status == Ipopt::Solve_Succeeded) {
    std::cout << "\n\n*** The problem solved! " << std::endl;
  }
  else {
    std::cout << "\n\n*** The problem FAILED! ";
    std::cout << status << std::endl;
  }

  return 0;
}

