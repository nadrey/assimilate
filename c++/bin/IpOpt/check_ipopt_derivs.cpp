// C++ includes
#include <iostream>
#include <algorithm>

// My includes
#include "FiniteDiffDerivs.h"
#include "IpOptInterface.h"
#include "TwinExperimentData.h"

// Models
#include "Lorenz63.h"
#include "Lorenz96.h"
#include "NaKLModel.h"
#include "ColpittsModel.h"
#include "TestModel.h"

// Actions
#include "A0_4DVar_Strong.h"
#include "A0_4DVar_Weak.h"
#include "A0_4DVar_Weak_Ld_ELx.h"
#include "A0_4DVar_Weak_Hxp.h"

////////////////////////////////////////////////////////////////
/// typedefs
typedef TestModel Model;
typedef A0_4DVar_Weak_Hxp Action;

////////////////////////////////////////////////////////////////
///
template <class IPOPT>
class FunctorF {
public:
  FunctorF(IPOPT &ipopt) :
    _ipopt (ipopt) {
    Ipopt::Index n,dummy;
    Ipopt::TNLP::IndexStyleEnum dummy2;
    _ipopt.get_nlp_info(n, dummy, dummy, dummy, dummy2);
    _num_argin = (Index)n;
  }
  void operator()(const double *xin, double *xout) {
    _ipopt.eval_f((int)_num_argin,xin,true,xout[0]);
  }
  Index num_argin() const
    { return _num_argin; }
  Index num_argout() const
    { return 1; }
    
  IPOPT &_ipopt;
  Index _num_argin;
};

////////////////////////////////////////////////////////////////
///
template <class IPOPT>
class FunctorG {
public:
  FunctorG(IPOPT &ipopt) :
    _ipopt (ipopt) {
    Ipopt::Index n,m,dummy;
    Ipopt::TNLP::IndexStyleEnum dummy2;
    _ipopt.get_nlp_info(n, m, dummy, dummy, dummy2);
    _num_argin  = (Index)n;
    _num_argout = (Index)m;
  }
  void operator()(const double *xin, double *xout) {
    _ipopt.eval_g((int)_num_argin,xin,true,(int)_num_argout,xout);
  }
  Index num_argin() const
    { return _num_argin; }
  Index num_argout() const
    { return _num_argout; }
    
  IPOPT &_ipopt;
  Index _num_argin , _num_argout;
};

////////////////////////////////////////////////////////////////
///
int main(int argc, char *argv[]) {
  // Initialize random seed:
  init_rand();

  // Initialize model
  Model model;
  const Index nD = model.num_states();

  // Initialize time domain
  const Index nN = 10;
  const Real dt = 0.01;
  TimeDomain time_domain(nN,dt);

  // Initialize observation operator
  Index  nL = nD;
  IndexVec meas_idxs;
  for(Index iL = 0; iL < nL; ++iL) {
    meas_idxs.push_back(iL);
  }
  ProjectionObsModel obs_model;
  obs_model.set_observed_state_idxs(meas_idxs);

  // Twin data initialization
  TwinExperimentData::InitialData initial_data;

  // Rm, Rf
  RealVec Rm_diag(meas_idxs.size(),1.e0),
          Rf_diag(nD,1.e0);

  // Initialize the action
  Action action;
  action.init_twin_data(model,time_domain,obs_model,initial_data);
  action.set_Rm_diag(Rm_diag);
  action.set_Rf_diag(Rf_diag);

  // Get initial path
  RealVec X(action.num_variables());
  action.initial_path(&X.front());

  // Construct IpOpt interface
  IpOptInterface ipOpt(action);
  
  // Initialize functors
  typedef FunctorF<IpOptInterface> FF;
  typedef FunctorG<IpOptInterface> FG;
  FF ff(ipOpt);
  FG fg(ipOpt);

  // Compute finite differences
  std::cout << "Computing finite differences..." << std::endl;
  const Index fIn (ff.num_argin()), fOut (ff.num_argout()),
             gIn (fg.num_argin()), gOut (fg.num_argout());
  Centered_Finite_Diff<FF> cfd_ff(ff);
  Centered_Finite_Diff<FG> cfd_fg(fg);
  RealVec df_approx(fOut*fIn), d2f_approx(fOut*fIn*fIn),
          dg_approx(gOut*gIn), d2g_approx(gOut*fIn*gIn);
  cfd_ff.df(&X.front(),&df_approx.front());
  cfd_ff.d2f(&X.front(),&d2f_approx.front());
  cfd_fg.df(&X.front(),&dg_approx.front());
  cfd_fg.d2f(&X.front(),&d2g_approx.front());
  std::cout << "...done" << std::endl;

  // Configure Lagrange multipliers sigma, lambda
  Real sigma_f = 0;
  RealVec lambda(gOut,0.);
  for(Index i = 0; i < gOut; ++i) {
    lambda[i] = 0.;
  }
  lambda[7] = 1;

  // Compute finite diff. approx. to d2L
  RealVec d2L_approx(fOut*fIn*fIn);
  for(Index i = 0; i < fIn; ++i) {
    for(Index j = 0; j < fIn; ++j) {
      d2L_approx[i*fIn + j] += sigma_f*d2f_approx[i*fIn + j];
    }
  }
  for(Index i = 0; i < gOut; ++i) {
    for(Index j = 0; j < gIn; ++j) {
      for(Index k = 0; k < gIn; ++k) {
        d2L_approx[j*gIn + k] +=
          lambda[i]*d2g_approx[i*gIn*gIn + j*gIn + k];
      }
    }
  }

  // Get info from IpOpt interface
  Ipopt::Index n,m,num_non0s_dg,num_non0s_d2L;
  Ipopt::TNLP::IndexStyleEnum dummy;
  ipOpt.get_nlp_info(n,m,num_non0s_dg,num_non0s_d2L,dummy);

  // Get dummy sparse structure for df
  IndexVec df_idx_vec[2];
  Index num_non0s_df = fIn*fOut;
  df_idx_vec[0].resize(num_non0s_df,0);
  df_idx_vec[1].resize(num_non0s_df,0);
  for(Index i = 0; i < num_non0s_df; ++i) {
    df_idx_vec[1][i] = i;
  }

  // Get sparse structure for dg
  typedef std::vector<Ipopt::Index> IndexVec;
  IndexVec idxVec[2];
  idxVec[0].resize(num_non0s_dg);
  idxVec[1].resize(num_non0s_dg);
  ipOpt.eval_jac_g(n,&X.front(),true,m,num_non0s_dg,
                   &idxVec[0].front(),
                   &idxVec[1].front(),NULL);
  IndexVec dg_idx_vec[2];
  for(Index i = 0; i < 2; ++i) {
    dg_idx_vec[i].resize(idxVec[i].size());
    for(Index j = 0; j < (Index)idxVec[i].size(); ++j) {
      dg_idx_vec[i][j] = (Index)idxVec[i][j];
    }
  }

  // Get sparse structure for d2L
  idxVec[0].resize(num_non0s_d2L);
  idxVec[1].resize(num_non0s_d2L);
  ipOpt.eval_h(n,&X.front(),true,sigma_f,m,&lambda.front(),
               true,num_non0s_d2L,&idxVec[0].front(),
               &idxVec[1].front(),NULL);
  IndexVec d2L_idx_vec[2];
  for(Index i = 0; i < 2; ++i) {
    d2L_idx_vec[i].resize(num_non0s_d2L);
    for(Index j = 0; j < (Index)idxVec[i].size(); ++j) {
      d2L_idx_vec[i][j] = (Index)idxVec[i][j];
    }
  }

  // Compute analytic derivatives
  std::cout << "Computing derivatives..." << std::endl;
  RealVec df(num_non0s_df),
          dg(num_non0s_dg),
          d2L(num_non0s_d2L);
  ipOpt.eval_grad_f(n,&X.front(),true,&df.front());
  ipOpt.eval_jac_g(n,&X.front(),true,m,num_non0s_dg,
                   NULL,NULL,&dg.front());
  ipOpt.eval_h(n,&X.front(),true,sigma_f,m,&lambda.front(),
               true,num_non0s_d2L,NULL,NULL,&d2L.front());
  std::cout << "...done" << std::endl;

  // Append symmetric indices of d2L
  d2L_idx_vec[0].reserve(2*num_non0s_d2L);
  d2L_idx_vec[1].reserve(2*num_non0s_d2L);
  d2L.reserve(num_non0s_d2L);
  for(Index idx_sparse = 0; idx_sparse < num_non0s_d2L; ++idx_sparse) {
    Index i = d2L_idx_vec[0][idx_sparse],
         j = d2L_idx_vec[1][idx_sparse];
    if(i != j) {
      d2L_idx_vec[0].push_back(j);
      d2L_idx_vec[1].push_back(i);
      d2L.push_back(d2L[idx_sparse]);
    }
  }

  // Sort sparse indices
  IndexVec df_idx_order, dg_idx_order, d2L_idx_order;
  SparseIndexSorter sorter;
  sorter.sort(df_idx_vec,2,df_idx_order);
  sorter.sort(dg_idx_vec,2,dg_idx_order);
  sorter.sort(d2L_idx_vec,2,d2L_idx_order);

  // Compute RMSE
  std::cout << "Objective gradient: ";
  calc_RMSE_df(fIn,fOut,df_idx_vec,df_idx_order,df,df_approx);
  std::cout << "Constraint Jacobian: ";
  calc_RMSE_df(gIn,gOut,dg_idx_vec,dg_idx_order,dg,dg_approx);
  std::cout << "Hessian of the Lagrangian: ";
  calc_RMSE_df(fIn,fIn,d2L_idx_vec,d2L_idx_order,d2L,d2L_approx);

  return 0;
};
