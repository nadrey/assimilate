#!/bin/bash

cdir=`pwd`;
for fdir in `find -wholename "./data*run_1"`; do 
  dir="${fdir%/*}"
  echo $dir;
  cd $dir;
  rm -f report;
  for file in `ls run*`; do
    echo -e "\n$file" >> report;
    grep RMSE $file   >> report;
  done;
  cd $cdir;
done;
