#!/bin/bash

L=1; DM=1;
dir_str="L";


for i in {1..10}; do
  dir_array[i]=$i;
done
for i in {1..10}; do
  run_array[i]=$i;
done

for i in ${dir_array[@]}; do
  mkdir -p data/$dir_str=$i;
  if [ $dir_str == "DM" ]; then
    DM=$i;
  elif [ $dir_str == "L" ]; then
    L=$i;
  else
    echo "Unknown dir. name";
  fi
  for j in ${run_array[@]}; do
    sleep 1;
    file=data/$dir_str=$i/run_$j;
    echo $file;
    (time (./run_ip_opt.exe $L $DM)) 2>&1 | tee $file;
  done 
done
