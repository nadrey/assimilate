#!/bin/bash

# Parse input parameters
clean=false;
rebaseline=false;
diff=false;
for arg in "$@"; do
  case $arg in
    "clean" )
    clean=true
    ;;
    "rebaseline" )
    rebaseline=true
    ;;
    "diff" )
    diff=true
  esac
done

# Run tests
test_dir=`pwd`;
for file in `find -name 'test.cpp'`; do 
  cur_dir=${file%/*};
  cd $cur_dir;
  if [ $clean != false ]; then
    make -f $test_dir/Makefile clean;
    rm -f test.out;
  elif [ $rebaseline != false ]; then
    cp test.out test.base;
  elif [ $diff != false ]; then
    diff test.out test.base;
  else
    make -f $test_dir/Makefile;
    ./test.exe | tee test.out
  fi
  cd $test_dir;
done
