/*****************************************************
 *
 *****************************************************/

// IpOPT includes
#include "IpIpoptApplication.hpp"

// My includes
#include "IpOptInterface.h"
#include "TwinExperimentData.h"

// Models
#include "Lorenz96.h"
#include "Lorenz63.h"
#include "NaKLModel.h"
#include "ColpittsModel.h"
#include "TestModel.h"

// Actions
#include "A0_4DVar_Strong.h"
#include "A0_4DVar_Weak.h"
#include "A0_4DVar_Weak_Ld_ELx.h"
#include "A0_4DVar_Weak_Hxp.h"

////////////////////////////////////////////////////////////////
/// typedefs
typedef NaKLModel Model;
typedef A0_4DVar_Weak_Hxp Action;

////////////////////////////////////////////////////////////////
// gateway function
int main(int argc, char *argv[]) {
  std::stringstream ss;

  // Initialize random seed:
  init_rand();

  // Initialize model
  Model model;
  const Index nD = model.num_states();

  // Initialize time domain
  const Index nN = 100;
  const Real dt = 0.01;
  TimeDomain time_domain(nN,dt);

  // Initialize observation operator
  Index  nL = nD;
  IndexVec meas_idxs;
  for(Index iL = 0; iL < nL; ++iL) {
    meas_idxs.push_back(iL);
  }
  ProjectionObsModel obs_model(meas_idxs);

  // Twin data initialization
  TwinExperimentData::InitialData initial_data;

  // Rm, Rf
  RealVec Rm_diag(meas_idxs.size(),1.e0),
          Rf_diag(nD,1.e0);

  // Initialize the action
  Action action;
  action.init_twin_data(model,time_domain,obs_model,initial_data);
  action.set_Rm_diag(Rm_diag);
  action.set_Rf_diag(Rf_diag);

  // Write input data
  std::cout << "Action type: " << action.classname() << std::endl;

  // Create IP opt interface to nonlinear problem
  Ipopt::SmartPtr<Ipopt::TNLP> myNLP = new IpOptInterface(action);

  // Create the IpOpt app. and set options
  Ipopt::SmartPtr<Ipopt::IpoptApplication> app = IpoptApplicationFactory();
  set_ipopt_app_options(Ipopt::GetRawPtr(app));

  // Initialize the IP Opt app
  Ipopt::ApplicationReturnStatus status;
  status = app->Initialize();
  if (status != Ipopt::Solve_Succeeded) {
    std::cout << "\n\n*** Error during initialization!" << std::endl;
  }

  // Ask Ipopt to solve the problem
  status = app->OptimizeTNLP(myNLP);
  // Anneal Rf
  //status = anneal_Rf(app,myNLP,action,Rf_diag,10.,-2,3);

  // Check solution status
  if(status == Ipopt::Solve_Succeeded) {
    std::cout << "\n\n*** The problem solved! " << std::endl;
  }
  else {
    std::cout << "\n\n*** The problem FAILED! ";
    std::cout << status << std::endl;
  }

  // Get solution
  const Index nNp1 = time_domain.num_time_points();
  const RealVec &X_init = initial_data._estimated_states,
                &X_true = initial_data._true_states,
                &X_est  = action.data()->estimated_states();
  /*
  // Write out initial condition
  const char *format_out = "%+16.6e";
  std::cout << "\nInitial path X_init" << std::endl;
  for(Index iN = 0; iN < nNp1; ++iN) {
    for(Index iD = 0; iD < nD; ++iD) {
      printf(format_out,X_init[iN*nD + iD]);
    }
    std::cout << std::endl;
  }
  // Write out final path
  std::cout << "\nEstimated path" << std::endl;
  for(Index iN = 0; iN < nNp1; ++iN) {
    for(Index iD = 0; iD < nD; ++iD) {
      printf(format_out,X_est[iN*nD + iD]);
    }
    std::cout << std::endl;
  }
  // Write out true data
  std::cout << "\n\nTrue data" << std::endl;
  for(Index iN = 0; iN < nNp1; ++iN) {
    for(Index iD = 0; iD < nD; ++iD) {
      printf(format_out,X_true[iN*nD + iD]);
    }
    std::cout << std::endl;
  }
  */
  // Compute RMSE between solution and twin data
  Index  nX = nNp1*nD;
  Real rmse = 0;
  for(Index iX = 0; iX < nX; ++iX) {
    Real tmp = X_init[iX] - X_true[iX];
    rmse += tmp*tmp;
  }
  rmse = std::sqrt(rmse/nX);
  std::cout << "\nInitial RMSE: " << rmse << std::endl;

  rmse = 0;
  for(Index iX = 0; iX < nX; ++iX) {
    Real tmp = X_est[iX] - X_true[iX];
    rmse += tmp*tmp;
  }
  rmse = std::sqrt(rmse/nX);
  std::cout << "Final RMSE: " << rmse << std::endl;

  return 0;
}

