#ifndef ESTIMATION_DATA_H_
#define ESTIMATION_DATA_H_

// My includes
#include "Utils.h"
#include "DynamicalModel.h"
#include "TimeDomain.h"
#include "ProjectionObsModel.h"

////////////////////////////////////////////////////////////////
/// Base class interface for experimental data
class EstimationData {
public:

  // Struct to hold initialization data
  struct InitialData {
    RealVec _estimated_states,
            _estimated_params,
            _estimated_inputs;
    //
    // List of indices determining which parameters to estimate
    // statically and which to estimate dynamically as inputs.
    // Any parameters whose index is not in the union of these
    // two lists will not be estimated.
    //
    IndexVec _estimated_param_idxs,
             _estimated_input_idxs;
  };

  // Constructors
  EstimationData(const DynamicalModel &dyn_model,
                 const TimeDomain &time_domain,
                 const ObservationModel &obs_model);
  virtual ~EstimationData() {};

  // Save step
  virtual void save_step(Index  n, const Real *x, const Real *p);
  virtual void save_path(const Real *X, const Real *P);

  // Write data to file
  void write_estimated_states_to_file(const char *filename) const;
  void write_estimated_params_to_file(const char *filename) const;
  void write_observations_to_file(const char *filename) const;

  // Get methods
  const DynamicalModel& dynamical_model() const;
  const ObservationModel& observation_model() const;
  const TimeDomain& time_domain() const;
  const RealVec& observations() const;
  const RealVec& estimated_states() const;
  const RealVec& estimated_params() const;
  const Real& observation_window_length() const;

protected:
  // Initialization methods called by EstimationAction
  friend class EstimationAction;
  virtual void initialize(InitialData &init);
  void init_estimated_states(InitialData &init);
  void init_estimated_params(InitialData &init);
  void init_estimated_inputs(InitialData &init);
  void check_parameter_idxs(const IndexVec &param_idxs,
                            const IndexVec &input_idxs);

  // Parent model
  const DynamicalModel &_dynamical_model;
  // Time domain
  const TimeDomain &_time_domain;
  // Observation model
  const ObservationModel &_observation_model;

  // Estimated states/parameters/inputs
  RealVec _estimated_states,
          _estimated_params,
          _estimated_inputs;
  //
  // List of indices determining which parameters to estimate
  // statically and which to estimate dynamically as inputs.
  // Any parameters whose index is not in the union of these
  // two lists will not be estimated.
  //
  IndexVec _estimated_param_idxs,
           _estimated_input_idxs;

  // Measured data
  RealVec _observations;

  // Temporary data for interpolation
  RealVec _interpolation_data;

private:
  EstimationData(const EstimationData&);
};

////////////////////////////////////////////////////////////////
/// Constructors
EstimationData::EstimationData(const DynamicalModel &dyn_model,
                               const TimeDomain &time_domain,
                               const ObservationModel &obs_model) :
  _dynamical_model (dyn_model),
  _time_domain (time_domain),
  _observation_model (obs_model) {
}

////////////////////////////////////////////////////////////////
/// get methods
///
const DynamicalModel& EstimationData::dynamical_model() const {
  return _dynamical_model;
}
///
const ObservationModel& EstimationData::observation_model() const {
  return _observation_model;
}
///
const TimeDomain& EstimationData::time_domain() const {
  return _time_domain;
}
///
const RealVec& EstimationData::observations() const {
  return _observations;
}
///
const RealVec& EstimationData::estimated_states() const {
  return _estimated_states;
}
///
const RealVec& EstimationData::estimated_params() const {
  return _estimated_params;
}

////////////////////////////////////////////////////////////////
/// save state and parameters at current step
/// will fail if called before initialize
void EstimationData::save_step(Index n, const Real *x, const Real *p) {
  assert(!_estimated_states.empty());
  Index  nD = _dynamical_model.num_states(),
         nP = _dynamical_model.num_params();
  std::copy(x,x+nD,_estimated_states.begin() + n*nD);
  if(nP) {
    std::copy(p,p+nP,_estimated_params.begin() + n*nP);
  }
}

////////////////////////////////////////////////////////////////
/// copy path to estimated state and parameters
void EstimationData::save_path(const Real *X, const Real *P) {
  assert(!_estimated_states.empty());
  const Index  nD = _dynamical_model.num_states(),
               nP = _dynamical_model.num_params(),
               nNp1 = _time_domain.num_time_points();
  std::copy(X,X+nD*nNp1,_estimated_states.begin());
  if(nP) {
    std::copy(P,P+nP*nNp1,_estimated_params.begin());
  }
}

////////////////////////////////////////////////////////////////
/// write estimated states/parameters to file
void EstimationData::write_estimated_states_to_file(const char *filename) const {
  Index num_rows = _time_domain.num_time_points(),
        num_cols = _dynamical_model.num_states();
  const Real *data = &_estimated_states.front();
  write_column_data(filename,data,num_rows,num_cols);
}

////////////////////////////////////////////////////////////////
/// write estimated states/parameters to file
void EstimationData::write_estimated_params_to_file(const char *filename) const {
  Index num_rows = _time_domain.num_time_points(),
        num_cols = _dynamical_model.num_params();
  const Real *data = &_estimated_params.front();
  write_column_data(filename,data,num_rows,num_cols);
}

////////////////////////////////////////////////////////////////
/// write measured data to file
void EstimationData::write_observations_to_file(const char *filename) const {
  Index num_rows = _time_domain.num_time_points(),
        num_cols = _observation_model.num_observed_states();
  const Real *data = &_observations.front();
  write_column_data(filename,data,num_rows,num_cols);
}

////////////////////////////////////////////////////////////////
/// Initialize state and parameter estimates
///
/// If estimated_states, estimated_params are not empty, they will be
/// used to initialized the estimated path.
///
/// If estimated_states/params/inputs is empty, estimates will
/// be randomly generated and copied to init
///
void EstimationData::initialize(InitialData &init) {
  // Initialize observation model
  // TODO const cast until we figure out a better way to initialize objects
  ObservationModel &obs = const_cast<ObservationModel&>(_observation_model);
  obs.initialize(_dynamical_model,_time_domain);

  /*
  if(_parameter_model.num_params() > 0) {
    check_parameter_idxs(init._estimated_param_idxs,
                         init._estimated_input_idxs);
    init_estimated_params(init);
    init_estimated_inputs(init);
  }
  */
  init_estimated_states(init);
}

////////////////////////////////////////////////////////////////
/// Initialze estimated states
void EstimationData::init_estimated_states(InitialData &init) {
  RealVec &initial_states = init._estimated_states;
  const Index nNp1 = _time_domain.num_time_points(),
                nD = _dynamical_model.num_states();

  // Initialize state estimates
  _estimated_states.resize(nD*nNp1);
  if(initial_states.empty()) {
    for(Index iN = 0; iN < nNp1; ++iN) {
      _dynamical_model.random_state(&_estimated_states[iN*nD]);
    }
    initial_states = _estimated_states;
  }
  else if(initial_states.size() == _estimated_states.size()) {
    _estimated_states = initial_states;
  }
  else {
    throw_exception("EstimationData::init_estimated_states: "
            "Invalid size of initial state estimates");
  }
}

////////////////////////////////////////////////////////////////
/// Initialize static parameter estimates
void EstimationData::init_estimated_params(InitialData &init) {
  const IndexVec &estimated_param_idxs = init._estimated_param_idxs;
  const Index num_estimated_params = estimated_param_idxs.size();
  const Index num_params = _dynamical_model.num_params();
  RealVec &initial_params = init._estimated_params;

  _estimated_params.resize(num_estimated_params);
  if(initial_params.empty()) {
    RealVec params(num_params,0);
    //_dynamical_model.random_param(&params.front());
    for(Index idx = 0; idx < num_estimated_params; ++idx) {
      _estimated_params[idx] = params[estimated_param_idxs[idx]];
    }
    initial_params = _estimated_params;
  }
  else if(initial_params.size() == _estimated_params.size()) {
    _estimated_params = initial_params;
  }
  else {
    throw_exception("EstimationData::init_estimated_params: "
            "Unexpected size of initial parameter estimates");
  }
}

////////////////////////////////////////////////////////////////
/// Initialize dynamical input estimates
void EstimationData::init_estimated_inputs(InitialData &init) {
  const IndexVec &estimated_input_idxs = init._estimated_input_idxs;
  const Index num_estimated_inputs = estimated_input_idxs.size();
  const Index num_params = _dynamical_model.num_params();
  const Index nNp1 = _time_domain.num_time_points();
  RealVec &initial_inputs = init._estimated_inputs;

  _estimated_inputs.resize(num_estimated_inputs*nNp1);
  if(initial_inputs.empty()) {
    RealVec params(num_params,0);
    for(Index iN = 0; iN < nNp1; ++iN) {
      //_dynamical_model.random_param(&params.front());
      for(Index idx = 0; idx < num_estimated_inputs; ++idx) {
        _estimated_inputs[idx] = params[estimated_input_idxs[idx]];
      }
    }
    initial_inputs = _estimated_inputs;
  }
  else if(initial_inputs.size() == _estimated_inputs.size()) {
    _estimated_inputs = initial_inputs;
  }
  else {
    throw_exception("EstimationData::init_estimated_inputs: "
            "Unexpected size of initial estimated inputs");
  }
}

////////////////////////////////////////////////////////////////
/// Some error checking on input parameter lists
void EstimationData::check_parameter_idxs(const IndexVec &param_idxs,
                                          const IndexVec &input_idxs) {
  const Index num_params = _dynamical_model.num_params();
  const Index num_estimated_params = param_idxs.size(),
              num_estimated_inputs = input_idxs.size();

  // Check static parameter list
  for(Index idx = 0; idx < num_estimated_params; ++idx) {
    if(param_idxs[idx] > num_params) {
      throw_exception("EstimationData::check_parameter_idxs: "
              "Invalid estimated parameter index");
    }
  }
  // Check dynamical input list
  for(Index idx = 0; idx < num_estimated_inputs; ++idx) {
    if(input_idxs[idx] > num_params) {
      throw_exception("EstimationData::check_parameter_idxs: "
              "Invalid estimated input index");
    }
  }
  // Check for collisions between lists
  for(Index idx0 = 0; idx0 < num_estimated_params; ++idx0) {
    for(Index idx1 = 0; idx1 < num_estimated_inputs; ++idx1) {
      if(param_idxs[idx0] == input_idxs[idx1]) {
        throw_exception("EstimationData::check_parameter_idxs: "
                "Identical indices in estimated parameters and inputs");
      }
    }
  }
}

#endif /* ESTIMATION_DATA_H_ */
