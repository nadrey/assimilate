#ifndef TRAPEZOID_RULE_H_
#define TRAPEZOID_RULE_H_

// My includes
#include "QuadRule.h"
#include "EstimationAction.h"
#include "ODEStepper.h"

////////////////////////////////////////////////////////////////
/// Trapezoid rule quadrature for approximated dynamical constraints
/// as integrals between two collocation times t(n), t(n+1)
///
///  int_{t(n)}^{t(n+1)} dt*[ dx/dt - f(x) ] ~
///     x(n+1) - x(n) - dt/2*[ f(x(n+1)) + f(x(n)) ]
///
class TrapezoidRule : public QuadRule {
public:
  // constructor
  TrapezoidRule(const DynamicalModel &model);
  // destructor
  virtual ~TrapezoidRule();
  // class name
  virtual std::string classname() const
    { return "TrapezoidRule"; }

  // initialize calculation
  virtual void init_dg_eval(const Real &t_0, const Real *x_0) const;
  virtual void init_d2g_eval(const Real &t_0, const Real *x_0) const;
  virtual void init_d3g_eval(const Real &t_0, const Real *x_0) const;

  // constraint evaluation
  virtual void g_eval(const Real &t_n, const Real &t_np1,
                      const Real *x_n, const Real *x_np1,
                      Real *g_n) const;
  virtual void dg_sparse_eval(const Real &t_n, const Real &t_np1,
                              const Real *x_n, const Real *x_np1,
                              Real *dg) const;
  virtual void d2g_sparse_eval(const Real &t_n, const Real &t_np1,
                               const Real *x_n, const Real *x_np1,
                               Real *d2g) const;
  virtual void d3g_sparse_eval(const Real &t_n, const Real &t_np1,
                               const Real *x_n, const Real *x_np1,
                               Real *d3g) const;

  // constraint sparsity pattern
  virtual void dg_sparse_idxs(IndexVec *sparse_idxs)  const;
  virtual void d2g_sparse_idxs(IndexVec *sparse_idxs) const;
  virtual void d3g_sparse_idxs(IndexVec *sparse_idxs) const;

private:
  // Model reference
  const DynamicalModel &_model;

  // ODE integrator
  mutable EulerStep<DynamicalModel> _ode;
  // Temp. data
  mutable RealVec _x_mid;
  // Cache model vector fields
  mutable RealVec _df_model_sparse,
                  _d2f_model_sparse,
                  _d3f_model_sparse;
  IndexVec _df_model_sparse_idxs[2],
           _d2f_model_sparse_idxs[3],
           _d3f_model_sparse_idxs[4];

  // Copy
  TrapezoidRule(const TrapezoidRule&);
};

////////////////////////////////////////////////////////////////
/// constructor
TrapezoidRule::TrapezoidRule(const DynamicalModel &model) :
  QuadRule(model),
  _model (model),
  _ode(_model,_model.num_states()) {
  // Allocate temp data
  _x_mid.resize(_model.num_states());
  // Sparse indices of model derivatives
  _model.df_sparse_idxs(_df_model_sparse_idxs);
  _model.d2f_sparse_idxs(_d2f_model_sparse_idxs);
  _model.d3f_sparse_idxs(_d3f_model_sparse_idxs);
  // Storage space for model derivatives
  _df_model_sparse.resize(_df_model_sparse_idxs[0].size());
  _d2f_model_sparse.resize(_d2f_model_sparse_idxs[0].size());
  _d3f_model_sparse.resize(_d3f_model_sparse_idxs[0].size());
  // TODO Right now, we only support models where df
  //      diagonal entries are all non zero
  Index tmp = 0;
  for(Index idx = 0; idx < (Index)_df_model_sparse_idxs[0].size(); ++idx) {
    if(_df_model_sparse_idxs[0][idx] == _df_model_sparse_idxs[1][idx]) {
      ++tmp;
    }
  }
  if(tmp != _model.num_states()) {
    throw_exception("TrapezoidRule::TrapezoidRule: "
            "model df/dx diagonal elements must be non zero");
  }
}
/// destructor
TrapezoidRule::~TrapezoidRule() {
}

////////////////////////////////////////////////////////////////
/// cache df, and d2f at initial point to avoid recomputing
void TrapezoidRule::init_dg_eval(const Real &t_0, const Real *x_0) const {
  _model.df_sparse_eval(t_0,x_0,&_df_model_sparse.front());
}
void TrapezoidRule::init_d2g_eval(const Real &t_0, const Real *x_0) const {
  _model.d2f_sparse_eval(t_0,x_0,&_d2f_model_sparse.front());
}
void TrapezoidRule::init_d3g_eval(const Real &t_0, const Real *x_0) const {
  _model.d3f_sparse_eval(t_0,x_0,&_d3f_model_sparse.front());
}

////////////////////////////////////////////////////////////////
/// Constraint evaluated at midpoint between t(n) and t(n+1)
void TrapezoidRule::g_eval(const Real &t_n, const Real &t_np1,
                           const Real *x_n, const Real *x_np1,
                           Real *g_n) const {
  const Real dt = t_np1 - t_n, half_dt = 0.5*dt;

  // Integrate forward a half step
  _ode.step(t_n,x_n,half_dt,&_x_mid.front());

  // Integrate backward a half step from x_np1
  _ode.step(t_np1,x_np1,-half_dt,g_n);

  // Compute difference at the midpoint
  const Index nD = _model.num_states();
  for(Index iD = 0; iD < nD; ++iD) {
    g_n[iD] -= _x_mid[iD];
  }
}

////////////////////////////////////////////////////////////////
///
void TrapezoidRule::dg_sparse_eval(const Real &t_n, const Real &t_np1,
                                   const Real *x_n, const Real *x_np1,
                                   Real *dg) const {
  const Real dt = t_np1 - t_n, half_dt = 0.5*dt;

  // Iterators
  IndexVec::const_iterator it_sp_i, it_sp_j;
  Index idx_df = 0;

  // _df_model_sparse already holds df(t_n,x_n)
  //_model.df_sparse_eval(t_n,x_n,&_df_model_sparse.front());

  // Fill dg/dx_n
  it_sp_i = _df_model_sparse_idxs[0].begin();
  it_sp_j = _df_model_sparse_idxs[1].begin();
  idx_df = 0;
  while(it_sp_i != _df_model_sparse_idxs[0].end()) {
    (*dg) = -half_dt*_df_model_sparse[idx_df];
    if(*it_sp_i == *it_sp_j) {
      (*dg) -= 1.;
    }
    ++dg; ++idx_df; ++it_sp_i; ++it_sp_j;
  }

  // Evaluate df at x_n+1
  _model.df_sparse_eval(t_np1,x_np1,&_df_model_sparse.front());

  // Fill dg/dx_np1
  it_sp_i = _df_model_sparse_idxs[0].begin();
  it_sp_j = _df_model_sparse_idxs[1].begin();
  idx_df = 0;
  while(it_sp_i != _df_model_sparse_idxs[0].end()) {
    (*dg) = -half_dt*_df_model_sparse[idx_df];
    if(*it_sp_i == *it_sp_j) {
      (*dg) += 1.;
    }
    ++dg; ++idx_df; ++it_sp_i; ++it_sp_j;
  }
}

////////////////////////////////////////////////////////////////
///
void TrapezoidRule::d2g_sparse_eval(const Real &t_n, const Real &t_np1,
                                    const Real *x_n, const Real *x_np1,
                                    Real *d2g) const {
  const Real dt = t_np1 - t_n, half_dt = 0.5*dt;

  // Iterators
  IndexVec::const_iterator it_sp_i;
  Index idx_d2f = 0;

  // _d2f_model_sparse already holds d2f(t_n,x_n)
  //_model.d2f_sparse_eval(t_n,x_n,&_d2f_model_sparse.front());

  // Fill d2g/dx_n^2
  it_sp_i = _d2f_model_sparse_idxs[0].begin();
  idx_d2f = 0;
  while(it_sp_i != _d2f_model_sparse_idxs[0].end()) {
    (*d2g) = -half_dt*_d2f_model_sparse[idx_d2f];
    ++d2g; ++idx_d2f; ++it_sp_i;
  }

  // Evaluate df at x_n+1
  _model.d2f_sparse_eval(t_np1,x_np1,&_d2f_model_sparse.front());

  // Fill d2g/dx_n+1^2
  it_sp_i = _d2f_model_sparse_idxs[0].begin();
  idx_d2f = 0;
  while(it_sp_i != _d2f_model_sparse_idxs[0].end()) {
    (*d2g) = -half_dt*_d2f_model_sparse[idx_d2f];
    ++d2g; ++idx_d2f; ++it_sp_i;
  }
}

////////////////////////////////////////////////////////////////
///
void TrapezoidRule::d3g_sparse_eval(const Real &t_n, const Real &t_np1,
                                    const Real *x_n, const Real *x_np1,
                                    Real *d3g) const {
  const Real dt = t_np1 - t_n, half_dt = 0.5*dt;

  // Iterators
  IndexVec::const_iterator it_sp_i;
  Index idx_d3f = 0;

  // _d3f_model_sparse already holds d3f(t_n,x_n)
  //_model.d3f_sparse_eval(t_n,x_n,&_d3f_model_sparse.front());

  // Fill d3g/dx_n^2
  it_sp_i = _d3f_model_sparse_idxs[0].begin();
  idx_d3f = 0;
  while(it_sp_i != _d3f_model_sparse_idxs[0].end()) {
    (*d3g) = -half_dt*_d3f_model_sparse[idx_d3f];
    ++d3g; ++idx_d3f; ++it_sp_i;
  }

  // Evaluate df at x_n+1
  _model.d3f_sparse_eval(t_np1,x_np1,&_d3f_model_sparse.front());

  // Fill d3g/dx_n+1^2
  it_sp_i = _d3f_model_sparse_idxs[0].begin();
  idx_d3f = 0;
  while(it_sp_i != _d3f_model_sparse_idxs[0].end()) {
    (*d3g) = -half_dt*_d3f_model_sparse[idx_d3f];
    ++d3g; ++idx_d3f; ++it_sp_i;
  }
}

////////////////////////////////////////////////////////////////
/// Get sparsity pattern of 1st derivative.
///
/// Block structure is organized as:
///     n  n+1
///  || 1 ||  n
///  ||---||
//   || 2 || n+1
///
/// where 1 and 2 are D*D blocks containing model df sparsity
/// structure at times n, n+1 respectively.
///
void TrapezoidRule::dg_sparse_idxs(IndexVec *sparse_idxs) const {
  // DynamicalModel
  const Index nD = _model.num_states();

  // Resize Index vectors
  Index num_df_non0s = _df_model_sparse_idxs[0].size();
  Index num_elems = 2*num_df_non0s;
  sparse_idxs[0].resize(num_elems);
  sparse_idxs[1].resize(num_elems);

  // Fill Index vectors
  typedef IndexVec::const_iterator Iter;
  Iter it_sp_i, it_sp_j;
  Index idx = 0;

  // Fill dg/dx_n
  it_sp_i = _df_model_sparse_idxs[0].begin();
  it_sp_j = _df_model_sparse_idxs[1].begin();
  while(it_sp_i != _df_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i ;
    sparse_idxs[1][idx] = *it_sp_j;
     ++idx; ++it_sp_i; ++it_sp_j;
  }

  // Fill dg/dx_np1
  it_sp_i = _df_model_sparse_idxs[0].begin();
  it_sp_j = _df_model_sparse_idxs[1].begin();
  while(it_sp_i != _df_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j + nD;
     ++idx; ++it_sp_i; ++it_sp_j;
  }
}

////////////////////////////////////////////////////////////////
/// Get sparsity pattern of 2rd derivative. Since there are no
/// cross terms at different times, (d_n d_n+1 g = 0), it is
/// easy to include the sparsity pattern of the model.
///
/// Block structure is organized as:
///     n  n+1
///  || 1 |   ||  n
///  ||---|---||
//   ||   | 2 || n+1
///
/// where 1 and 2 are D*D blocks containing model d2f sparsity
/// structure at times n, n+1 respectively.
///
void TrapezoidRule::d2g_sparse_idxs(IndexVec *sparse_idxs) const {
  // DynamicalModel
  const Index nD = _model.num_states();

  // Resize Index vectors
  Index num_d2f_non0s = _d2f_model_sparse_idxs[0].size();
  Index num_elems = 2*num_d2f_non0s;
  sparse_idxs[0].resize(num_elems);
  sparse_idxs[1].resize(num_elems);
  sparse_idxs[2].resize(num_elems);

  // Fill Index vectors
  typedef IndexVec::const_iterator Iter;
  Iter it_sp_i, it_sp_j, it_sp_k;
  Index idx = 0;

  // Fill d2g/dx_n^2
  it_sp_i = _d2f_model_sparse_idxs[0].begin();
  it_sp_j = _d2f_model_sparse_idxs[1].begin();
  it_sp_k = _d2f_model_sparse_idxs[2].begin();
  while(it_sp_i != _d2f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j;
    sparse_idxs[2][idx] = *it_sp_k;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k;
  }

  // Fill d2g/dx_n+1^2
  it_sp_i = _d2f_model_sparse_idxs[0].begin();
  it_sp_j = _d2f_model_sparse_idxs[1].begin();
  it_sp_k = _d2f_model_sparse_idxs[2].begin();
  while(it_sp_i != _d2f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j + nD;
    sparse_idxs[2][idx] = *it_sp_k + nD;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k;
  }
}

////////////////////////////////////////////////////////////////
/// Get sparsity pattern of 3rd derivative. Since there are no
/// cross terms at different times, (d_n d_n+1 g = 0), it is
/// easy to include the sparsity pattern of the model.
///
/// Block structure is organized as:
///       n           n+1
///  || 1 |   ||  ||   |   ||  n
///  ||---|---||  ||---|---||
//   ||   |   ||  ||   | 2 || n+1
///
/// where 1 and 2 are D*D*D blocks containing model d3f sparsity
/// structure at times n, n+1 respectively.
///
void TrapezoidRule::d3g_sparse_idxs(IndexVec *sparse_idxs) const {
  // DynamicalModel
  const Index nD = _model.num_states();

  // Resize Index vectors
  Index num_d3f_non0s = _d3f_model_sparse_idxs[0].size();
  Index num_elems = 2*num_d3f_non0s;
  sparse_idxs[0].resize(num_elems);
  sparse_idxs[1].resize(num_elems);
  sparse_idxs[2].resize(num_elems);
  sparse_idxs[3].resize(num_elems);

  // Fill Index vectors
  typedef IndexVec::const_iterator Iter;
  Iter it_sp_i, it_sp_j, it_sp_k, it_sp_l;
  Index idx = 0;

  // Fill d3g/dx_n^2
  it_sp_i = _d3f_model_sparse_idxs[0].begin();
  it_sp_j = _d3f_model_sparse_idxs[1].begin();
  it_sp_k = _d3f_model_sparse_idxs[2].begin();
  it_sp_l = _d3f_model_sparse_idxs[3].begin();
  while(it_sp_i != _d3f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j;
    sparse_idxs[2][idx] = *it_sp_k;
    sparse_idxs[3][idx] = *it_sp_l;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k; ++ it_sp_l;
  }

  // Fill d3g/dx_n+1^2
  it_sp_i = _d3f_model_sparse_idxs[0].begin();
  it_sp_j = _d3f_model_sparse_idxs[1].begin();
  it_sp_k = _d3f_model_sparse_idxs[2].begin();
  it_sp_l = _d3f_model_sparse_idxs[3].begin();
  while(it_sp_i != _d3f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j + nD;
    sparse_idxs[2][idx] = *it_sp_k + nD;
    sparse_idxs[3][idx] = *it_sp_l + nD;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k; ++it_sp_l;
  }
}

#endif /* TRAPEZOID_RULE_H_ */
