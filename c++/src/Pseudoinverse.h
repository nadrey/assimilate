#ifndef PSEUDO_INVERSE_H_
#define PSEUDO_INVERSE_H_

// My includes
#include "Utils.h"

// GSL SVD Library
#include "gsl/gsl_linalg.h"

////////////////////////////////////////////////////////////////
///
class Pseudoinverse {
public:

  // constructor
  Pseudoinverse(Index  N,Index  M);
  ~Pseudoinverse();

  // compute pseudoinverse
  void operator()(const Real *in, Real *out);

  // temporary storage
  gsl_matrix *_U, *_V;
  gsl_vector *_S, *_work;
  bool _transpose;
  Real _tol;
};
///
Pseudoinverse::Pseudoinverse(Index  M, Index  N) {
  _transpose = false;
  if(M < N) {
    _transpose = true;
    Index  tmp = M;
    M = N;
    N = tmp;
  }
  _U = gsl_matrix_alloc(M,N);
  _V = gsl_matrix_alloc(N,N);
  _S = gsl_vector_alloc(N);
  _work = gsl_vector_alloc(N);
  _tol = std::numeric_limits<Real>::epsilon();
}
///
Pseudoinverse::~Pseudoinverse() {
  gsl_matrix_free(_U);
  gsl_matrix_free(_V);
  gsl_vector_free(_S);
  gsl_vector_free(_work);
}
///
void Pseudoinverse::operator()(const Real *in, Real *out) {
  Real sum;
  Index  M = _U->size1,
         N = _U->size2;

  // fill U matrix with input
  for(Index i = 0; i < M; ++i) {
    for(Index  j = 0; j < N; ++j) {
      if(!_transpose) {
        gsl_matrix_set(_U,i,j,in[i*M + j]);
      }
      else {
        gsl_matrix_set(_U,j,i,in[i*M + j]);
      }
    }
  }

  // compute SVD
  gsl_linalg_SV_decomp(_U,_V,_S,_work);
  // truncate SVs to specified tolerance
  Index iMax = M;
  for(Index i = 0; i < M; ++i) {
    if(gsl_vector_get(_S,i) < _tol) {
      iMax = i;
      break;
    }
  }
  // compute pseudoinverse
  for(Index i = 0; i < M; ++i) {
    for(Index  j = 0; j < N; ++j) {
      sum = 0;
      for(Index  k = 0; k < iMax; ++k) {
        sum += gsl_matrix_get(_V,i,k)*
                 (1./gsl_vector_get(_S,k))*
                   gsl_matrix_get(_U,j,k);
      }
      if(!_transpose) {
        out[i*M + j] = sum;
      }
      else {
        out[j*M + i] = sum;
      }
    }
  }
}



#endif /* PSEUDO_INVERSE_H_ */
