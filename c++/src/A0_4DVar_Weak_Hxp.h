#ifndef A0_4DVAR_WEAK_HXP_H_
#define A0_4DVAR_WEAK_HXP_H_

// My includes
#include "EstimationAction.h"
#include "HamiltonsEqns.h"
#include "TrapezoidRule.h"
#include "ImplicitMidpointRule.h"

////////////////////////////////////////////////////////////////
/// Derived action that considers minimization of the Lagrangian
/// with Hamilton's equations as dynamical constraints.
///
/// The path vector is organized as:
///
///   XP = [x(t_0),p(t_0),x(t_1),p(t_1),...,x(t_N),p(t_N)]
///
/// where x(t_n), p(t_n) are respectively the dynamical variables
/// and canonical momenta at time t_n.
///
/// The reason for organizing it this way (as opposed to the other
/// obvious choice XP = [X,P]) is to simplify the constraint
/// calculations. We want to be able to use a quadrature rule
/// g_n(x_n,x_{n+1}) that acts between points x_n and x_n+1.
///
/// In order to do this, it is convenient to lump all the variables
/// at time t_n and t_{n+1} into a single array. Otherwise, we would
/// have to change the function signature to either: (1) treat x and
/// p separately, or (2) pass in some index to tell the quadrature
/// method where the relevant variables are located. Both of these
/// approaches are not ideal.
///
/// Thus, if we were to choose XP = [X,P] the easiest option is to copy
/// x(t_n) and p(t_n) into some local array xp(t_n), since x(t_n) and
/// p(t_n) are not  at contiguous locations in memory. This is a lot of
/// extra copying,  that we avoid by choosing to lay out the memory with
/// the XP = [x(t_0),p(t_0),...,x(t_N),p(t_N)] approach.
///
/// The trade off is that we will have explicitly reshape the final output
/// to separate the X and P paths. But this can be done in a single pass
/// through the data.
///
class A0_4DVar_Weak_Hxp : public EstimationAction {
public:

  // Constructors
  A0_4DVar_Weak_Hxp();
  A0_4DVar_Weak_Hxp(const char* quad_type);

  // Destructor
  virtual ~A0_4DVar_Weak_Hxp();

  // Reimplemented from EstimationAction
  virtual void initialize();

  // Class name
  virtual std::string classname() const;

  // Problem dimensions
  virtual Index num_variables() const;
  virtual Index num_constraints() const;

  // Set methods
  virtual void set_Rm_diag(const RealVec &Rm_diag);
  virtual void set_Rf_diag(const RealVec &Rf_diag);

  // Get bounds information
  virtual void variable_bounds(Real *x_lower, Real *x_upper) const;
  virtual void constraint_bounds(Real *g_lower, Real *g_upper) const;

  // Reshape the output to only save the states
  virtual void save_path(const Real *XP);
  // Include momentum in initial path
  virtual void initial_path(Real *XP) const;

  // Compute objective function
  virtual void f_eval(const Real *XP, Real &f, bool new_XP);
  virtual void df_eval(const Real *XP, Real *df, bool new_XP);
  virtual void d2f_sparse_eval(const Real *XP, Real *d2f, bool new_XP);

  // Compute dynamical constraints
  virtual void g_eval(const Real *XP, Real *g, bool new_XP);
  virtual void dg_sparse_eval(const Real *XP, Real *dg, bool new_XP);
  virtual void d2g_sparse_eval(const Real *XP, Real *d2g, bool new_XP);

  // Sparsity patterns
  virtual void d2f_sparse_idxs(IndexVec *sparse_idxs) const;
  virtual void dg_sparse_idxs(IndexVec *sparse_idxs)  const;
  virtual void d2g_sparse_idxs(IndexVec *sparse_idxs) const;

  // Overloaded method to get separate contributions from Rm, Rf
  virtual void f_eval(const Real *X, Real &Rm_est,  Real &Rf_est,
                                     Real &Rm_pred, Real &Rf_pred);
protected:

  // Current path
  RealVec _path;
  // Internal class to implement Hamilton's equations
  HamiltonsEqns *_hamiltons_eqns;
  // Quadrature rule for constraints
  std::string _quad_type_str;
  QuadRule *_quad;
  // Model/measurement error
  RealVec _Rm_diag, _Rf_diag;
  // Number of non zero elements
  Index _dg_num_non0s, _d2g_num_non0s;

private:
  // Copy constructor
  A0_4DVar_Weak_Hxp(const A0_4DVar_Weak_Hxp&);
};


////////////////////////////////////////////////////////////////
/// Constructors
A0_4DVar_Weak_Hxp::A0_4DVar_Weak_Hxp() :
  EstimationAction(),
  _hamiltons_eqns (NULL),
  _quad_type_str ("TrapezoidRule"), _quad (NULL),
  _dg_num_non0s (-1), _d2g_num_non0s (-1) {
}
A0_4DVar_Weak_Hxp::A0_4DVar_Weak_Hxp(const char *quad_type) :
  EstimationAction(),
  _hamiltons_eqns (NULL),
  _quad_type_str (quad_type), _quad (NULL),
  _dg_num_non0s (-1), _d2g_num_non0s (-1) {
}
/// Destructor
A0_4DVar_Weak_Hxp::~A0_4DVar_Weak_Hxp() {
  delete _quad; _quad = NULL;
  delete _hamiltons_eqns; _hamiltons_eqns = NULL;
}

////////////////////////////////////////////////////////////////
///
std::string A0_4DVar_Weak_Hxp::classname() const {
  if(_quad->classname() == "TrapezoidRule") {
    return "A0_4DVar_Weak_Hxp_TrapezoidRule";
  }
  else if(_quad->classname() == "ImplicitMidpointRule") {
    return "A0_4DVar_Weak_Hxp_ImplicitMidpointRule";
  }
  else {
    std::string msg;
    msg = "A0_4DVar_Weak_Hxp::classname: unknown quad. rule\n"
             + _quad->classname();
    throw_exception(msg.c_str());
  }
  return std::string("");
}

////////////////////////////////////////////////////////////////
/// Variables are XP and P at each time point
Index A0_4DVar_Weak_Hxp::num_variables() const {
  const Index nNp1 = _data->time_domain().num_time_points(),
              nD = _data->dynamical_model().num_states();
  return 2*nNp1*nD;
}

////////////////////////////////////////////////////////////////
/// Get number of constraints:
/// 2*D*(N-1) constraints for Hamilton's equations
///
/// Optional: an additional 2*D constraints as boundary conditions
/// on P(t_0) and P(t_N)
///
Index A0_4DVar_Weak_Hxp::num_constraints() const {
  const Index nN = _data->time_domain().num_time_intervals(),
              nD = _data->dynamical_model().num_states();
  Index ret = 2*nN*nD;
  return ret;
}

////////////////////////////////////////////////////////////////
/// Get initial path
void A0_4DVar_Weak_Hxp::initial_path(Real *XP) const {
  const RealVec &X0 = _data->estimated_states();
  const Index nNp1 = _data->time_domain().num_time_points(),
              nD = _data->dynamical_model().num_states();
  for(Index iN = 0; iN < nNp1; ++iN) {
    for(Index iD = 0; iD < nD; ++iD) {
      XP[nD*(2*iN) + iD] = X0[iN*nD + iD];
      XP[nD*(2*iN+1) + iD] = 0;
      // TODO When debugging derivatives, p should be non zero
      //XP[nD*(2*iN+1) + iD] = (iD+1)*(nD+1)*0.5;
    }
  }
}

////////////////////////////////////////////////////////////////
/// Default bounds information
void A0_4DVar_Weak_Hxp::variable_bounds(Real *x_lower, Real *x_upper) const {
  // State bounds set by model
  const Index nD = _data->dynamical_model().num_states();
  RealVec m_lower(nD), m_upper(nD);
  _data->dynamical_model().state_bounds(&m_lower.front(),&m_upper.front());

  // Momentum bounds
  Real p_upper = +1.e12,
       p_lower = -p_upper;

  // Initialize variable bounds
  const Index nNp1 = _data->time_domain().num_time_points();
  for(Index iN = 0; iN < nNp1; ++iN) {
    for(Index iD = 0; iD < nD; ++iD) {
      x_lower[nD*(2*iN) + iD] = m_lower[iD];
      x_upper[nD*(2*iN) + iD] = m_upper[iD];
      x_lower[nD*(2*iN+1) + iD] = p_lower;
      x_upper[nD*(2*iN+1) + iD] = p_upper;
    }
  }

  // Boundary conditions for momenta p(0), p(N)
  p_upper = 1.e-12; p_lower = -p_upper;
  for(Index iD = 0; iD < nD; ++iD) {
    x_lower[nD + iD] = p_lower;
    x_upper[nD + iD] = p_upper;
    x_lower[nD*(2*nNp1-1) + iD] = p_lower;
    x_upper[nD*(2*nNp1-1) + iD] = p_upper;
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak_Hxp::constraint_bounds(Real *g_lower, Real *g_upper) const {
  // Set constraint bounds for Hamilton's equations to O(dt)
  const Index nN = _data->time_domain().num_time_intervals(),
              nD = _data->dynamical_model().num_states();
  Real g_bound = std::pow(_data->time_domain().dt(),1.);
  for(Index iN = 0; iN < 2*nN; ++iN) {
    for(Index iD = 0; iD < nD; ++iD) {
      g_lower[iN*nD + iD] = -g_bound;
      g_upper[iN*nD + iD] = +g_bound;
    }
  }
}

////////////////////////////////////////////////////////////////
/// Reimplemented to only save the estimated state variables
void A0_4DVar_Weak_Hxp::save_path(const Real *XP) {
  const Index nD = _data->dynamical_model().num_states();
  const Index nNp1 = _data->time_domain().num_time_points();
  for(Index iN = 0; iN < nNp1; ++iN) {
    _data->save_step(iN,&XP[iN*2*nD],NULL);
  }
}

////////////////////////////////////////////////////////////////
/// Reimplemented from EstimationAction to set Rm, Rf
void A0_4DVar_Weak_Hxp::set_Rm_diag(const RealVec &Rm_diag) {
  // Consistency check
  Index nL = _data->observation_model().num_observed_states();
  if((Index)Rm_diag.size() != nL) {
    throw_exception("A0_4DVar_Weak_Hxp::set_Rm_diag: "
            "Rm has improper length");
  }
  _Rm_diag = Rm_diag;
}
///
void A0_4DVar_Weak_Hxp::set_Rf_diag(const RealVec &Rf_diag) {
  // Consistency check
  Index nD = _data->dynamical_model().num_states();
  if((Index)Rf_diag.size() != nD) {
    throw_exception("A0_4DVar_Weak_Hxp::set_Rf_diag: "
            "Rf has improper length");
  }
  _Rf_diag = Rf_diag;
}

////////////////////////////////////////////////////////////////
void A0_4DVar_Weak_Hxp::initialize() {

  // Set default measurement error scaling
  Index nL = _data->observation_model().num_observed_states();
  if(_Rm_diag.empty()) {
    _Rm_diag.resize(nL,1.);
  }

  // Set default model error scaling
  Index nD = _data->dynamical_model().num_states();
  if(_Rf_diag.empty()) {
    _Rf_diag.resize(nD,1.);
  }

  // Initialize Hamilton's equations
  delete _hamiltons_eqns; _hamiltons_eqns = NULL;
  _hamiltons_eqns = new HamiltonsEqns(*_data,_Rm_diag,_Rf_diag);

  // Initialize constraint quadrature
  delete _quad; _quad = NULL;
  if(_quad_type_str == "TrapezoidRule") {
    _quad = new TrapezoidRule(*_hamiltons_eqns);
  }
  else if(_quad_type_str == "ImplicitMidpointRule") {
    _quad = new ImplicitMidpointRule(*_hamiltons_eqns);
  }
  else {
    std::string msg;
    msg = "A0_4DVar_Weak_Hxp::initialize: unknown quad. rule\n"
             + _quad_type_str;
    throw_exception(msg.c_str());
  }

  // Cache number of non 0 elements for constraints
  IndexVec tmp_idxs[3];
  _quad->dg_sparse_idxs(tmp_idxs);
  _dg_num_non0s = tmp_idxs[0].size();
  _quad->d2g_sparse_idxs(tmp_idxs);
  _d2g_num_non0s = tmp_idxs[0].size();
}

////////////////////////////////////////////////////////////////
/// Objective function is time integral of `phase space' Lagrangian
///
///    L = 1/2|y(t) - h(x)|^2_Rm + 1/2|p|^2_Rfinv
///
void A0_4DVar_Weak_Hxp::f_eval(const Real *XP, Real &f, bool new_XP) {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();
  // Time domain
  const TimeDomain time_domain = _data->time_domain();
  const Index nNp1 = time_domain.num_time_points();
  const Real &dt = time_domain.dt();
  // Observation model
  const ObservationModel &obs_model = _data->observation_model();
  const Index nL = obs_model.num_observed_states();
  const IndexVec &observed_state_idxs = obs_model.observed_state_idxs();
  // Observation window length
  const Real &obs_window_length = obs_model.observation_window_length();
  const Index nNp1_obs = round_to_int(obs_window_length/dt) + 1;
  // Observations
  const Real *y = &_data->observations().front();

  // Reset f to 0
  f = 0;

  // Measurement error term
  for(Index iN = 0; iN < nNp1_obs; ++iN) {
    for(Index iL = 0; iL < nL; ++iL) {
      const Index idx_meas = observed_state_idxs[iL];
      const  Real tmp = y[iN*nL + iL] - XP[nD*(2*iN) + idx_meas];
      f += tmp*tmp*_Rm_diag[iL];
    }
  }
  // Model error term
  for(Index iN = 0; iN < nNp1; ++iN) {
    for(Index iD = 0; iD < nD; ++iD) {
      f += XP[nD*(2*iN+1) + iD]*XP[nD*(2*iN+1) + iD]/_Rf_diag[iD];
    }
  }

  // Apply overall scale factor
  const Real sf = 0.5/nNp1;
  f *= sf;
}

////////////////////////////////////////////////////////////////
/// Overloaded method to get separate contributions from Rm, Rf
void A0_4DVar_Weak_Hxp::f_eval(const Real *XP,
                               Real &Rm_est,  Real &Rf_est,
                               Real &Rm_pred, Real &Rf_pred) {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();
  // Time domain
  const TimeDomain time_domain = _data->time_domain();
  const Index nNp1 = time_domain.num_time_points();
  const Real &dt = time_domain.dt();
  // Observation model
  const ObservationModel &obs_model = _data->observation_model();
  const Index nL = obs_model.num_observed_states();
  const IndexVec &observed_state_idxs = obs_model.observed_state_idxs();
  // Observation window length
  const Real &obs_window_length = obs_model.observation_window_length();
  const Index nNp1_obs = round_to_int(obs_window_length/dt) + 1;
  // Observations
  const Real *y = &_data->observations().front();

  // Set values to 0
  Rm_est = 0;  Rf_est = 0;
  Rm_pred = 0; Rf_pred = 0;

  // Estimated measurement error term
  for(Index iN = 0; iN < nNp1_obs; ++iN) {
    for(Index iL = 0; iL < nL; ++iL) {
      const Index idx_meas = observed_state_idxs[iL];
      const  Real tmp = y[iN*nL + iL] - XP[nD*(2*iN) + idx_meas];
      Rm_est += tmp*tmp*_Rm_diag[iL];
    }
  }
  // Predicted measurement error term
  for(Index iN = nNp1_obs; iN < nNp1; ++iN) {
    for(Index iL = 0; iL < nL; ++iL) {
      const Index idx_meas = observed_state_idxs[iL];
      const  Real tmp = y[iN*nL + iL] - XP[nD*(2*iN) + idx_meas];
      Rm_pred += tmp*tmp*_Rm_diag[iL];
    }
  }
  // Estimated model error term
  for(Index iN = 0; iN < nNp1_obs; ++iN) {
    for(Index iD = 0; iD < nD; ++iD) {
      Rf_est += XP[nD*(2*iN+1) + iD]*XP[nD*(2*iN+1) + iD]/_Rf_diag[iD];
    }
  }
  // Predicted model error term
  for(Index iN = nNp1_obs; iN < nNp1; ++iN) {
    for(Index iD = 0; iD < nD; ++iD) {
      Rf_pred += XP[nD*(2*iN+1) + iD]*XP[nD*(2*iN+1) + iD]/_Rf_diag[iD];
    }
  }

  // Apply overall scale factor
  const Real sf = 0.5/nNp1;
  Rm_est  *= sf; Rf_est  *= sf;
  Rm_pred *= sf; Rf_pred *= sf;
}

////////////////////////////////////////////////////////////////
/// Compute Jacobian of objective function
///
///    dL/dx = -[dh/dx]^T*Rm*(y(t)-h(x))
///    dL/dp = +Rfinv*p
///
void A0_4DVar_Weak_Hxp::df_eval(const Real *XP, Real *df, bool new_XP) {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states(),
              numXP = 2*nD;
  // Time domain
  const TimeDomain time_domain = _data->time_domain();
  const Index nNp1 = time_domain.num_time_points();
  const Real &dt = time_domain.dt();
  // Observation model
  const ObservationModel &obs_model = _data->observation_model();
  const Index nL = obs_model.num_observed_states();
  const IndexVec &observed_state_idxs = obs_model.observed_state_idxs();
  // Observation window length
  const Real &obs_window_length = obs_model.observation_window_length();
  const Index nNp1_obs = round_to_int(obs_window_length/dt) + 1;
  // Observations
  const Real *y = &_data->observations().front();
  // Overall scale factor
  const Real sf = 1./nNp1;

  // Reset df to zero
  memset(df,0.,num_variables()*sizeof(Real));

  // Measurement error term
  for(Index iN = 0; iN < nNp1_obs; ++iN) {
    for(Index iL = 0; iL < nL; ++iL) {
      const Index idx_meas = observed_state_idxs[iL];
      const Real tmp = y[iN*nL + iL] - XP[iN*numXP + idx_meas];
      df[iN*numXP + idx_meas] = -sf*tmp*_Rm_diag[iL];
    }
  }

  // Model error term
  for(Index iN = 0; iN < nNp1; ++iN) {
    for(Index iD = 0; iD < nD; ++iD) {
      df[iN*numXP + nD + iD] = sf*XP[iN*numXP + nD + iD]/_Rf_diag[iD];
    }
  }
}

////////////////////////////////////////////////////////////////
/// Compute Hessian of objective function.
///
///    d2L/dx^2 = +[dh/dx]^T*Rm*dh/dx + ...
///                  d^2h/dx^2*Rm*(y(t)-h(x))
///    d2L/dxdp = d2L/dp/dx = 0
///    d2L/dp^2 = +Rfinv
///
///  TODO For now, structure is assumed to be two diagonal blocks
///
void A0_4DVar_Weak_Hxp::d2f_sparse_eval(const Real *XP, Real *d2f, bool new_XP) {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();
  // Time domain
  const TimeDomain time_domain = _data->time_domain();
  const Index nNp1 = time_domain.num_time_points();
  const Real &dt = time_domain.dt();
  // Observation model
  const ObservationModel &obs_model = _data->observation_model();
  const Index nL = obs_model.num_observed_states();
  // Observation window length
  const Real &obs_window_length = obs_model.observation_window_length();
  const Index nNp1_obs = round_to_int(obs_window_length/dt) + 1;

  // Overall scale factor
  const Real sf = 1./nNp1;

  // Measurement error block
  for(Index iN = 0; iN < nNp1_obs; ++iN) {
    for(Index iL = 0; iL < nL; ++iL) {
      d2f[iN*nL + iL] = sf*_Rm_diag[iL];
    }
  }
  // Model error block
  for(Index iN = 0; iN < nNp1; ++iN) {
    for(Index iD = 0; iD < nD; ++iD) {
      d2f[nL*nNp1 + iN*nD + iD] = sf/_Rf_diag[iD];
    }
  }
}

////////////////////////////////////////////////////////////////
/// Sparsity pattern of objective function Hessian
///
///    d2L/dx^2 = -[dh/dx]^T*Rm*dh/dx + ...
///                  d^2h/dx^2*Rm*(y(t)-h(x))
///    d2L/dxdp = d2L/dp/dx = 0
///    d2L/dp^2 = +Rfinv
///
///  For now, Rm and Rf are stored in separate diagonal blocks
///
void A0_4DVar_Weak_Hxp::d2f_sparse_idxs(IndexVec *sparse_idxs) const {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();
  // Time domain
  const TimeDomain time_domain = _data->time_domain();
  const Index nNp1 = time_domain.num_time_points();
  // Observations
  const ObservationModel &obs = _data->observation_model();
  const Index nL = obs.num_observed_states();
  const IndexVec &observed_state_idxs = obs.observed_state_idxs();

  // Resize
  const Index num_elems = nNp1*(nL + nD);
  sparse_idxs[0].resize(num_elems);
  sparse_idxs[1].resize(num_elems);

  // Measurement error block
  for(Index iN = 0; iN < nNp1; ++iN) {
    for(Index iL = 0; iL < nL; ++iL) {
      Index idx_meas = observed_state_idxs[iL];
      sparse_idxs[0][iN*nL + iL] = nD*(2*iN) + idx_meas;
      sparse_idxs[1][iN*nL + iL] = nD*(2*iN) + idx_meas;
    }
  }

  // Model error block
  for(Index iN = 0; iN < nNp1; ++iN) {
    for(Index iD = 0; iD < nD; ++iD) {
      sparse_idxs[0][nL*nNp1 + iN*nD + iD] = nD*(2*iN+1) + iD;
      sparse_idxs[1][nL*nNp1 + iN*nD + iD] = nD*(2*iN+1) + iD;
    }
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak_Hxp::g_eval(const Real *XP, Real *g, bool new_XP) {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states(),
              numXP = 2*nD;
  // Time domain
  const TimeDomain &time_domain = _data->time_domain();
  const RealVec &time_points = time_domain.time_points();
  const Index nN = time_domain.num_time_intervals();

  // Adjacent time points
  const Real *t_n = &time_points.front(), *t_np1 = t_n+1;
  const Real *x_n = XP, *x_np1 = x_n + numXP;

  // Rescale constraints by 1/dt
  const Real sf = 1./time_domain.dt();

  // Compute g
  for(Index iN = 0; iN < nN; ++iN) {
    _quad->g_eval(*t_n,*t_np1,x_n,x_np1,g);
    for(Index iD = 0; iD < numXP; ++iD) {
      g[iD] *= sf;
    }
    x_n += numXP, x_np1 += numXP, ++t_n, ++t_np1;
    g += numXP;
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak_Hxp::dg_sparse_eval(const Real *XP, Real *dg, bool new_XP) {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states(),
              numXP = 2*nD;
  // Time domain
  const TimeDomain &time_domain = _data->time_domain();
  const RealVec &time_points = time_domain.time_points();
  const Index nN = time_domain.num_time_intervals();

  // Adjacent time points
  const Real *t_n = &time_points.front(), *t_np1 = t_n+1;
  const Real *x_n = XP, *x_np1 = x_n + numXP;

  // Rescale constraints by 1/dt
  const Real sf = 1./time_domain.dt();

  // Initialize calculation
  _quad->init_dg_eval(*t_n,x_n);
  // Compute dg
  for(Index iN = 0; iN < nN; ++iN) {
    _quad->dg_sparse_eval(*t_n,*t_np1,x_n,x_np1,dg);
    for(Index iD = 0; iD < _dg_num_non0s; ++iD) {
      dg[iD] *= sf;
    }
    x_n += numXP, x_np1 += numXP, ++t_n, ++t_np1;
    dg += _dg_num_non0s;
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak_Hxp::d2g_sparse_eval(const Real *XP, Real *d2g, bool new_XP) {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states(),
              numXP = 2*nD;
  // Time domain
  const TimeDomain &time_domain = _data->time_domain();
  const RealVec &time_points = time_domain.time_points();
  const Index nN = time_domain.num_time_intervals();

  // Adjacent time points
  const Real *t_n = &time_points.front(), *t_np1 = t_n+1;
  const Real *x_n = XP, *x_np1 = x_n + numXP;

  // Rescale constraints by 1/dt
  const Real sf = 1./time_domain.dt();

  // Initialize calculation
  _quad->init_d2g_eval(*t_n,x_n);
  // Compute d2g
  for(Index iN = 0; iN < nN; ++iN) {
    _quad->d2g_sparse_eval(*t_n,*t_np1,x_n,x_np1,d2g);
    for(Index iD = 0; iD < _d2g_num_non0s; ++iD) {
      d2g[iD] *= sf;
    }
    x_n += numXP, x_np1 += numXP, ++t_n, ++t_np1;
    d2g += _d2g_num_non0s;
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak_Hxp::dg_sparse_idxs(IndexVec *sparse_idxs) const {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states(),
              numXP = 2*nD;
  // Time domain
  const TimeDomain &time_domain = _data->time_domain();
  const Index nN = time_domain.num_time_intervals();

  // Clear any existing data
  Index num_derivs = 2;
  for(Index i_deriv = 0; i_deriv < num_derivs; ++i_deriv) {
    sparse_idxs[i_deriv].clear();
  }

  // Compute dg_n/dx_n and dg_n/dx_n+1
  _quad->dg_sparse_idxs(sparse_idxs);

  // Reserve space for the rest of the indices
  Index num_sp_idxs = sparse_idxs[0].size();
  for(Index i_deriv = 0; i_deriv < num_derivs; ++i_deriv) {
    sparse_idxs[i_deriv].resize(num_sp_idxs*nN);
  }

  // Fill in the remaining indices
  Index idx_i = num_sp_idxs;
  for(Index iN = 1; iN < nN; ++iN) {
    Index offset = iN*numXP;
    for(Index idx_j = 0; idx_j < num_sp_idxs; ++idx_i, ++idx_j) {
      for(Index i_deriv = 0; i_deriv < num_derivs; ++i_deriv) {
        sparse_idxs[i_deriv][idx_i] = sparse_idxs[i_deriv][idx_j] + offset;
      }
    }
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak_Hxp::d2g_sparse_idxs(IndexVec *sparse_idxs) const {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states(),
              numXP = 2*nD;
  // Time domain
  const TimeDomain &time_domain = _data->time_domain();
  const Index nN = time_domain.num_time_intervals();

  // Clear any existing data
  Index num_derivs = 3;
  for(Index i_deriv = 0; i_deriv < num_derivs; ++i_deriv) {
    sparse_idxs[i_deriv].clear();
  }

  // Compute d2g_n/dx_n^2 and d2g_n/dx_n+1^2
  _quad->d2g_sparse_idxs(sparse_idxs);

  // Reserve space for the rest of the indices
  Index num_sp_idxs = sparse_idxs[0].size();
  for(Index i_deriv = 0; i_deriv < num_derivs; ++i_deriv) {
    sparse_idxs[i_deriv].resize(num_sp_idxs*nN);
  }

  // Fill in the remaining indices
  Index idx_i = num_sp_idxs;
  for(Index iN = 1; iN < nN; ++iN) {
    Index offset = iN*numXP;
    for(Index idx_j = 0; idx_j < num_sp_idxs; ++idx_i, ++idx_j) {
      for(Index i_deriv = 0; i_deriv < num_derivs; ++i_deriv) {
        sparse_idxs[i_deriv][idx_i] = sparse_idxs[i_deriv][idx_j] + offset;
      }
    }
  }
}

#endif /* A0_4DVAR_WEAK_HXP_H_ */
