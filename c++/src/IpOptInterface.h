#ifndef IpOptInterface_H_
#define IpOptInterface_H_

/************************************************
 * This class serves as an interface to IPOPT
 * providing the objective function and
 * derivatives of the action
 ************************************************/

// IPOPT includes
#include "IpTNLP.hpp"
#include "IpIpoptCalculatedQuantities.hpp"
#include "IpIpoptData.hpp"
#include "IpTNLPAdapter.hpp"
#include "IpOrigIpoptNLP.hpp"
#include "IpIpoptApplication.hpp"

// My includes
#include "EstimationAction.h"
#include "SparseTensor.h"

// STL includes
#include <limits>
#include <set>

// Typedef for convenience
typedef Ipopt::Number Number;

///////////////////////////////////////////////////////////////////////
/// Interface class between our code and IPOPT
class IpOptInterface : public Ipopt::TNLP {
public:

  // Constructor
  IpOptInterface(EstimationAction &action);

  // Get methods
  const RealVec& x_final() const
    { return _x_final; }

  // Set methods
  void set_variable_bounds(Number *g_lower, Number *g_upper);
  void set_constraint_bounds(Number *g_lower, Number *g_upper);

  // Method to return some info about the nlp
  virtual bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                            Index& nnz_h_lag, IndexStyleEnum& index_style);

  // Method to return the bounds for my problem
  virtual bool get_bounds_info(Index n, Number* x_l, Number* x_u,
                               Index m, Number* g_l, Number* g_u);

  // Method to return the starting point for the algorithm
  virtual bool get_starting_point(Index n, bool init_x, Number* x,
                                  bool init_z, Number* z_L, Number* z_U,
                                  Index m, bool init_lambda,
                                  Number* lambda);

  // Method to return the objective value
  virtual bool eval_f(Index n, const Number* x, bool new_x, Number& obj_value);

  // Method to return the gradient of the objective
  virtual bool eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f);

  // Method to return the constraint residuals
  virtual bool eval_g(Index n, const Number* x, bool new_x, Index m, Number* g);

  // Method to return:
  //   1) The structure of the jacobian (if "values" is NULL)
  //   2) The values of the jacobian    (if "values" is not NULL)
  virtual bool eval_jac_g(Index n, const Number* x, bool new_x,
                          Index m, Index nele_jac, Index* iRow, Index *jCol,
                          Number* values);

  // Method to return:
  //   1) The structure of the hessian of the lagrangian (if "values" is NULL)
  //   2) The values of the hessian of the lagrangian    (if "values" is not NULL)
  virtual bool eval_h(Index n, const Number* x, bool new_x,
                      Number obj_factor, Index m, const Number* lambda,
                      bool new_lambda, Index nele_hess, Index* iRow,
                      Index* jCol, Number* values);

  // This method is called when the algorithm is complete so the
  // TNLP can store/write the solution
  virtual void finalize_solution(Ipopt::SolverReturn status,
                                 Index n, const Number* x,
                                 const Number* z_L, const Number* z_U,
                                 Index m, const Number* g, const Number* lambda,
                                 Number obj_value,
                                 const Ipopt::IpoptData* ip_data,
                                 Ipopt::IpoptCalculatedQuantities* ip_cq);

  // Method called once per iteration during convergence check
  virtual bool intermediate_callback(Ipopt::AlgorithmMode mode,
                                     Index iter, Number obj_value,
                                     Number inf_pr, Number inf_du,
                                     Number mu, Number d_norm,
                                     Number regularization_size,
                                     Number alpha_du, Number alpha_pr,
                                     Index ls_trials,
                                     const Ipopt::IpoptData* ip_data,
                                     Ipopt::IpoptCalculatedQuantities* ip_cq);

private:
  EstimationAction &_action;

  // Bounds on x and g
  RealVec _x_lower, _x_upper,
          _g_lower, _g_upper;

  // Sparsity structure of dg and d2L
  IndexVec _dg_sparse_idxs[2],
           _d2g_sparse_idxs[3],
           _d2L_sparse_idxs[2];

  // Pointers to d2L where values of d2f and d2g should be added
  IndexVec _d2L_idxs_d2f,
           _d2L_idxs_d2g;

  // Temp. data for storing d2f and d2g while computing d2L
  RealVec _tmp_d2fg;

  // TODO Implement symmetric Hessian
  // Mask for supplying only lower triangular derivatives
  typedef std::vector<bool> Mask;
  Mask _d2L_mask;
  RealVec _tmp_d2L;

  // Store final path
  RealVec _x_final;

  // copy constructor
  IpOptInterface(const IpOptInterface&);
};

////////////////////////////////////////////////////////////////
/// Set methods
void IpOptInterface::set_variable_bounds(Number *x_lower, Number *x_upper) {
  const Index num_vars = _action.num_variables();
  std::copy(x_lower,x_lower+num_vars,_x_lower.begin());
  std::copy(x_upper,x_upper+num_vars,_x_upper.begin());
}
///
void IpOptInterface::set_constraint_bounds(Number *g_lower, Number *g_upper) {
  const Index num_cons = _action.num_constraints();
  std::copy(g_lower,g_lower+num_cons,_g_lower.begin());
  std::copy(g_upper,g_upper+num_cons,_g_upper.begin());
}

////////////////////////////////////////////////////////////////
/// constructor
IpOptInterface::IpOptInterface(EstimationAction &action) :
  _action (action) {

  // Initialize bounds on state variables
  Index num_vars = _action.num_variables();
  _x_lower.resize(num_vars,0);
  _x_upper.resize(num_vars,0);
  _action.variable_bounds(&_x_lower.front(),&_x_upper.front());

  // Initialize bounds on constraints
  Index num_cons = _action.num_constraints();
  _g_lower.resize(num_cons,0);
  _g_upper.resize(num_cons,0);
  _action.constraint_bounds(&_g_lower.front(),&_g_upper.front());

  // Get sparsity structure of dg
  _action.dg_sparse_idxs(_dg_sparse_idxs);

  // Get sparsity structure of d2g
  _action.d2g_sparse_idxs(_d2g_sparse_idxs);

  // Get sparsity structure of d2f
  IndexVec d2f_sparse_idxs[2];
  _action.d2f_sparse_idxs(d2f_sparse_idxs);

  // ================================
  //  Build sparsity structure of d2L,
  //  the Hessian of the Lagrangian
  // ================================

  // Convert to set to remove duplicates
  typedef std::pair<Index,Index> IdxPair;
  typedef std::set<IdxPair> IdxSet;
  IdxSet d2L_idx_set;

  // Add indices for d2f
  Index num_d2f_sparse = d2f_sparse_idxs[0].size();
  for(Index idx_sparse = 0; idx_sparse < num_d2f_sparse; ++idx_sparse) {
    Index idx_i = d2f_sparse_idxs[0][idx_sparse],
          idx_j = d2f_sparse_idxs[1][idx_sparse];
    d2L_idx_set.insert(IdxPair(idx_i,idx_j));
  }

  // Add indices for d2g
  Index num_d2g_sparse = _d2g_sparse_idxs[0].size();
  for(Index idx_sparse = 0; idx_sparse < num_d2g_sparse; ++idx_sparse) {
    Index idx_i = _d2g_sparse_idxs[1][idx_sparse],
          idx_j = _d2g_sparse_idxs[2][idx_sparse];
    d2L_idx_set.insert(IdxPair(idx_i,idx_j));
  }

  // Read off sparse indices for d2L
  Index num_d2L_sparse = d2L_idx_set.size();
  _d2L_sparse_idxs[0].resize(num_d2L_sparse);
  _d2L_sparse_idxs[1].resize(num_d2L_sparse);
  typedef IdxSet::const_iterator SetIter;
  Index idx_sparse = 0;
  for(SetIter it = d2L_idx_set.begin();
        it != d2L_idx_set.end(); ++it) {
    _d2L_sparse_idxs[0][idx_sparse] = it->first;
    _d2L_sparse_idxs[1][idx_sparse] = it->second;
    //std::cout << it->first  << "  "
    //          << it->second << "  "
    //          << idx_sparse << std::endl;
    ++idx_sparse;
  }

  // =========================================================
  //  Read off pointers to d2L where values of d2f and d2g
  //  should be added. This lets us to quickly recompute d2L,
  //  without having to redo index calculations at each call
  // =========================================================

  // Sort d2f so we can get the indices of d2L in one pass
  SparseIndexSorter sorter;
  IndexVec d2f_order;
  sorter.sort(d2f_sparse_idxs,2,d2f_order);

  // Add indices for d2f
  Index idx_d2L = 0; idx_sparse = 0;
  _d2L_idxs_d2f.resize(num_d2f_sparse);
  for(SetIter it = d2L_idx_set.begin();
        it != d2L_idx_set.end(); ++it) {
    Index idx_sort = d2f_order[idx_sparse],
          idx_i = d2f_sparse_idxs[0][idx_sort],
          idx_j = d2f_sparse_idxs[1][idx_sort];
    if(idx_i == it->first && idx_j == it->second) {
      _d2L_idxs_d2f[d2f_order[idx_sparse]] = idx_d2L;
      ++idx_sparse;
      if(idx_sparse == num_d2f_sparse) {
        break;
      }
    }
    ++idx_d2L;
  }

  //std::cout << "==============" << std::endl;
  //for(Index i = 0; i < num_d2f_sparse; ++i) {
  //  std::cout << d2f_sparse_idxs[0][i] << "  "
  //            << d2f_sparse_idxs[1][i] << "  "
  //            << _d2L_idxs_d2f[i]  << std::endl;
  //}

  // Sort d2g
  IndexVec d2g_order;
  sorter.sort(_d2g_sparse_idxs,3,d2g_order);

  // Extract sorted indices for d2g
  // so we can construct row pointers
  IndexVec d2g_sorted[3];
  for(Index i = 0; i < 3; ++i) {
    d2g_sorted[i].resize(num_d2g_sparse);
    for(Index j = 0; j < num_d2g_sparse; ++j) {
      d2g_sorted[i][j] = _d2g_sparse_idxs[i][d2g_order[j]];
    }
  }

  // Get row pointers to each constraint
  IndexVec d2g_ptrs;
  Index  nRows[3] = {num_cons,num_vars,num_vars};
  inner_row_ptrs(d2g_sorted,2,nRows,d2g_ptrs);

  // Add indices for d2g
  _d2L_idxs_d2g.resize(num_d2g_sparse);
  for(Index iPtr = 0; iPtr < num_cons; ++iPtr) {
    Index lower = d2g_ptrs[iPtr],
          upper = d2g_ptrs[iPtr+1];
    if(lower == upper)
      continue;
    idx_d2L = 0; idx_sparse = lower;
    for(SetIter it = d2L_idx_set.begin();
          it != d2L_idx_set.end(); ++it) {
      Index idx_i = d2g_sorted[1][idx_sparse],
           idx_j = d2g_sorted[2][idx_sparse];
      if(idx_i == it->first && idx_j == it->second) {
        _d2L_idxs_d2g[d2g_order[idx_sparse]] = idx_d2L;
        ++idx_sparse;
        if(idx_sparse == upper) {
          break;
        }
      }
      ++idx_d2L;
    }
  }

  //std::cout << "==============" << std::endl;
  //for(Index i = 0; i < num_d2g_sparse; ++i) {
  //  std::cout << _d2g_sparse_idxs[0][i] << "  "
  //            << _d2g_sparse_idxs[1][i] << "  "
  //            << _d2g_sparse_idxs[2][i] << "  "
  //            << _d2L_idxs_d2g[i]  << std::endl;
  //}

  // Initialize tmp. data to hold d2f/d2g calculation
  _tmp_d2fg.resize(std::max(num_d2f_sparse,num_d2g_sparse));

  // TODO Implement symmetric Hessian
  // Mask upper triangular indices from d2L
  _d2L_mask.resize(num_d2L_sparse);
  typedef IndexVec::iterator Iter;
  Iter it = _d2L_sparse_idxs[0].begin(),
       jt = _d2L_sparse_idxs[1].begin();
  for(Index idx_sparse = 0; idx_sparse < num_d2L_sparse; ++idx_sparse) {
    if(*it <= *jt) {
      _d2L_mask[idx_sparse] = true;
      ++it; ++jt;
    }
    else {
      _d2L_mask[idx_sparse] = false;
      it = _d2L_sparse_idxs[0].erase(it);
      jt = _d2L_sparse_idxs[1].erase(jt);
    }
  }
  _tmp_d2L.resize(num_d2L_sparse);
}

////////////////////////////////////////////////////////////////
/// Give IPOPT the information about the size of the problem
/// (and hence, the size of the arrays that it needs to allocate).
///
/// Parameters:
///   1) n (out) - the number of variables in the problem (dimension of x).
///   2) m (out) - the number of constraints in the problem (dimension of g(x)).
///   3) nnz_jac_g (out) - the number of nonzero entries in the Jacobian.
///   4) nnz_h_lag (out) - the number of nonzero entries in the Hessian.
///   5) index_style (out) - the numbering style used for row/col entries
///                          in the sparse matrix format
///                          (C_STYLE: 0-based, FORTRAN_STYLE: 1-based)
///
bool IpOptInterface::get_nlp_info(Index& n, Index& m,
                                  Index& nnz_jac_g,
                                  Index& nnz_h_lag,
                                  IndexStyleEnum& index_style) {
  n = _action.num_variables();
  m = _action.num_constraints();
  index_style = C_STYLE;

  // Determine number of non-0s in dg
  nnz_jac_g = _dg_sparse_idxs[0].size();

  // Determine number of non-0s in Hessian of the Lagrangian
  nnz_h_lag = _d2L_sparse_idxs[0].size();

  return true;
}

////////////////////////////////////////////////////////////////
/// Give IPOPT the value of the bounds on the variables and constraints.
///
/// Parameters:
///   1) n    (in) - the number of variables in the problem (dimension of x)
///   2) x_l (out) - the lower bounds x^L for x
///   3) x_u (out) - the upper bounds x^U for x
///   4) m    (in) - the number of constraints in the problem (dimension of g(x))
///   5) g_l (out) - the lower bounds g^L for g(x)
///   6) g_u (out) - the upper bounds g^U for g(x)
///
bool IpOptInterface::get_bounds_info(Index n, Number* x_l, Number* x_u,
                                     Index m, Number* g_l, Number* g_u) {
  std::copy(_x_lower.begin(),_x_lower.end(),x_l);
  std::copy(_x_upper.begin(),_x_upper.end(),x_u);
  std::copy(_g_lower.begin(),_g_lower.end(),g_l);
  std::copy(_g_upper.begin(),_g_upper.end(),g_u);

  return true;
}

////////////////////////////////////////////////////////////////
/// Give IPOPT the starting point before it begins iterating.
///
/// Parameters
///   1) n (in) - the number of variables in the problem (dimension of x)
///   2) init_x (in) - if true, this method must provide an initial value for x
///   3) x (out) - the initial values for the primal variables, x
///   4) init_z (in) - if true, this method must provide an initial value
///                    for the bound multipliers z^L and z^U
///   5) z_L (out) - the initial values for the bound multipliers, z^L
///   6) z_U (out) - the initial values for the bound multipliers, z^U
///   7) m (in) - the number of constraints in the problem (dimension of g(x))
///   8) init_lambda (in) - if true, this method must provide an initial
///                         value for the constraint multipliers, \lambda
///   9) lambda (out) - the initial values for the constraint multipliers, \lambda
///
bool IpOptInterface::get_starting_point(Index n, bool init_x, Number* x,
                                        bool init_z, Number* z_L, Number* z_U,
                                        Index m, bool init_lambda,
                                        Number* lambda) {
  // Here, we assume we only have starting values for x, if you code
  // your own NLP, you can provide starting values for the dual variables
  // if you wish to use a warmstart option
  if(init_x == false || init_z == true || init_lambda == true) {
    throw_exception("Error: unexpected option in IpOptInterface::get_starting_point");
  }

  // Get initial path
  // TODO Figure out what to do about actions that require additional variables
  if(_x_final.empty()) {
    _action.initial_path(x);
  }
  else {
    for(Index i = 0; i < n; ++i) {
      x[i] = _x_final[i];
    }
  }

  return true;
}

////////////////////////////////////////////////////////////////
/// Return the value of the objective function at the point x
///
/// Parameters:
///   1) n (in) - the number of variables in the problem
///   2) x (in) - the values for the primal variables x, at which
///               f(x) is to be evaluated.
///   3) new_x (in) - false if any evaluation method was previously
///                   called with the same values in x, true otherwise.
///   4) obj_value (out) - the value of the objective function (f(x)).
///
bool IpOptInterface::eval_f(Index n, const Number* x,
                             bool new_x, Number& obj_value) {
  _action.f_eval(x,obj_value,new_x);
  return true;
}

////////////////////////////////////////////////////////////////
/// Return the gradient of the objective function at the point x
///
/// Parameters:
///   1) n (in) - the number of variables in the problem
///   2) x (in) - the values for the primal variables x, at which
///               df(x) is to be evaluated.
///   3) new_x (in) - false if any evaluation method was previously
///                   called with the same values in x, true otherwise
///   4) grad_f (out) - the array of values for the gradient of the
///                     objective function df(x).
///
bool IpOptInterface::eval_grad_f(Index n, const Number* x,
                                 bool new_x, Number* grad_f) {
  _action.df_eval(x,grad_f,new_x);
  return true;
}

////////////////////////////////////////////////////////////////
/// Return the value of the constraint function at the point $ x$ .
///
/// Parameters:
///   1) n (in) - the number of variables in the problem
///   2) x (in) - the values for the primal variables x, at which
///               the constraint functions g(x) are to be evaluated
///   3) new_x (in) - false if any evaluation method was previously
///                   called with the same values in x, true otherwise
///   4) m (in)  - the number of constraints in the problem
///   5) g (out) - the array of constraint function values, g(x)
///
bool IpOptInterface::eval_g(Index n, const Number* x,
                            bool new_x, Index m, Number* g) {
  _action.g_eval(x,g,new_x);
  return true;
}

////////////////////////////////////////////////////////////////
/// Return either the sparsity structure of the Jacobian of
/// the constraints, or the values for the Jacobian of the
/// constraints at the point x. If iRow and jRow are not NULL return
/// sparsity structure (values will be NULL). Otherwise, return
/// values of the Jacobian.
///
/// Parameters:
///   1) n (in) - the number of variables in the problem
///   2) x (in) - the values for the primal variables x, at which
///               the constraint Jacobian, dg(x)^T, is to be evaluated.
///   3) new_x (in) - false if any evaluation method was previously
///                   called with the same values in x, true otherwise.
///   4) m (in) - the number of constraints in the problem
///   5) n_ele_jac (in) - the number of nonzero elements in the Jacobian
///                       (dimension of iRow, jCol, and values).
///   6) iRow (out) - the row indices of entries in the Jacobian
///                   of the constraints.
///   7) jCol (out) - the column indices of entries in the Jacobian
///                   of the constraints.
///   8) values (out) - the values of the entries in the Jacobian
///                     of the constraints.
///
bool IpOptInterface::eval_jac_g(Index n, const Number* x, bool new_x,
                                Index m, Index nele_jac,
                                Index* iRow, Index *jCol,
                                Number* values) {
  if(iRow && jCol) {
    std::copy(_dg_sparse_idxs[0].begin(),_dg_sparse_idxs[0].end(),iRow);
    std::copy(_dg_sparse_idxs[1].begin(),_dg_sparse_idxs[1].end(),jCol);
    return true;
  }
  _action.dg_sparse_eval(x,values,new_x);
  return true;
}

////////////////////////////////////////////////////////////////
/// Return either the sparsity structure of the Hessian of the
/// Lagrangian, or the values of the Hessian of the Lagrangian
/// for the given values for x, \sigma_f, and \lambda.
///
///   d2L := sigma_f*d2f(x) + lambda_i*d2g_i(x)
///
/// Parameters:
///   1)  n (in) - the number of variables in the problem
///   2)  x (in) - the values for the primal variables x,
///                at which the Hessian is to be evaluated.
///   3)  new_x (in) - false if any evaluation method was previously
///                    called with the same values in x, true otherwise.
///   4)  sigma_f (in) - factor in front of the objective term
///                         in the Hessian
///   5)  m (in) - the number of constraints in the problem
///   6)  lambda (in) - the values for the constraint multipliers \lambda,
///                     at which the Hessian is to be evaluated.
///   7)  new_lambda (in) - false if any evaluation method was
///                         previously called with the same values
///                         in lambda, true otherwise.
///   8)  nele_hess (in) - the number of nonzero elements in the Hessian
///                        (dimension of iRow, jCol, and values).
///   9)  iRow (out) - the row indices of entries in the Hessian.
///   10) jCol (out) - the column indices of entries in the Hessian.
///   11) values (out) - the values of the entries in the Hessian.
///
bool IpOptInterface::eval_h(Index n, const Number* x, bool new_x,
                             Number sigma_f, Index m, const Number* lambda,
                             bool new_lambda, Index nele_hess, Index* iRow,
                             Index* jCol, Number* values) {
  if(iRow && jCol) {
    std::copy(_d2L_sparse_idxs[0].begin(),_d2L_sparse_idxs[0].end(),iRow);
    std::copy(_d2L_sparse_idxs[1].begin(),_d2L_sparse_idxs[1].end(),jCol);
    return true;
  }

  // CAREFUL!!! Reset temp. data as they store results
  // from previous evaluations
  std::fill(_tmp_d2L.begin(),_tmp_d2L.end(),0);

  // Compute d2f
  std::fill(_tmp_d2fg.begin(),_tmp_d2fg.end(),0.);
  _action.d2f_sparse_eval(x,&_tmp_d2fg.front(),new_x);

  // Add contribution from d2f
  Index num_d2f_sparse = _d2L_idxs_d2f.size();
  for(Index idx_sparse = 0; idx_sparse < num_d2f_sparse; ++idx_sparse) {
    Index d2L_idx = _d2L_idxs_d2f[idx_sparse];
    _tmp_d2L[d2L_idx] = sigma_f*_tmp_d2fg[idx_sparse];
  }

  // Compute d2g
  std::fill(_tmp_d2fg.begin(),_tmp_d2fg.end(),0.);
  _action.d2g_sparse_eval(x,&_tmp_d2fg.front(),new_x);

  // Add contribution from d2g
  Index num_d2g_sparse = _d2L_idxs_d2g.size();
  for(Index idx_sparse = 0; idx_sparse < num_d2g_sparse; ++idx_sparse) {
    Index d2L_idx = _d2L_idxs_d2g[idx_sparse],
          idx_g = _d2g_sparse_idxs[0][idx_sparse];
    _tmp_d2L[d2L_idx] += lambda[idx_g]*_tmp_d2fg[idx_sparse];
  }

  // TODO Implement symmetric Hessian
  Index num_d2L_sparse = _tmp_d2L.size();
  Index d2L_idx = 0;
  for(Index idx_sparse = 0; idx_sparse < num_d2L_sparse; ++idx_sparse) {
    if(_d2L_mask[idx_sparse]) {
      values[d2L_idx] = _tmp_d2L[idx_sparse];
      ++d2L_idx;
    }
  }

  return true;
}

////////////////////////////////////////////////////////////////
/// This method is called when the algorithm is complete so the
/// TNLP can store/write the solution
void IpOptInterface::finalize_solution(Ipopt::SolverReturn status,
                                       Index n, const Number* x,
                                       const Number* z_L,
                                       const Number* z_U,
                                       Index m, const Number* g,
                                       const Number* lambda,
                                       Number obj_value,
                                       const Ipopt::IpoptData* ip_data,
                                       Ipopt::IpoptCalculatedQuantities* ip_cq) {
  _action.save_path(x);
  // TODO Don't really like storing this separately
  //      but needed for annealing actions that use other
  //      variables besides the path
  _x_final.resize(n); std::copy(x,x+n,_x_final.begin());
}

////////////////////////////////////////////////////////////////
/// Method is called once per iteration during the convergence
/// check. Overloaded here to allow us to anneal alpha parameter
bool IpOptInterface::intermediate_callback(Ipopt::AlgorithmMode mode,
                                           Index iter, Number obj_value,
                                           Number inf_pr, Number inf_du,
                                           Number mu, Number d_norm,
                                           Number regularization_size,
                                           Number alpha_du, Number alpha_pr,
                                           Index ls_trials,
                                           const Ipopt::IpoptData* ip_data,
                                           Ipopt::IpoptCalculatedQuantities* ip_cq) {
  // Call base method
  Ipopt::TNLP::intermediate_callback(mode,iter,obj_value,inf_pr,inf_du,mu,d_norm,
                                     regularization_size,alpha_du,alpha_pr,ls_trials,
                                     ip_data,ip_cq);
  /*
  // Get state variables
  Ipopt::TNLPAdapter* tnlp_adapter = NULL;
  if(ip_cq != NULL) {
    Ipopt::OrigIpoptNLP* orignlp;
    orignlp = dynamic_cast<OrigIpoptNLP*>(GetRawPtr(ip_cq->GetIpoptNLP()));
    if(orignlp != NULL) {
      tnlp_adapter = dynamic_cast<TNLPAdapter*>(GetRawPtr(orignlp->nlp()));
      if(tnlp_adapter != NULL) {
        tnlp_adapter->ResortX(*ip_data->curr()->x(),&_xSol.front());
      }
    }
  }

  // Call back to action
  _action.callback(obj_value,inf_pr,inf_du);
  */

  return true;
}

#endif // IpOptInterface_H_
