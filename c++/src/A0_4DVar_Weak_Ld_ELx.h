#ifndef A0_4DVAR_WEAK_LD_ELX_H_
#define A0_4DVAR_WEAK_LD_ELX_H_

// My includes
#include "A0_4DVar_Weak_Ld.h"
#include "SparseTensor.h"

////////////////////////////////////////////////////////////////
/// Derived action for weak-constrained, collocated 4DVar action
/// written as a discrete Lagrangian
///
///  A0 ~ 1/T \sum_{n=0}^{N-1} dt*Ld(x(n),x(n+1),n,n+1)
///
///  where
///
///  Ld(x(n),x(n+1),n,n+1) =
///    1/2 |y(n)-h(x_n)|^2_Rm + 1/2|g(x(n),x(n+1))|^2_Rf
///
///  and g(x(n),x(n+1)) is a discretized version of the
///  model equations dx/dt - f(x)
///
/// Reimplemented to impose constraints based on the discrete
/// Euler-Lagrange equations represented in x space.
///
class A0_4DVar_Weak_Ld_ELx : public A0_4DVar_Weak_Ld {
public:

  // Constructors
  A0_4DVar_Weak_Ld_ELx();
  // Destructor
  virtual ~A0_4DVar_Weak_Ld_ELx();

  // Reimplemented from A0_4DVar_Weak
  virtual void initialize();

  // Class name
  virtual std::string classname() const;

  // Problem dimensions
  virtual Index num_constraints() const;

  // Bounds information
  virtual void constraint_bounds(Real *g_lower, Real *g_upper) const;

  // Compute dynamical constraints
  virtual void g_eval(const Real *X, Real *g, bool new_X);
  virtual void dg_sparse_eval(const Real *X, Real *dg, bool new_X);
  virtual void d2g_sparse_eval(const Real *X, Real *d2g, bool new_X);

  // Sparsity patterns
  virtual void d3f_sparse_idxs(IndexVec *sparse_idxs) const;
  virtual void dg_sparse_idxs(IndexVec *sparse_idxs)  const;
  virtual void d2g_sparse_idxs(IndexVec *sparse_idxs) const;

  // Set methods
  void set_only_constrain_endpoints(bool b)
    { _only_constrain_endpoints = b; }

protected:

  // Add discrete EL constraints
  bool _EL_constraints_x,
       _EL_constraints_t;
  // Constrain boundaries only
  bool _only_constrain_endpoints;

  // Additional derivative for constraint evaluation
  void d3L_sparse_eval(const Real &t_n, const Real *x_n,
                       const Real &t_np1, const Real *x_np1,
                       Real *d3L_sparse);
  void d3L_sparse_idxs(IndexVec *sparse_idxs) const;

  // Partials with respect to t
  void dLdt_eval(const Real &t_n, const Real *x_n,
                 const Real &t_np1, const Real *x_np1,
                 Real *dLdt);
  void d2Ldt_eval(const Real &t_n, const Real *x_n,
                  const Real &t_np1, const Real *x_np1,
                  Real *d2Ldt);
  void d3Ldt_eval(const Real &t_n, const Real *x_n,
                  const Real &t_np1, const Real *x_np1,
                  Real *d3Ldt);

  // Static function for converting d3L indices
  static Index d3L_idxs_to_array(Index nD, Index i, Index j, Index k);

  // Compute and cache objective function and derivatives
  virtual void eval_new_X(const Real *X, bool new_X);
  RealVec _d3f_sparse;

  // Temporary data from quadrature
  RealVec _d3g_quad_sparse;
  IndexVec _d3g_quad_sparse_idxs[4];

private:
  // Copy constructor
  A0_4DVar_Weak_Ld_ELx(const A0_4DVar_Weak_Ld_ELx&);
};

////////////////////////////////////////////////////////////////
/// Constructors
A0_4DVar_Weak_Ld_ELx::A0_4DVar_Weak_Ld_ELx() :
  A0_4DVar_Weak_Ld(),
  _EL_constraints_x (true),
  _EL_constraints_t (false),
  _only_constrain_endpoints  (false) {
}

/// Destructor
A0_4DVar_Weak_Ld_ELx::~A0_4DVar_Weak_Ld_ELx() {
}

////////////////////////////////////////////////////////////////
// Class name
std::string A0_4DVar_Weak_Ld_ELx::classname() const {
  if(!_only_constrain_endpoints) {
    return "A0_4DVar_Weak_Ld_ELx";
  }
  else {
    return "A0_4DVAr_WEak_Ld_wBCs";
  }
}

////////////////////////////////////////////////////////////////
/// Get number of constraints
Index A0_4DVar_Weak_Ld_ELx::num_constraints() const {
  const Index nNp1 = _data->time_domain().num_time_points(),
              nD = _data->dynamical_model().num_states();
  Index ret = 0;
  // Apply EL constraints to full path
  if(!_only_constrain_endpoints) {
    if(_EL_constraints_x) {
      ret += nD*nNp1;
    }
    if(_EL_constraints_t) {
      ret += nNp1;
    }
  }
  // Apply EL constraints to x(0) and x(N) only
  else {
    if(_EL_constraints_x) {
      ret += nD*2;
    }
    if(_EL_constraints_t) {
      ret += 2;
    }
  }
  return ret;
}

////////////////////////////////////////////////////////////////
/// Get bounds on states/constraints
void A0_4DVar_Weak_Ld_ELx::constraint_bounds(Real *g_lower, Real *g_upper) const {
  // Set constraint bounds to O(dt)
  Real g_bound =  std::pow(_data->time_domain().dt(),1);
  const Index nG = num_constraints();
  for(Index iG = 0; iG < nG; ++iG) {
    g_lower[iG] = -g_bound;
    g_upper[iG] = +g_bound;
  }
}

////////////////////////////////////////////////////////////////
void A0_4DVar_Weak_Ld_ELx::initialize() {
  A0_4DVar_Weak_Ld::initialize();

  // Initialize d3f cache
  IndexVec tmp_idxs[3];
  d3f_sparse_idxs(tmp_idxs);
  _d3f_sparse.resize(tmp_idxs[0].size());

  // Initialize constraint cache
  _quad->d3g_sparse_idxs(_d3g_quad_sparse_idxs);
  _d3g_quad_sparse.resize(_d3g_quad_sparse_idxs[0].size());
}

////////////////////////////////////////////////////////////////
/// Compute and cache objective function and constraints
void A0_4DVar_Weak_Ld_ELx::eval_new_X(const Real *X, bool new_X) {
  if(!new_X)
    return;

  // Dynamical model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();

  // Time domain
  const TimeDomain &time_domain = _data->time_domain();
  const RealVec &time_points = time_domain.time_points();
  const Index nN = time_domain.num_time_intervals();
  const Real T = time_points.back() - time_points.front();

  // Reset cached values
  _f = 0;
  std::fill(_df.begin(),_df.end(),0.);
  std::fill(_d2f_sparse.begin(),_d2f_sparse.end(),0.);
  std::fill(_d3f_sparse.begin(),_d3f_sparse.end(),0.);

  // Initialize Lagrangian calculation
  const Real *x_n = X, *x_np1 = x_n + nD;
  const Real *t_n = &time_points.front(), *t_np1 = t_n+1;
  _quad->init_dg_eval(*t_n,x_n);
  _quad->init_d2g_eval(*t_n,x_n);
  _quad->init_d3g_eval(*t_n,x_n);

  // Number of nonzeros per time step
  const Index offset_d2L = 3*nD*nD,
              offset_d3L = 7*nD*nD*nD;

  // Time loop
  for(Index iN = 0; iN < nN; ++iN) {
    // Cache intermediate values to avoid recomputation
    _quad->g_eval(*t_n,*t_np1,x_n,x_np1,&_g_quad.front());
    _quad->dg_sparse_eval(*t_n,*t_np1,x_n,x_np1,&_dg_quad_sparse.front());
    _quad->d2g_sparse_eval(*t_n,*t_np1,x_n,x_np1,&_d2g_quad_sparse.front());
    _quad->d3g_sparse_eval(*t_n,*t_np1,x_n,x_np1,&_d3g_quad_sparse.front());
    // Compute objective function
    L_eval(*t_n,x_n,*t_np1,x_np1,_f);
    // Compute Jacobian
    dL_eval(*t_n,x_n,*t_np1,x_np1,&_df[iN*nD]);
    // Compute Hessian
    d2L_sparse_eval(*t_n,x_n,*t_np1,x_np1,&_d2f_sparse[iN*offset_d2L]);
    // Compute constraints and their derivatives
    d3L_sparse_eval(*t_n,x_n,*t_np1,x_np1,&_d3f_sparse[iN*offset_d3L]);
    // Iterate
    ++t_n; ++t_np1; x_n += nD; x_np1 += nD;
  }

  // Apply overall scale factor
  Real sf = 1./T;
  _f *= 0.5*sf;
  for(RealVec::iterator iter = _df.begin();
        iter != _df.end(); ++iter) {
    (*iter) *= sf;
  }
  for(RealVec::iterator iter = _d2f_sparse.begin();
        iter != _d2f_sparse.end(); ++iter) {
    (*iter) *= sf;
  }
  for(RealVec::iterator iter = _d3f_sparse.begin();
        iter != _d3f_sparse.end(); ++iter) {
    (*iter) *= sf;
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak_Ld_ELx::g_eval(const Real *X, Real *g, bool new_X) {
  eval_new_X(X,new_X);
  // Apply EL constraints to full path
  if(!_only_constrain_endpoints) {
    if(_EL_constraints_x) {
      std::copy(_df.begin(),_df.end(),g);
    }
  }
  // Apply EL constraints only to x(0), x(N)
  else {
    if(_EL_constraints_x) {
      const Index nD = _data->dynamical_model().num_states(),
                  offset = nD;
      std::copy(_df.begin(),_df.begin()+offset,g);
      std::copy(_df.end()-offset,_df.end(),g+offset);
    }
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak_Ld_ELx::dg_sparse_eval(const Real *X, Real *dg, bool new_X) {
  eval_new_X(X,new_X);
  // Apply EL constraints to full path
  if(!_only_constrain_endpoints) {
    if(_EL_constraints_x) {
      std::copy(_d2f_sparse.begin(),_d2f_sparse.end(),dg);
    }
  }
  // Apply EL constraints only to x(0), x(N)
  else {
    const Index nD = _data->dynamical_model().num_states(),
                offset = 2*nD*nD;
    std::copy(_d2f_sparse.begin(),_d2f_sparse.begin()+offset,dg);
    std::copy(_d2f_sparse.end()-offset,_d2f_sparse.end(),dg+offset);
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak_Ld_ELx::d2g_sparse_eval(const Real *X, Real *d2g, bool new_X) {
  eval_new_X(X,new_X);
  // Apply EL constraints to full path
  if(!_only_constrain_endpoints) {
    if(_EL_constraints_x) {
      std::copy(_d3f_sparse.begin(),_d3f_sparse.end(),d2g);
    }
  }
  // Apply EL constraints only to x(0), x(N)
  else {
    const Index nD = _data->dynamical_model().num_states(),
                offset = 4*nD*nD*nD;
    std::copy(_d3f_sparse.begin(),_d3f_sparse.begin()+offset,d2g);
    std::copy(_d3f_sparse.end()-offset,_d3f_sparse.end(),d2g+offset);
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak_Ld_ELx::dg_sparse_idxs(IndexVec *sparse_idxs) const {
  // Apply EL constraints to full path
  if(!_only_constrain_endpoints) {
    if(_EL_constraints_x) {
      A0_4DVar_Weak_Ld::d2f_sparse_idxs(sparse_idxs);
    }
  }
  // Apply EL constraints only to x(0), x(N)
  else {
    if(_EL_constraints_x) {
      const Index nD = _data->dynamical_model().num_states(),
                  nN = _data->time_domain().num_time_intervals(),
                  offset = 2*nD*nD,
                  num_elems = 2*offset;
      sparse_idxs[0].resize(num_elems);
      sparse_idxs[1].resize(num_elems);
      IndexVec tmp_idxs[2];
      d2L_sparse_idxs(tmp_idxs);
      for(Index idx = 0; idx < offset; ++idx) {
        sparse_idxs[0][idx] = tmp_idxs[0][idx];
        sparse_idxs[1][idx] = tmp_idxs[1][idx];
      }
      for(Index idx = 0; idx < offset; ++idx) {
        sparse_idxs[0][idx + offset] = tmp_idxs[0][idx + offset];
        sparse_idxs[1][idx + offset] = tmp_idxs[1][idx + offset] + nD*(nN-1);
      }
    }
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak_Ld_ELx::d2g_sparse_idxs(IndexVec *sparse_idxs) const {
  // Apply EL constraints to full path
  if(!_only_constrain_endpoints) {
    if(_EL_constraints_x) {
      d3f_sparse_idxs(sparse_idxs);
    }
  }
  // Apply EL constraints only to x(0), x(N)
  else {
    const Index nD = _data->dynamical_model().num_states(),
                nN = _data->time_domain().num_time_intervals(),
                offset = 4*nD*nD*nD,
                num_elems = 2*offset;
    sparse_idxs[0].resize(num_elems);
    sparse_idxs[1].resize(num_elems);
    sparse_idxs[2].resize(num_elems);
    IndexVec tmp_idxs[3];
    d3L_sparse_idxs(tmp_idxs);
    for(Index idx = 0; idx < offset; ++idx) {
      sparse_idxs[0][idx] = tmp_idxs[0][idx];
      sparse_idxs[1][idx] = tmp_idxs[1][idx];
      sparse_idxs[2][idx] = tmp_idxs[2][idx];
    }
    for(Index idx = 0; idx < offset; ++idx) {
      sparse_idxs[0][idx + offset] = tmp_idxs[0][idx + offset];
      sparse_idxs[1][idx + offset] = tmp_idxs[1][idx + offset] + nD*(nN-1);
      sparse_idxs[2][idx + offset] = tmp_idxs[2][idx + offset] + nD*(nN-1);
    }
  }
}

////////////////////////////////////////////////////////////////
/// Compute sparse structure of d3f. For N = 3 time points, the
/// the block structure is organized as:
///          n = 0              n = 1              n = 2
///   || 01 | 02 |    || || 05 | 06 |    || ||    |    |    ||  n = 0
///   ||----|----|----|| ||----|----|----|| ||----|----|----||
///   || 03 | 04 |    || || 07 | 08 | 09 || ||    | 12 | 13 ||  n = 1
///   ||----|----|----|| ||----|----|----|| ||----|----|----||
///   ||    |    |    || ||    | 10 | 11 || ||    | 14 | 15 ||  n = 2
///
///  where block is nD*nD*nD. Indices are generated with a time loop
///  the blocks 1-7 for each d3L(n,n+1), since the 8th block at time
///  n is shared with the 1st block at time n+1. The final block is
///  then written out at the end.
///
/// TODO Sparsity structure of model, quadrature and symmetry is ignored
///
void A0_4DVar_Weak_Ld_ELx::d3f_sparse_idxs(IndexVec *sparse_idxs) const {
  // DynamicalModel
  const Index nD = _data->dynamical_model().num_states();
  // Time domain
  const Index nN = _data->time_domain().num_time_intervals();

  // d3L pattern
  IndexVec d3L_sparse_idxs[3];
  this->d3L_sparse_idxs(d3L_sparse_idxs);
  const Index nD3L = d3L_sparse_idxs[0].size();
  assert(nD3L == 8*nD*nD*nD);

  // Resize arrays
  const Index num_elems = nN*nD3L - nD*nD*nD*(nN - 1);
  sparse_idxs[0].resize(num_elems);
  sparse_idxs[1].resize(num_elems);
  sparse_idxs[2].resize(num_elems);

  // Compute sparsity pattern for d3f
  Index idx = 0, offset = 0;
  for(Index iN = 0; iN < nN; ++iN) {
    for(Index idx1 = 0; idx1 < 7*nD*nD*nD; ++idx1) {
      sparse_idxs[0][idx] = offset + d3L_sparse_idxs[0][idx1];
      sparse_idxs[1][idx] = offset + d3L_sparse_idxs[1][idx1];
      sparse_idxs[2][idx] = offset + d3L_sparse_idxs[2][idx1];
      ++idx;
    }
    offset += nD;
  }
  // Write out last block
  offset -= nD;
  for(Index idx1 = 7*nD*nD*nD; idx1 < 8*nD*nD*nD; ++idx1) {
    sparse_idxs[0][idx] = offset + d3L_sparse_idxs[0][idx1];
    sparse_idxs[1][idx] = offset + d3L_sparse_idxs[1][idx1];
    sparse_idxs[2][idx] = offset + d3L_sparse_idxs[2][idx1];
    ++idx;
  }
}

////////////////////////////////////////////////////////////////
/// Third derivative of discrete Lagrangian
///
/// TODO h(x) is assumed linear, so observations are ignored.
///
///   d3Ldx(t_n,x_n,t_n+1,x_n+1) = ...
///
Index A0_4DVar_Weak_Ld_ELx::d3L_idxs_to_array(Index nD, Index i,
                                              Index j, Index k) {
  Index ret = 0;
  if(i >= nD) {
    ret += 4*nD*nD*nD; i -= nD;
  }
  if(j >= nD) {
    ret += 2*nD*nD*nD; j -= nD;
  }
  if(k >= nD) {
    ret +=   nD*nD*nD; k -= nD;
  }
  ret += i*nD*nD + j*nD + k;
  //std::cout << i << "  " << j << "  " << k << "  " << ret << std::endl;
  return ret;
}

void A0_4DVar_Weak_Ld_ELx::d3L_sparse_eval(const Real &t_n, const Real *x_n,
                                           const Real &t_np1, const Real *x_np1,
                                           Real *d3L_sparse) {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();
  // TODO Add nonlinear observations

  // Rescale constraints by 1/dt
  const Real dt = t_np1 - t_n, sf = 1/dt;

  // Model error: d2g^T*Rf*dg term
  const Index num_dg_quad_non0s =  _dg_quad_sparse.size(),
              num_d2g_quad_non0s = _d2g_quad_sparse.size();
  for(Index idx0 = 0; idx0 < num_d2g_quad_non0s; ++idx0) {
    Index l = _d2g_quad_sparse_idxs[0][idx0];
    for(Index idx1 = 0; idx1 < num_dg_quad_non0s; ++idx1) {
      if(l != _dg_quad_sparse_idxs[0][idx1]) {
        continue;
      }
      Index i = _d2g_quad_sparse_idxs[1][idx0],
            j = _d2g_quad_sparse_idxs[2][idx0],
            k =  _dg_quad_sparse_idxs[1][idx1];
      Real tmp = sf*_d2g_quad_sparse[idx0]*_Rf_diag[l]*_dg_quad_sparse[idx1];
      //std::cout << "\n" << tmp << std::endl;
      d3L_sparse[d3L_idxs_to_array(nD,i,j,k)] += tmp;
      d3L_sparse[d3L_idxs_to_array(nD,i,k,j)] += tmp;
      d3L_sparse[d3L_idxs_to_array(nD,k,j,i)] += tmp;
    }
  }

  // Model error: d3g*Rf*g term
  const Index num_d3g_quad_non0s = _d3g_quad_sparse.size();
  for(Index idx = 0; idx < num_d3g_quad_non0s; ++idx) {
    Index i = _d3g_quad_sparse_idxs[1][idx],
          j = _d3g_quad_sparse_idxs[2][idx],
          k = _d3g_quad_sparse_idxs[3][idx],
          l = _d3g_quad_sparse_idxs[0][idx];
    d3L_sparse[d3L_idxs_to_array(nD,i,j,k)] +=
      sf*_d3g_quad_sparse[idx]*_Rf_diag[l]*_g_quad[l];
  }
}

////////////////////////////////////////////////////////////////
/// Sparsity structure of 3rd derivative of discrete Lagrangian
/// TODO Model sparsity and symmetry is ignored.
///
/// Block structure is organized as:
///        n          n+1
///   || 1 | 2 || || 5 | 6 ||   n
///   ||---|---|| ||---|---||
///   || 3 | 4 || || 7 | 8 ||  n+1
///
///  where block is nD*nD*nD
///
void A0_4DVar_Weak_Ld_ELx::d3L_sparse_idxs(IndexVec *sparse_idxs) const {
  // DynamicalModel
  const Index nD = _data->dynamical_model().num_states();

  // Resize arrays
  const Index num_elems = 8*nD*nD*nD;
  sparse_idxs[0].resize(num_elems);
  sparse_idxs[1].resize(num_elems);
  sparse_idxs[2].resize(num_elems);

  Index idx = 0;
  // (n,n,n) block
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
  for(Index kD = 0; kD < nD; ++kD) {
      sparse_idxs[0][idx] = iD;
      sparse_idxs[1][idx] = jD;
      sparse_idxs[2][idx] = kD;
      ++idx;
  }}}
  // (n,n,n+1) block
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
  for(Index kD = 0; kD < nD; ++kD) {
      sparse_idxs[0][idx] = iD;
      sparse_idxs[1][idx] = jD;
      sparse_idxs[2][idx] = kD+nD;
      ++idx;
  }}}
  // (n,n+1,n) block
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
  for(Index kD = 0; kD < nD; ++kD) {
      sparse_idxs[0][idx] = iD;
      sparse_idxs[1][idx] = jD+nD;
      sparse_idxs[2][idx] = kD;
      ++idx;
  }}}
  // (n,n+1,n+1) block
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
  for(Index kD = 0; kD < nD; ++kD) {
      sparse_idxs[0][idx] = iD;
      sparse_idxs[1][idx] = jD+nD;
      sparse_idxs[2][idx] = kD+nD;
      ++idx;
  }}}
  // (n+1,n,n) block
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
  for(Index kD = 0; kD < nD; ++kD) {
      sparse_idxs[0][idx] = iD+nD;
      sparse_idxs[1][idx] = jD;
      sparse_idxs[2][idx] = kD;
      ++idx;
  }}}
  // (n+1,n,n+1) block
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
  for(Index kD = 0; kD < nD; ++kD) {
      sparse_idxs[0][idx] = iD+nD;
      sparse_idxs[1][idx] = jD;
      sparse_idxs[2][idx] = kD+nD;
      ++idx;
  }}}
  // (n+1,n+1,n) block
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
  for(Index kD = 0; kD < nD; ++kD) {
      sparse_idxs[0][idx] = iD+nD;
      sparse_idxs[1][idx] = jD+nD;
      sparse_idxs[2][idx] = kD;
      ++idx;
  }}}
  // (n+1,n+1,n+1) block
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
  for(Index kD = 0; kD < nD; ++kD) {
      sparse_idxs[0][idx] = iD+nD;
      sparse_idxs[1][idx] = jD+nD;
      sparse_idxs[2][idx] = kD+nD;
      ++idx;
  }}}
}

////////////////////////////////////////////////////////////////
/// 1st partial time derivative of discrete Lagrangian
///
///   pd_t L(t_n,x_n,t_n+1,x_n+1) = ...
///
void A0_4DVar_Weak_Ld_ELx::dLdt_eval(const Real &t_n, const Real *x_n,
                                const Real &t_np1, const Real *x_np1,
                                Real *dLdt) {
  // TODO Figure out what to do about the ydots
}

////////////////////////////////////////////////////////////////
/// 2nd partial time derivative of discrete Lagrangian
///
///   pd^2_t L(t_n,x_n,t_n+1,x_n+1) = ...
///
void A0_4DVar_Weak_Ld_ELx::d2Ldt_eval(const Real &t_n, const Real *x_n,
                                 const Real &t_np1, const Real *x_np1,
                                 Real *d2Ldt) {
  // TODO Figure out what to do about the ydots
}

////////////////////////////////////////////////////////////////
/// 3rd partial time derivative of discrete Lagrangian
///
///   pd^3_t L(t_n,x_n,t_n+1,x_n+1) = ...
///
void A0_4DVar_Weak_Ld_ELx::d3Ldt_eval(const Real &t_n, const Real *x_n,
                                 const Real &t_np1, const Real *x_np1,
                                 Real *d3Ldt) {
  // TODO Figure out what to do about the ydots
}

#endif // A0_4DVAR_WEAK_LD_ELX_H_
