#ifndef LORENZ_96_H_
#define LORENZ_96_H_

// My includes
#include "Utils.h"
#include "DynamicalModel.h"
#include "ODEStepper.h"

////////////////////////////////////////////////////////////////
/// Lorenz 1996 model
///
///  dx_{i}/dt = x_{i-1} * (x_{i+1} - x_{i-2)) - x_i + f_i
///
/// TODO: Cyclic index iterators should provide more concise code.
///
class Lorenz96 : public DynamicalModel {
public:
  // Constructors
  Lorenz96();
  Lorenz96(Index  num_states);
  
  // Get methods
  virtual Index num_states() const;
  virtual Index num_params() const;
  virtual Index num_derivs() const;
  virtual void state_bounds(Real *x_lower, Real *x_upper) const;
  virtual void param_bounds(Real *p_lower, Real *p_upper) const;

  // Vector field evaluations
  virtual void f_eval(const Real &t, const Real *X, Real *dxdt) const;
  virtual void df_eval(const Real &t, const Real *X, Real *df) const;
  virtual void df_sparse_eval(const Real &t, const Real *X, Real *df) const;
  virtual void d2f_sparse_eval(const Real &t, const Real *X, Real *d2f) const;

  // Sparsity patterns
  virtual void df_sparse_idxs(IndexVec *df_sparse_idxs) const;
  virtual void d2f_sparse_idxs(IndexVec *d2f_sparse_idxs) const;

  // Random initial conditions
  virtual void random_state(Real *x) const;
  //virtual void random_param(Real *p) const;

private:
  // Initialize model
  void initialize();

  // State dimension
  Index _num_states, _num_params;
  // Parameter
  Real _p;
  // State/parameter bounds
  Real _x_bounds[2],
       _p_bounds[2];
};

////////////////////////////////////////////////////////////////
/// constructors
// TODO For now, hard-code parameter values
Lorenz96::Lorenz96() :
  _num_states (5), _num_params (0), _p (8.17) {
  initialize();
}
Lorenz96::Lorenz96(Index  num_states) :
  _num_states (num_states), _num_params (0), _p (8.17) {
  initialize();
  if(_num_states < 3) {
    throw_exception("Lorenz96::Lorenz96: Invalid dimension");
  }
  if(_num_states == 3) {
    throw_exception("Lorenz96::Lorenz96: D = 3 not supported");
  }
}

////////////////////////////////////////////////////////////////
/// Get methods
Index Lorenz96::num_states() const {
  return _num_states;
}
///
Index Lorenz96::num_params() const {
  return _num_params;
}
///
Index Lorenz96::num_derivs() const {
  return 2;
}
///
void Lorenz96::state_bounds(Real *x_lower, Real *x_upper) const {
  for(Index iD = 0; iD < _num_states; ++iD) {
    x_lower[iD] = _x_bounds[0];
    x_upper[iD] = _x_bounds[1];
  }
}
///
void Lorenz96::param_bounds(Real *p_lower, Real *p_upper) const {
  for(Index iP = 0; iP < _num_params; ++iP) {
    p_lower[iP] = _p_bounds[0];
    p_upper[iP] = _p_bounds[1];
  }
}

////////////////////////////////////////////////////////////////
void Lorenz96::initialize() {
  _x_bounds[0] = -20; _x_bounds[1] = 20;
  _p_bounds[0] = 5;   _p_bounds[1] = 10;
}

////////////////////////////////////////////////////////////////
/// generate random state on the attractor
void Lorenz96::random_state(Real *x) const {
  Index  nD = _num_states;
  Real delta = _x_bounds[1] - _x_bounds[0];
  for(Index iD = 0; iD < nD; ++iD) {
    x[iD] = delta*(rand_real()-0.5) + _x_bounds[0];
  }

  // Integrate to remove transients
  Index  nNp1 = 1001; Real dt = 0.01;
  RK4Step<Lorenz96> ode(*this,this->num_states());
  for(Index iN = 0; iN < nNp1; ++iN) {
    ode.step(0.,x,dt,x);
  }

}

////////////////////////////////////////////////////////////////
/// evaluate vector field at x(t)
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) dxdt (out) - output vector field
void Lorenz96::f_eval(const Real &t, const Real *x, Real *dxdt) const {
  Real p = _p;
  Index  nD = _num_states;
  dxdt[0] = x[nD-1]*(x[1]-x[nD-2]) - x[0] + p;
  dxdt[1] = x[0]*(x[2]-x[nD-1]) - x[1] + p;
  for(Index i = 2; i < nD-1; ++i) {
    dxdt[i] = x[i-1]*(x[i+1]-x[i-2]) - x[i] + p;
  }
  dxdt[nD-1] = x[nD-2]*(x[0] - x[nD-3]) - x[nD-1] + p;
};

////////////////////////////////////////////////////////////////
/// evaluate Jacobian of vector field df_i/dx_j
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) df (out) - output Jacobian nonzero elements
///                in row-major order
void Lorenz96::df_eval(const Real &t, const Real *x, Real *df) const {
  Index nD = _num_states;
  // 1st row
  df[0] = -1.;
  df[1] =  x[nD-1];
  for(Index i = 2; i < nD-2; ++i) {
    df[i] = 0.;
  }
  df[nD-2] = -x[nD-1];
  df[nD-1] = x[1]-x[nD-2];
  // 2nd row
  Index idx = nD;
  df[idx] = x[2]-x[nD-1]; ++idx;
  df[idx] = -1; ++idx;
  df[idx] = x[0]; ++idx;
  for(Index i = 3; i < nD-1; ++i) {
    df[idx] = 0.; ++idx;
  }
  df[idx] = -x[0]; ++idx;
  // Middle rows
  for(Index i = 2; i < nD-1; ++i) {
    for(Index  j = 0; j < i-2; ++j) {
      df[idx] = 0; ++idx;
    }
    df[idx] = -x[i-1]; ++idx;
    df[idx] = x[i+1]-x[i-2]; ++idx;
    df[idx] = -1; ++idx;
    df[idx] =  x[i-1]; ++idx;
    for(Index  j = i+2; j < nD; ++j) {
      df[idx] = 0; ++idx;
    }
  }
  // Last row
  df[idx] =  x[nD-2]; ++idx;
  for(Index i = 1; i < nD-3; ++i) {
    df[idx] = 0.; ++idx;
  }
  df[idx] = -x[nD-2]; ++idx;
  df[idx] = x[0]-x[nD-3]; ++idx;
  df[idx] = -1;
}

////////////////////////////////////////////////////////////////
/// evaluate sparse Jacobian of vector field df_i/dx_j
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) df (out) - output Jacobian nonzero elements
///                in row-major order
void Lorenz96::df_sparse_eval(const Real &t, const Real *x, Real *df) const {
  Index  nD = _num_states;
  // 1st row
  df[0] = -1.;
  df[1] =  x[nD-1];
  df[2] = -x[nD-1];
  df[3] =  x[1]-x[nD-2];
  // 2nd row
  df[4] =  x[2]-x[nD-1];
  df[5] = -1;
  df[6] =  x[0];
  df[7] = -x[0];
  // Middle rows
  Index idx_sparse = 8;
  for(Index i = 2; i < nD-1; ++i) {
    df[idx_sparse] = -x[i-1]; ++idx_sparse;
    df[idx_sparse] =  x[i+1]-x[i-2]; ++idx_sparse;
    df[idx_sparse] = -1; ++idx_sparse;
    df[idx_sparse] =  x[i-1]; ++idx_sparse;
  }
  // Last row
  df[idx_sparse] =  x[nD-2]; ++idx_sparse;
  df[idx_sparse] = -x[nD-2]; ++idx_sparse;
  df[idx_sparse] = x[0]-x[nD-3]; ++idx_sparse;
  df[idx_sparse] = -1;
}

////////////////////////////////////////////////////////////////
/// get sparsity structure of vector field Jacobian in matrix
/// form
/// Parameters:
///  1) idxs[2]  (out) - i,j indices of nonzero elements
void Lorenz96::df_sparse_idxs(IndexVec *idxs) const {
  Index  nD = _num_states;
  idxs[0].resize(4*nD);
  idxs[1].resize(4*nD);
  // 1st row
  idxs[0][0] = 0; idxs[1][0] = 0;
  idxs[0][1] = 0; idxs[1][1] = 1;
  idxs[0][2] = 0; idxs[1][2] = nD-2;
  idxs[0][3] = 0; idxs[1][3] = nD-1;
  // 2nd row
  idxs[0][4] = 1; idxs[1][4] = 0;
  idxs[0][5] = 1; idxs[1][5] = 1;
  idxs[0][6] = 1; idxs[1][6] = 2;
  idxs[0][7] = 1; idxs[1][7] = nD-1;
  // Middle rows
  Index idx_sparse = 8;
  for(Index irow = 2; irow < nD-1; ++irow) {
    idxs[0][idx_sparse] = irow; idxs[1][idx_sparse] = irow - 2; ++idx_sparse;
    idxs[0][idx_sparse] = irow; idxs[1][idx_sparse] = irow - 1; ++idx_sparse;
    idxs[0][idx_sparse] = irow; idxs[1][idx_sparse] = irow;     ++idx_sparse;
    idxs[0][idx_sparse] = irow; idxs[1][idx_sparse] = irow + 1; ++idx_sparse;
  }
  // Last row
  idxs[0][idx_sparse] = nD-1; idxs[1][idx_sparse] = 0;        ++idx_sparse;
  idxs[0][idx_sparse] = nD-1; idxs[1][idx_sparse] = nD - 3; ++idx_sparse;
  idxs[0][idx_sparse] = nD-1; idxs[1][idx_sparse] = nD - 2; ++idx_sparse;
  idxs[0][idx_sparse] = nD-1; idxs[1][idx_sparse] = nD - 1; ++idx_sparse;
}

////////////////////////////////////////////////////////////////
/// evaluate sparse Hessian of vector field d2f_i / dx_j dx_k
/// Parameters:
///  1) t (in) - time of evaluation
///  2) X (in) - point of evaluation
///  3) d2f (out) - output Hessian nonzero elements
///                 in row-major order
void Lorenz96::d2f_sparse_eval(const Real &t, const Real *x, Real *d2f) const {
  Index  nD = _num_states;
  // 1st row
  d2f[0] =  1.;
  d2f[1] = -1.;
  d2f[2] =  1.;
  d2f[3] = -1.;  
  // 2nd row
  d2f[4] =  1.;
  d2f[5] = -1.;
  d2f[6] =  1.;
  d2f[7] = -1.;
  // Middle rows
  Index idx_sparse = 8;
  for(Index i = 2; i < nD-1; ++i) {
    d2f[idx_sparse] = -1.; ++idx_sparse;
    d2f[idx_sparse] = -1.; ++idx_sparse;
    d2f[idx_sparse] =  1.; ++idx_sparse;
    d2f[idx_sparse] =  1.; ++idx_sparse;
  }
  // Last row
  d2f[idx_sparse] =  1.; ++idx_sparse;
  d2f[idx_sparse] = -1.; ++idx_sparse;
  d2f[idx_sparse] =  1.; ++idx_sparse;
  d2f[idx_sparse] = -1;
}

////////////////////////////////////////////////////////////////
/// get sparsity structure of vector field Hessian in 3 tensor
/// format
/// Parameters:
///  1) idxs[2]  (out) - i,j,k indices of nonzero elements
void Lorenz96::d2f_sparse_idxs(IndexVec *idxs) const {
  Index  nD = _num_states;
  idxs[0].resize(4*nD);
  idxs[1].resize(4*nD);
  idxs[2].resize(4*nD);
  // 1st row
  idxs[0][0] = 0; idxs[1][0] = 1;     idxs[2][0] = nD-1;
  idxs[0][1] = 0; idxs[1][1] = nD-2; idxs[2][1] = nD-1;
  idxs[0][2] = 0; idxs[1][2] = nD-1; idxs[2][2] = 1;
  idxs[0][3] = 0; idxs[1][3] = nD-1; idxs[2][3] = nD-2;
  // 2nd row
  idxs[0][4] = 1; idxs[1][4] = 0;     idxs[2][4] = 2;
  idxs[0][5] = 1; idxs[1][5] = 0;     idxs[2][5] = nD-1;
  idxs[0][6] = 1; idxs[1][6] = 2;     idxs[2][6] = 0;
  idxs[0][7] = 1; idxs[1][7] = nD-1; idxs[2][7] = 0;
  // Middle rows
  Index idx_sparse = 8;
  for(Index irow = 2; irow < nD-1; ++irow) {
    idxs[0][idx_sparse] = irow; idxs[1][idx_sparse] = irow-2; idxs[2][idx_sparse] = irow-1;
    ++idx_sparse;
    idxs[0][idx_sparse] = irow; idxs[1][idx_sparse] = irow-1; idxs[2][idx_sparse] = irow-2;
    ++idx_sparse;
    idxs[0][idx_sparse] = irow; idxs[1][idx_sparse] = irow-1; idxs[2][idx_sparse] = irow+1;
    ++idx_sparse;
    idxs[0][idx_sparse] = irow; idxs[1][idx_sparse] = irow+1; idxs[2][idx_sparse] = irow-1;
    ++idx_sparse;
  }
  // Last row
  idxs[0][idx_sparse] = nD-1; idxs[1][idx_sparse] = 0;     idxs[2][idx_sparse] = nD-2;
  ++idx_sparse;
  idxs[0][idx_sparse] = nD-1; idxs[1][idx_sparse] = nD-3; idxs[2][idx_sparse] = nD-2;
  ++idx_sparse;
  idxs[0][idx_sparse] = nD-1; idxs[1][idx_sparse] = nD-2; idxs[2][idx_sparse] = 0;
  ++idx_sparse;
  idxs[0][idx_sparse] = nD-1; idxs[1][idx_sparse] = nD-2; idxs[2][idx_sparse] = nD-3;
}


#endif // LORENZ_96_H_
