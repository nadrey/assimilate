#ifndef TWIN_EXP_DATA_H_
#define TWIN_EXP_DATA_H_

// My includes
#include "EstimationData.h"
#include "ODEStepper.h"

////////////////////////////////////////////////////////////////
/// 
class TwinExperimentData : public EstimationData {
public:

  // Struct to hold initialization data
  struct InitialData : public EstimationData::InitialData {
    // Initial states, parameter, inputs
    RealVec _true_states,
            _true_params,
            _true_inputs;
    IndexVec _true_param_idxs,
             _true_input_idxs;
    // Measurement noise
    RealVec _sigma_meas;
  };

  // Constructors
  TwinExperimentData(const DynamicalModel &dyn_model,
                     const TimeDomain &time_domain,
                     const ObservationModel &obs_model);
  // Destructor
  virtual ~TwinExperimentData();

  // Get methods
  const RealVec& true_states() const;
  const RealVec& true_params() const;

  // Write data to file
  void write_true_states_to_file(const char *filename) const;
  void write_true_params_to_file(const char *filename) const;

private:
  // Initialization methods
  virtual void initialize(EstimationData::InitialData &init);
  void init_true_states(InitialData &init);
  void init_true_params(InitialData &init);
  void init_true_inputs(InitialData &init);
  void init_observations(InitialData &init);

  // True states, parameters, inputs
  RealVec _true_states,
          _true_params,
          _true_inputs;
  IndexVec _true_param_idxs,
           _true_input_idxs;

  // Copy constructor
  TwinExperimentData(const TwinExperimentData&);
};

////////////////////////////////////////////////////////////////
/// constructors
TwinExperimentData::TwinExperimentData(const DynamicalModel &dyn_model,
                                       const TimeDomain &time_domain,
                                       const ObservationModel &obs_model) :
  EstimationData(dyn_model,time_domain,obs_model) {
}

////////////////////////////////////////////////////////////////
/// destructor
TwinExperimentData::~TwinExperimentData() {
}

////////////////////////////////////////////////////////////////
/// Get methods
///
const RealVec& TwinExperimentData::true_states() const {
  return _true_states;
}
///
const RealVec& TwinExperimentData::true_params() const {
  return _true_params;
}

////////////////////////////////////////////////////////////////
/// write estimated states/parameters to file
void TwinExperimentData::write_true_states_to_file(const char *filename) const {
  Index num_rows = _time_domain.num_time_points(),
        num_cols = _dynamical_model.num_states();
  const Real *data = &_true_states.front();
  write_column_data(filename,data,num_rows,num_cols);
}

////////////////////////////////////////////////////////////////
/// write estimated states/parameters to file
void TwinExperimentData::write_true_params_to_file(const char *filename) const {
  Index num_rows = _time_domain.num_time_points(),
        num_cols = _dynamical_model.num_params();
  const Real *data = &_true_params.front();
  write_column_data(filename,data,num_rows,num_cols);
}
////////////////////////////////////////////////////////////////
/// Initialize twin experiment observations
///
/// If true_states/params/inputs are not empty, they will be
/// used to initialized the path.
///
/// If true_states/params/inputs are empty, true data will
/// be randomly generated and copied to init
///
void TwinExperimentData::initialize(EstimationData::InitialData &init) {
  EstimationData::initialize(init);

  TwinExperimentData::InitialData &twin_data_init
    = static_cast<TwinExperimentData::InitialData&>(init);
  /*
  if(_dynamical_model.num_params() > 0) {
    check_parameter_idxs(twin_data_init._true_param_idxs,
                         twin_data_init._true_input_idxs);
    init_true_params(twin_data_init);
    init_true_inputs(twin_data_init);
  }
  */
  init_true_states(twin_data_init);
  init_observations(twin_data_init);
}

////////////////////////////////////////////////////////////////
/// Initialize true states
void TwinExperimentData::init_true_states(InitialData &init) {
  const Index nNp1 = _time_domain.num_time_points(),
                nN = _time_domain.num_time_intervals(),
                nD = _dynamical_model.num_states();
  const Real dt = _time_domain.dt();
  const RealVec &time_points = _time_domain.time_points();
  RealVec &initial_states = init._true_states;

  // Check if full twin data is provided
  _true_states.resize(nNp1*nD);
  if(initial_states.size() == _true_states.size()) {
    _true_states == initial_states;
  }
  // Full twin data not provided
  else {
    // Generate from random initial condition
    if(initial_states.empty()) {
      initial_states.resize(nNp1*nD);
      _dynamical_model.random_state(&initial_states.front());
    }
    // Only I=initial condition provided
    else if(initial_states.size() == static_cast<size_t>(nD)) {
      initial_states.resize(nNp1*nD);
    }
    else {
      throw_exception("TwinExperimentData::init_true_states: "
              "Invalid size of initial true states");
    }

    // Generate twin data
    RK4Step<DynamicalModel> ode(_dynamical_model,nD);
    for(Index iN = 0; iN < nN; ++iN) {
      const Real t = time_points[iN];
      const Real *x_n = &initial_states[iN*nD];
      Real *x_np1 = &initial_states[(iN+1)*nD];
      ode.step(t,x_n,dt,x_np1);
    }
    _true_states = initial_states;
  }
}

////////////////////////////////////////////////////////////////
/// Initialize true static parameters
void TwinExperimentData::init_true_params(InitialData &init) {
  const IndexVec &true_param_idxs = init._true_param_idxs;
  const Index num_true_params = true_param_idxs.size();
  const Index num_params = _dynamical_model.num_params();
  RealVec &initial_params = init._estimated_params;

  _true_params.resize(num_true_params);
  if(initial_params.empty()) {
    RealVec params(num_params,0);
    //_parameter_model.eval_p(time_points[0],&params.front());
    for(Index idx = 0; idx < num_true_params; ++idx) {
      _true_params[idx] = params[true_param_idxs[idx]];
    }
    initial_params = _true_params;
  }
  else if(initial_params.size() == _true_params.size()) {
    _true_params = initial_params;
  }
  else {
    throw_exception("EstimationData::init_true_params: "
            "Unexpected size of initial true parameters");
  }
}

////////////////////////////////////////////////////////////////
/// Initialize true dynamical inputs
void TwinExperimentData::init_true_inputs(InitialData &init) {
  const IndexVec &true_input_idxs = init._true_input_idxs;
  const Index num_true_inputs = true_input_idxs.size();
  const Index num_params = _dynamical_model.num_params();
  const Index nNp1 = _time_domain.num_time_points();
  RealVec &initial_inputs = init._true_inputs;

  // Initialize dynamical inputs
  _true_inputs.resize(num_true_inputs*nNp1);
  if(initial_inputs.empty()) {
    RealVec params(num_params,0);
    for(Index iN = 0; iN < nNp1; ++iN) {
      //_parameter_model.eval_p(time_points[iN],&params.front());
      for(Index idx = 0; idx < num_true_inputs; ++idx) {
        _true_inputs[idx] = params[true_input_idxs[idx]];
      }
    }
    initial_inputs = _true_inputs;
  }
  else if(initial_inputs.size() == _true_inputs.size()) {
    _true_inputs = initial_inputs;
  }
  else {
    throw_exception("EstimationData::init_true_inputs: "
            "Unexpected size of initial true inputs");
  }
}

////////////////////////////////////////////////////////////////
///
void TwinExperimentData::init_observations(InitialData &init) {
  const IndexVec meas_idxs = _observation_model.observed_state_idxs();
  const Index nL = _observation_model.num_observed_states();
  const Index nD = _dynamical_model.num_states();
  const Index nNp1 = _time_domain.num_time_points();

  // Resize parameters, states, data
  _observations.resize(nL*nNp1);
  // Construct observations from true states
  for(Index iN = 0; iN < nNp1; ++iN) {
    for(Index iL = 0; iL < nL; ++iL) {
      _observations[iN*nL + iL] = _true_states[iN*nD + meas_idxs[iL]];
    }
  }
  // Add measurement noise
  Real mu_meas = 0;
  if(!init._sigma_meas.empty()) {
    for(Index iN = 0; iN < nNp1; ++iN) {
      for(Index iL = 0; iL < nL; ++iL) {
        const Real sigma_meas = init._sigma_meas[iL];
        _observations[iN*nL + iL] += rand_normal(mu_meas,sigma_meas);
      }
    }
  }
}
# endif // TWIN_EXP_DATA_H_
