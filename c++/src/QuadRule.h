#ifndef QUAD_RULE_H_
#define QUAD_RULE_H_

// My includes
#include "DynamicalModel.h"

////////////////////////////////////////////////////////////////
/// base class quadrature rule used for evaluating dynamical
/// constraints
class QuadRule {
public:
  // constructor
  QuadRule(const DynamicalModel &model) {};
  // destructor
  virtual ~QuadRule() {};
  // class name
  virtual std::string classname() const = 0;

  // initialize calculation
  virtual void init_dg_eval(const Real &t_0, const Real *x_0) const {};
  virtual void init_d2g_eval(const Real &t_0, const Real *x_0) const {};
  virtual void init_d3g_eval(const Real &t_0, const Real *x_0) const {};

  // constraint evaluation
  virtual void g_eval(const Real &t_n, const Real &t_np1,
                      const Real *x_n, const Real *x_np1,
                      Real *g_n) const {};
  virtual void dg_sparse_eval(const Real &t_n, const Real &t_np1,
                              const Real *x_n, const Real *x_np1,
                              Real *dg) const {};
  virtual void d2g_sparse_eval(const Real &t_n, const Real &t_np1,
                               const Real *x_n, const Real *x_np1,
                               Real *d2g) const {};
  virtual void d3g_sparse_eval(const Real &t_n, const Real &t_np1,
                               const Real *x_n, const Real *x_np1,
                               Real *d3g) const {};

  // constraint sparsity pattern
  virtual void dg_sparse_idxs(IndexVec *sparse_idxs)  const {};
  virtual void d2g_sparse_idxs(IndexVec *sparse_idxs) const {};
  virtual void d3g_sparse_idxs(IndexVec *sparse_idxs) const {};

private:
  // Copy
  QuadRule(const QuadRule&);
};

#endif /* QUAD_RULE_H_ */
