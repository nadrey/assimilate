#ifndef TIME_DELAY_OBS_H_
#define TIME_DELAY_OBS_H_

// STL include
#include <list>

// My includes
#include "Utils.h"
#include "TrueExperimentData.h"
#include "TangentLinearModel.h"

////////////////////////////////////////////////////////////////
/// 
template <class Model>
class TimeDelayObs {
public:
  // Typedefs
  typedef TrueExperimentData<Model> Data;
  typedef TangentLinearModel<Model> TLM;
  typedef RK4Step<TLM> ODE;

  // Constructors
  TimeDelayObs(const Data &data);

  // Get methods
  uint nM() const; // embedding dimension
  uint nL() const; // total number of observations

  // Set methods
  void setIdxs(const IntVec &idxs);

  // Time delayed observations
  void y(uint n, Real *Y, char *mask) const;

  // Time delayed estimates
  void h(uint n, Real *H, Real *dHdx, Real *dHdp) const;

private: 
  // Filter data
  const Data &_data;
  // Time delay indices
  UIntVec _idxsF, // forward indices 
          _idxsB; // backward indices
  // Tangent linear model  
  TLM _tlm;
  // Vector for var. eqn.
  mutable RealVec _X;
  // ODE stepper
  mutable ODE _ode;
};

////////////////////////////////////////////////////////////////
/// constructors
template <class Model>
TimeDelayObs<Model>::TimeDelayObs(const Data &data) :
  _data (data), _idxsF(1,0), _idxsB(),
  _tlm (data.model()), _X(_tlm.dim(),0), _ode (_tlm) {
}

////////////////////////////////////////////////////////////////
/// get methods
template <class Model>
uint TimeDelayObs<Model>::nM() const {
  return _idxsF.size() + _idxsB.size();
}
///
template <class Model>
uint TimeDelayObs<Model>::nL() const {
  return this->nM()*_data->obsModel().nL();
}

////////////////////////////////////////////////////////////////
/// set methods
template <class Model>
void TimeDelayObs<Model>::setIdxs(const IntVec &idxs) {
  // Split indices into forward/backward
  uint m0 = 0;
  for(; m0 < idxs.size(); ++m0) {
    if(idxs[m0] >= 0) {
      break;
    }
  }
  //Backward
  _idxsB.resize(m0);
  for(uint mb = 0; mb < m0; ++mb) {
    _idxsB[mb] = -idxs[m0-mb-1];
  }
  // Forward
  _idxsF.resize(idxs.size()-m0);
  for(uint mf = m0; mf < idxs.size(); ++mf) {
    _idxsF[mf-m0] = idxs[mf];
  }

}

////////////////////////////////////////////////////////////////
/// get time delayed observations
template <class Model>
void TimeDelayObs<Model>::y(uint n, Real *Y, char *mask) const {
  const uint nN = _data->timeDomain().nN(),
             nL = _data->obsModel().nL();

  // Reset output to zero
  memset(Y,0.,this->nL()*sizeof(Real));
  memset(mask,true,this->nL()*sizeof(char));

  // Measured data
  const Real *yMeas = &_data->yMeas().front();

  // Backwards
  const uint m0 = _idxsB.size();
  for(uint m = 0; m < _idxsB.size(); ++m) {
    uint nn = n - _idxsB[m],
         idxM = m0 - m - 1;
    if(n < _idxsB[m]) {
      for(uint j = 0; j < nL; ++j) {
        mask[idxM*nL + j] = false;
      }
    }
    else {
      std::copy(yMeas+nL*nn,yMeas+nL*(nn+1),Y+idxM*nL);
    }
  }

  // Forwards
  for(uint m = 0; m < _idxsF.size(); ++m) {
    uint nn = n + _idxsF[m],
         idxM = m0 + m;
    if(nn >= nN) {
      for(uint j = 0; j < nL; ++j) {
        mask[idxM*nL + j] = false;
      }
    }
    else {
      std::copy(yMeas+nL*nn,yMeas+nL*(nn+1),Y+idxM*nL);
    }
  }
}

////////////////////////////////////////////////////////////////
/// get time delayed estimates
template <class Model>
void TimeDelayObs<Model>::h(uint n, Real *H,
                            Real *dHdx, Real *dHdp) const {
  // Model parameters
  const uint nD = _data->model().nD(),
             nP = _data->model().nP();

  // Reset output to zero
  memset(H,0.,this->nL()*sizeof(Real));
  memset(dHdx,0.,this->nL()*sizeof(Real)*nD);
  memset(dHdp,0.,this->nL()*sizeof(Real)*nP);

  // Time domain
  const TimeDomain &timeDomain = _data->timeDomain();
  const Real dt = timeDomain.dt();
  const uint nN = timeDomain.nN();

  // Observation model
  const typename Data::ObsModel &obsModel = _data->obsModel();
  const UIntVec &measIdxs = obsModel.idxs();

  // Current estimates
  const Real *xEst = &_data->xEst().front(),
             *pEst = &_data->pEst().front();

  // Set initial condition for var. equation
  std::fill(_X.begin(),_X.end(),0.);
  std::copy(xEst+n*nD,xEst+(n+1)*nD,_X.begin());
  for(uint i = 0; i < nD; ++i) {
    _X[(i+1)*nD + i] = 1.;
  }

  // Backwards
  Real *X = &_X.front();
  uint nn = n, m0 = _idxsB.size(), mLast = 0;
  for(uint m = 0; m < _idxsB.size(); ++m) {
    if(n < _idxsB[m]) {
      break;
    }
    for(uint i = 0; i < _idxsB[m]-mLast; ++i) {
      _ode.step(nn*dt,X,-dt,X);
      --nn;
    }
    mLast = _idxsB[m];
    uint idxM = m0 - m - 1;
    for(uint j = 0; j < measIdxs.size(); ++j) {
      // measurement
      uint idxX = measIdxs[j],
           idxH = idxM + j;
      H[idxH] = X[idxX];
      // first derivative
      idxX = nD*(1 + measIdxs[j]);
      idxH = nD*(idxM + j);
      std::copy(X+idxX,X+idxX+nD,dHdx+idxH); 
    }
  }

  // Set initial condition for var. equation
  std::fill(_X.begin(),_X.end(),0.);
  std::copy(xEst+n*nD,xEst+(n+1)*nD,_X.begin());
  for(uint i = 0; i < nD; ++i) {
    _X[(i+1)*nD + i] = 1.;
  }

  // Forwards
  nn = n; mLast = 0;
  for(uint m = 0; m < _idxsF.size(); ++m) {
    if(n + _idxsF[m] >= nN) {
      break;
    }
    for(uint i = 0; i < _idxsF[m]-mLast; ++i) {
      _ode.step(nn*dt,X,+dt,X);
      ++nn;
    }
    mLast = _idxsF[m];
    uint idxM = m0 + m;
    for(uint j = 0; j < measIdxs.size(); ++j) {
      // measurement
      uint idxX = measIdxs[j],
           idxH = idxM + j;
      H[idxH] = X[idxX];
      // first derivative
      idxX = nD*(1 + measIdxs[j]);
      idxH = nD*(idxM + j);
      std::copy(X+idxX,X+idxX+nD,dHdx+idxH); 
    }
  }
}

# endif // TIME_DELAY_OBS_H_
