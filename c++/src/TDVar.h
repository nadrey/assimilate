#ifndef TD_ACTION_H_
#define TD_ACTION_H_

// My includes
#include "Strong4DVar.h"

////////////////////////////////////////////////////////////////
///
template<class Model>
class TD_Action : public Strong4DVar<Model> {
public:

  // Constructor
  TD_Action();
  virtual ~TD_Action();
  virtual std::string name_str() const
    { return "StrongTDVar"; }
  
  // Initialize from MATLAB object
#ifdef MATLAB_MEX_FILE
  virtual void init_from_mx(const mxArray *obj);
#endif

  // Compute objective function
  virtual void f(const double *Xin, double &f);
  virtual void df(const double *Xin, double *df);
  using Strong4DVar<Model>::df_sparse;
  virtual void d2f_sparse(const double *Xin, double *d2f);
  virtual void d2f_sparse(UIntVec &sp_idxs_i, 
                      UIntVec &sp_idxs_j);

  // Compute dynamical constraints
  virtual void g(const double *Xin, double *g);
  virtual void dg_sparse(const double *Xin, double *dg);
  virtual void dg_sparse(UIntVec &sp_idxs_i, 
                     UIntVec &sp_idxs_j);
  using Strong4DVar<Model>::d2g_sparse;
  
  // Compute number of variables 
  virtual uint num_variables() const;
                      
  // Generate random initial condition
  virtual void random_X0(DoubleVec &x0) const;

  // Generate twin exp. data
  virtual void gen_twin_data();

  // Set model
  virtual void set_model(const Model *model);
  
  // Callback at each iteration
  void callback(double obj_value,
                double inf_pr,
                double inf_du);

  // Notification method called whenever a
  // new point x must be evaluated
  virtual void new_X()
    { _recompute = true; };

  // Set number of derivatives
  void set_num_derivs(uint num_derivs);
  
  // Make base member functions available
  using Strong4DVar<Model>::num_constraints;
  using Strong4DVar<Model>::dimX;
  using Strong4DVar<Model>::nT;
  
public: // TODO Should be protected 

  // Special class that implements ODE coupling
  class Coupling;
  friend class Coupling;
  Coupling *_coupling;

  // Special class that implements var. eqn.
  class VarEqn;
  friend class VarEqn;
  VarEqn *_varEqn;
  
  // Integrator for var. eqn.
  typedef RK4Step<double,VarEqn> My_ODE;
  My_ODE *_ode;

  // Make base data members available
  using Strong4DVar<Model>::_model;
  using Strong4DVar<Model>::_dimX;
  using Strong4DVar<Model>::_nT;
  using Strong4DVar<Model>::_dimY;
  using Strong4DVar<Model>::_nMeas;
  using Strong4DVar<Model>::_measIdxs;
  using Strong4DVar<Model>::_dt;
  using Strong4DVar<Model>::_Y;
  using Strong4DVar<Model>::_idxs_df_sparse;
  using Strong4DVar<Model>::_idxs_d2f_sparse;
  UIntVec _idxs_d3f_sparse[4];
  
  // Weight of delta_x^2 term
  double _alpha;
  bool _anneal_alpha;
  // Weight of delta_x term in constraints
  double _beta;

  // Time delay parameters
  uint _dimS, _nTau;

  // Cache obj. function and derivatives
  void recompute_f(const double *Xin);
  void accumulate_f(uint it,
                    const double *x,
                    const double *y,
                    const double *dx);
  double _f;
  DoubleVec _df, _d2f_sparse, _phi;
  bool _recompute;
  uint _num_derivs;
};

////////////////////////////////////////////////////////////////
/// constructor that extracts parameters/data from object fields
template<class Model>
TD_Action<Model>::TD_Action() :
  Strong4DVar<Model>(),
  _coupling (new Coupling(*this)),
  _varEqn   (NULL),
  _ode      (NULL) {
  // TODO Hard code these for now
  _dimS = 3;
  _nTau = 5;
  _alpha = 1.e0;
  _anneal_alpha = false;
  _beta = 1.e0;
  _f = 0.;
  _recompute = true;
  _num_derivs = 2;
}

////////////////////////////////////////////////////////////////
/// destructor
template<class Model>
TD_Action<Model>::~TD_Action() {
  delete _coupling;
  delete _varEqn;
  delete _ode;
}

////////////////////////////////////////////////////////////////
/// initialize form MATLAB object, reimplemented to set
/// time-delay parameters
#ifdef MATLAB_MEX_FILE
template<class Model>
void TD_Action<Model>::init_from_mx(const mxArray *obj) {
  Action<Model>::init_from_mx(obj);
  _dimS = data_from_mx_class<uint>(obj,"dim_K");
  _nTau = data_from_mx_class<uint>(obj,"nTau");
}
#endif

////////////////////////////////////////////////////////////////
/// return the number of optimization variables
/// reimplemented to include control variables
template<class Model>
uint TD_Action<Model>::num_variables() const { 
 return 2*_dimX*(_nT+1); 
}

////////////////////////////////////////////////////////////////
/// use model to generate a random initial path
/// reimplemented to include control variables
template<class Model>
void TD_Action<Model>::random_X0(DoubleVec &X0) const {
  X0.resize(num_variables(),0.);
  for(uint it = 0; it <= _nT; ++it) {
    _model->random_x0(&X0.front() + it*_dimX);
  }
  uint offset = _dimX*(_nT+1);
  for(uint it = 0; it <= _nT; ++it) {
    for(uint ix = 0; ix < _dimX; ++ix) {
      X0[offset + it*_dimX + ix] = rand_real()-0.5;
    }
  }
}

////////////////////////////////////////////////////////////////
/// use model to generate a twin experiment data
/// reimplemented to extend length of data window
template<class Model>
void TD_Action<Model>::gen_twin_data() {
  // This hack allows us to reuse base method
  uint nExtend = (_dimS-1)*_nTau;
  _nT += nExtend;
  Action<Model>::gen_twin_data();
  _nT -= nExtend;
}

////////////////////////////////////////////////////////////////
/// Reimplemented to modify sparse indices to set d3f indices
/// needed to compute the third derivative of the embedding
template<class Model>
void TD_Action<Model>::set_model(const Model *m) {
  Strong4DVar<Model>::set_model(m);

  // Get sparse structure of d3f
  _model->d3f_sparse(_idxs_d3f_sparse[0],_idxs_d3f_sparse[1],
                 _idxs_d3f_sparse[2],_idxs_d3f_sparse[3]);
            
  // Set number of derivatives to (re)initialize storage
  set_num_derivs(_num_derivs);
}

////////////////////////////////////////////////////////////////
/// Set number of derivatives of objective function to calculate
template<class Model>
void TD_Action<Model>::set_num_derivs(uint num_derivs) {
  
  // Set number of derivatives
  _num_derivs = num_derivs;
  
  // Create variational equation
  delete _varEqn;
  _varEqn = new VarEqn(*this);

  // Create ODE integrator for var. eqn.
  delete _ode;
  _ode = new My_ODE(*_varEqn);

  // Allocate data for obj. function calculation
  if(_num_derivs > 0) {
    uint nVars = num_variables();
    _df.resize(nVars);
    if(_num_derivs > 1) {
      _d2f_sparse.resize(nVars*_dimX*2);
    }
  }
  
  // Data for variational equation
  uint nPhi = _varEqn->dim();
  _phi.resize(nPhi);
}

////////////////////////////////////////////////////////////////
/// Callback at each iteration, reimplemented to anneal alpha
template<class Model>
void TD_Action<Model>::callback(double obj_value,
                                double inf_pr,
                                double inf_du) {
  if(!_anneal_alpha) {
    return;
  }
  double obj_tol = 1.e-2,
         alpha_max = 1.e6,
         alpha_step = 10;
  bool anneal = obj_value < obj_tol;
  if(anneal) {
    _alpha *= alpha_step;
    if(_alpha > alpha_max) {
      _alpha = alpha_max;
    }
    std::cout << "New alpha: " << _alpha << std::endl;
    _recompute = true;
  }
};

////////////////////////////////////////////////////////////////
///
template<class Model>
void TD_Action<Model>::f(const double *Xin, double &f) {
  if(_recompute) {
    recompute_f(Xin);
  }
  f = _f;
}

////////////////////////////////////////////////////////////////
/// compute gradient of objective function
/// reimplemented to include control variables
template<class Model>
void TD_Action<Model>::df(const double *Xin, double *df) {
  if(_recompute) {
    recompute_f(Xin);
  }
  std::copy(_df.begin(),_df.end(),df);
}

////////////////////////////////////////////////////////////////
///
template<class Model>
void TD_Action<Model>::d2f_sparse(const double *Xin, double *d2f) {
  if(_recompute) {
    recompute_f(Xin);
  }
  std::copy(_d2f_sparse.begin(),_d2f_sparse.end(),d2f);
}

////////////////////////////////////////////////////////////////
/// 
template<class Model>
void TD_Action<Model>::d2f_sparse(UIntVec &sp_idxs_i,
                              UIntVec &sp_idxs_j) {
  uint  dim = _dimX,
        dimXT = dim*(_nT+1),
        nElems = 4*dimXT*dim;
  sp_idxs_i.resize(nElems);
  sp_idxs_j.resize(nElems);
  // d2f/dx2
  uint idx_sparse = 0;
  for(uint it = 0; it < _nT+1; ++it) {
    for(uint ix = 0; ix < dim; ++ix) {
      for(uint jx = 0; jx < dim; ++jx) {
        sp_idxs_i[idx_sparse] = it*dim + ix;
        sp_idxs_j[idx_sparse] = it*dim + jx;
        ++idx_sparse;
      }
    }
  }
  // d2f/ddx2
  for(uint it = 0; it < _nT+1; ++it) {
    for(uint ix = 0; ix < dim; ++ix) {
      for(uint jx = 0; jx < dim; ++jx) {
        sp_idxs_i[idx_sparse] = it*dim + ix + dimXT;
        sp_idxs_j[idx_sparse] = it*dim + jx + dimXT;
        ++idx_sparse;
      }
    }
  }
  // d2f/dxddx
  for(uint it = 0; it < _nT+1; ++it) {
    for(uint ix = 0; ix < dim; ++ix) {
      for(uint jx = 0; jx < dim; ++jx) {
        sp_idxs_i[idx_sparse] = it*dim + ix;
        sp_idxs_j[idx_sparse] = it*dim + jx + dimXT;
        ++idx_sparse;
      }
    }
  }
  for(uint it = 0; it < _nT+1; ++it) {
    for(uint ix = 0; ix < dim; ++ix) {
      for(uint jx = 0; jx < dim; ++jx) {
        sp_idxs_i[idx_sparse] = it*dim + ix + dimXT;
        sp_idxs_j[idx_sparse] = it*dim + jx;
        ++idx_sparse;
      }
    }
  }
}

////////////////////////////////////////////////////////////////
/// recompute and cache f and derivatives
template<class Model>
void TD_Action<Model>::recompute_f(const double *Xin) {
  // Reset action and its derivatives to zero
  _recompute = false;
  _f = 0;
  std::fill(_df.begin(),_df.end(),0.);
  std::fill(_d2f_sparse.begin(),_d2f_sparse.end(),0.);

  // Initialize pointers
  uint dim = _dimX,
       nPhi = _phi.size();
  double *x = &_phi.front(),
         *phi = x + dim;
  const double *deltaX = Xin + (_nT+1)*dim,
               *y, *dx;

  // Compute objective function
  double tmp, t = 0;
  uint idx_meas;
  for(uint it = 0; it <= _nT; ++it) {

    // Set initial condition for temporary path
    std::fill(_phi.begin(),_phi.end(),0.);
    for (uint ix = 0; ix < dim; ++ix) {
      x[ix] = Xin[it*dim + ix];
      phi[ix*dim + ix] = 1.;
    }

    // Coupling at current time
    dx = deltaX + it*dim;

    // Contribution from forward embedding
    double t = it*_dt;
    uint t_idx = it;
    for(uint jt = 0; jt < _dimS-1; ++jt) {
      // Compute f, df, and d2f at t' = t_idx*dt
      y = _Y + t_idx*_nMeas;
      accumulate_f(it,x,y,dx);
      // Iterate the var. eqn. ODE to next embedding point
      for(uint kt = 0; kt < _nTau; ++kt) {
        _ode->step(t,x,_dt,x);
        t += _dt; ++t_idx;
      }
    }
    // Compute f for last point point
    y = _Y + t_idx*_nMeas;
    accumulate_f(it,x,y,dx);
  }

  // Apply overall scale factor
  double sf = 1./(_dimS*(_nT+1.));
  _f *= 0.5*sf;
  typedef DoubleVec::iterator Iter;
  for(Iter i = _df.begin(); i != _df.end(); ++i) {
    (*i) *= sf;
  }
  for(Iter i = _d2f_sparse.begin(); i != _d2f_sparse.end(); ++i) {
    (*i) *= sf;
  }
}

////////////////////////////////////////////////////////////////
/// function to support recompute_f by computing the cumulative
/// contribution the action and its derivatives at the current
/// time step along the embedding
template<class Model>
void TD_Action<Model>::accumulate_f(uint it,
                                    const double *x,
                                    const double *y,
                                    const double *dx) {
  uint dim = _dimX;
  const double *phi = x + dim,
               *phi2 = phi + dim*dim,
               *phi3 = phi2 + dim*dim*dim;

  //=====================
  //    Compute f
  //=====================
  DoubleVec dSdx_minus_dS(_nMeas,0.); // dSdx*deltaX - deltaS
  for(uint imeas = 0; imeas < _nMeas; ++imeas) {
    uint idx_meas = _measIdxs[imeas];
    dSdx_minus_dS[imeas] = 0;
    for(uint ix = 0; ix < dim; ++ix) {
      dSdx_minus_dS[imeas] += phi[idx_meas*dim + ix]*dx[ix];
    }
    dSdx_minus_dS[imeas] -= y[imeas] - x[idx_meas];
    _f += dSdx_minus_dS[imeas]*dSdx_minus_dS[imeas];
  }
  // Add control term
  for(uint ix = 0; ix < dim; ++ix) {
    _f += _alpha*dx[ix]*dx[ix];
  }
  if(_num_derivs < 1)
    return;

  //=====================
  //    Compute df/dx
  //=====================
  DoubleVec d2Sdx2_plus_dSdx(_nMeas*dim,0.); // d2Sdx2*deltaX + dS/dx
  for(uint imeas = 0; imeas < _nMeas; ++imeas) {
    uint idx_meas = _measIdxs[imeas];
    for(uint ix = 0; ix < dim; ++ix) {
      double tmp = 0;
      for(uint jx = 0; jx < dim; ++jx) {
        tmp += phi2[idx_meas*dim*dim + ix*dim + jx]*dx[jx];
      }
      tmp += phi[idx_meas*dim + ix];
      _df[it*dim + ix] += tmp*dSdx_minus_dS[imeas];
      d2Sdx2_plus_dSdx[imeas*dim + ix] = tmp;
    }
  }

  //=====================
  //    Compute df/ddx
  //=====================
  uint offset = dim*(_nT+1);
  for(uint imeas = 0; imeas < _nMeas; ++imeas) {
    uint idx_meas = _measIdxs[imeas];
    for(uint ix = 0; ix < dim; ++ix) {
      _df[offset + it*dim + ix] += phi[idx_meas*dim + ix]*dSdx_minus_dS[imeas];
    }
  }
  for(uint ix = 0; ix < dim; ++ix) {
    _df[offset + it*dim + ix] += _alpha*dx[ix];
  }
  if(_num_derivs < 2)
    return;

  //=====================
  //    Compute d2f/dx2
  //=====================
  for(uint imeas = 0; imeas < _nMeas; ++imeas) {
    uint idx_meas = _measIdxs[imeas];
    for(uint ix = 0; ix < dim; ++ix) {
      for(uint jx = 0; jx < dim; ++jx) {
        double tmp = 0;
        for(uint kx = 0; kx < dim; ++kx) {
          tmp += phi3[idx_meas*dim*dim*dim + ix*dim*dim + jx*dim + kx]*dx[kx];
        }
        tmp += phi2[idx_meas*dim*dim + ix*dim + jx];
        tmp *= dSdx_minus_dS[imeas];
        tmp += d2Sdx2_plus_dSdx[imeas*dim + ix]*d2Sdx2_plus_dSdx[imeas*dim + jx];
        _d2f_sparse[it*dim*dim + ix*dim + jx] += tmp;
      }
    }
  }

  //=====================
  //    Compute d2f/ddx2
  //=====================
  offset *= dim;
  for(uint imeas = 0; imeas < _nMeas; ++imeas) {
    uint idx_meas = _measIdxs[imeas];
    for(uint ix = 0; ix < dim; ++ix) {
      for(uint jx = 0; jx < dim; ++jx) {
        _d2f_sparse[offset + it*dim*dim + ix*dim + jx] +=
          phi[idx_meas*dim + ix]*phi[idx_meas*dim + jx];
      }
    }
  }
  for(uint ix = 0; ix < dim; ++ix) {
    _d2f_sparse[offset + it*dim*dim + ix*dim + ix] += _alpha;
  }

  //=====================
  //    Compute d2f/(dx ddx)
  //=====================
  uint tmp_offset = offset;
  offset *= 2;
  for(uint imeas = 0; imeas < _nMeas; ++imeas) {
    uint idx_meas = _measIdxs[imeas];
    for(uint ix = 0; ix < dim; ++ix) {
      for(uint jx = 0; jx < dim; ++jx) {
        double tmp = 0;
        tmp += phi2[idx_meas*dim*dim + ix*dim + jx]*dSdx_minus_dS[imeas];
        tmp += phi[idx_meas*dim + jx]*d2Sdx2_plus_dSdx[imeas*dim + ix];
        _d2f_sparse[offset + it*dim*dim + ix*dim + jx] += tmp;
        // TODO The following values are redundant
        //      due to symmetry of the Hessian
        _d2f_sparse[offset + tmp_offset + it*dim*dim + jx*dim + ix] += tmp;
      }
    }
  }
}

////////////////////////////////////////////////////////////////
/// compute nonlinear constraints
/// reimplemented to include control variables
template <class Model>
void TD_Action<Model>::g(const double *Xin, double *g) {
  
  // Create ODE integrator
  Coupling &coupling = *_coupling;
  EulerStep<double,Coupling> my_ode(coupling);
  
  // Allocate space for computation
  DoubleVec x_tmp(_dimX), dx(_dimX,0.);
  
  // Set coupling perturbation
  coupling._deltaX = &dx.front();
  
  // Calculate coupling perturbation
  // all non-measured components are zero
  const double *x = Xin,
               *u = Xin + (_nT+1)*_dimX;

  // Compute constraints
  double t = 0, half_dt = 0.5*_dt;
  uint it = 0;
  while(it < _nT) {
    // Constraint evaluated at midpoint between t_{i} and t_{i+1}
    double *x_mid = g + it*_dimX;
    // Apply coupling control
    for(uint ix = 0; ix < _dimX; ++ix) {
      dx[ix] = _beta*u[ix]; 
    }
    // Integrate forward a half step
    my_ode.step(t,x,half_dt,x_mid);
    // Iterate to t_{i+1}
    ++it; t += _dt; x += _dimX; u += _dimX; 
    // Apply coupling control (positive or negative)?
    for(uint ix = 0; ix < _dimX; ++ix) {
      dx[ix] = -_beta*u[ix]; 
    }
    // Integrate backward a half step
    my_ode.step(t,x,-half_dt,&x_tmp.front());
    // Compute difference at the midpoint
    for(uint ix = 0; ix < _dimX; ++ix) {
      x_mid[ix] -= x_tmp[ix]; 
    }
  }
}

////////////////////////////////////////////////////////////////
///
template <class Model>
void TD_Action<Model>::dg_sparse(const double *Xin, double *dg) {
  Strong4DVar<Model>::dg_sparse(Xin,dg);
  
  // Add control terms
  uint num_df_non0s = _idxs_df_sparse[0].size();
  uint nElems = 2*_nT*num_df_non0s;
  dg += nElems;
  double half_dt_beta = 0.5*_dt*_beta;
  for(uint it = 0; it < _nT; ++it) {
    for(uint ix = 0; ix < _dimX; ++ix) {
      (*dg) =  half_dt_beta; ++dg;
      (*dg) = -half_dt_beta; ++dg;
    }
  }
}

////////////////////////////////////////////////////////////////
/// 
template<class Model>
void TD_Action<Model>::dg_sparse(UIntVec &sp_idxs_i, 
                             UIntVec &sp_idxs_j) {
  Strong4DVar<Model>::dg_sparse(sp_idxs_i,sp_idxs_j);
  
  // Add control terms
  uint num_df_non0s = sp_idxs_i.size(),
       num_ctrl_terms = 2*_dimX*_nT,
       dimXT = _dimX*(_nT+1),
       idx_sparse = num_df_non0s;
  sp_idxs_i.resize(num_df_non0s + num_ctrl_terms);
  sp_idxs_j.resize(num_df_non0s + num_ctrl_terms);

  for(uint it = 0; it < _nT; ++it) {
    for(uint ix = 0; ix < _dimX; ++ix) {
      sp_idxs_i[idx_sparse] = it*_dimX + ix;
      sp_idxs_j[idx_sparse] = dimXT + it*_dimX + ix;
      //std::cout << sp_idxs_i[idx_sparse] 
        //<< "  " << sp_idxs_j[idx_sparse] << std::endl;
      ++idx_sparse;
      sp_idxs_i[idx_sparse] = it*_dimX + ix;
      sp_idxs_j[idx_sparse] = dimXT + (it+1)*_dimX + ix;
      //std::cout << sp_idxs_i[idx_sparse] 
        //<< "  " << sp_idxs_j[idx_sparse] << std::endl;
      ++idx_sparse;
    }
  }
}

////////////////////////////////////////////////////////////////
///
template <class Model>
class TD_Action<Model>::Coupling {
public:
  // Friends with TD_Action
  friend class TD_Action<Model>;

  // Constructor
  typedef TD_Action<Model> Parent;
  Coupling(const Parent &parent) :
    _parent (parent), 
    _deltaX (NULL) {
  };

  // Dimension of model
  uint dim() const
    { return _parent._dimX; }
  
  /// Compute model vector field with constant coupling
  void f(const double &t, const double *x, double *dxdt) const {
    // Compute vector field
    _parent._model->f(t,x,dxdt);
    // Add perturbation
    for(uint ix = 0; ix < dim(); ++ix) {
      dxdt[ix] += _deltaX[ix]; 
    }
  }
    
private:
  Coupling(const Coupling&) {};
  const Parent &_parent;
  const double *_deltaX;
};

////////////////////////////////////////////////////////////////
///
template <class Model>
class TD_Action<Model>::VarEqn {
public:

  typedef TD_Action<Model> Parent;
  VarEqn(const Parent &parent) :
    _parent (parent) {
    // Space for vector field derivatives
    _df_sparse.resize(_parent._idxs_df_sparse[0].size());
    _d2f_sparse.resize(_parent._idxs_d2f_sparse[0].size());
    _d3f_sparse.resize(_parent._idxs_d3f_sparse[0].size());
    // Get pointers to inner rows for fast multiplication
    uint dim = _parent._dimX,
         nRows[4] = {dim,dim,dim,dim};
    inner_row_ptrs(_parent._idxs_df_sparse ,2,nRows,_df_ptrs[0]);
    inner_row_ptrs(_parent._idxs_d2f_sparse,2,nRows,_d2f_ptrs[0]);
    inner_row_ptrs(_parent._idxs_d2f_sparse,3,nRows,_d2f_ptrs[1]);
    inner_row_ptrs(_parent._idxs_d3f_sparse,2,nRows,_d3f_ptrs[0]);
    inner_row_ptrs(_parent._idxs_d3f_sparse,3,nRows,_d3f_ptrs[1]);
    inner_row_ptrs(_parent._idxs_d3f_sparse,4,nRows,_d3f_ptrs[2]);
  }
  ~VarEqn() {
  }

  //
  uint dim() const {
    uint dim = _parent._dimX,
         num_derivs = _parent._num_derivs,
         ret = dim+1;
    for(uint i = 0; i < num_derivs; ++i) {
      ret = ret*dim + 1;
    }
    ret *= dim;
    return ret;
  }

  //
  void f(const double &t, const double *x, double *dxdt) const;
  
private:
  VarEqn(const VarEqn&) {};
  const Parent &_parent;

  mutable DoubleVec _df_sparse, _d2f_sparse, _d3f_sparse;
  UIntVec _df_ptrs[1], _d2f_ptrs[2], _d3f_ptrs[3];
}; 

////////////////////////////////////////////////////////////////
///
template <class Model>
void TD_Action<Model>::VarEqn::f(const double &t, 
                                 const double *x, 
                                 double *dxdt) const {
  // TODO To be safe, initialize to zero
  memset(dxdt,0.,this->dim()*sizeof(double));

  // Compute vector field
  const Model &model = *_parent._model;
  model.f(t,x,dxdt);
  // Compute Jacobian df
  model.df_sparse(t,x,&_df_sparse.front());

  // Variables for sparse matrix multiply
  const UIntVec *sp_idxs, *ptrs;
  uint dim = _parent._dimX;

  // Calculate dPhi_ij = df_im * Phi_mj
  const double *Phi = x + dim;
  double *dPhi = dxdt + dim;
  sp_idxs = _parent._idxs_df_sparse;
  ptrs = _df_ptrs;
  if(!sp_idxs[0].empty()) {
    for(uint idx_ptr = 0; idx_ptr < dim; ++idx_ptr) {
      uint i = sp_idxs[0][ptrs[0][idx_ptr]];
      for(uint j = 0; j < dim; ++j) {
        double sum = 0.;
        uint m_begin = ptrs[0][idx_ptr],
             m_end   = ptrs[0][idx_ptr+1];
        for(uint idx_m = m_begin; idx_m < m_end; ++idx_m) {
          uint m = sp_idxs[1][idx_m];
          sum += _df_sparse[idx_m]*Phi[m*dim + j];
          //std::cout << "df_" << i << m << " * "
          //          << "Phi_" << m << j << " + ";
        }
        //std::cout << std::endl;
        dPhi[i*dim + j] = sum;
      }
    }
  }
  if(_parent._num_derivs < 1)
    return;
    
    // Compute Hessian d2f
  model.d2f_sparse(t,x,&_d2f_sparse.front());
  
  // Calculate d2Phi_ijk = d2f_imn * Phi_mj * Phi_nk + ...
  double *d2Phi = dxdt + dim*(dim+1);
  sp_idxs = _parent._idxs_d2f_sparse;
  ptrs = _d2f_ptrs;
  if(!sp_idxs[0].empty()) {
    for(uint idx_ptr = 0; idx_ptr < dim; ++idx_ptr) {
      uint i = sp_idxs[0][ptrs[0][idx_ptr]];
      for(uint j = 0; j < dim; ++j) {
        for(uint k = 0; k < dim; ++k) {
          double sum = 0.;
          uint m_begin = ptrs[0][idx_ptr],
               m_end   = ptrs[0][idx_ptr+1];
          for(uint idx_m = m_begin; idx_m < m_end; ++idx_m) {
            uint m = sp_idxs[1][idx_m],
                 n = sp_idxs[2][idx_m];
            sum += _d2f_sparse[idx_m]*Phi[m*dim + j]*Phi[n*dim + k];
            //std::cout << "d2f_" << i << m << n << " * "
            //          << "Phi_" << m << j << " * "
            //          << "Phi_" << n << k << " + ";
          }
          //std::cout << std::endl;
          d2Phi[i*dim*dim + j*dim + k] = sum;
        }
      }
    }
  }
  // Calculate d2Phi_ijk = ... + df_im * Phi2_mjk
  const double *Phi2 = x + dim*(dim+1);
  sp_idxs = _parent._idxs_df_sparse;
  ptrs = _df_ptrs;
  if(!sp_idxs[0].empty()) {
    for(uint idx_ptr = 0; idx_ptr < dim; ++idx_ptr) {
      uint i = sp_idxs[0][ptrs[0][idx_ptr]];
      for(uint j = 0; j < dim; ++j) {
        for(uint k = 0; k < dim; ++k) {
          double sum = 0.;
          uint m_begin = ptrs[0][idx_ptr],
               m_end   = ptrs[0][idx_ptr+1];
          for(uint idx_m = m_begin; idx_m < m_end; ++idx_m) {
            uint m = sp_idxs[1][idx_m];
            sum += _df_sparse[idx_m]*Phi2[m*dim*dim + j*dim + k];
            //std::cout << "df_" << i << m << " * "
            //          << "Phi2_" << m << j << k << " + ";
          }
          //std::cout << std::endl;
          d2Phi[i*dim*dim + j*dim + k] += sum;
        }
      }
    }
  }
  if(_parent._num_derivs < 2)
    return;
  
  // Compute 3rd derivative d3f
  model.d3f_sparse(t,x,&_d3f_sparse.front());
  
  // Calculate d3Phi_ijkl = d3f_imno * Phi_mj * Phi_nk * Phi_ol + ...
  double *d3Phi = dxdt + dim*(dim*(dim+1)+1);
  sp_idxs = _parent._idxs_d3f_sparse;
  ptrs = _d3f_ptrs;
  if(!sp_idxs[0].empty()) {
    for(uint idx_ptr = 0; idx_ptr < dim; ++idx_ptr) {
      uint i = sp_idxs[0][ptrs[0][idx_ptr]];
      for(uint j = 0; j < dim; ++j) {
        for(uint k = 0; k < dim; ++k) {
          for(uint l = 0; l < dim; ++l) {
            double sum = 0.;
            uint m_begin = ptrs[0][idx_ptr],
                 m_end   = ptrs[0][idx_ptr+1];
            for(uint idx_m = m_begin; idx_m < m_end; ++idx_m) {
              uint m = sp_idxs[1][idx_m],
                   n = sp_idxs[2][idx_m],
                   o = sp_idxs[3][idx_m];
              sum += _d3f_sparse[idx_m]*Phi[m*dim + j]*Phi[n*dim + k]*Phi[o*dim + l];
              //std::cout << "d2f_" << i << m << n << " * "
              //          << "Phi_" << m << j << " * "
              //          << "Phi_" << n << k << " + "
              //          << "Phi_" << o << l << " + ";
            }
            //std::cout << std::endl;
            d3Phi[i*dim*dim*dim + j*dim*dim + k*dim + l] = sum;
          }
        }
      }
    }
  }

  // Calculate d3Phi_ijkl = ... + d2f_imn * Phi2_mjl * Phi_nk +
  //                              d2f_imn * Phi2_nkl * Phi_mj +
  //                              d2f_imn * Phi2_mjk * Phi_nl + ...
  sp_idxs = _parent._idxs_d2f_sparse;
  ptrs = _d2f_ptrs;
  if(!sp_idxs[0].empty()) {
    for(uint idx_ptr = 0; idx_ptr < dim; ++idx_ptr) {
      uint i = sp_idxs[0][ptrs[0][idx_ptr]];
      for(uint j = 0; j < dim; ++j) {
        for(uint k = 0; k < dim; ++k) {
          for(uint l = 0; l < dim; ++l) {
            double sum = 0.;
            uint m_begin = ptrs[0][idx_ptr],
                 m_end   = ptrs[0][idx_ptr+1];
            for(uint idx_m = m_begin; idx_m < m_end; ++idx_m) {
              uint m = sp_idxs[1][idx_m],
                   n = sp_idxs[2][idx_m];
              sum += _d2f_sparse[idx_m]*Phi2[m*dim*dim + j*dim + l]*Phi[n*dim + k];
              sum += _d2f_sparse[idx_m]*Phi2[n*dim*dim + k*dim + l]*Phi[m*dim + j];
              sum += _d2f_sparse[idx_m]*Phi2[m*dim*dim + j*dim + k]*Phi[n*dim + l];
              //std::cout << "d2f_" << i << m << n << " * [ "
              //          << "Phi2_" << m << j << l << " * " << Phi_ << n << k << " + "
              //          << "Phi2_" << n << k << l << " * " << Phi_ << m << j << " + "
              //          << "Phi2_" << m << j << k << " * " << Phi_ << n << l << " + ";
            }
            //std::cout << std::endl;
            d3Phi[i*dim*dim*dim + j*dim*dim + k*dim + l] += sum;
          }
        }
      }
    }
  }

  // Calculate d3Phi_ijkl = ... + df_im * Phi3_mjkl
  const double *Phi3 = x + dim*(dim*(dim+1)+1);
  sp_idxs = _parent._idxs_df_sparse;
  ptrs = _df_ptrs;
  if(!sp_idxs[0].empty()) {
    for(uint idx_ptr = 0; idx_ptr < dim; ++idx_ptr) {
      uint i = sp_idxs[0][ptrs[0][idx_ptr]];
      for(uint j = 0; j < dim; ++j) {
        for(uint k = 0; k < dim; ++k) {
          for(uint l = 0; l < dim; ++l) {
            double sum = 0.;
            uint m_begin = ptrs[0][idx_ptr],
                 m_end   = ptrs[0][idx_ptr+1];
            for(uint idx_m = m_begin; idx_m < m_end; ++idx_m) {
              uint m = sp_idxs[1][idx_m];
              sum += _df_sparse[idx_m]*Phi3[m*dim*dim*dim + j*dim*dim + k*dim + l];
              //std::cout << "df_" << i << m << " * "
              //          << "Phi3_" << m << j << k << l << " + ";
            }
            //std::cout << std::endl;
            d3Phi[i*dim*dim*dim + j*dim*dim + k*dim + l] += sum;
          }
        }
      }
    }
  }
}

#endif // TD_ACTION_H_
