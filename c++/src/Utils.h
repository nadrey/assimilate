 #ifndef UTILS_H_
 #define UTILS_H_
 
// STL includes
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <limits>
#include <string>
#include <cstring>
#include <assert.h>

// for getpid
#include <sys/types.h> 
#include <unistd.h>

// Typedefs
typedef int Index;
typedef std::vector<Index> IndexVec;
typedef double Real;
typedef std::vector<double> RealVec;
typedef unsigned int uint;

// Global defs
#define PI 3.14159265358979323846;

// Macros
#define CALL_MEMBER_FN(object,ptrToMember)((object).*(ptrToMember))

////////////////////////////////////////////////////////////////
/// print msg to std::err and then throw exception
void throw_exception(const char *msg) {
  std::cerr << msg << std::endl;
  throw(msg);
}

////////////////////////////////////////////////////////////////
///
Index round_to_int(double x) {
  return (x > 0.0) ? std::floor(x + 0.5) : std::ceil(x - 0.5);
}

////////////////////////////////////////////////////////////////
/// Linear interpolation on a grid with fixed spacing dt
void linear_interpolation(const Index num_x, const Real &t, const Real t0,
                          const Real &dt, const Real *x_vals, Real *x_interp) {
  Real alpha = (t - t0)/dt;
  Index idx_t = std::floor(alpha);
  alpha -= static_cast<Real>(idx_t);
  const Real *x_n = x_vals + idx_t*num_x,
             *x_np1 = x_n + num_x;
  for(Index ix = 0; ix < num_x; ++ix) {
//    x_interp[ix] = alpha*x_n[ix] + (1.-alpha)*x_np1[ix];
    x_interp[ix] = (1.-alpha)*x_n[ix] + alpha*x_np1[ix];
  }
}

////////////////////////////////////////////////////////////////
/// extract file extension after final "."
/// e.g. path.path/filename.blah.ext
std::string get_file_extension(const char *filename) {
  std::string tmp = filename;
  std::string::size_type idx = tmp.rfind('.');
  if(idx != std::string::npos) {
    return tmp.substr(idx+1);
  }
  else {
    return "";
  }
}

////////////////////////////////////////////////////////////////
/// overloaded function so we can output to cout if needed
void write_column_data_txt(std::ostream &out, const Real *data,
                           Index num_rows, Index num_cols) {
  std::ios::fmtflags old_flags = out.flags();
  std::streamsize old_prec = out.precision();

  out << std::scientific;
  const Index prec = 16, space = 2, width = prec + space + 6;
  for(Index i = 0; i < num_rows; ++i) {
    for(Index j = 0; j < num_cols; ++j) {
      out << std::setw(width) << std::setprecision(prec);
      out << (*data);
      ++data;
    }
    out << std::endl;
  }

  out.flags(old_flags);
  out.precision(old_prec);
}
/// write array data in columnated text format
void write_column_data_txt(const char *filename, const Real *data,
                           Index num_rows, Index num_cols) {
  std::ofstream file(filename,std::ios::out);
  if(!file.is_open()) {
    std::string msg = "write_column_data: could not open file\n";
    msg = msg + filename;
    throw_exception(msg.c_str());
  }
  write_column_data_txt(file,data,num_rows,num_cols);
}

////////////////////////////////////////////////////////////////
///
void write_column_data_bin(const char *filename, const Real *data,
                           Index num_rows, Index num_cols) {
  std::ofstream file(filename,std::ios::out|std::ios::binary);
  if(!file.is_open()) {
    std::string msg = "write_column_data: could not open file\n";
    msg = msg + filename;
    throw_exception(msg.c_str());
  }
  const char *raw;
  raw = reinterpret_cast<const char*>(data);
  file.write(raw,num_rows*num_cols*sizeof(Real));
  file.close();
}

////////////////////////////////////////////////////////////////
/// write data array either in columnated text format or
/// as a binary array based on the file extension
///
/// filename.txt -> columnated text output
/// filename.bin -> binary array output
///
void write_column_data(const char *filename, const Real *data,
                       Index num_rows, Index num_cols) {
  // Check file extension
  std::string file_extension = get_file_extension(filename);
  // Write text file
  if(file_extension == "txt") {
    write_column_data_txt(filename,data,num_rows,num_cols);
  }
  // Binary file
  else if(file_extension == "bin") {
    write_column_data_bin(filename,data,num_rows,num_cols);
  }
  else {
    std::string msg = "write_column_data: unknown file extension\n";
    msg = msg + filename;
    throw_exception(msg.c_str());
  }
}

////////////////////////////////////////////////////////////////
/// read columnated text data into numeric array
void read_column_data_txt(const char *filename, Real *data,
                          Index num_rows, Index num_cols) {
  std::ifstream file(filename,std::ios::out);
  if(!file.is_open()) {
    std::string msg = "read_column_data_txt: could not open file\n";
    msg = msg + filename;
    throw_exception(msg.c_str());
  }
  for(Index i = 0; i < num_rows; ++i) {
    for(Index j = 0; j < num_cols; ++j) {
      file >> (*data); ++data;
    }
  }
}

////////////////////////////////////////////////////////////////
/// read columnated text data into numeric array
void read_column_data_bin(const char *filename, Real *data,
                          Index num_rows, Index num_cols) {
  // Open stream at end of file
  std::ifstream file(filename,std::ios::in|std::ios::binary|std::ios::ate);
  if(!file.is_open()) {
    std::string msg = "read_column_data_bin: could not open file\n";
    msg = msg + filename;
    throw_exception(msg.c_str());
  }
  // Check file size
  std::streampos fSize = file.tellg();
  if((size_t)fSize < num_rows*num_cols*sizeof(Real)) {
    std::string msg = "read_column_data_bin: could not open file\n";
    msg = msg + filename;
    throw_exception(msg.c_str());
  }
  // Read file into data vector
  file.seekg(0,std::ios::beg);
  char *raw = reinterpret_cast<char*>(data);
  file.read(raw,num_rows*num_cols*sizeof(Real));
  file.close();
}

////////////////////////////////////////////////////////////////
/// read data array either in columnated text format or
/// as a binary array based on the file extension
///
/// filename.txt -> columnated text output
/// filename.bin -> binary array output
///
void read_column_data(const char *filename, Real *data,
                      Index num_rows, Index num_cols) {
  // Check file extension
  std::string file_extension = get_file_extension(filename);
  // Write text file
  if(file_extension == "txt") {
    read_column_data_txt(filename,data,num_rows,num_cols);
  }
  // Binary file
  else if(file_extension == "bin") {
    read_column_data_bin(filename,data,num_rows,num_cols);
  }
  else {
    std::string msg = "read_column_data: unknown file extension\n";
    msg = msg + filename;
    throw_exception(msg.c_str());
  }
}

////////////////////////////////////////////////////////////////
///
void init_rand() {
  srand(0);
  //srand(time(NULL) + getpid());
}
////////////////////////////////////////////////////////////////
///
Real rand_real() {
  return (Real)rand()/(Real)RAND_MAX;
}
////////////////////////////////////////////////////////////////
/// Box muller transform
/// from https://en.wikipedia.org/wiki/Box-Muller_transform
Real rand_normal(Real mu, Real sigma) {
  const Real epsilon = std::numeric_limits<Real>::min();
  const Real tau = 2.0*PI;

  static Real z0, z1;
  static bool generate;
  generate = !generate;

  if (!generate) {
    return z1 * sigma + mu;
  }

  double u1, u2;
  do {
    u1 = rand_real();
    u2 = rand_real();
  }
  while (u1 <= epsilon);

  z0 = std::sqrt(-2.0 * std::log(u1)) * std::cos(tau * u2);
  z1 = std::sqrt(-2.0 * std::log(u1)) * std::sin(tau * u2);
  return z0 * sigma + mu;
}

////////////////////////////////////////////////////////////////
///
void calc_RMSE_df(const Index num_argin,
                  const Index num_argout,
                  const IndexVec *df_idxs,
                  const IndexVec &df_order,
                  const RealVec &df_sparse,
                  const RealVec &df_approx) {
  if(df_sparse.empty())
    return;

  Index idx, tmp_idx;
  IndexVec::const_iterator idx_sparse;
  Real rmse, tmp, diff;

  // Calculate RMS error for Jacobian df
  idx_sparse = df_order.begin();
  idx = 0; rmse = 0;
  for(Index i = 0; i < num_argout; ++i) {\
    //std::cout << "==================" << std::endl;
    //std::cout << "(" << i << ")" << std::endl;
    for(Index  j = 0; j < num_argin; ++j) {
      tmp_idx = df_idxs[0][*idx_sparse]*num_argin +
                df_idxs[1][*idx_sparse];
      if(tmp_idx != idx)
        tmp = 0;
      else {
        tmp = df_sparse[*idx_sparse];
        ++idx_sparse;
        if(idx_sparse == df_order.end()) {
          --idx_sparse;
        }
      }
      //std::cout << tmp << "  " << df_approx[idx] << "  " << tmp - df_approx[idx] << std::endl;
      diff = df_approx[idx] - tmp;
      rmse += diff*diff;
      ++idx;
    }
  }
  std::cout << "RMS Error for df: "
    << std::sqrt(rmse/(num_argout*num_argin)) << std::endl;
}

////////////////////////////////////////////////////////////////
///
void calc_RMSE_d2f(const Index num_argin,
                   const Index num_argout,
                   const IndexVec *d2f_idxs,
                   const IndexVec &d2f_order,
                   const RealVec &d2f_sparse,
                   const RealVec &d2f_approx) {
  if(d2f_sparse.empty())
    return;

  Index idx, tmp_idx;
  IndexVec::const_iterator idx_sparse;
  Real rmse, tmp, diff;

  // Calculate RMS error for Hessian d2f
  idx_sparse = d2f_order.begin();
  idx = 0; rmse = 0;

  for(Index i = 0; i < num_argout; ++i) {
    for(Index  j = 0; j < num_argin; ++j) {
      //std::cout << "==================" << std::endl;
      //std::cout << "(" << i << "," << j << ")" << std::endl;
      for(Index  k = 0; k < num_argin; ++k) {
      tmp_idx = d2f_idxs[0][*idx_sparse]*num_argin*num_argin +
                d2f_idxs[1][*idx_sparse]*num_argin +
                d2f_idxs[2][*idx_sparse];
        if(tmp_idx != idx)
          tmp = 0;
        else {
          tmp = d2f_sparse[*idx_sparse];
          ++idx_sparse;
          if(idx_sparse == d2f_order.end()) {
            --idx_sparse;
          }
        }
        //std::cout << tmp << "  " << d2f_approx[idx] << "  " << tmp - d2f_approx[idx] << std::endl;
        diff = d2f_approx[idx] - tmp;
        rmse += diff*diff;
        ++idx;
      }
    }
  }
  std::cout << "RMS Error for d2f: "
    << std::sqrt(rmse/(num_argout*num_argin*num_argin)) << std::endl;
}

////////////////////////////////////////////////////////////////
///
void calc_RMSE_d3f(const Index num_argin,
                   const Index num_argout,
                   const IndexVec *d3f_idxs,
                   const IndexVec &d3f_order,
                   const RealVec &d3f_sparse,
                   const RealVec &d3f_approx) {
  Index idx, tmp_idx;
  IndexVec::const_iterator idx_sparse;
  Real rmse, tmp, diff;

  // Calculate RMS error for d3f
  idx_sparse = d3f_order.begin();
  idx = 0; rmse = 0;

  for(Index i = 0; i < num_argout; ++i) {
    for(Index  j = 0; j < num_argin; ++j) {
      for(Index  k = 0; k < num_argin; ++k) {
        //std::cout << "==================" << std::endl;
        //std::cout << "(" << i << "," << j << "," << k << ")" << std::endl;
        for(Index  l = 0; l < num_argin; ++l) {
          tmp_idx = d3f_idxs[0][*idx_sparse]*num_argin*num_argin*num_argin +
                    d3f_idxs[1][*idx_sparse]*num_argin*num_argin +
                    d3f_idxs[2][*idx_sparse]*num_argin +
                    d3f_idxs[3][*idx_sparse];
          if(tmp_idx != idx)
            tmp = 0;
          else {
            tmp = d3f_sparse[*idx_sparse];
            ++idx_sparse;
            if(idx_sparse == d3f_order.end()) {
              --idx_sparse;
            }
          }
          //std::cout << tmp << "  " << d3f_approx[idx] << "  " << tmp - d3f_approx[idx] << std::endl;
          diff = d3f_approx[idx] - tmp;
          rmse += diff*diff;
          ++idx;
        }
      }
    }
  }
  std::cout << "RMS Error for d3f: "
    << std::sqrt(rmse/(num_argout*num_argin*num_argin*num_argin)) << std::endl;
}

////////////////////////////////////////////////////////////////
///
/*
void calc_RMSE_df(const Index num_argin,
                  const Index num_argout,
                  const Index num_derivs,
                  const IndexVec *d3f_idxs,
                  const IndexVec &d3f_order,
                  const RealVec &d3f_sparse,
                  const RealVec &d3f_approx) {
  Index idx, tmp_idx;
  IndexVec::const_iterator idx_sparse;
  Real rmse, tmp, diff;

  // Calculate RMS error for d3f
  idx_sparse = d3f_order.begin();
  idx = 0; rmse = 0;

  Index  num_elements = num_argout*std::pow(num_argin,num_derivs);
  for(Index i = 0; i < num_elements; ++i) {

  }

  for(Index i = 0; i < num_argout; ++i) {
    for(Index  j = 0; j < num_argin; ++j) {
      for(Index  k = 0; k < num_argin; ++k) {
        //std::cout << "(" << i << "," << j << "," << k << ")" << std::endl;
        for(Index  l = 0; l < num_argin; ++l) {
          tmp_idx = d3f_idxs[0][*idx_sparse]*num_argin*num_argin*num_argin +
                    d3f_idxs[1][*idx_sparse]*num_argin*num_argin +
                    d3f_idxs[2][*idx_sparse]*num_argin +
                    d3f_idxs[3][*idx_sparse];
          if(tmp_idx != idx)
            tmp = 0;
          else {
            tmp = d3f_sparse[*idx_sparse];
            ++idx_sparse;
            if(idx_sparse == d3f_order.end()) {
              --idx_sparse;
            }
          }
          //std::cout << tmp << "  " << d3f_approx[idx] << std::endl;
          diff = d3f_approx[idx] - tmp;
          rmse += diff*diff;
          ++idx;
        }
      }
      //std::cout << "==================" << std::endl;
    }
  }
  std::cout << "RMS Error for d3f: "
    << std::sqrt(rmse/(num_argout*num_argin*num_argin*num_argin)) << std::endl;
}
*/

#endif // UTILS_H_
