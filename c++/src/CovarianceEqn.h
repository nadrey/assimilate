#ifndef COVARIANCE_EQN
#define COVARIANCE_EQN

// My includes
#include "Utils.h"
#include "TrueExperimentData.h"

////////////////////////////////////////////////////////////////
/// Covariance equation
template <class Model>
class CovarianceEqn {
public:
  // Realdefs
  typedef std::vector<Real> RealVec;
  typedef TrueExperimentData<Model> Data;
  typedef RK4Step<CovarianceEqn> ODE;
 
  // Constructors
  CovarianceEqn(const Data &data);
  
  // Get methods
  uint dim() const;
  
  // ODE Step
  void step(const Real &t, const Real *x0, const Real &dt, Real *x);
  
  // Covariance equation
  void f(const Real &t, const Real *X, Real *dxdt) const;

private:
  
  // Filter data
  const Data &_data;
  // ODE stepper
  ODE _ode;
  // Model error inverse covariance
  RealVec _RfDiag;
  // Jacobian
  mutable RealVec _dfdx, _dfdp;
};

////////////////////////////////////////////////////////////////
/// Constructors
template <class Model>
CovarianceEqn<Model>::CovarianceEqn(const Data &data) :
  _data (data), _ode (*this) {
  // Resize data structures
  const uint nD = _data->model().nD(),
             nP = _data->model().nP(),
             nDP = nD + nP;
  _RfDiag.resize(nDP,1);
  _dfdx.resize(nD*nD,0);
  _dfdp.resize(nD*nP,0);
}

////////////////////////////////////////////////////////////////
///
template <class Model>
uint CovarianceEqn<Model>::dim() const {
  const uint nD = _data->model().nD(),
             nP = _data->model().nP(),
             nDP = nD + nP;
  return nDP*(nDP + 1);
}

////////////////////////////////////////////////////////////////
///
template <class Model>
void CovarianceEqn<Model>::f(const Real &t,
                             const Real *x,
                             Real *dxdt) const {
  const uint nD = _data->model().nD(),
             nP = _data->model().nP();
  
  // TODO To be safe, initialize to zero
  memset(dxdt,0.,this->dim()*sizeof(Real));
  
  // Evaluate vector field
  _data->model().f(t,x,dxdt);
  
  // Evaluate Jacobian
  _data->model().df(t,x,&_dfdx.front());

  // Tangent linear covariance propagation
  // d/dt invP = df*invP + invP*df^T + Rf
  const Real *invP = x + nD;
  for(uint i = 0; i < nD; ++i) {
    for(uint j = 0; j < nD; ++j) {
      Real sum = 0;
      for(uint k = 0; k < nD; ++k) {
        sum += _dfdx[i*nD + k]*invP[k*nD + j];
      }
      dxdt[(i+1)*nD + j] = sum;
    }   
  }
  for(uint i = 0; i < nD; ++i) {
    for(uint j = i; j < nD; ++j) {
      Real tmp = dxdt[(i+1)*nD + j] + dxdt[(j+1)*nD + i];
      dxdt[(i+1)*nD + j] = dxdt[(j+1)*nD + i] = tmp;
    }
    dxdt[(i+1)*nD + i] += _RfDiag[i];
  }
}

////////////////////////////////////////////////////////////////
///
template <class Model>
void CovarianceEqn<Model>::step(const Real &t,const Real *x0,
                                const Real &dt, Real *x) {
  _ode.step(t,x0,dt,x);
}

#endif // COVARIANCE_EQN
