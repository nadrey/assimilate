#ifndef TIME_DOMAIN_H_
#define TIME_DOMAIN_H_

// My includes
#include "Utils.h"

////////////////////////////////////////////////////////////////
/// 
class TimeDomain {
public:
  // Constructors
  TimeDomain();
  TimeDomain(Index nN, Real dt);
  TimeDomain(Real T, Real dt);

  // Initialization methods
  void initialize(Index nN, Real dt);
  void initialize(Real T, Real dt);

  // Get methods
  const RealVec& time_points() const;
  Index num_time_intervals() const;
  Index num_time_points() const;
  Real dt() const;

private:
  // Data
  bool _uniform;
  RealVec _time_domain;
};

////////////////////////////////////////////////////////////////
/// constructors
TimeDomain::TimeDomain() {
  initialize(100,0.01);
}
TimeDomain::TimeDomain(Index nN, Real dt) {
  initialize(nN,dt);
}
TimeDomain::TimeDomain(Real T, Real dt) {
  initialize(T,dt);
}

////////////////////////////////////////////////////////////////
/// get methods
const RealVec& TimeDomain::time_points() const {
  return _time_domain;
}
///
Index  TimeDomain::num_time_points() const {
  return _time_domain.size();
}
///
Index  TimeDomain::num_time_intervals() const {
  return _time_domain.size()-1;
}
///
Real TimeDomain::dt() const {
  return _time_domain[1];
}

////////////////////////////////////////////////////////////////
void TimeDomain::initialize(Index  nN, Real dt) {
  _uniform = true;
  _time_domain.resize(nN+1);
  for(Index iN = 0; iN <= nN; ++iN) {
    _time_domain[iN] = dt*iN;
  }
}

////////////////////////////////////////////////////////////////
void TimeDomain::initialize(Real T, Real dt) {
  const Index nN = round_to_int(T/dt);
  initialize(nN,dt);
}

#endif // TIME_DOMAIN_H_

