#ifndef HAMILTONS_EQNS_H_
#define HAMILTONS_EQNS_H_


////////////////////////////////////////////////////////////////
/// Derived model class to implement Hamilton's equations for
/// estimation on a given dynamical model. Assumes that the
/// 2*nD state vector is organized as [x,p].
class HamiltonsEqns : public DynamicalModel {
public:
  // Constructor/destructor
  HamiltonsEqns(const EstimationData &data,
                const RealVec &Rm_diag,
                const RealVec &Rf_diag);
  virtual ~HamiltonsEqns();

  // Get methods
  virtual Index num_states() const;
  virtual Index num_derivs() const;

  // Vector field evaluations
  virtual void f_eval(const Real &t, const Real *xp, Real *f) const;
  virtual void df_sparse_eval(const Real &t, const Real *xp, Real *df) const;
  virtual void d2f_sparse_eval(const Real &t, const Real *xp, Real *d2f) const;

  // Sparsity patterns
  virtual void df_sparse_idxs(IndexVec *df_sparse_idxs) const;
  virtual void d2f_sparse_idxs(IndexVec *d2f_sparse_idxs) const;

private:
  // Initialize data members
  void initialize();

  // Estimation data
  const EstimationData &_data;
  // Model/measurement error
  const RealVec &_Rm_diag, &_Rf_diag;
  // Number of non-zero elements in Hamiltonian derivatives
  Index _num_df_non0s, _num_d2f_non0s;
  // Cache vector field derivatives for model
  mutable RealVec _df_model_sparse,
                  _d2f_model_sparse,
                  _d3f_model_sparse;
  IndexVec _df_model_sparse_idxs[2],
           _d2f_model_sparse_idxs[3],
           _d3f_model_sparse_idxs[4],
           _d3f_model_row_pointers,
           _d3f_model_idx_order;

  // Observation interpolation
  mutable RealVec _interpolation_data;

  // Copy constructor
  HamiltonsEqns(const HamiltonsEqns&);
};

////////////////////////////////////////////////////////////////
/// constructor
HamiltonsEqns::HamiltonsEqns(const EstimationData &data,
                             const RealVec &Rm_diag,
                             const RealVec &Rf_diag) :
  _data (data), _Rm_diag (Rm_diag), _Rf_diag (Rf_diag) {
  initialize();
}
/// destructor
HamiltonsEqns::~HamiltonsEqns() {
}

////////////////////////////////////////////////////////////////
/// Get methods
Index HamiltonsEqns::num_states() const {
  return 2*_data.dynamical_model().num_states();
}
///
Index HamiltonsEqns::num_derivs() const {
  return 2;
}

////////////////////////////////////////////////////////////////

void HamiltonsEqns::initialize() {
  // Cache model sparsity patterns
  const DynamicalModel &model = _data.dynamical_model();
  model.df_sparse_idxs(_df_model_sparse_idxs);
  model.d2f_sparse_idxs(_d2f_model_sparse_idxs);
  model.d3f_sparse_idxs(_d3f_model_sparse_idxs);
  // Initialize cache for model derivatives
  _df_model_sparse.resize(_df_model_sparse_idxs[0].size());
  _d2f_model_sparse.resize(_d2f_model_sparse_idxs[0].size());
  _d3f_model_sparse.resize(_d3f_model_sparse_idxs[0].size());
  // Cache sparsity patterns of Hamiltonian
  IndexVec tmp_idxs[4];
  df_sparse_idxs(tmp_idxs);
  _num_df_non0s = tmp_idxs[0].size();
  d2f_sparse_idxs(tmp_idxs);
  _num_d2f_non0s = tmp_idxs[0].size();
  //
  // 1st index of d3f gets contracted with p
  // permute and sort d3f to identify any indices
  // that only differ in the first index
  //
  tmp_idxs[0] = _d3f_model_sparse_idxs[1];
  tmp_idxs[1] = _d3f_model_sparse_idxs[2];
  tmp_idxs[2] = _d3f_model_sparse_idxs[3];
  tmp_idxs[3] = _d3f_model_sparse_idxs[0];
  _d3f_model_idx_order.clear();
  SparseIndexSorter sorter;
  sorter.sort(tmp_idxs,4,_d3f_model_idx_order);
  const Index num_d3f_non0s = _d3f_model_idx_order.size();
  for(Index idx = 0; idx < num_d3f_non0s; ++idx) {
    for(Index i = 0; i < 4; ++i) {
      _d3f_model_sparse_idxs[i][idx] =
        tmp_idxs[i][_d3f_model_idx_order[idx]];
    }
  }
  const Index nD = model.num_states(),
              dims[4] = {nD,nD,nD,nD};
  inner_row_ptrs(_d3f_model_sparse_idxs,4,dims,_d3f_model_row_pointers);
  //
  // Observation interpolation
  //
  const Index nL = _data.observation_model().num_observed_states();
  _interpolation_data.resize(nL,0);
}

////////////////////////////////////////////////////////////////
/// Vector field evaluations
///
/// H(x,p,y) = 1/2 |p|^2_Rf^{-1} + p.f(x,t) - 1/2 |y - h(x)|^2_Rm
///
/// dx/dt =  dH/dp = f(x(t),t) + Rf^{-1}.p(t)
/// dp/dt = -dH/dx = -(df/dx)^T.p(t) - (dh/dx)^T.Rm.(y(t)-h(x))
///
void HamiltonsEqns::f_eval(const Real &t, const Real *xp, Real *f) const {
  // Model
  const DynamicalModel &model = _data.dynamical_model();
  const Index nD = model.num_states();
  // Time domain
  const TimeDomain time_domain = _data.time_domain();
  // Observations
  const ObservationModel &obs = _data.observation_model();
  const Index nL = obs.num_observed_states();
  const IndexVec &observed_state_idxs = obs.observed_state_idxs();

  // Observations at fixed times only
  //Index idx_t = static_cast<Index>(t/time_domain.dt());
  //const Real *y = &(_data.observations()[idx_t*nL]);

  // Interpolate observations
  const Real *y = &_data.observations().front(),
             &dt = time_domain.dt();
  Real *y_interp = &_interpolation_data.front();
  linear_interpolation(nL,t,0.,dt,y,y_interp);

  // TODO This can be more efficient
  model.df_sparse_eval(t,xp,&_df_model_sparse.front());

  // Split x and p
  const Real *x = xp, *p = xp + nD;
  // Reset f to 0
  memset(f,0.,2*nD*sizeof(Real));

  // Evaluate dx/dt
  model.f_eval(t,x,f);
  for(Index iD = 0; iD < nD; ++iD) {
    f[iD] += p[iD]/_Rf_diag[iD];
  }

  // Evaluate dp/dt
  const IndexVec *df_sparse_idxs = _df_model_sparse_idxs;
  const Index num_df_non0s = _df_model_sparse.size();
  for(Index idx = 0; idx < num_df_non0s; ++idx) {
    Index iD = df_sparse_idxs[1][idx],
          jD = df_sparse_idxs[0][idx];
    f[nD + iD] -= _df_model_sparse[idx]*p[jD];
  }

  // Measurement error term
  const Real &obs_window_length = obs.observation_window_length();
  if(t <= obs_window_length) {
    for(Index iL = 0; iL < nL; ++iL) {
      Index idx_meas = observed_state_idxs[iL];
      f[nD + idx_meas] -= _Rm_diag[iL]*(y_interp[iL] - x[idx_meas]);
    }
  }
}

////////////////////////////////////////////////////////////////
/// Jacobian evaluation of vector field
///
/// d/dx[ dx/dt ] = df/dx
/// d/dp[ dx/dt ] = Rf^{-1}
///
/// d/dx[ dp/dt ] = -d2f/dx^2.p + (dh/dx)^T.Rm.dh/dx ...
///                   - (d2h/dx)^2.Rm.(y(t)-h(x))
/// d/dp[ dp/dt ] = -(df/dx)^T
///
/// TODO For now, assume d/dx[ dp/dt ] block is dense
///
void HamiltonsEqns::df_sparse_eval(const Real &t, const Real *xp, Real *df) const {
  // Model
  const DynamicalModel &model = _data.dynamical_model();
  const Index nD = model.num_states();
  // Time domain
  const TimeDomain time_domain = _data.time_domain();
  // Observations
  const ObservationModel &obs = _data.observation_model();
  const Index nL = obs.num_observed_states();
  const IndexVec &observed_state_idxs = obs.observed_state_idxs();

  // Split x and p
  const Real *x = xp, *p = xp + nD;
  // Reset df to 0
  memset(df,0.,_num_df_non0s*sizeof(Real));

  // TODO This can be more efficient
  model.df_sparse_eval(t,x,&_df_model_sparse.front());
  model.d2f_sparse_eval(t,x,&_d2f_model_sparse.front());

  // d/dx[ dx/dt ] = df/dx
  const Index num_df_model_non0s = _df_model_sparse.size();
  for(Index idx = 0; idx < num_df_model_non0s; ++idx) {
    df[idx] = +_df_model_sparse[idx];
  }
  Index offset = num_df_model_non0s;
  // d/dp[ dx/dt ] = Rf^{-1}
  for(Index iD = 0; iD < nD; ++iD) {
    df[offset + iD] = 1./_Rf_diag[iD];
  }
  offset += nD;
  // d/dx[ dp/dt ] = -d2f/dx^2.p + (dh/dx)^T.Rm.dh/dx  (assumed dense)
  const Index num_d2f_model_non0s = _d2f_model_sparse.size();
  for(Index idx = 0; idx < num_d2f_model_non0s; ++idx) {
    Index iD = _d2f_model_sparse_idxs[1][idx],
          jD = _d2f_model_sparse_idxs[2][idx],
          kD = _d2f_model_sparse_idxs[0][idx];
    df[offset + iD*nD + jD] -= _d2f_model_sparse[idx]*p[kD];
  }
  const Real &obs_window_length = obs.observation_window_length();
  if(t <= obs_window_length) {
    for(Index iL = 0; iL < nL; ++iL) {
      Index idx_meas = observed_state_idxs[iL];
      df[offset + idx_meas*(nD + 1)] += _Rm_diag[iL];
    }
  }
  offset += nD*nD;
  // d/dp[ dp/dt ] = -(df/dx)^T
  for(Index idx = 0; idx < num_df_model_non0s; ++idx) {
    df[offset + idx] = -_df_model_sparse[idx];
  }
}

////////////////////////////////////////////////////////////////
/// Hessian evaluation of vector field
///
/// d^2/dx^2[ dx/dt ] = d2f/dx^2
/// d^2/dxdp[ dx/dt ] = 0
/// d^2/dp^2[ dx/dt ] = 0
///
/// d^2/dx^2[ dp/dt ] = -d3f/dx^3.p ...
///                      - (d2h/dx)^3.Rm.(y(t)-h(x)) + 3*(d2h/dx)^2.Rm.dh/dx
/// d^2/dxdp[ dp/dt ] = -d2f/dx^2
/// d^2/dp^2[ dp/dt ] = 0
///
void HamiltonsEqns::d2f_sparse_eval(const Real &t,const Real *xp,Real *d2f) const {
  // Model
  const DynamicalModel &model = _data.dynamical_model();
  const Index nD = model.num_states();
  // Time domain
  const TimeDomain time_domain = _data.time_domain();

  // Split x and p
  const Real *x = xp, *p = xp + nD;
  // Reset d2f to 0
  memset(d2f,0.,_num_d2f_non0s*sizeof(Real));

  // TODO This can be more efficient
  model.d2f_sparse_eval(t,x,&_d2f_model_sparse.front());
  model.d3f_sparse_eval(t,x,&_d3f_model_sparse.front());

  // d^2/dx^2[ dx/dt ] = d2f/dx^2
  const Index num_d2f_model_non0s = _d2f_model_sparse.size();
  for(Index idx = 0; idx < num_d2f_model_non0s; ++idx) {
    d2f[idx] = +_d2f_model_sparse[idx];
  }
  Index offset = num_d2f_model_non0s;

  // d^2/dx^2[ dp/dt ] = -d3f/dx^3.p (assumed sparse)
  Index num_d3f_ptrs = _d3f_model_row_pointers.size();
  for(Index idx_ptr = 0; idx_ptr < num_d3f_ptrs-1; ++idx_ptr) {
    Index idx0 = _d3f_model_row_pointers[idx_ptr],
          idx1 = _d3f_model_row_pointers[idx_ptr+1];
    if(idx0 == idx1) {
      continue;
    }
    for(; idx0 < idx1; ++idx0) {
      Index lD = _d3f_model_sparse_idxs[3][idx0];
      const Index idx = _d3f_model_idx_order[idx0];
      d2f[offset] -= _d3f_model_sparse[idx]*p[lD];
    }
    ++offset;
  }
  /*
  // d^2/dx^2[ dp/dt ] = -d3f/dx^3.p (assumed dense)
  const Index num_d3f_model_non0s = _d3f_model_sparse.size();
  for(Index idx = 0; idx < num_d3f_model_non0s; ++idx) {
    Index iD = _d3f_model_sparse_idxs[1][idx],
          jD = _d3f_model_sparse_idxs[2][idx],
          kD = _d3f_model_sparse_idxs[3][idx],
          lD = _d3f_model_sparse_idxs[0][idx];
    d2f[offset + iD*nD*nD + jD*nD + kD] -= _d3f_model_sparse[idx]*p[lD];
  }
  offset += nD*nD*nD;
  */
  // d^2/dxdp[ dp/dt ] = -d2f/dx^2
  for(Index idx = 0; idx < num_d2f_model_non0s; ++idx) {
    d2f[offset + idx] = -_d2f_model_sparse[idx];
  }
  offset += num_d2f_model_non0s;
  // d^2/dpdx[ dp/dt ] = -d2f/dx^2
  for(Index idx = 0; idx < num_d2f_model_non0s; ++idx) {
    d2f[offset + idx] = -_d2f_model_sparse[idx];
  }
}

////////////////////////////////////////////////////////////////
/// Sparsity patten of vector field Jacobian
///
/// d/dx[ dx/dt ] = df/dx
/// d/dp[ dx/dt ] = Rf^{-1}
///
/// d/dx[ dp/dt ] = -d2f/dx^2.p + (dh/dx)^T.Rm.dh/dx ...
///                   - (d2h/dx)^2.Rm.(y(t)-h(x))
/// d/dp[ dp/dt ] = -(df/dx)^T
///
/// TODO For now, assume d/dx[ dp/dt ] block is dense
///
void HamiltonsEqns::df_sparse_idxs(IndexVec *sparse_idxs) const {
  sparse_idxs[0].clear();
  sparse_idxs[1].clear();
  // d/dx[ dx/dt ] = df/dx
  const Index df_model_size = _df_model_sparse_idxs[0].size();
  for(Index idx = 0; idx < df_model_size; ++idx) {
    const Index iD = _df_model_sparse_idxs[0][idx],
                jD = _df_model_sparse_idxs[1][idx];
    sparse_idxs[0].push_back(iD);
    sparse_idxs[1].push_back(jD);
  }
  // d/dp[ dx/dt ] = Rf^{-1}
  const Index nD = _data.dynamical_model().num_states();
  for(Index iD = 0; iD < nD; ++iD) {
    sparse_idxs[0].push_back(iD);
    sparse_idxs[1].push_back(nD + iD);
  }
  // d/dx[ dp/dt ] = -d2f/dx^2.p + (dh/dx)^T.Rm.dh/dx (assumed dense)
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
    sparse_idxs[0].push_back(nD + iD);
    sparse_idxs[1].push_back(jD);
  }}
  // d/dp[ dp/dt ] = -(df/dx)^T
  for(Index idx = 0; idx < df_model_size; ++idx) {
    // df transposed
    const Index iD = _df_model_sparse_idxs[1][idx],
                jD = _df_model_sparse_idxs[0][idx];
    sparse_idxs[0].push_back(nD + iD);
    sparse_idxs[1].push_back(nD + jD);
  }
}

////////////////////////////////////////////////////////////////
/// Sparsity patten of vector field Hessian
///
/// d^2/dx^2[ dx/dt ] = d2f/dx^2
/// d^2/dxdp[ dx/dt ] = 0
/// d^2/dpdx[ dx/dt ] = 0
/// d^2/dp^2[ dx/dt ] = 0
///
/// d^2/dx^2[ dp/dt ] = -d3f/dx^3.p ...
///                      - (d2h/dx)^3.Rm.(y(t)-h(x)) + 3*(d2h/dx)^2.Rm.dh/dx
/// d^2/dxdp[ dp/dt ] = -d2f/dx^2
/// d^2/dpdx[ dp/dt ] = -d2f/dx^2
/// d^2/dp^2[ dp/dt ] = 0
///
void HamiltonsEqns::d2f_sparse_idxs(IndexVec *sparse_idxs) const {
  sparse_idxs[0].clear();
  sparse_idxs[1].clear();
  sparse_idxs[2].clear();
  // d^2/dx^2[ dx/dt ] = d2f/dx^2
  const Index d2f_model_size = _d2f_model_sparse_idxs[0].size();
  for(Index idx = 0; idx < d2f_model_size; ++idx) {
    const Index iD = _d2f_model_sparse_idxs[0][idx],
                jD = _d2f_model_sparse_idxs[1][idx],
                kD = _d2f_model_sparse_idxs[2][idx];
    sparse_idxs[0].push_back(iD);
    sparse_idxs[1].push_back(jD);
    sparse_idxs[2].push_back(kD);
  }

  // d^2/dx^2[ dp/dt ] = -d3f/dx^3.p ... (assumed sparse)
  const Index nD = _data.dynamical_model().num_states();
  Index num_d3f_ptrs = _d3f_model_row_pointers.size();
  for(Index idx_ptr = 0; idx_ptr < num_d3f_ptrs-1; ++idx_ptr) {
    Index idx0 = _d3f_model_row_pointers[idx_ptr],
          idx1 = _d3f_model_row_pointers[idx_ptr+1];
    if(idx0 == idx1) {
      continue;
    }
    Index iD = _d3f_model_sparse_idxs[0][idx0],
          jD = _d3f_model_sparse_idxs[1][idx0],
          kD = _d3f_model_sparse_idxs[2][idx0];
    sparse_idxs[0].push_back(nD + iD);
    sparse_idxs[1].push_back(jD);
    sparse_idxs[2].push_back(kD);
  }
  /*
  // d^2/dx^2[ dp/dt ] = -d3f/dx^3.p ... (assumed dense)
  const Index nD = _data.dynamical_model().num_states();
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
  for(Index kD = 0; kD < nD; ++kD) {
      sparse_idxs[0].push_back(nD + iD);
      sparse_idxs[1].push_back(jD);
      sparse_idxs[2].push_back(kD);
  }}}
  */
  // d^2/dxdp[ dp/dt ] = -d2f^T/dx^2
  for(Index idx = 0; idx < d2f_model_size; ++idx) {
    const Index iD = _d2f_model_sparse_idxs[0][idx],
                jD = _d2f_model_sparse_idxs[1][idx],
                kD = _d2f_model_sparse_idxs[2][idx];
    sparse_idxs[0].push_back(nD + jD);
    sparse_idxs[1].push_back(kD);
    sparse_idxs[2].push_back(nD + iD);
  }
  // d^2/dpdx[ dp/dt ] = -d2f^T/dx^2
  for(Index idx = 0; idx < d2f_model_size; ++idx) {
    const Index iD = _d2f_model_sparse_idxs[0][idx],
                jD = _d2f_model_sparse_idxs[1][idx],
                kD = _d2f_model_sparse_idxs[2][idx];
    sparse_idxs[0].push_back(nD + jD);
    sparse_idxs[1].push_back(nD + iD);
    sparse_idxs[2].push_back(kD);
  }
}

#endif /* HAMILTONS_EQNS_H_ */
