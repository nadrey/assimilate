#ifndef DYNAMICAL_MODEL_H_
#define DYNAMICAL_MODEL_H_

// My includes
#include "Utils.h"

////////////////////////////////////////////////////////////////
/// Base class for Model
class DynamicalModel {
public:

  // Constructor/destructor
  DynamicalModel() {};
  virtual ~DynamicalModel() {};

  // Get methods
  virtual Index  num_states() const
    { return 0; }
  virtual Index  num_params() const
    { return 0; }
  virtual Index  num_derivs() const
    { return 0; }

  // Bounds
  virtual void state_bounds(Real *x_lower, Real *x_upper) const {};
  virtual void param_bounds(Real *p_lower, Real *p_upper) const {};

  // Overload () operator so model can be used as functor
  virtual void operator()(const Real &t, const Real *x, Real *dxdt) const
    { f_eval(t,x,dxdt); }

  // Vector field evaluations
  virtual void   f_eval(const Real &t, const Real *x, Real *f)   const = 0;
  virtual void  df_eval(const Real &t, const Real *x, Real *df)  const {};

  // Sparse dervative evaluation
  virtual void  df_sparse_eval(const Real &t, const Real *x, Real *df) const {};
  virtual void d2f_sparse_eval(const Real &t, const Real *x, Real *d2f) const {};
  virtual void d3f_sparse_eval(const Real &t, const Real *x, Real *d3f) const {};

  // Sparsity structures
  virtual void df_sparse_idxs(IndexVec *df_sparse_idxs) const {};
  virtual void d2f_sparse_idxs(IndexVec *d2f_sparse_idxs) const {};
  virtual void d3f_sparse_idxs(IndexVec *d3f_sparse_idxs) const {};

  // Random state/parameter
  virtual void random_state(Real *x) const {};
  virtual void random_param(Real *p) const {};

protected:

private:
  DynamicalModel(const DynamicalModel&);
};



#endif /* DYNAMICAL_MODEL_H_ */
