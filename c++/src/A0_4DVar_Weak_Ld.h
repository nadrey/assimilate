#ifndef A0_4DVAR_WEAK_LD_H_
#define A0_4DVAR_WEAK_LD_H_

// My includes
#include "EstimationAction.h"
#include "SparseTensor.h"

////////////////////////////////////////////////////////////////
/// Derived action for weak-constrained, collocated 4DVar action
/// written as a discrete Lagrangian
///
///  A0 ~ 1/T \sum_{n=0}^{N-1} dt*Ld(x(n),x(n+1),n,n+1)
///
///  where
///
///  Ld(x(n),x(n+1),n,n+1) =
///    1/2 |y(n)-h(x_n)|^2_Rm + 1/2|g(x(n),x(n+1))|^2_Rf
///
///  and g(x(n),x(n+1)) is a discretized version of the
///  model equations dx/dt - f(x)
///
/// The action is discretized using the left endpoint rule for Riemann
/// sums. In contrast to our original implmentation of weak 4DVar,
/// this ignores the measurement at the final endpoint.
///
/// This convention was chosen so that the stationary condition
/// dL(x(N-1),x(N))/dx(N) = 0 enforces dx(N)/dt ~ f(x(N)).
///
class A0_4DVar_Weak_Ld : public A0_4DVar_Weak {
public:

  // Constructors
  A0_4DVar_Weak_Ld();

  // Destructor
  virtual ~A0_4DVar_Weak_Ld();

  // Class name
  virtual std::string classname() const
    { return "A0_4DVar_Weak_Ld"; }

protected:

  // Discrete Lagrangian
  void L_eval(const Real &t_n, const Real *x_n,
              const Real &t_np1, const Real *x_np1,
              Real &L);
  void dL_eval(const Real &t_n, const Real *x_n,
               const Real &t_np1, const Real *x_np1,
               Real *dL);
  void d2L_sparse_eval(const Real &t_n, const Real *x_n,
                       const Real &t_np1, const Real *x_np1,
                       Real *d2L_sparse);

  // Compute and cache objective function and derivatives
  virtual void eval_new_X(const Real *X, bool new_X);

private:
  // Copy constructor
  A0_4DVar_Weak_Ld(const A0_4DVar_Weak_Ld&);
};

////////////////////////////////////////////////////////////////
/// Constructors
A0_4DVar_Weak_Ld::A0_4DVar_Weak_Ld() :
  A0_4DVar_Weak() {
}

/// Destructor
A0_4DVar_Weak_Ld::~A0_4DVar_Weak_Ld() {
}

////////////////////////////////////////////////////////////////
/// New evaluation method, based on discrete Lagrangian
void A0_4DVar_Weak_Ld::eval_new_X(const Real *X, bool new_X) {
  if(!new_X)
    return;

  // Dynamical model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();

  // Time domain
  const TimeDomain &time_domain = _data->time_domain();
  const RealVec &time_points = time_domain.time_points();
  const Index nN = time_domain.num_time_intervals();
  const Real T = time_points.back() - time_points.front();

  // Reset cached values
  _f = 0;
  std::fill(_df.begin(),_df.end(),0.);
  std::fill(_d2f_sparse.begin(),_d2f_sparse.end(),0.);

  // Initialize Lagrangian calculation
  const Real *x_n = X, *x_np1 = x_n + nD;
  const Real *t_n = &time_points.front(), *t_np1 = t_n+1;
  _quad->init_dg_eval(*t_n,x_n);
  _quad->init_d2g_eval(*t_n,x_n);

  // Number of nonzeros per time step
  const Index offset_d2L = 3*nD*nD;

  // Time loop
  for(Index iN = 0; iN < nN; ++iN) {
    // Cache intermediate values to avoid recomputation
    _quad->g_eval(*t_n,*t_np1,x_n,x_np1,&_g_quad.front());
    _quad->dg_sparse_eval(*t_n,*t_np1,x_n,x_np1,&_dg_quad_sparse.front());
    _quad->d2g_sparse_eval(*t_n,*t_np1,x_n,x_np1,&_d2g_quad_sparse.front());
    // Compute objective
    L_eval(*t_n,x_n,*t_np1,x_np1,_f);
    // Compute Jacobian
    dL_eval(*t_n,x_n,*t_np1,x_np1,&_df[iN*nD]);
    // Compute Hessian
    d2L_sparse_eval(*t_n,x_n,*t_np1,x_np1,&_d2f_sparse[iN*offset_d2L]);
    // Iterate
    ++t_n; ++t_np1; x_n += nD; x_np1 += nD;
  }

  // Apply overall scale factor to objective
  Real sf = 1./T;
  _f *= 0.5*sf;
  for(RealVec::iterator iter = _df.begin();
        iter != _df.end(); ++iter) {
    (*iter) *= sf;
  }
  for(RealVec::iterator iter = _d2f_sparse.begin();
        iter != _d2f_sparse.end(); ++iter) {
    (*iter) *= sf;
  }
}

////////////////////////////////////////////////////////////////
/// Evaluate discrete Lagrangian
void A0_4DVar_Weak_Ld::L_eval(const Real &t_n, const Real *x_n,
                              const Real &t_np1, const Real *x_np1,
                              Real &L) {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();
  // Observations
  const ObservationModel &obs = _data->observation_model();
  const Index nL = obs.num_observed_states();
  const IndexVec &observed_state_idxs = obs.observed_state_idxs();

  // TODO Figure what to do here
  const Real dt = t_np1 - t_n;
  Index idx_n = static_cast<Index>(t_n/dt);
  const Real *y_n = &_data->observations()[idx_n*nL];

  // Measurement error
  for(Index iL = 0; iL < nL; ++iL) {
    Index idx_meas = observed_state_idxs[iL];
    Real d_n = y_n[iL] - x_n[idx_meas];
    L += dt*_Rm_diag[iL]*d_n*d_n;
  }

  // Rescale constraints by 1/dt
  const Real sf = 1/dt;

  // cached
  //_quad->g_eval(*t_n,*t_np1,x_n,x_np1,&_g_quad->front());

  // Model error -- divide by dt since g_quad is integrated
  for(Index iD = 0; iD < nD; ++iD) {
    L += sf*_g_quad[iD]*_Rf_diag[iD]*_g_quad[iD];
  }
}

////////////////////////////////////////////////////////////////
/// Jacobian of discrete Lagrangian
void A0_4DVar_Weak_Ld::dL_eval(const Real &t_n, const Real *x_n,
                               const Real &t_np1, const Real *x_np1,
                               Real *dL) {
  // Observations
  const ObservationModel &obs = _data->observation_model();
  const Index nL = obs.num_observed_states();
  const IndexVec &observed_state_idxs = obs.observed_state_idxs();

  // TODO Figure what to do here
  const Real dt = t_np1 - t_n;
  Index idx_n = static_cast<Index>(t_n/dt);
  const Real *y_n = &_data->observations()[idx_n*nL];

  // Measurement error
  for(Index iL = 0; iL < nL; ++iL) {
    Index idx_meas = observed_state_idxs[iL];
    Real d_n = y_n[iL] -   x_n[idx_meas];
    dL[idx_meas] -= dt*_Rm_diag[iL]*d_n;
  }

  // Rescale constraints by 1/dt
  const Real sf = 1/dt;

  // cached
  //_quad->dg_eval(*t_n,*t_np1,x_n,x_np1,&_dg_quad_sparse->front());

  // Model error
  const Index num_dg_non0s = _dg_quad_sparse.size();
  for(Index idx = 0; idx < num_dg_non0s; ++idx) {
    Index i = _dg_quad_sparse_idxs[0][idx],
          j = _dg_quad_sparse_idxs[1][idx];
    dL[j] += sf*_dg_quad_sparse[idx]*_Rf_diag[i]*_g_quad[i];
  }
}

////////////////////////////////////////////////////////////////
/// Hessian of discrete Lagrangian
///
/// TODO Model sparsity and symmetry is ignored.
///
void A0_4DVar_Weak_Ld::d2L_sparse_eval(const Real &t_n, const Real *x_n,
                                       const Real &t_np1, const Real *x_np1,
                                       Real *d2L_sparse) {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();
  // Observations
  const ObservationModel &obs = _data->observation_model();
  const Index nL = obs.num_observed_states();
  const IndexVec &observed_state_idxs = obs.observed_state_idxs();
  // TODO Add nonlinear observations

  // Measurement error
  const Real dt = t_np1 - t_n;
  for(Index iL = 0; iL < nL; ++iL) {
    Index idx_meas = observed_state_idxs[iL];
    d2L_sparse[idx_meas*(nD + 1)] += dt*_Rm_diag[iL]; // x_n block
  }

  // Rescale constraints by 1/dt
  const Real sf = 1/dt;

  // Model error: dg^T*Rf*dg term
  const Index num_dg_quad_non0s = _dg_quad_sparse.size();
  for(Index idx0 = 0; idx0 < num_dg_quad_non0s; ++idx0) {
    Index k = _dg_quad_sparse_idxs[0][idx0];
    for(Index idx1 = 0; idx1 < num_dg_quad_non0s; ++idx1) {
      if(k != _dg_quad_sparse_idxs[0][idx1]) {
        continue;
      }
      Index i = _dg_quad_sparse_idxs[1][idx0],
            j = _dg_quad_sparse_idxs[1][idx1];
      Real tmp = _dg_quad_sparse[idx0]*_Rf_diag[k]*_dg_quad_sparse[idx1];
      d2L_sparse[d2L_idxs_to_array(nD,i,j)] += sf*tmp;
    }
  }

  // cached
  //_quad->d2g_sparse_eval(*t_n,*t_np1,x_n,x_np1,&_d2g_quad_sparse.front());

  // Model error: d2g*Rf*g term
  // -- divide by dt since g_quad is integrated
  const Index num_d2g_quad_non0s = _d2g_quad_sparse.size();
  for(Index idx = 0; idx < num_d2g_quad_non0s; ++idx) {
    Index i = _d2g_quad_sparse_idxs[1][idx],
          j = _d2g_quad_sparse_idxs[2][idx],
          k = _d2g_quad_sparse_idxs[0][idx];
    Real tmp = _d2g_quad_sparse[idx]*_Rf_diag[k]*_g_quad[k];
    d2L_sparse[d2L_idxs_to_array(nD,i,j)] += sf*tmp;
  }
}

#endif /* A0_4DVAR_WEAK_LD_H_ */
