#ifndef ESTIMATION_ACTION_H_
#define ESTIMATION_ACTION_H_

// My includes
#include "Utils.h"
#include "EstimationData.h"
#include "TwinExperimentData.h"
#include "TrueExperimentData.h"

////////////////////////////////////////////////////////////////
///
class EstimationAction {
public:

  // Constructor
  EstimationAction();
  // Destructor
  virtual ~EstimationAction();
  // Class name
  virtual std::string classname() const = 0;

  // Initialize estimation data
  virtual void init_twin_data(const DynamicalModel &dyn_model,
                              const TimeDomain &time_domain,
                              const ObservationModel &obs_model,
                              TwinExperimentData::InitialData &init);
  virtual void init_true_data(const DynamicalModel &dyn_model,
                              const TimeDomain &time_domain,
                              const ObservationModel &obs_model,
                              TrueExperimentData::InitialData &init);

  // Problem information
  virtual Index num_variables() const = 0;
  virtual Index num_constraints() const
    { return 0; };
  virtual void variable_bounds(Real *x_lower, Real *x_upper) const = 0;
  virtual void constraint_bounds(Real *g_lower, Real *g_upper) const {};

  // Save path
  virtual void save_path(const Real *X);
  // Get initial path
  virtual void initial_path(Real *X) const;

  // Compute objective function
  virtual void f_eval(const Real *X, Real &f, bool new_X) = 0;
  virtual void df_eval(const Real *X, Real *df, bool new_X) {};
  virtual void d2f_sparse_eval(const Real *X, Real *d2f, bool new_X) {};

  // Compute dynamical constraints
  virtual void g_eval(const Real *X, Real *g, bool new_X) {};
  virtual void dg_sparse_eval(const Real *X, Real *dg, bool new_X) {};
  virtual void d2g_sparse_eval(const Real *X, Real *d2g, bool new_X) {};

  // Sparsity patterns
  virtual void d2f_sparse_idxs(IndexVec *sp_idxs) const {};
  virtual void  dg_sparse_idxs(IndexVec *sp_idxs) const {};
  virtual void d2g_sparse_idxs(IndexVec *sp_idxs) const {};

  // Overloaded functions for default new_X = true
  void f_eval(const Real *X, Real &f)
    { f_eval(X,f,true); }
  void df_eval(const Real *X, Real *df)
    { df_eval(X,df,true); };
  void d2f_sparse_eval(const Real *X, Real *d2f)
    { d2f_sparse_eval(X,d2f,true); }
  void g_eval(const Real *X, Real *g)
    { g_eval(X,g,true); }
  void dg_sparse_eval(const Real *X, Real *dg)
    { dg_sparse_eval(X,dg,true); }
  void d2g_sparse_eval(const Real *X, Real *d2g)
    { d2g_sparse_eval(X,d2g,true); }

  // Get methods
  const EstimationData* data() const
    { return _data; };

  // Set methods
  virtual void set_Rm_diag(const RealVec &Rm_diag) {};
  virtual void set_Rf_diag(const RealVec &Rf_diag) {};

  // Overloaded method to get separate contributions from Rm, Rf
  virtual void f_eval(const Real *X, Real &Rm_est,  Real &Rf_est,
                                     Real &Rm_pred, Real &Rf_pred) {};

protected:
  // Subclass dependent initialization
  virtual void initialize() {};

  // Estimation data
  EstimationData *_data;

private:
  EstimationAction(const EstimationAction&);
};

////////////////////////////////////////////////////////////////
/// constructor
EstimationAction::EstimationAction() :
  _data (NULL) {
}

/// destructor
EstimationAction::~EstimationAction() {
  delete _data; _data = NULL;
}

////////////////////////////////////////////////////////////////
/// save path
void EstimationAction::save_path(const Real *X) {
  // TODO Parameters
  _data->save_path(X,NULL);
}

////////////////////////////////////////////////////////////////
/// get initial path
void EstimationAction::initial_path(Real *x) const {
  const RealVec &X0 = _data->estimated_states();
  std::copy(X0.begin(),X0.end(),x);
}

////////////////////////////////////////////////////////////////
///
void EstimationAction::init_twin_data(const DynamicalModel &dyn_model,
                                      const TimeDomain &time_domain,
                                      const ObservationModel &obs_model,
                                      TwinExperimentData::InitialData &init) {
  // Initialize twin experiment data
  delete _data; _data = NULL;
  _data = new TwinExperimentData(dyn_model,time_domain,obs_model);
  _data->initialize(init);

  // Subclass dependent initialization
  initialize();
}

////////////////////////////////////////////////////////////////
///
void EstimationAction::init_true_data(const DynamicalModel &dyn_model,
                                      const TimeDomain &time_domain,
                                      const ObservationModel &obs_model,
                                      TrueExperimentData::InitialData &init) {
  // Initialize true experiment data
  delete _data; _data = NULL;
  _data = new TrueExperimentData(dyn_model,time_domain,obs_model);
  _data->initialize(init);

  // Subclass dependent initialization
  initialize();
}

#endif // ESTIMATION_ACTION_H_
