#ifndef FINITE_DIFF_DERIVS_H_
#define FINITE_DIFF_DERIVS_H_

// My includes
#include "Utils.h"

////////////////////////////////////////////////////////////////
///
template <class Functor>
class Centered_Finite_Diff {
public:
  Centered_Finite_Diff(Functor &functor) :
    _functor (functor), _dx (1.e-6) {
  };

  void df(const Real *x, Real *df) {
    Index dim = _functor.num_argin(),
          num_argout = _functor.num_argout();
    Real dx = _dx;
    RealVec x_pdxj(dim), f_pdxj(num_argout),
            x_mdxj(dim), f_mdxj(num_argout);
    for(Index  j = 0; j < dim; ++j) {
      std::copy(x,x+dim,x_pdxj.begin());
      std::copy(x,x+dim,x_mdxj.begin());
      Real dxj = dx,//*(1+std::fabs(x[j])),
            sf = 0.5/dxj;
      x_pdxj[j] += dx;
      x_mdxj[j] -= dx;
      _functor(&x_pdxj.front(),&f_pdxj.front());
      _functor(&x_mdxj.front(),&f_mdxj.front());
      for(Index i = 0; i < num_argout; ++i) {
        df[i*dim + j] = sf*(f_pdxj[i] - f_mdxj[i]);
      }
    }
  }

  void d2f(const Real *x, Real *d2f) {
    Index dim = _functor.num_argin(),
          num_argout = _functor.num_argout();
    Real dx = std::pow(_dx,1./2);
    RealVec x_pdxj_pdxk(dim), f_pdxj_pdxk(num_argout),
            x_mdxj_mdxk(dim), f_mdxj_mdxk(num_argout),
            x_pdxj_mdxk(dim), f_pdxj_mdxk(num_argout),
            x_mdxj_pdxk(dim), f_mdxj_pdxk(num_argout);
    for(Index  k = 0; k < dim; ++k) {
    for(Index  j = 0; j < dim; ++j) {
      Real dxj = dx,//*(1+std::fabs(x[j])),
           dxk = dx,//*(1+std::fabs(x[k])),
           sf = 0.25/(dxj*dxk);
      std::copy(x,x+dim,x_pdxj_pdxk.begin());
      std::copy(x,x+dim,x_pdxj_mdxk.begin());
      std::copy(x,x+dim,x_mdxj_pdxk.begin());
      std::copy(x,x+dim,x_mdxj_mdxk.begin());
      x_pdxj_pdxk[j] += dxj; x_pdxj_pdxk[k] += dxk;
      x_pdxj_mdxk[j] += dxj; x_pdxj_mdxk[k] -= dxk;
      x_mdxj_pdxk[j] -= dxj; x_mdxj_pdxk[k] += dxk;
      x_mdxj_mdxk[j] -= dxj; x_mdxj_mdxk[k] -= dxk;
      _functor(&x_pdxj_pdxk.front(),&f_pdxj_pdxk.front());
      _functor(&x_pdxj_mdxk.front(),&f_pdxj_mdxk.front());
      _functor(&x_mdxj_pdxk.front(),&f_mdxj_pdxk.front());
      _functor(&x_mdxj_mdxk.front(),&f_mdxj_mdxk.front());
      for(Index i = 0; i < num_argout; ++i) {
        d2f[i*dim*dim + j*dim + k] =
           sf*(f_pdxj_pdxk[i] + f_mdxj_mdxk[i] -
               f_mdxj_pdxk[i] - f_pdxj_mdxk[i]);
      }
    }
    }
  }

  void d3f(const Real *x, Real *d3f) {
    Index dim = _functor.num_argin(),
          num_argout = _functor.num_argout();
    Real dx = std::pow(_dx,1./3);
    RealVec x_pdxj_pdxk_pdxl(dim), f_pdxj_pdxk_pdxl(num_argout),
            x_pdxj_pdxk_mdxl(dim), f_pdxj_pdxk_mdxl(num_argout),
            x_mdxj_mdxk_pdxl(dim), f_mdxj_mdxk_pdxl(num_argout),
            x_mdxj_mdxk_mdxl(dim), f_mdxj_mdxk_mdxl(num_argout),
            x_pdxj_mdxk_pdxl(dim), f_pdxj_mdxk_pdxl(num_argout),
            x_pdxj_mdxk_mdxl(dim), f_pdxj_mdxk_mdxl(num_argout),
            x_mdxj_pdxk_pdxl(dim), f_mdxj_pdxk_pdxl(num_argout),
            x_mdxj_pdxk_mdxl(dim), f_mdxj_pdxk_mdxl(num_argout);
    for(Index  k = 0; k < dim; ++k) {
    for(Index  j = 0; j < dim; ++j) {
    for(Index  l = 0; l < dim; ++l) {
      Real dxj = dx,//*(1+std::fabs(x[j])),
           dxk = dx,//*(1+std::fabs(x[k])),
           dxl = dx,//*(1+std::fabs(x[l])),
           sf = 0.125/(dxj*dxk*dxl);
      std::copy(x,x+dim,x_pdxj_pdxk_pdxl.begin());
      std::copy(x,x+dim,x_pdxj_pdxk_mdxl.begin());
      std::copy(x,x+dim,x_pdxj_mdxk_pdxl.begin());
      std::copy(x,x+dim,x_pdxj_mdxk_mdxl.begin());
      std::copy(x,x+dim,x_mdxj_pdxk_pdxl.begin());
      std::copy(x,x+dim,x_mdxj_pdxk_mdxl.begin());
      std::copy(x,x+dim,x_mdxj_mdxk_pdxl.begin());
      std::copy(x,x+dim,x_mdxj_mdxk_mdxl.begin());
      x_pdxj_pdxk_pdxl[j] += dxj; x_pdxj_pdxk_pdxl[k] += dxk; x_pdxj_pdxk_pdxl[l] += dxl;
      x_pdxj_pdxk_mdxl[j] += dxj; x_pdxj_pdxk_mdxl[k] += dxk; x_pdxj_pdxk_mdxl[l] -= dxl;
      x_pdxj_mdxk_pdxl[j] += dxj; x_pdxj_mdxk_pdxl[k] -= dxk; x_pdxj_mdxk_pdxl[l] += dxl;
      x_pdxj_mdxk_mdxl[j] += dxj; x_pdxj_mdxk_mdxl[k] -= dxk; x_pdxj_mdxk_mdxl[l] -= dxl;
      x_mdxj_pdxk_pdxl[j] -= dxj; x_mdxj_pdxk_pdxl[k] += dxk; x_mdxj_pdxk_pdxl[l] += dxl;
      x_mdxj_pdxk_mdxl[j] -= dxj; x_mdxj_pdxk_mdxl[k] += dxk; x_mdxj_pdxk_mdxl[l] -= dxl;
      x_mdxj_mdxk_pdxl[j] -= dxj; x_mdxj_mdxk_pdxl[k] -= dxk; x_mdxj_mdxk_pdxl[l] += dxl;
      x_mdxj_mdxk_mdxl[j] -= dxj; x_mdxj_mdxk_mdxl[k] -= dxk; x_mdxj_mdxk_mdxl[l] -= dxl;
      _functor(&x_pdxj_pdxk_pdxl.front(),&f_pdxj_pdxk_pdxl.front());
      _functor(&x_pdxj_pdxk_mdxl.front(),&f_pdxj_pdxk_mdxl.front());
      _functor(&x_pdxj_mdxk_pdxl.front(),&f_pdxj_mdxk_pdxl.front());
      _functor(&x_pdxj_mdxk_mdxl.front(),&f_pdxj_mdxk_mdxl.front());
      _functor(&x_mdxj_pdxk_pdxl.front(),&f_mdxj_pdxk_pdxl.front());
      _functor(&x_mdxj_pdxk_mdxl.front(),&f_mdxj_pdxk_mdxl.front());
      _functor(&x_mdxj_mdxk_pdxl.front(),&f_mdxj_mdxk_pdxl.front());
      _functor(&x_mdxj_mdxk_mdxl.front(),&f_mdxj_mdxk_mdxl.front());
      for(Index i = 0; i < num_argout; ++i) {
        d3f[i*dim*dim*dim + j*dim*dim + k*dim + l] =
           sf*(f_pdxj_pdxk_pdxl[i] + f_pdxj_mdxk_mdxl[i] +
               f_mdxj_pdxk_mdxl[i] + f_mdxj_mdxk_pdxl[i] -
               f_mdxj_mdxk_mdxl[i] - f_mdxj_pdxk_pdxl[i] -
               f_pdxj_mdxk_pdxl[i] - f_pdxj_pdxk_mdxl[i]);
      }
    }
    }
    }
  }

private:
  Functor &_functor;
  Real _dx;
};

////////////////////////////////////////////////////////////////
///
template <class Functor>
class One_Sided_Finite_Diff {
public:
  One_Sided_Finite_Diff(Functor &functor) :
    _functor (functor), _dx (1.e-6) {
  };

  void df(const Real *x, Real *df) {
    Index  dim = _functor.num_argin(),
           num_argout = _functor.num_argout();
    Real dx = _dx, sf = 1./dx;
    RealVec x_pdxj(dim),
            f_pdxj(num_argout),
            f(num_argout);
    _functor(x,&f.front());
    for(Index  j = 0; j < dim; ++j) {
      std::copy(x,x+dim,x_pdxj.begin());
      x_pdxj[j] += dx;
      _functor(&x_pdxj.front(),&f_pdxj.front());
      for(Index i = 0; i < num_argout; ++i) {
        df[i*num_argout + j] = sf*(f_pdxj[i] - f[i]);
      }
    }
  }

  void d2f(const Real *x, Real *d2f) {
    Index  dim = _functor.num_argin(),
           num_argout = _functor.num_argout();
    Real dx = _dx, sf = 1./(dx*dx);
    RealVec x_pdxk_pdxj(dim), x_pdxk(dim), x_pdxj(dim),
            f_pdxk_pdxj(num_argout), f_pdxk(num_argout),
            f_pdxj(num_argout), f(num_argout);
    _functor(x,&f.front());
    for(Index  k = 0; k < dim; ++k) {
      for(Index  j = 0; j < dim; ++j) {
        std::copy(x,x+dim,x_pdxk_pdxj.begin());
        std::copy(x,x+dim,x_pdxk.begin());
        std::copy(x,x+dim,x_pdxj.begin());
        x_pdxk_pdxj[k] += dx;
        x_pdxk_pdxj[j] += dx;
        x_pdxk[k] += dx;
        x_pdxj[j] += dx;
        _functor(&x_pdxk_pdxj.front(),&f_pdxk_pdxj.front());
        _functor(&x_pdxk.front(),&f_pdxk.front());
        _functor(&x_pdxj.front(),&f_pdxj.front());
        for(Index i = 0; i < num_argout; ++i) {
          d2f[i*dim*num_argout + j*dim + k] =
           sf*(f_pdxk_pdxj[i] + f[i] - f_pdxj[i] - f_pdxk[i]);
        }
      }
    }
  }

private:
  Functor &_functor;
  Real _dx;
};

#endif /* FINITEDIFFDERIVS_H_ */
