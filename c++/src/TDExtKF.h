#ifndef TD_EXT_KF_H_
#define TD_EXT_KF_H_

// My includes
#include "Utils.h"
#include "CovarianceEqn.h"
#include "TrueExperimentData.h"
#include "TimeDelayObs.h"

////////////////////////////////////////////////////////////////
/// 
template <class Model>
class TDExtKF {
public:
  
  // Typedefs
  typedef TrueExperimentData<Model> Data;

  // Constructors
  TDExtKF(Data &data, const IntVec &tdIdxs);
  
  // Run filter
  void run();

private: 
  // Filter data
  Data &_data;
  // Time delay observation operator
  TimeDelayObs<Model> _tdObs;
  // Covariance equation
  CovarianceEqn<Model> _covEqn;
  // Measurement error 
  RealVec _RmDiag;
  // Mean and inverse covariance
  RealVec _X;
  // Max. number of inner iterations
  uint _maxIters;
  // Inner iteration convergence tolerance
  Real _iterConvTol;
  // Enforce bounds
  bool _checkBounds;
};

////////////////////////////////////////////////////////////////
/// constructors
template <class Model>
TDExtKF<Model>::TDExtKF(Data &data, const IntVec &tdIdxs) : 
  _data (data), _covEqn (data), _tdObs(data),
  _maxIters (1), _iterConvTol (1.e-2), _checkBounds (false) {
  
  // Initialize time delay observations
  _tdObs.setIdxs(tdIdxs);

  // Measurement error
  uint nLM = _tdObs.nL();
  _RmDiag.resize(nLM,1.e0);
  
  // Initial mean/covariance
  uint nD = _data->model().nD();
       //nP = _data->model().nP(),
       //nDP = nD + nP;
  const RealVec &xEst (_data->xEst());
  _X.resize(_covEqn.dim(),0.);
  for(uint i = 0; i < nD; ++i) {
    _X[i] = xEst[i];
    _X[nD + i*nD + i] = 1.e0;
  }
}

////////////////////////////////////////////////////////////////
template <class Model>
void TDExtKF<Model>::run() {
  Real sum;

  // Dimensions
  const uint nD  = _data->model().nD(),
             nP  = _data->model().nP(),
             nDP = nD + nP,
             nL  = _data->obsModel().nL(),
             nLM = _tdObs.nL();

  // Time domain 
  const TimeDomain &timeDomain (_data->timeDomain());
  uint nN = timeDomain.nN();
  Real dt = timeDomain.dt();
  RealVec tVals;
  timeDomain.tVals(tVals);
  
  // Initialize time delayed observations
  RealVec S(nLM,0.), Y(nLM,0),
          dHdx(nLM*nD,0.), dHdp(nLM*nP,0);

  // Mask to skip embedding points at endpoints
  std::vector<char> mask(nLM,0);

  // Initialize Kalman gain
  uint maxDim = std::max(nDP,nLM);
  RealVec K(maxDim*nLM,0);

  // Temporary workspace 
  RealVec work(maxDim*maxDim,0);

  // Perturbations in S and X space
  RealVec deltaS(nLM,0), deltaX(nDP,0);

  // Create pseudoinverse module
  Pseudoinverse pInv(nLM,nLM);

  // Pointer to mean/inverse covariance
  Real *X = &_X.front();

  // Current best mean/covariance
  RealVec hatX(_X.size(),0.);
  
  // Run time-delay ExtKF
  for(uint n = 1; n < nN; ++n) {
    // Current estimate time
    Real t = tVals[n-1];
    
    // Iterated measurement update
    std::copy(_X.begin(),_X.end(),hatX.begin());
    for(uint iter = 0; iter < _maxIters; ++iter) {

      // Get time delayed observations
      _tdObs.h(n-1,&S.front(),&dHdx.front(),&dHdp.front());
      _tdObs.y(n-1,&Y.front(),&mask.front());
      
      // Construct Kalman gain
      // K = dHdx*invP*dHdx^T + Rm
      for(uint i = 0; i < nLM; ++i) {
        for(uint j = 0; j < nLM; ++j) {
          sum = 0;
          for(uint k = 0; k < nDP; ++k) {
            for(uint l = 0; l < nDP; ++l) {
              sum += dHdx[i*nDP + k]*X[(k+1)*nDP + l]*dHdx[j*nDP + l];
            }
          }
          K[i*nLM + j] = sum;
        }
        // TODO This can be more efficient
        if(mask[i]) {
          K[i*nLM + i] += _RmDiag[i];
        }
      }
      // Pseudoinverse
      pInv(&K.front(),&work.front());
      // K = Pinv*dHdx^T*Kinv
      for(uint i = 0; i < nDP; ++i) {
        for(uint j = 0; j < nLM; ++j) {
          sum = 0;
          for(uint k = 0; k < nDP; ++k) {
            for(uint l = 0; l < nLM; ++l) {
              sum += X[(i+1)*nDP + k]*dHdx[l*nDP + k]*work[l*nLM + j];
            }
          }
          K[i*nLM + j] = sum;
        }
      }
      
      // Update states/parameters
      for(uint i = 0; i < nLM; ++i) {
        sum = 0;
        for(uint j = 0; j < nDP; ++j) {
          sum += dHdx[i*nD + j]*(hatX[j] - X[j]);
        }
        deltaS[i] = Y[i] - S[i] - sum;
      }
      Real res = 0; // residual
      for(uint i = 0; i < nDP; ++i) {
        sum = 0;
        for(uint j = 0; j < nLM; ++j) {
          sum += K[i*nLM + j]*deltaS[j];
        }
        res += sum*sum;
        X[i] = hatX[i] + sum;
      }
      
      // TODO Check bounds

      // Check for convergence
      sum = 0;
      for(uint i = 0; i < nDP; ++i) {
        sum += X[i]*X[i];
      }
      res = std::sqrt(res/sum)/((double)nDP);
      if(res < _iterConvTol) {
        break;
      }
    }

    // Update covariance
    std::copy(X+nDP,X+nDP*(1+nDP),work.begin());
    for(uint i = 0; i < nDP; ++i) {
      for(uint j = 0; j < nDP; ++j) {
        sum = 0;
        for(uint k = 0; k < nLM; ++k) {
          for(uint l = 0; l < nDP; ++l) {
            sum += K[i*nLM + k]*dHdx[k*nDP + l]*work[l*nDP + j];
          }
        }
        _X[(i+1)*nDP + j] -= sum;
      }
    }

    // Time update
    _covEqn.step(t,X,dt,X);

    // Save state/parameter estimates
    _data->save(n,X,X+nD);
  }
}

#endif // TD_EXT_KF_H_
