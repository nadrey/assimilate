#ifndef NAKL_MODEL_H_
#define NAKL_MODEL_H_

// My includes
#include "Utils.h"
#include "DynamicalModel.h"
#include "ODEStepper.h"

////////////////////////////////////////////////////////////////
/// Hodgkin-Huxley NaKL Model
///
///  C*dV/dt = I_Na(V,m,h) + I_K(V,n) + I_L(g,V) + I_inj(t)
///  dm/dt = (m_inf(V,...) - m)/m_tau(V,...)
///  dh/dt = (h_inf(V,...) - h)/h_tau(V,...)
///  dn/dt = (n_inf(V,...) - n)/n_tau(V,...)
///
///  Ionic currents:
///    I_Na(V,m,h) = g_Na*m^3*h*(V_Na-V)
///    I_K(V,n) = g_K*n^4*(V_K-V)
///    I_L(V) = g_L*(V_L-V)
///
///  Gating variables for a = {m,h,n}:
///    x_inf(V,theta_a,sigma_a) = 0.5*(1.0 + tanh[(V-theta_a)/sigma_a])
///    x_tau(V,t0_x,t1_x,theta_a,sigma_a) = t0_x + t1_x*(1.0 - tanh[(V-theta_a)/sigma_a]^2)
///
class NaKLModel : public DynamicalModel {
public:
  NaKLModel();

  // Get methods
  virtual Index num_states() const;
  virtual Index num_params() const;
  virtual Index num_derivs() const;
  virtual void state_bounds(Real *x_lower, Real *x_upper) const;
  virtual void param_bounds(Real *p_lower, Real *p_upper) const;

  // Vector field evaluations
  void f_eval(const Real &t, const Real *x, Real *dxdt) const;
  void df_sparse_eval(const Real &t, const Real *x, Real *df) const;
  void d2f_sparse_eval(const Real &t, const Real *x, Real *d2f) const;
  void d3f_sparse_eval(const Real &t, const Real *x, Real *d3f) const;

  // Sparsity patterns
  virtual void df_sparse_idxs(IndexVec *df_sparse_idxs) const;
  virtual void d2f_sparse_idxs(IndexVec *d2f_sparse_idxs) const;
  virtual void d3f_sparse_idxs(IndexVec *d3f_sparse_idxs) const;

  // Random initial conditions
  virtual void random_state(Real *x) const;
  //virtual void random_param(Real *p) const;

private:
  // Initialize model
  void initialize();

  // Injected current
  Real injected_current(Real t) const;

  // Parameters
  Real _params[19];
  // Parameter aliases
  Real &_gL, &_gNa, &_gK,
       &_vL, &_vNa, &_vK,
       &_theta_m, &_theta_h, &_theta_n,
       &_sigma_m, &_sigma_h, &_sigma_n,
       &_tau0_m, &_tau0_h, &_tau0_n,
       &_tau1_m, &_tau1_h, &_tau1_n,
       &_membrane_capacitance;
};

////////////////////////////////////////////////////////////////
/// constructor
NaKLModel::NaKLModel() : DynamicalModel(),
  _gL (_params[0]), _gNa (_params[1]), _gK (_params[2]),
  _vL (_params[3]), _vNa (_params[4]), _vK (_params[5]),
  _theta_m (_params[6]), _theta_h (_params[7]),  _theta_n (_params[8]),
  _sigma_m (_params[9]), _sigma_h (_params[10]), _sigma_n (_params[11]),
  _tau0_m (_params[12]), _tau0_h (_params[13]), _tau0_n (_params[14]),
  _tau1_m (_params[15]), _tau1_h (_params[16]), _tau1_n (_params[17]),
  _membrane_capacitance (_params[18]) {
  initialize();
}

////////////////////////////////////////////////////////////////
/// Get methods
Index NaKLModel::num_states() const {
  return 4;
}
///
Index NaKLModel::num_params() const {
  return 0;
}
///
Index NaKLModel::num_derivs() const {
  return 3;
}
///
void NaKLModel::state_bounds(Real *x_lower, Real *x_upper) const {
  x_lower[0] = -200; x_upper[0] = +50;
  x_lower[1] = 0;    x_upper[1] = 1;
  x_lower[2] = 0;    x_upper[2] = 1;
  x_lower[3] = 0;    x_upper[3] = 1;
}
///
void NaKLModel::param_bounds(Real *p_lower, Real *p_upper) const {
}

////////////////////////////////////////////////////////////////
void NaKLModel::initialize() {
  // Default parameter values
  // Ion channel conductances (nS)
  _gL  = 0.003;
  _gNa = 1.2;
  _gK  = 0.20;
  // Reversal potentials (mV)
  _vL = -54.0; // -67.  #-50.  #-80.4
  _vNa = 50.0;
  _vK = -77.0;
  // Gating parameters (mV)
  _theta_m = -40.0;
  _theta_h = -60.0;
  _theta_n = -55.0;
  _sigma_m =  16.0;  // 15.0
  _sigma_h = -16.0;  //-15.0
  _sigma_n =  25.0;  //30.  #5.
  // Time constants (milli-seconds)
  _tau0_m = 0.1;
  _tau0_h = 1.0;
  _tau0_n = 1.0;
  _tau1_m = 0.4;
  _tau1_h = 7.0;
  _tau1_n = 5.0;
  // Membrance capacitance
  _membrane_capacitance = 0.01;
}

////////////////////////////////////////////////////////////////
/// returns resting state of neuron
void NaKLModel::random_state(Real *x) const {
  const Index nD = num_states();
  RealVec x_lower(nD), x_upper(nD);
  state_bounds(&x_lower.front(),&x_upper.front());
  for(Index iD = 0; iD < nD; ++iD) {
    x[iD] = (x_upper[iD]-x_lower[iD])*(rand_real()-0.5) + x_lower[iD];
  }

  // Integrate to remove transients
  std::ofstream out;
  Index  nNp1 = 10001; Real dt = 0.01;
  RK4Step<NaKLModel> ode(*this,this->num_states());
  for(Index iN = 0; iN < nNp1; ++iN) {
    ode.step(-1,x,dt,x);
  }
}

////////////////////////////////////////////////////////////////
/// Injected current
Real NaKLModel::injected_current(Real t) const {
  if(t > 0) {
    return 1;
  }
  else {
    return 0;
  }
}

////////////////////////////////////////////////////////////////
/// evaluate vector field at x(t)
///
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) dxdt (out) - output vector field
///
void NaKLModel::f_eval(const Real &t, const Real *x, Real *dxdt) const {
  const Real &V = x[0], &m = x[1], &h = x[2], &n = x[3];

  // Membrane potential
  Real I_Na = _gNa*m*m*m*h*(_vNa-V),
       I_K  = _gK*n*n*n*n*(_vK-V),
       I_L  = _gL*(_vL-V),
       I_inj = injected_current(t),
       inv_c = 1./_membrane_capacitance;
  dxdt[0] = (I_Na + I_K + I_L + I_inj)*inv_c;

  // Gating variables
  for(Index idx = 0; idx < 3; ++idx) {
    Real theta_a = _params[6+idx],
         sigma_a = _params[9+idx],
          tau0_a = _params[12+idx],
          tau1_a = _params[15+idx];
    Real tanh = std::tanh((V-theta_a)/sigma_a),
         x_inf = 0.5*(1+tanh),
         x_tau = tau0_a + tau1_a*(1-tanh*tanh);
    dxdt[idx+1] = (x_inf - x[idx+1])/x_tau;
  }
}

////////////////////////////////////////////////////////////////
/// evaluate sparse Jacobian of vector field df_i/dx_j
///
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) df (out) - output Jacobian nonzero elements in row-major order
///
void NaKLModel::df_sparse_eval(const Real &t, const Real *x, Real *df) const {
  const Real &V = x[0], &m = x[1], &h = x[2], &n = x[3];

  // Membrane potential
  const Real inv_c = 1./_membrane_capacitance;
  df[0] = -(_gNa*m*m*m*h + _gK*n*n*n*n + _gL)*inv_c;
  df[1] = _gNa*3*m*m*h*(_vNa-V)*inv_c;
  df[2] = _gNa*m*m*m*(_vNa-V)*inv_c;
  df[3] = _gK*4*n*n*n*(_vK-V)*inv_c;

  // Gating variables
  for(Index idx = 0; idx < 3; ++idx) {
    Real       a = x[idx+1],
         theta_a = _params[6+idx],
         sigma_a = _params[9+idx],
          tau0_a = _params[12+idx],
          tau1_a = _params[15+idx];
    Real VV = (V-theta_a)/sigma_a,
         tanh = std::tanh(VV),
         cosh = std::cosh(VV),
         sech = 1./cosh,
         sech2 = sech*sech,
         tmp = 1./(tau0_a + tau1_a*sech2);
    // dV
    Index offset = 4 + 2*idx;
    df[offset] = (tau0_a + tau1_a*(1 + tanh*(2-4*a+tanh)))
                   *(0.5/sigma_a)*sech2*tmp*tmp;
    // da
    df[offset + 1] = -tmp;
  }
}

////////////////////////////////////////////////////////////////
/// get sparsity structure of vector field Jacobian in matrix format
///
/// Parameters:
///  1) idxs[2] - i,j indices of nonzero elements
///
void NaKLModel::df_sparse_idxs(IndexVec *idxs) const {
  const Index num_elems = 10;
  idxs[0].resize(num_elems);
  idxs[1].resize(num_elems);

  Index idx = 0;
  for(Index j = 0; j < 4; ++j) {
    idxs[0][idx] = 0; idxs[1][idx] = j; ++idx;
  }
  for(Index i = 1; i < 4; ++i) {
    idxs[0][idx] = i; idxs[1][idx] = 0; ++idx;
    idxs[0][idx] = i; idxs[1][idx] = i; ++idx;
  }
}

////////////////////////////////////////////////////////////////
/// evaluate sparse Hessian of vector field d2f_i / dx_j dx_k
///
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) d2f (out) - output Hessian nonzero elements in row-major order
///
/// TODO Utilize symmetry
///
void NaKLModel::d2f_sparse_eval(const Real &t, const Real *x, Real *d2f) const {
  const Real &V = x[0], &m = x[1], &h = x[2], &n = x[3];

  // Membrane potential
  const Real inv_c = 1./_membrane_capacitance;
  d2f[0] = -3*_gNa*m*m*h*inv_c;  // dV dm
  d2f[1] =   -_gNa*m*m*m*inv_c;  // dV dh
  d2f[2] =  -4*_gK*n*n*n*inv_c;  // dV dn
  d2f[3] = d2f[0]; // dm dV
  d2f[4] = _gNa*6*m*h*(_vNa-V)*inv_c; // dm dm
  d2f[5] = _gNa*3*m*m*(_vNa-V)*inv_c; // dm dh
  d2f[6] = d2f[1]; // dh dV
  d2f[7] = d2f[5]; // dh dm
  d2f[8] = d2f[2]; // dn dV
  d2f[9] = _gK*12*n*n*(_vK-V)*inv_c; // dn*dn

  // Gating variables
  for(Index idx = 0; idx < 3; ++idx) {
    Real       a = x[idx+1],
         theta_a = _params[6+idx],
         sigma_a = _params[9+idx],
          tau0_a = _params[12+idx],
          tau1_a = _params[15+idx];
    Real VV = (V-theta_a)/sigma_a,
         tanh = std::tanh(VV),
         cosh = std::cosh(VV),
         sech = 1./cosh,
         sech2 = sech*sech,
         sech4 = sech2*sech2,
         tmp = 1./(tau0_a + tau1_a*sech2);
    // dV dV
    Index offset = 10 + 3*idx;
    d2f[offset] = ((-1+2*a)*sech4*tau1_a*tau1_a -
                     sech2*(-1+2*a-tanh)*tau1_a*(3*tau0_a+2*tau1_a) -
                       tau0_a*(2*tau1_a-4*a*tau1_a+tanh*(tau0_a + 2*tau1_a)))
                         *sech2*tmp*tmp*tmp/sigma_a/sigma_a;
    // dV da
    d2f[offset + 1] = -2*sech*sech*tanh*tau1_a*tmp*tmp/sigma_a;
    // da dV
    d2f[offset + 2] = d2f[offset + 1];
  }
}

////////////////////////////////////////////////////////////////
/// get sparsity structure of vector field Hessian in 3 tensor format
///
/// Parameters:
///  1) idxs[3]  (out) - i,j,k indices of nonzero elements
///
void NaKLModel::d2f_sparse_idxs(IndexVec *idxs) const {
  const Index num_elems = 19;
  idxs[0].resize(num_elems);
  idxs[1].resize(num_elems);
  idxs[2].resize(num_elems);

  // Membrane potential
  Index idx = 0;
  idxs[0][idx] = 0; idxs[1][idx] = 0; idxs[2][idx] = 1; ++idx; // dV dm
  idxs[0][idx] = 0; idxs[1][idx] = 0; idxs[2][idx] = 2; ++idx; // dV dh
  idxs[0][idx] = 0; idxs[1][idx] = 0; idxs[2][idx] = 3; ++idx; // dV dn
  idxs[0][idx] = 0; idxs[1][idx] = 1; idxs[2][idx] = 0; ++idx; // dm dV --
  idxs[0][idx] = 0; idxs[1][idx] = 1; idxs[2][idx] = 1; ++idx; // dm dm
  idxs[0][idx] = 0; idxs[1][idx] = 1; idxs[2][idx] = 2; ++idx; // dm dh
  idxs[0][idx] = 0; idxs[1][idx] = 2; idxs[2][idx] = 0; ++idx; // dh dV --
  idxs[0][idx] = 0; idxs[1][idx] = 2; idxs[2][idx] = 1; ++idx; // dh dm --
  idxs[0][idx] = 0; idxs[1][idx] = 3; idxs[2][idx] = 0; ++idx; // dn dV --
  idxs[0][idx] = 0; idxs[1][idx] = 3; idxs[2][idx] = 3; ++idx; // dn dn

  // Gating variables
  for(Index i = 1; i < 4; ++i) {
    idxs[0][idx] = i; idxs[1][idx] = 0; idxs[2][idx] = 0; ++idx; // dV dV
    idxs[0][idx] = i; idxs[1][idx] = 0; idxs[2][idx] = i; ++idx; // dV da
    idxs[0][idx] = i; idxs[1][idx] = i; idxs[2][idx] = 0; ++idx; // da dV --
  }
}

////////////////////////////////////////////////////////////////
/// evaluate sparse 3rd derivative of vector field
///   d3f_i / dx_j dx_k dx_l
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) d3f (out) - output d3f nonzero elements in row-major order
///
/// TODO Utilize symmetry
///
void NaKLModel::d3f_sparse_eval(const Real &t, const Real *x, Real *d3f) const {
  const Real &V = x[0], &m = x[1], &h = x[2], &n = x[3];

  // Membrane potential
  const Real inv_c = 1./_membrane_capacitance;
  d3f[0]  = -6*_gNa*m*h*inv_c;  // dV dm dm
  d3f[1]  = -3*_gNa*m*m*inv_c;  // dV dm dh
  d3f[2]  = d3f[1]; // dV dh dm
  d3f[3]  =  -12*_gK*n*n*inv_c;  // dV dn dn
  d3f[4]  = d3f[0]; // dm dV dm
  d3f[5]  = d3f[1]; // dm dV dh
  d3f[6]  = d3f[0]; // dm dm dV
  d3f[7]  = _gNa*6*h*(_vNa-V)*inv_c;  // dm dm dm
  d3f[8]  = _gNa*6*m*(_vNa-V)*inv_c;  // dm dm dh
  d3f[9]  = d3f[1]; // dm dh dV
  d3f[10] = d3f[8]; // dm dh dm
  d3f[11] = d3f[1]; // dh dV dm
  d3f[12] = d3f[1]; // dh dm dV
  d3f[13] = d3f[8]; // dh dm dm
  d3f[14] = d3f[3]; // dn dV dn
  d3f[15] = d3f[3]; // dn dn dV
  d3f[16] = _gK*24*n*(_vK-V)*inv_c; // dn dn dn

  // Gating variables
  for(Index idx = 0; idx < 3; ++idx) {
    const Real a = x[idx+1],
         theta_a = _params[6+idx],
         sigma_a = _params[9+idx],
          tau0_a = _params[12+idx],
          tau1_a = _params[15+idx],
         tau0_a2 = tau0_a*tau0_a,
         tau1_a2 = tau1_a*tau1_a,
         VV = (V-theta_a)/sigma_a,
         tanh = std::tanh(VV),
         cosh = std::cosh(VV),
         sech = 1./cosh,
         sech2 = sech*sech,
         sech4 = sech2*sech2,
         sech6 = sech2*sech4,
         tmp = 1./(tau0_a + tau1_a*sech*sech);
    // dV dV dV
    Index offset = 17 + 4*idx;
    d3f[offset] = (2*tau0_a2*(tau0_a + 2*tau1_a) -
                   sech6*tau1_a2*(3*tau0_a + 2*tau1_a) +
                   2*sech4*tau1_a*(tau0_a + tau1_a)*(9*tau0_a + 2*tau1_a) -
                   sech2*tau0_a*(3*tau0_a2 + 22*tau0_a*tau1_a + 16*tau1_a2) -
                   4*(-1 + 2*a)*tanh*tau1_a*
                     (tau0_a2 + sech4*tau1_a*(3*tau0_a + tau1_a) -
                      sech2*tau0_a*(3*tau0_a + 4*tau1_a)))
                   *sech2*tmp*tmp*tmp*tmp/sigma_a/sigma_a/sigma_a;
    // dV dV da
    d3f[offset + 1] = tau1_a*(-2*(tau0_a + 2*tau1_a)*std::cosh(2*VV)
                        + tau0_a*(-3 + std::cosh(4*VV)))
                          *0.5*sech6*tmp*tmp*tmp/sigma_a/sigma_a;
    // dV da dV
    d3f[offset + 2] = d3f[offset + 1];
    // da dV dV
    d3f[offset + 3] = d3f[offset + 1];
  }
}

////////////////////////////////////////////////////////////////
/// get sparsity structure of vector field d3f in 3 tensor format
///
/// Parameters:
///  1) idxs[4]  (out) - i,j,k,l indices of nonzero elements
///
void NaKLModel::d3f_sparse_idxs(IndexVec *idxs) const {
  const Index num_elems = 29;
  idxs[0].resize(num_elems);
  idxs[1].resize(num_elems);
  idxs[2].resize(num_elems);
  idxs[3].resize(num_elems);

  // Membrane potential
  Index idx = 0;
  idxs[0][idx] = 0; idxs[1][idx] = 0; idxs[2][idx] = 1; idxs[3][idx] = 1; ++idx; // dV dm dm
  idxs[0][idx] = 0; idxs[1][idx] = 0; idxs[2][idx] = 1; idxs[3][idx] = 2; ++idx; // dV dm dh
  idxs[0][idx] = 0; idxs[1][idx] = 0; idxs[2][idx] = 2; idxs[3][idx] = 1; ++idx; // dV dh dm --
  idxs[0][idx] = 0; idxs[1][idx] = 0; idxs[2][idx] = 3; idxs[3][idx] = 3; ++idx; // dV dn dn
  idxs[0][idx] = 0; idxs[1][idx] = 1; idxs[2][idx] = 0; idxs[3][idx] = 1; ++idx; // dm dV dm --
  idxs[0][idx] = 0; idxs[1][idx] = 1; idxs[2][idx] = 0; idxs[3][idx] = 2; ++idx; // dm dV dh --
  idxs[0][idx] = 0; idxs[1][idx] = 1; idxs[2][idx] = 1; idxs[3][idx] = 0; ++idx; // dm dm dV --
  idxs[0][idx] = 0; idxs[1][idx] = 1; idxs[2][idx] = 1; idxs[3][idx] = 1; ++idx; // dm dm dm
  idxs[0][idx] = 0; idxs[1][idx] = 1; idxs[2][idx] = 1; idxs[3][idx] = 2; ++idx; // dm dm dh
  idxs[0][idx] = 0; idxs[1][idx] = 1; idxs[2][idx] = 2; idxs[3][idx] = 0; ++idx; // dm dh dV --
  idxs[0][idx] = 0; idxs[1][idx] = 1; idxs[2][idx] = 2; idxs[3][idx] = 1; ++idx; // dm dh dm --
  idxs[0][idx] = 0; idxs[1][idx] = 2; idxs[2][idx] = 0; idxs[3][idx] = 1; ++idx; // dh dV dm --
  idxs[0][idx] = 0; idxs[1][idx] = 2; idxs[2][idx] = 1; idxs[3][idx] = 0; ++idx; // dh dm dV --
  idxs[0][idx] = 0; idxs[1][idx] = 2; idxs[2][idx] = 1; idxs[3][idx] = 1; ++idx; // dh dm dm --
  idxs[0][idx] = 0; idxs[1][idx] = 3; idxs[2][idx] = 0; idxs[3][idx] = 3; ++idx; // dn dV dn --
  idxs[0][idx] = 0; idxs[1][idx] = 3; idxs[2][idx] = 3; idxs[3][idx] = 0; ++idx; // dn dn dV --
  idxs[0][idx] = 0; idxs[1][idx] = 3; idxs[2][idx] = 3; idxs[3][idx] = 3; ++idx; // dn dn dn

  // Gating variables
  for(Index i = 1; i < 4; ++i) {
    idxs[0][idx] = i; idxs[1][idx] = 0; idxs[2][idx] = 0; idxs[3][idx] = 0; ++idx; // dV dV dV
    idxs[0][idx] = i; idxs[1][idx] = 0; idxs[2][idx] = 0; idxs[3][idx] = i; ++idx; // dV dV da
    idxs[0][idx] = i; idxs[1][idx] = 0; idxs[2][idx] = i; idxs[3][idx] = 0; ++idx; // dV da dV --
    idxs[0][idx] = i; idxs[1][idx] = i; idxs[2][idx] = 0; idxs[3][idx] = 0; ++idx; // da dV dV --
  }
}

#endif /* NAKL_MODEL_H_ */
