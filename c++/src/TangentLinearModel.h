#ifndef TANGENT_LINEAR_MODEL_H_
#define TANGENT_LINEAR_MODEL_H_

// My includes
#include "Utils.h"

////////////////////////////////////////////////////////////////
/// 
template <class Model>
class TangentLinearModel {
public:

  // Constructors
  TangentLinearModel(const Model &model);

  // Var. eqn evaluation
  void f(const Real &t, const Real *x, Real *dxdt) const;

  // Get methods
  uint dim() const;

private:
  // Model reference
  const Model &_model;
  // Order of approximation
  uint _order;
  // Pointers to inner rows for fast multiplication
  UIntVec _df_ptrs[1], _d2f_ptrs[2], _d3f_ptrs[3];
  // Temporary storage
  mutable RealVec _df_sparse, _d2f_sparse, _d3f_sparse;
};

////////////////////////////////////////////////////////////////
template <class Model>
TangentLinearModel<Model>::TangentLinearModel(const Model &model) :
  _model (model), _order (1) {

  // Get sparse structure of model derivatives
  const UIntVec *idxs_df_sparse  (model.df_sparse_idxs()),
                *idxs_d2f_sparse (model.d2f_sparse_idxs()),
                *idxs_d3f_sparse (model.d3f_sparse_idxs());

  // Space for vector field derivatives
  _df_sparse.resize(idxs_df_sparse[0].size());
  _d2f_sparse.resize(idxs_d2f_sparse[0].size());
  _d3f_sparse.resize(idxs_d3f_sparse[0].size());

  // Get pointers to inner rows for fast multiplication
  uint nD = _model.dim(),
       nRows[4] = {nD,nD,nD,nD};
  inner_row_ptrs(idxs_df_sparse ,2,nRows,_df_ptrs[0]);
  inner_row_ptrs(idxs_d2f_sparse,2,nRows,_d2f_ptrs[0]);
  inner_row_ptrs(idxs_d2f_sparse,3,nRows,_d2f_ptrs[1]);
  inner_row_ptrs(idxs_d3f_sparse,2,nRows,_d3f_ptrs[0]);
  inner_row_ptrs(idxs_d3f_sparse,3,nRows,_d3f_ptrs[1]);
  inner_row_ptrs(idxs_d3f_sparse,4,nRows,_d3f_ptrs[2]);
}

////////////////////////////////////////////////////////////////
/// 
template <class Model>
uint TangentLinearModel<Model>::dim() const {
  uint nD = _model.nD(),
       ret = 1;
  for(uint i = 0; i < _order; ++i) {
    ret = ret*nD + 1;
  }
  ret *= nD;
  return ret;
}

////////////////////////////////////////////////////////////////
///
template <class Model>
void TangentLinearModel<Model>::f(const Real &t, 
                                  const Real *x, 
                                  Real *dxdt) const {
  // TODO To be safe, initialize to zero
  memset(dxdt,0.,this->dim()*sizeof(Real));

  // Reference to model
  const Model &model = _model;
  uint nD = model.nD();

  // Compute vector field
  model.f(t,x,dxdt);

  // Compute Jacobian df
  model.df_sparse(t,x,&_df_sparse.front());

  // Variables for sparse matrix multiply
  const UIntVec *sp_idxs, *ptrs;

  // Calculate dPhi_ij = df_im * Phi_mj
  const Real *Phi = x + nD;
  Real *dPhi = dxdt + nD;
  sp_idxs = model.df_sparse_idxs();
  ptrs = _df_ptrs;
  if(!sp_idxs[0].empty()) {
    for(uint idx_ptr = 0; idx_ptr < nD; ++idx_ptr) {
      uint i = sp_idxs[0][ptrs[0][idx_ptr]];
      for(uint j = 0; j < nD; ++j) {
        Real sum = 0.;
        uint m_begin = ptrs[0][idx_ptr],
             m_end   = ptrs[0][idx_ptr+1];
        for(uint idx_m = m_begin; idx_m < m_end; ++idx_m) {
          uint m = sp_idxs[1][idx_m];
          sum += _df_sparse[idx_m]*Phi[m*nD + j];
          //std::cout << "df_" << i << m << " * "
          //          << "Phi_" << m << j << " + ";
        }
        //std::cout << std::endl;
        dPhi[i*nD + j] = sum;
      }
    }
  }
  if(_order < 2)
    return;
    
    // Compute Hessian d2f
  model.d2f_sparse(t,x,&_d2f_sparse.front());
  
  // Calculate d2Phi_ijk = d2f_imn * Phi_mj * Phi_nk + ...
  Real *d2Phi = dxdt + nD*(nD+1);
  sp_idxs = model.d2f_sparse_idxs();
  ptrs = _d2f_ptrs;
  if(!sp_idxs[0].empty()) {
    for(uint idx_ptr = 0; idx_ptr < nD; ++idx_ptr) {
      uint i = sp_idxs[0][ptrs[0][idx_ptr]];
      for(uint j = 0; j < nD; ++j) {
        for(uint k = 0; k < nD; ++k) {
          Real sum = 0.;
          uint m_begin = ptrs[0][idx_ptr],
               m_end   = ptrs[0][idx_ptr+1];
          for(uint idx_m = m_begin; idx_m < m_end; ++idx_m) {
            uint m = sp_idxs[1][idx_m],
                 n = sp_idxs[2][idx_m];
            sum += _d2f_sparse[idx_m]*Phi[m*nD + j]*Phi[n*nD + k];
            //std::cout << "d2f_" << i << m << n << " * "
            //          << "Phi_" << m << j << " * "
            //          << "Phi_" << n << k << " + ";
          }
          //std::cout << std::endl;
          d2Phi[i*nD*nD + j*nD + k] = sum;
        }
      }
    }
  }
  // Calculate d2Phi_ijk = ... + df_im * Phi2_mjk
  const Real *Phi2 = x + nD*(nD+1);
  sp_idxs = model.df_sparse_idxs();
  ptrs = _df_ptrs;
  if(!sp_idxs[0].empty()) {
    for(uint idx_ptr = 0; idx_ptr < nD; ++idx_ptr) {
      uint i = sp_idxs[0][ptrs[0][idx_ptr]];
      for(uint j = 0; j < nD; ++j) {
        for(uint k = 0; k < nD; ++k) {
          Real sum = 0.;
          uint m_begin = ptrs[0][idx_ptr],
               m_end   = ptrs[0][idx_ptr+1];
          for(uint idx_m = m_begin; idx_m < m_end; ++idx_m) {
            uint m = sp_idxs[1][idx_m];
            sum += _df_sparse[idx_m]*Phi2[m*nD*nD + j*nD + k];
            //std::cout << "df_" << i << m << " * "
            //          << "Phi2_" << m << j << k << " + ";
          }
          //std::cout << std::endl;
          d2Phi[i*nD*nD + j*nD + k] += sum;
        }
      }
    }
  }
  if(_order < 3)
    return;
  
  // Compute 3rd derivative d3f
  model.d3f_sparse(t,x,&_d3f_sparse.front());
  
  // Calculate d3Phi_ijkl = d3f_imno * Phi_mj * Phi_nk * Phi_ol + ...
  Real *d3Phi = dxdt + nD*(nD*(nD+1)+1);
  sp_idxs = model.d3f_sparse_idxs();
  ptrs = _d3f_ptrs;
  if(!sp_idxs[0].empty()) {
    for(uint idx_ptr = 0; idx_ptr < nD; ++idx_ptr) {
      uint i = sp_idxs[0][ptrs[0][idx_ptr]];
      for(uint j = 0; j < nD; ++j) {
        for(uint k = 0; k < nD; ++k) {
          for(uint l = 0; l < nD; ++l) {
            Real sum = 0.;
            uint m_begin = ptrs[0][idx_ptr],
                 m_end   = ptrs[0][idx_ptr+1];
            for(uint idx_m = m_begin; idx_m < m_end; ++idx_m) {
              uint m = sp_idxs[1][idx_m],
                   n = sp_idxs[2][idx_m],
                   o = sp_idxs[3][idx_m];
              sum += _d3f_sparse[idx_m]*Phi[m*nD + j]*Phi[n*nD + k]*Phi[o*nD + l];
              //std::cout << "d2f_" << i << m << n << " * "
              //          << "Phi_" << m << j << " * "
              //          << "Phi_" << n << k << " + "
              //          << "Phi_" << o << l << " + ";
            }
            //std::cout << std::endl;
            d3Phi[i*nD*nD*nD + j*nD*nD + k*nD + l] = sum;
          }
        }
      }
    }
  }

  // Calculate d3Phi_ijkl = ... + d2f_imn * Phi2_mjl * Phi_nk +
  //                              d2f_imn * Phi2_nkl * Phi_mj +
  //                              d2f_imn * Phi2_mjk * Phi_nl + ...
  sp_idxs = model.d2f_sparse_idxs();
  ptrs = _d2f_ptrs;
  if(!sp_idxs[0].empty()) {
    for(uint idx_ptr = 0; idx_ptr < nD; ++idx_ptr) {
      uint i = sp_idxs[0][ptrs[0][idx_ptr]];
      for(uint j = 0; j < nD; ++j) {
        for(uint k = 0; k < nD; ++k) {
          for(uint l = 0; l < nD; ++l) {
            Real sum = 0.;
            uint m_begin = ptrs[0][idx_ptr],
                 m_end   = ptrs[0][idx_ptr+1];
            for(uint idx_m = m_begin; idx_m < m_end; ++idx_m) {
              uint m = sp_idxs[1][idx_m],
                   n = sp_idxs[2][idx_m];
              sum += _d2f_sparse[idx_m]*Phi2[m*nD*nD + j*nD + l]*Phi[n*nD + k];
              sum += _d2f_sparse[idx_m]*Phi2[n*nD*nD + k*nD + l]*Phi[m*nD + j];
              sum += _d2f_sparse[idx_m]*Phi2[m*nD*nD + j*nD + k]*Phi[n*nD + l];
              //std::cout << "d2f_" << i << m << n << " * [ "
              //          << "Phi2_" << m << j << l << " * " << Phi_ << n << k << " + "
              //          << "Phi2_" << n << k << l << " * " << Phi_ << m << j << " + "
              //          << "Phi2_" << m << j << k << " * " << Phi_ << n << l << " + ";
            }
            //std::cout << std::endl;
            d3Phi[i*nD*nD*nD + j*nD*nD + k*nD + l] += sum;
          }
        }
      }
    }
  }

  // Calculate d3Phi_ijkl = ... + df_im * Phi3_mjkl
  const Real *Phi3 = x + nD*(nD*(nD+1)+1);
  sp_idxs = model.df_sparse_idxs();
  ptrs = _df_ptrs;
  if(!sp_idxs[0].empty()) {
    for(uint idx_ptr = 0; idx_ptr < nD; ++idx_ptr) {
      uint i = sp_idxs[0][ptrs[0][idx_ptr]];
      for(uint j = 0; j < nD; ++j) {
        for(uint k = 0; k < nD; ++k) {
          for(uint l = 0; l < nD; ++l) {
            Real sum = 0.;
            uint m_begin = ptrs[0][idx_ptr],
                 m_end   = ptrs[0][idx_ptr+1];
            for(uint idx_m = m_begin; idx_m < m_end; ++idx_m) {
              uint m = sp_idxs[1][idx_m];
              sum += _df_sparse[idx_m]*Phi3[m*nD*nD*nD + j*nD*nD + k*nD + l];
              //std::cout << "df_" << i << m << " * "
              //          << "Phi3_" << m << j << k << l << " + ";
            }
            //std::cout << std::endl;
            d3Phi[i*nD*nD*nD + j*nD*nD + k*nD + l] += sum;
          }
        }
      }
    }
  }
}

# endif // TANGENT_LINEAR_MODEL_H_

