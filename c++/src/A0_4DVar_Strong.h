#ifndef A0_4DVAR_STRONG_H_
#define A0_4DVAR_STRONG_H_

// My includes
#include "EstimationAction.h"
#include "TrapezoidRule.h"

////////////////////////////////////////////////////////////////
/// Derived action to implement strong-constrained, collocated 4DVar
class A0_4DVar_Strong : public EstimationAction {
public:

  // Constructors
  A0_4DVar_Strong();
  // Destructor
  virtual ~A0_4DVar_Strong();

  // Class name
  virtual std::string classname() const
    { return "A0_4DVar_Strong"; }
  
  // Problem dimensions
  virtual Index num_variables() const;
  virtual Index num_constraints() const;

  // Problem information
  virtual void variable_bounds(Real *x_lower, Real *x_upper) const;
  virtual void constraint_bounds(Real *g_lower, Real *g_upper) const;

  // Set methods
  virtual void set_Rm_diag(const RealVec &Rm_diag);

  // Compute objective function
  virtual void f_eval(const Real *X, Real &f, bool new_X);
  virtual void df_eval(const Real *X, Real *df, bool new_X);
  virtual void d2f_sparse_eval(const Real *X, Real *d2f, bool new_X);

  // Compute dynamical constraints
  virtual void g_eval(const Real *X, Real *g, bool new_X);
  virtual void dg_sparse_eval(const Real *X, Real *dg, bool new_X);
  virtual void d2g_sparse_eval(const Real *X, Real *d2g, bool new_X);

  // Sparsity patterns
  virtual void d2f_sparse_idxs(IndexVec *sparse_idxs) const;
  virtual void dg_sparse_idxs(IndexVec *sparse_idxs)  const;
  virtual void d2g_sparse_idxs(IndexVec *sparse_idxs) const;

protected:
  // Reimplemented from EstimationAction
  virtual void initialize();

  // Measurement error
  RealVec _Rm_diag;
  // Quadrature rule for constraints
  QuadRule *_quad;
  // Number of non zero elements
  Index _dg_num_non0s, _d2g_num_non0s;

private:
  // Copy constructor
  A0_4DVar_Strong(const A0_4DVar_Strong&);
};

////////////////////////////////////////////////////////////////
/// Constructors
A0_4DVar_Strong::A0_4DVar_Strong() :
  EstimationAction(),
  _quad (NULL), _dg_num_non0s (-1), _d2g_num_non0s (-1) {
}
/// Destructor
A0_4DVar_Strong::~A0_4DVar_Strong() {
  delete _quad; _quad = NULL;
}

////////////////////////////////////////////////////////////////
/// Get number of independent variables
Index A0_4DVar_Strong::num_variables() const {
  const Index nNp1 = _data->time_domain().num_time_points(),
              nD = _data->dynamical_model().num_states();
  return nNp1*nD;
}

////////////////////////////////////////////////////////////////
/// Get number of constraints
Index A0_4DVar_Strong::num_constraints() const {
  const Index nN = _data->time_domain().num_time_intervals(),
              nD = _data->dynamical_model().num_states();
  return nN*nD;
}

////////////////////////////////////////////////////////////////
/// Get variable bounds
void A0_4DVar_Strong::variable_bounds(Real *x_lower, Real *x_upper) const {
  // State bounds set by model
  const Index nNp1 = _data->time_domain().num_time_points(),
                nD = _data->dynamical_model().num_states();
  for(Index iN = 0; iN < nNp1; ++iN) {
    _data->dynamical_model().state_bounds(x_lower,x_upper);
    x_lower += nD; x_upper += nD;
  }
}

////////////////////////////////////////////////////////////////
/// Get constraint bounds
void A0_4DVar_Strong::constraint_bounds(Real *g_lower, Real *g_upper) const {
  // Set constraint bounds to O(dt)
  const Index nN = _data->time_domain().num_time_intervals(),
              nD = _data->dynamical_model().num_states();
  Real g_bound =  std::pow(_data->time_domain().dt(),1);
  for(Index iN = 0; iN < nN; ++iN) {
    for(Index iD = 0; iD < nD; ++iD) {
      g_lower[iN*nD + iD] = -g_bound;
      g_upper[iN*nD + iD] = +g_bound;
    }
  }
}

////////////////////////////////////////////////////////////////
/// Reimplemented from EstimationAction to set Rm, Rf
void A0_4DVar_Strong::set_Rm_diag(const RealVec &Rm_diag) {
  // Consistency check
  Index nL = _data->observation_model().num_observed_states();
  if((Index)Rm_diag.size() != nL) {
    throw_exception("A0_4DVar_Strong::set_Rm_diag: "
            "Rm has improper length");
  }
  _Rm_diag = Rm_diag;
}

////////////////////////////////////////////////////////////////
void A0_4DVar_Strong::initialize() {
  // Set default measurement error scaling
  Index nL = _data->observation_model().num_observed_states();
  if(_Rm_diag.empty()) {
    _Rm_diag.resize(nL,1.);
  }

  // Initialize constraint quadrature
  delete _quad; _quad = NULL;
  _quad = new TrapezoidRule(_data->dynamical_model());

  // Cache number of non 0 elements for constraints
  IndexVec tmp_idxs[3];
  _quad->dg_sparse_idxs(tmp_idxs);
  _dg_num_non0s = tmp_idxs[0].size();
  _quad->d2g_sparse_idxs(tmp_idxs);
  _d2g_num_non0s = tmp_idxs[0].size();
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Strong::f_eval(const Real *X, Real &f, bool new_X) {
  // Dynamical model
  const Index nD = _data->dynamical_model().num_states();
  // Time domain
  const Index nNp1 = _data->time_domain().num_time_points();
  // Observation model
  const ObservationModel &obs = _data->observation_model();
  const Index nL = obs.num_observed_states();
  const IndexVec &observed_state_idxs = obs.observed_state_idxs();
  // Measured data
  const Real *y = &_data->observations().front();

  // Compute f
  const Real *x = X;
  Real tmp;
  f = 0;
  for(Index iN = 0; iN < nNp1; ++iN,
        x += nD, y += nL) {
    for(Index iL = 0; iL < nL; ++iL) {
      tmp = y[iL] - x[observed_state_idxs[iL]];
      f += _Rm_diag[iL]*tmp*tmp;
    }
  }
  
  // Apply scale factor
  Real sf = 0.5/nNp1;
  f *= sf;
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Strong::df_eval(const Real *X, Real *df, bool new_X) {
  // Dynamical model
  const Index nD = _data->dynamical_model().num_states();
  // Time domain
  const Index nNp1 = _data->time_domain().num_time_points();
  // Observation model
  const ObservationModel &obs = _data->observation_model();
  const Index nL = obs.num_observed_states();
  const IndexVec &observed_state_idxs = obs.observed_state_idxs();
  // Measured data
  const Real *y = &_data->observations().front();

  // Initialize df to zero
  memset(df,0.,num_variables()*sizeof(Real));

  // Scale factor
  Real sf = 1./nNp1;

  // Compute df
  const Real *x = X;
  for(Index iN = 0; iN < nNp1; ++iN,
        x += nD, y += nL) {
    for(Index iL = 0; iL < nL; ++iL) {
      Index idx = observed_state_idxs[iL];
      df[iN*nD + idx] = -sf*_Rm_diag[iL]*(y[iL] - x[idx]);
    }
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Strong::d2f_sparse_eval(const Real *X, Real *d2f, bool new_X) {
  // Time domain
  const Index nNp1 = _data->time_domain().num_time_points();
  // Measurements
  const ObservationModel &obs = _data->observation_model();
  Index nL = obs.num_observed_states();

  // Scale factor
  Real sf = 1./nNp1;

  // Compute d2f
  Index idx = 0;
  for(Index iN = 0; iN < nNp1; ++iN) {
    for(Index iL = 0; iL < nL; ++iL) {
      d2f[idx] = sf*_Rm_diag[iL]; ++idx;
    }
  }
}

////////////////////////////////////////////////////////////////
/// 
void A0_4DVar_Strong::d2f_sparse_idxs(IndexVec *sparse_idxs) const {
  // DynamicalModel
  const Index nD = _data->dynamical_model().num_states();
  // Time domain
  const Index nNp1 = _data->time_domain().num_time_points();
  // Measurements
  const ObservationModel &obs = _data->observation_model();
  const IndexVec &observed_state_idxs = obs.observed_state_idxs();
  Index nL = obs.num_observed_states();

  // Compute sparsity pattern for d2f
  sparse_idxs[0].resize(nL*nNp1);
  sparse_idxs[1].resize(nL*nNp1);
  Index idx = 0, idx_meas;
  for(Index iN = 0; iN < nNp1; ++iN) {
    for(Index iL = 0; iL < nL; ++iL) {
      idx_meas = observed_state_idxs[iL];
      sparse_idxs[0][idx] = iN*nD + idx_meas;
      sparse_idxs[1][idx] = sparse_idxs[0][idx];
      ++idx;
    }
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Strong::g_eval(const Real *X, Real *g, bool new_X) {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();
  const Real *x_n = X, *x_np1 = x_n + nD;
  // Time domain
  const TimeDomain &time_domain = _data->time_domain();
  const RealVec &time_points = time_domain.time_points();
  const Real *t_n = &time_points.front(), *t_np1 = t_n+1;
  const Index nN = time_domain.num_time_intervals();

  // Rescale constraints by 1/dt
  const Real sf = 1./time_domain.dt();

  // Compute g
  for(Index iN = 0; iN < nN; ++iN) {
    _quad->g_eval(*t_n,*t_np1,x_n,x_np1,g);\
    for(Index iD = 0; iD < nD; ++iD) {
      g[iD] *= sf;
    }
    x_n += nD, x_np1 += nD, ++t_n, ++t_np1;
    g += nD;
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Strong::dg_sparse_eval(const Real *X, Real *dg, bool new_X) {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();
  const Real *x_n = X, *x_np1 = x_n + nD;
  // Time domain
  const TimeDomain &time_domain = _data->time_domain();
  const RealVec &time_points = time_domain.time_points();
  const Real *t_n = &time_points.front(), *t_np1 = t_n+1;
  const Index nN = time_domain.num_time_intervals();

  // Rescale constraints by 1/dt
  const Real sf = 1./time_domain.dt();

  // Initialize calculation
  _quad->init_dg_eval(*t_n,x_n);
  // Compute dg
  for(Index iN = 0; iN < nN; ++iN) {
    _quad->dg_sparse_eval(*t_n,*t_np1,x_n,x_np1,dg);
    for(Index iD = 0; iD < _dg_num_non0s; ++iD) {
      dg[iD] *= sf;
    }
    x_n += nD, x_np1 += nD, ++t_n, ++t_np1;
    dg += _dg_num_non0s;
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Strong::d2g_sparse_eval(const Real *X, Real *d2g, bool new_X) {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();
  const Real *x_n = X, *x_np1 = x_n + nD;
  // Time domain
  const TimeDomain &time_domain = _data->time_domain();
  const RealVec &time_points = time_domain.time_points();
  const Real *t_n = &time_points.front(), *t_np1 = t_n+1;
  const Index nN = time_domain.num_time_intervals();

  // Rescale constraints by 1/dt
  const Real sf = 1./time_domain.dt();

  // Initialize calculation
  _quad->init_d2g_eval(*t_n,x_n);
  // Compute d2g
  for(Index iN = 0; iN < nN; ++iN) {
    _quad->d2g_sparse_eval(*t_n,*t_np1,x_n,x_np1,d2g);
    for(Index iD = 0; iD < _d2g_num_non0s; ++iD) {
      d2g[iD] *= sf;
    }
    x_n += nD, x_np1 += nD, ++t_n, ++t_np1;
    d2g += _d2g_num_non0s;
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Strong::dg_sparse_idxs(IndexVec *sparse_idxs) const {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();
  // Time domain
  const TimeDomain &time_domain = _data->time_domain();
  const Index nN = time_domain.num_time_intervals();

  // Clear any existing data
  Index num_derivs = 2;
  for(Index i_deriv = 0; i_deriv < num_derivs; ++i_deriv) {
    sparse_idxs[i_deriv].clear();
  }

  // Compute dg_n/dx_n and dg_n/dx_n+1
  _quad->dg_sparse_idxs(sparse_idxs);

  // Reserve space for the rest of the indices
  Index num_sp_idxs = sparse_idxs[0].size();
  for(Index i_deriv = 0; i_deriv < num_derivs; ++i_deriv) {
    sparse_idxs[i_deriv].resize(num_sp_idxs*nN);
  }

  // Fill in the remaining indices
  Index idx_i = num_sp_idxs;
  for(Index iN = 1; iN < nN; ++iN) {
    Index offset = iN*nD;
    for(Index idx_j = 0; idx_j < num_sp_idxs; ++idx_i, ++idx_j) {
      for(Index i_deriv = 0; i_deriv < num_derivs; ++i_deriv) {
        sparse_idxs[i_deriv][idx_i] = sparse_idxs[i_deriv][idx_j] + offset;
      }
    }
  }
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Strong::d2g_sparse_idxs(IndexVec *sparse_idxs) const {
  // Model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();
  // Time domain
  const TimeDomain &time_domain = _data->time_domain();
  const Index nN = time_domain.num_time_intervals();

  // Clear any existing data
  Index num_derivs = 3;
  for(Index i_deriv = 0; i_deriv < num_derivs; ++i_deriv) {
    sparse_idxs[i_deriv].clear();
  }

  // Compute d2g_n/dx_n^2 and d2g_n/dx_n+1^2
  _quad->d2g_sparse_idxs(sparse_idxs);

  // Reserve space for the rest of the indices
  Index num_sp_idxs = sparse_idxs[0].size();
  for(Index i_deriv = 0; i_deriv < num_derivs; ++i_deriv) {
    sparse_idxs[i_deriv].resize(num_sp_idxs*nN);
  }

  // Fill in the remaining indices
  Index idx_i = num_sp_idxs;
  for(Index iN = 1; iN < nN; ++iN) {
    Index offset = iN*nD;
    for(Index idx_j = 0; idx_j < num_sp_idxs; ++idx_i, ++idx_j) {
      for(Index i_deriv = 0; i_deriv < num_derivs; ++i_deriv) {
        sparse_idxs[i_deriv][idx_i] = sparse_idxs[i_deriv][idx_j] + offset;
      }
    }
  }
}

#endif // A0_4DVAR_STRONG_H_
