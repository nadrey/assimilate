#ifndef TEST_MODEL_H_
#define TEST_MODEL_H_

// My includes
#include "Utils.h"
#include "DynamicalModel.h"
#include "ODEStepper.h"

////////////////////////////////////////////////////////////////
///
class TestModel : public DynamicalModel {
public:
  TestModel();
  TestModel(Index dim);

  // Get methods
  virtual Index num_states() const;
  virtual Index num_params() const;
  virtual Index num_derivs() const;
  virtual void state_bounds(Real *x_lower, Real *x_upper) const;
  virtual void param_bounds(Real *p_lower, Real *p_upper) const;

  // Vector field evaluations
  void f_eval(const Real &t, const Real *x, Real *dxdt) const;
  void df_sparse_eval(const Real &t, const Real *x, Real *df) const;
  void d2f_sparse_eval(const Real &t, const Real *x, Real *d2f) const;
  void d3f_sparse_eval(const Real &t, const Real *x, Real *d3f) const;

  // Sparsity patterns
  virtual void df_sparse_idxs(IndexVec *df_sparse_idxs) const;
  virtual void d2f_sparse_idxs(IndexVec *d2f_sparse_idxs) const;
  virtual void d3f_sparse_idxs(IndexVec *d3f_sparse_idxs) const;

  // Random initial conditions
  virtual void random_state(Real *x) const;

private:
  // Initialize model
  void initialize();

  // Injected current
  Real injected_current(Real t) const;

  // Parameters
  RealVec _params;
};

////////////////////////////////////////////////////////////////
/// constructor
TestModel::TestModel() : DynamicalModel(),
  _params(2,1.) {
  initialize();
}

TestModel::TestModel(Index dim) : DynamicalModel(),
  _params(dim,1.) {
  initialize();
}

////////////////////////////////////////////////////////////////
/// Get methods
Index TestModel::num_states() const {
  return _params.size();
}
///
Index TestModel::num_params() const {
  return 0;
}
///
Index TestModel::num_derivs() const {
  return 3;
}
///
void TestModel::state_bounds(Real *x_lower, Real *x_upper) const {
  const Index nD = num_states();
  for(Index iD = 0; iD < nD; ++iD) {
    x_lower[iD] = -1.e2;
    x_upper[iD] = +1.e2;
  }
}
///
void TestModel::param_bounds(Real *p_lower, Real *p_upper) const {
}

////////////////////////////////////////////////////////////////
void TestModel::initialize() {
  for(Index i = 0; i < (Index)_params.size(); ++i) {
    _params[i] = 1.;
  }
}

////////////////////////////////////////////////////////////////
/// returns random initial condition on attractor
void TestModel::random_state(Real *x) const {
  const Index nD = num_states();
  RealVec x_lower(nD,-1),
          x_upper(nD,+1);
  for(Index iD = 0; iD < nD; ++iD) {
    x[iD] = (x_upper[iD]-x_lower[iD])*(rand_real()-0.5) + x_lower[iD];
    //x[iD] = 1;
  }
}

////////////////////////////////////////////////////////////////
/// evaluate vector field at x(t)
///
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) dxdt (out) - output vector field
///
void TestModel::f_eval(const Real &t, const Real *x, Real *f) const {
  const Index nD = num_states();
  for(Index iD = 0; iD < nD; ++iD) {
    f[iD] = _params[iD];
    for(Index jD = 0; jD < nD; ++jD) {
      f[iD] *= std::pow(x[jD],3.);
    }
  }
}

////////////////////////////////////////////////////////////////
/// evaluate sparse Jacobian of vector field df_i/dx_j
///
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) df (out) - output Jacobian nonzero elements in row-major order
///
void TestModel::df_sparse_eval(const Real &t, const Real *x, Real *df) const {
  const Index nD = num_states();
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
    df[iD*nD + jD] = _params[iD];
    for(Index kD = 0; kD < nD; ++kD) {
      if(kD == jD) {
        df[iD*nD + jD] *= 3*std::pow(x[kD],2.);
      }
      else {
        df[iD*nD + jD] *= std::pow(x[kD],3.);
      }
    }
  }
  }
}

////////////////////////////////////////////////////////////////
/// get sparsity structure of vector field Jacobian in matrix format
///
/// Parameters:
///  1) idxs[2] - i,j indices of nonzero elements
///
void TestModel::df_sparse_idxs(IndexVec *idxs) const {
  const Index nD = num_states();
  const Index num_elems = nD*nD;
  idxs[0].resize(num_elems);
  idxs[1].resize(num_elems);

  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
    idxs[0][iD*nD + jD] = iD;
    idxs[1][iD*nD + jD] = jD;
  }
  }
}

////////////////////////////////////////////////////////////////
/// evaluate sparse Hessian of vector field d2f_i / dx_j dx_k
///
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) d2f (out) - output Hessian nonzero elements in row-major order
///
/// TODO Utilize symmetry
///
void TestModel::d2f_sparse_eval(const Real &t, const Real *x, Real *d2f) const {
  const Index nD = num_states();
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
  for(Index kD = 0; kD < nD; ++kD) {
    d2f[iD*nD*nD + jD*nD + kD] = _params[iD];
    for(Index lD = 0; lD < nD; ++lD) {
      if(jD == kD && kD == lD) {
        d2f[iD*nD*nD + jD*nD + kD] *= 6*x[lD];
      }
      else if(jD == lD || kD == lD) {
        d2f[iD*nD*nD + jD*nD + kD] *= 3*std::pow(x[lD],2.);
      }
      else {
        d2f[iD*nD*nD + jD*nD + kD] *= std::pow(x[lD],3.);
      }
    }
  }
  }
  }
}

////////////////////////////////////////////////////////////////
/// get sparsity structure of vector field Hessian in 3 tensor format
///
/// Parameters:
///  1) idxs[3]  (out) - i,j,k indices of nonzero elements
///
void TestModel::d2f_sparse_idxs(IndexVec *idxs) const {
  const Index nD = num_states();
  const Index num_elems = nD*nD*nD;
  idxs[0].resize(num_elems);
  idxs[1].resize(num_elems);
  idxs[2].resize(num_elems);

  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
  for(Index kD = 0; kD < nD; ++kD) {
    idxs[0][iD*nD*nD + jD*nD + kD] = iD;
    idxs[1][iD*nD*nD + jD*nD + kD] = jD;
    idxs[2][iD*nD*nD + jD*nD + kD] = kD;
  }
  }
  }
}

////////////////////////////////////////////////////////////////
/// evaluate sparse 3rd derivative of vector field
///   d3f_i / dx_j dx_k dx_l
///
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) d3f (out) - output d3f nonzero elements in row-major order
///
void TestModel::d3f_sparse_eval(const Real &t, const Real *x, Real *d3f) const {
  const Index nD = num_states();
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
  for(Index kD = 0; kD < nD; ++kD) {
  for(Index lD = 0; lD < nD; ++lD) {
    d3f[iD*nD*nD*nD + jD*nD*nD + kD*nD + lD] = _params[iD];
    for(Index mD = 0; mD < nD; ++mD) {
      if(jD == kD && kD == lD && lD == mD) {
        d3f[iD*nD*nD*nD + jD*nD*nD + kD*nD + lD] *= 6;
      }
      else if((jD == kD && kD == mD) ||
              (jD == lD && lD == mD) ||
              (kD == lD && lD == mD)) {
        d3f[iD*nD*nD*nD + jD*nD*nD + kD*nD + lD] *= 6*x[mD];
      }
      else if(jD == mD || kD == mD || lD == mD) {
        d3f[iD*nD*nD*nD + jD*nD*nD + kD*nD + lD] *= 3*std::pow(x[mD],2.);
      }
      else {
        d3f[iD*nD*nD*nD + jD*nD*nD + kD*nD + lD] *= std::pow(x[mD],3.);
      }
    }
  }
  }
  }
  }
}

////////////////////////////////////////////////////////////////
/// get sparsity structure of vector field d3f in 3 tensor format
///
/// Parameters:
///  1) idxs[4]  (out) - i,j,k,l indices of nonzero elements
///
void TestModel::d3f_sparse_idxs(IndexVec *idxs) const {
  const Index nD = num_states();
  const Index num_elems = nD*nD*nD*nD;
  idxs[0].resize(num_elems);
  idxs[1].resize(num_elems);
  idxs[2].resize(num_elems);
  idxs[3].resize(num_elems);

  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
  for(Index kD = 0; kD < nD; ++kD) {
  for(Index lD = 0; lD < nD; ++lD) {
    idxs[0][iD*nD*nD*nD + jD*nD*nD + kD*nD + lD] = iD;
    idxs[1][iD*nD*nD*nD + jD*nD*nD + kD*nD + lD] = jD;
    idxs[2][iD*nD*nD*nD + jD*nD*nD + kD*nD + lD] = kD;
    idxs[3][iD*nD*nD*nD + jD*nD*nD + kD*nD + lD] = lD;
  }
  }
  }
  }
}

#endif /* TEST_MODEL_H_ */
