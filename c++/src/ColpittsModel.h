#ifndef COLPITTS_MODEL_H_
#define COLPITTS_MODEL_H_

// My includes
#include "Utils.h"
#include "DynamicalModel.h"
#include "ODEStepper.h"

////////////////////////////////////////////////////////////////
/// Our version of the Colpitts oscillator, taken from
///
///   H. D. I. Abarbanel, D. R. Creveling, R. Farsian, and M. Kostuk,
///   "Dynamical state and parameter estimation"
///   J. Appl. Dyn. Syst. 8, 1341 (2009)
///
/// dx[0]/dt =  p[0]*x[1]
/// dx[1]/dt = -p[1]*(x[0] + x[2]) - p[2]*x[1]
/// dx[2]/dt =  p[3]*(x[1] + 1 - exp[-x[0] + u[0]])
///
class ColpittsModel : public DynamicalModel {
public:
  ColpittsModel();

  // Get methods
  virtual Index num_states() const;
  virtual Index num_params() const;
  virtual Index num_derivs() const;
  virtual void state_bounds(Real *x_lower, Real *x_upper) const;
  virtual void param_bounds(Real *p_lower, Real *p_upper) const;

  // Vector field evaluations
  void f_eval(const Real &t, const Real *x, Real *dxdt) const;
  void df_sparse_eval(const Real &t, const Real *x, Real *df) const;
  void d2f_sparse_eval(const Real &t, const Real *x, Real *d2f) const;
  void d3f_sparse_eval(const Real &t, const Real *x, Real *d3f) const;

  // Sparsity patterns
  virtual void df_sparse_idxs(IndexVec *df_sparse_idxs) const;
  virtual void d2f_sparse_idxs(IndexVec *d2f_sparse_idxs) const;
  virtual void d3f_sparse_idxs(IndexVec *d3f_sparse_idxs) const;

  // Random initial conditions
  virtual void random_state(Real *x) const;
  //virtual void random_param(Real *p) const;

private:
  // Initialize model
  void initialize();

  // Injected current
  Real injected_current(Real t) const;

  // Parameters
  Real _params[4];
};

////////////////////////////////////////////////////////////////
/// constructor
ColpittsModel::ColpittsModel() : DynamicalModel() {
  initialize();
}

////////////////////////////////////////////////////////////////
/// Get methods
Index ColpittsModel::num_states() const {
  return 3;
}
///
Index ColpittsModel::num_params() const {
  return 0;
}
///
Index ColpittsModel::num_derivs() const {
  return 3;
}
///
void ColpittsModel::state_bounds(Real *x_lower, Real *x_upper) const {
  x_lower[0] =   0; x_upper[0] = +1;
  x_lower[1] =  -1; x_upper[1] = +1;
  x_lower[2] =  -1; x_upper[2] = +1;
}
///
void ColpittsModel::param_bounds(Real *p_lower, Real *p_upper) const {
}

////////////////////////////////////////////////////////////////
void ColpittsModel::initialize() {
  _params[0] = 5.0;
  _params[1] = 0.0797;
  _params[2] = 0.6898;
  _params[3] = 6.2723;
}

////////////////////////////////////////////////////////////////
/// returns random initial condition on attractor
void ColpittsModel::random_state(Real *x) const {
  const Index nD = num_states();
  RealVec x_lower(nD), x_upper(nD);
  state_bounds(&x_lower.front(),&x_upper.front());
  for(Index iD = 0; iD < nD; ++iD) {
    x[iD] = (x_upper[iD]-x_lower[iD])*(rand_real()-0.5) + x_lower[iD];
  }

  // Integrate to remove transients
  Index  nNp1 = 2001; Real dt = 0.01;
  RK4Step<ColpittsModel> ode(*this,nD);
  for(Index iN = 0; iN < nNp1; ++iN) {
    ode.step(-1.,x,dt,x);
  }
}

////////////////////////////////////////////////////////////////
/// evaluate vector field at x(t)
///
/// dx[0]/dt =  p[0]*x[1]
/// dx[1]/dt = -p[1]*(x[0] + x[2]) - p[2]*x[1]
/// dx[2]/dt =  p[3]*(x[1] + 1 - exp[-x[0] + u[0])
///
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) dxdt (out) - output vector field
///
void ColpittsModel::f_eval(const Real &t, const Real *x, Real *f) const {
  const Real *p = _params;
  f[0] = +p[0]*x[1];
  f[1] = -p[1]*(x[0] + x[2]) - p[2]*x[1];
  f[2] = +p[3]*(x[1] + 1 - std::exp(-x[0]));
}

////////////////////////////////////////////////////////////////
/// evaluate sparse Jacobian of vector field df_i/dx_j
///
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) df (out) - output Jacobian nonzero elements in row-major order
///
void ColpittsModel::df_sparse_eval(const Real &t, const Real *x, Real *df) const {
  const Real *p = _params;

  df[1] = +p[0]; // df[0]
  df[2] = -p[1]; df[3] = -p[2];  df[4] = -p[1]; // df[1]
  df[5] = +p[3]*std::exp(-x[0]); df[6] = +p[3]; // df[2]

  // TODO For now, df diagonal elements cannot be zero
  df[0] = df[8] = 0;
}

////////////////////////////////////////////////////////////////
/// get sparsity structure of vector field Jacobian in matrix format
///
/// Parameters:
///  1) idxs[2] - i,j indices of nonzero elements
///
void ColpittsModel::df_sparse_idxs(IndexVec *idxs) const {
  const Index num_elems = 8;
  idxs[0].resize(num_elems);
  idxs[1].resize(num_elems);

  idxs[0][1] = 0; idxs[1][1] = 1;
  idxs[0][2] = 1; idxs[1][2] = 0;
  idxs[0][3] = 1; idxs[1][3] = 1;
  idxs[0][4] = 1; idxs[1][4] = 2;
  idxs[0][5] = 2; idxs[1][5] = 0;
  idxs[0][6] = 2; idxs[1][6] = 1;

  // TODO For now, df diagonal elements cannot be zero
  idxs[0][0] = 0; idxs[1][0] = 0;
  idxs[0][7] = 2; idxs[1][7] = 2;
}

////////////////////////////////////////////////////////////////
/// evaluate sparse Hessian of vector field d2f_i / dx_j dx_k
///
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) d2f (out) - output Hessian nonzero elements in row-major order
///
/// TODO Utilize symmetry
///
void ColpittsModel::d2f_sparse_eval(const Real &t, const Real *x, Real *d2f) const {
  const Real *p = _params;
  d2f[0] = -p[3]*std::exp(-x[0]);
}

////////////////////////////////////////////////////////////////
/// get sparsity structure of vector field Hessian in 3 tensor format
///
/// Parameters:
///  1) idxs[3]  (out) - i,j,k indices of nonzero elements
///
void ColpittsModel::d2f_sparse_idxs(IndexVec *idxs) const {
  const Index num_elems = 1;
  idxs[0].resize(num_elems);
  idxs[1].resize(num_elems);
  idxs[2].resize(num_elems);
  idxs[0][0] = 2; idxs[1][0] = 0; idxs[2][0] = 0;
}

////////////////////////////////////////////////////////////////
/// evaluate sparse 3rd derivative of vector field
///   d3f_i / dx_j dx_k dx_l
///
/// Parameters:
///  1) t (in) - time of evaluation
///  2) x (in) - point of evaluation
///  3) d3f (out) - output d3f nonzero elements in row-major order
///
void ColpittsModel::d3f_sparse_eval(const Real &t, const Real *x, Real *d3f) const {
  const Real *p = _params;
  d3f[0] = +p[3]*std::exp(-x[0]);
}

////////////////////////////////////////////////////////////////
/// get sparsity structure of vector field d3f in 3 tensor format
///
/// Parameters:
///  1) idxs[4]  (out) - i,j,k,l indices of nonzero elements
///
void ColpittsModel::d3f_sparse_idxs(IndexVec *idxs) const {
  const Index num_elems = 1;
  idxs[0].resize(num_elems);
  idxs[1].resize(num_elems);
  idxs[2].resize(num_elems);
  idxs[3].resize(num_elems);
  idxs[0][0] = 2; idxs[1][0] = 0; idxs[2][0] = 0; idxs[3][0] = 0;
}

#endif /* COLPITTS_MODEL_H_ */
