#ifndef A0_4DVAR_WEAK_H_
#define A0_4DVAR_WEAK_H_

// My includes
#include "EstimationAction.h"
#include "TrapezoidRule.h"
#include "SparseTensor.h"

////////////////////////////////////////////////////////////////
/// Derived action for weak-constrained, collocated 4DVar action
///
///  A0 = 1/T [\sum_{n=0}^N 1/2 |y(n)-h(x(n))|^2_Rm
///              + \sum_{n=0}^{N-1} 1/2 |g(x(n),x(n+1)|^2_Rf ]
///
///  where g(x(n),x(n+1)) is an discretized version of the
///  model equations dx/dt - f(x)
///
/// implemented as unconstrained optimization
///
class A0_4DVar_Weak : public EstimationAction {
public:

  // Constructors
  A0_4DVar_Weak();

  // Destructor
  virtual ~A0_4DVar_Weak();

  // Reimplemented from EstimationAction
  virtual void initialize();

  // Class name
  virtual std::string classname() const
    { return "A0_4DVar_Weak"; }
  
  // Problem dimensions
  virtual Index num_variables() const;
  virtual Index num_constraints() const;

  // Bounds information
  virtual void variable_bounds(Real *x_lower, Real *x_upper) const;

  // Set methods
  virtual void set_Rm_diag(const RealVec &Rm_diag);
  virtual void set_Rf_diag(const RealVec &Rf_diag);

  // Compute objective function
  virtual void f_eval(const Real *X, Real &f, bool new_X);
  virtual void df_eval(const Real *X, Real *df, bool new_X);
  virtual void d2f_sparse_eval(const Real *X, Real *d2f, bool new_X);

  // Sparsity patterns
  virtual void d2f_sparse_idxs(IndexVec *sparse_idxs) const;

  // Overloaded method to get separate contributions from Rm, Rf
  virtual void f_eval(const Real *X, Real &Rm_est,  Real &Rf_est,
                                     Real &Rm_pred, Real &Rf_pred);

protected:

  // Model/measurement error
  RealVec _Rf_diag, _Rm_diag;
  // Quadrature rule for constraints
  QuadRule *_quad;

  // Compute and cache objective function and derivatives
  virtual void eval_new_X(const Real *X, bool new_X);
  Real _f;
  RealVec _df, _d2f_sparse;

  // Split action evaluation
  Real _Rm_est, _Rf_est,
       _Rm_pred, _Rf_pred;

  // Temporary data from quadrature
  RealVec _g_quad,
          _dg_quad_sparse,
          _d2g_quad_sparse;
  IndexVec  _dg_quad_sparse_idxs[2],
           _d2g_quad_sparse_idxs[3];

  // Static function for converting d2L indices
  static Index d2L_idxs_to_array(Index nD, Index i, Index j);

  // Sparsity pattern of d2L
  void d2L_sparse_idxs(IndexVec *sparse_idxs) const;


private:
  // Copy constructor
  A0_4DVar_Weak(const A0_4DVar_Weak&);
};

////////////////////////////////////////////////////////////////
/// Constructors
A0_4DVar_Weak::A0_4DVar_Weak() :
  EstimationAction(),
  _quad (NULL), _f (0.),
  _Rm_est (0.),  _Rf_est (0.),
  _Rm_pred (0.), _Rf_pred (0.) {
}

/// Destructor
A0_4DVar_Weak::~A0_4DVar_Weak() {
  delete _quad; _quad = NULL;
}

////////////////////////////////////////////////////////////////
/// Get number of independent variables
Index A0_4DVar_Weak::num_variables() const {
  const Index nNp1 = _data->time_domain().num_time_points(),
              nD = _data->dynamical_model().num_states();
  return nNp1*nD;
}

////////////////////////////////////////////////////////////////
/// Get number of constraints
Index A0_4DVar_Weak::num_constraints() const {
  return 0;
}

////////////////////////////////////////////////////////////////
/// Get bounds on states/constraints
void A0_4DVar_Weak::variable_bounds(Real *x_lower, Real *x_upper) const {
  // State bounds set by model
  const Index nNp1 = _data->time_domain().num_time_points(),
                nD = _data->dynamical_model().num_states();
  for(Index iN = 0; iN < nNp1; ++iN) {
    _data->dynamical_model().state_bounds(x_lower,x_upper);
    x_lower += nD; x_upper += nD;
  }
}

////////////////////////////////////////////////////////////////
/// Reimplmented from EstimationAction to set Rm, Rf
void A0_4DVar_Weak::set_Rm_diag(const RealVec &Rm_diag) {
  // Consistency check
  Index nL = _data->observation_model().num_observed_states();
  if((Index)Rm_diag.size() != nL) {
    throw_exception("A0_4DVar_Weak::set_Rm_diag: "
            "Rm has improper length");
  }
  _Rm_diag = Rm_diag;
}
///
void A0_4DVar_Weak::set_Rf_diag(const RealVec &Rf_diag) {
  // Consistency check
  Index nD = _data->dynamical_model().num_states();
  if((Index)Rf_diag.size() != nD) {
    throw_exception("A0_4DVar_Weak::set_Rf_diag: "
            "Rf has improper length");
  }
  _Rf_diag = Rf_diag;
}

////////////////////////////////////////////////////////////////
/// Called by EstimationAction::init_t***_data(...)
void A0_4DVar_Weak::initialize() {

  // Set default measurement error scaling
  Index nL = _data->observation_model().num_observed_states();
  if(_Rm_diag.empty()) {
    _Rm_diag.resize(nL,1.);
  }

  // Set default model error scaling
  Index nD = _data->dynamical_model().num_states();
  if(_Rf_diag.empty()) {
    _Rf_diag.resize(nD,1.);
  }

  // Initialize objective cache
  _f = 0;
  const Index num_vars = num_variables();
  _df.resize(num_vars);
  IndexVec tmp_idxs[3];
  d2f_sparse_idxs(tmp_idxs);
  _d2f_sparse.resize(tmp_idxs[0].size());

  // Initialize constraint quadrature
  delete _quad; _quad = NULL;
  _quad = new TrapezoidRule(_data->dynamical_model());

  // Initialize constraint cache
  _g_quad.resize(nD);
  _quad->dg_sparse_idxs(_dg_quad_sparse_idxs);
  _dg_quad_sparse.resize(_dg_quad_sparse_idxs[0].size());
  _quad->d2g_sparse_idxs(_d2g_quad_sparse_idxs);
  _d2g_quad_sparse.resize(_d2g_quad_sparse_idxs[0].size());
}


////////////////////////////////////////////////////////////////
/// Evaluate the weak 4DVar action and its derivatives
void A0_4DVar_Weak::eval_new_X(const Real *X, bool new_X) {
  if(!new_X)
    return;

  // Dynamical model
  const DynamicalModel &model = _data->dynamical_model();
  const Index nD = model.num_states();

  // Time domain
  const TimeDomain &time_domain = _data->time_domain();
  const RealVec &time_points = time_domain.time_points();
  const Index nN   = time_domain.num_time_intervals(),
              nNp1 = time_domain.num_time_points();
  const Real &dt = time_domain.dt();

  // Observation model
  const ObservationModel &obs_model = _data->observation_model();
  const Index nL = obs_model.num_observed_states();
  const IndexVec &observed_state_idxs = obs_model.observed_state_idxs();

  // Observation window length
  const Real &obs_window_length = obs_model.observation_window_length();
  const Index nNp1_obs = round_to_int(obs_window_length/dt) + 1;

  // Observations
  const RealVec &Y = _data->observations();

  // Reset cached values
  _f = 0;
  std::fill(_df.begin(),_df.end(),0.);
  std::fill(_d2f_sparse.begin(),_d2f_sparse.end(),0.);

  // Reset individual components
  _Rm_est = 0;
  _Rf_est = 0;
  _Rm_pred = 0;
  _Rf_pred = 0;

  // Measurement error terms
  for(Index iN = 0; iN < nNp1; ++iN) {
    const Index d2f_offset = iN*nD*nD*3;
    for(Index iL = 0; iL < nL; ++iL) {
      // Estimated error (innovation)
      const Index idx_meas = observed_state_idxs[iL];
      Real d_n = Y[iN*nL + iL] - X[iN*nD + idx_meas];
      // Estimated Rm
      if(iN < nNp1_obs) {
        // Objective
        _Rm_est += dt*_Rm_diag[iL]*d_n*d_n;
        // Jacobian
        _df[iN*nD + idx_meas] -= dt*_Rm_diag[iL]*d_n;
        // Sparse Hessian
        _d2f_sparse[d2f_offset + idx_meas*(nD+1)] += dt*_Rm_diag[iL];
      }
      // Predicted Rm
      else {
        _Rm_pred += dt*_Rm_diag[iL]*d_n*d_n;
      }
    }
  }

  // Rescale model error by 1/dt since _g_quad is integrated
  Real sf = 1./dt;

  // Model error terms
  const Real *x_n = X, *x_np1 = x_n + nD;
  const Real *t_n = &time_points.front(), *t_np1 = t_n+1;
  _quad->init_dg_eval(*t_n,x_n);
  _quad->init_d2g_eval(*t_n,x_n);
  for(Index iN = 0; iN < nN; ++iN) {
    // Cache intermediate values to avoid recomputation
    _quad->g_eval(*t_n,*t_np1,x_n,x_np1,&_g_quad.front());
    _quad->dg_sparse_eval(*t_n,*t_np1,x_n,x_np1,&_dg_quad_sparse.front());
    _quad->d2g_sparse_eval(*t_n,*t_np1,x_n,x_np1,&_d2g_quad_sparse.front());
    // Switch between Rf estimated and Rf predicted
    Real *f = &_Rf_est;
    if(iN+1 >= nNp1_obs) {
      f = &_Rf_pred;
    }
    // Objective
    for(Index iD = 0; iD < nD; ++iD) {
      Real tmp = _g_quad[iD]*_Rf_diag[iD]*_g_quad[iD];
      (*f) += sf*tmp;
    }
    // Jacobian
    const Index num_dg_quad_non0s = _dg_quad_sparse.size();
    for(Index idx = 0; idx < num_dg_quad_non0s; ++idx) {
      Index i = _dg_quad_sparse_idxs[0][idx],
            j = _dg_quad_sparse_idxs[1][idx];
      Real tmp = _dg_quad_sparse[idx]*_Rf_diag[i]*_g_quad[i];
      _df[iN*nD + j] += sf*tmp;
    }
    // Sparse Hessian: dg^T*Rf*dg term
    const Index d2f_offset = iN*nD*nD*3;
    for(Index idx0 = 0; idx0 < num_dg_quad_non0s; ++idx0) {
      Index k = _dg_quad_sparse_idxs[0][idx0];
      for(Index idx1 = 0; idx1 < num_dg_quad_non0s; ++idx1) {
        if(k != _dg_quad_sparse_idxs[0][idx1]) {
          continue;
        }
        Index i = _dg_quad_sparse_idxs[1][idx0],
              j = _dg_quad_sparse_idxs[1][idx1];
        Real tmp = _dg_quad_sparse[idx0]*_Rf_diag[k]*_dg_quad_sparse[idx1];
        _d2f_sparse[d2f_offset + d2L_idxs_to_array(nD,i,j)] += sf*tmp;
      }
    }
    // Sparse Hessian: dg^T*Rf*dg term
    const Index num_d2g_quad_non0s = _d2g_quad_sparse.size();
    for(Index idx = 0; idx < num_d2g_quad_non0s; ++idx) {
      Index i = _d2g_quad_sparse_idxs[1][idx],
            j = _d2g_quad_sparse_idxs[2][idx],
            k = _d2g_quad_sparse_idxs[0][idx];
      Real tmp = _d2g_quad_sparse[idx]*_Rf_diag[k]*_g_quad[k];
      _d2f_sparse[d2f_offset + d2L_idxs_to_array(nD,i,j)] += sf*tmp;
    }
    // Iterate
    ++t_n; ++t_np1; x_n += nD; x_np1 += nD;
  }

  // Apply overall scale factor to objective
  sf = 1./(nNp1*dt);
  _f = 0.5*sf*(_Rm_est + _Rf_est + _Rf_pred);
  for(RealVec::iterator iter = _df.begin();
        iter != _df.end(); ++iter) {
    (*iter) *= sf;
  }
  for(RealVec::iterator iter = _d2f_sparse.begin();
        iter != _d2f_sparse.end(); ++iter) {
    (*iter) *= sf;
  }

  // Scale individual components
  _Rm_est  *= 0.5*sf; _Rf_est  *= 0.5*sf;
  _Rm_pred *= 0.5*sf; _Rf_pred *= 0.5*sf;
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak::f_eval(const Real *X, Real &f, bool new_X) {
  eval_new_X(X,new_X);
  f = _f;
}

////////////////////////////////////////////////////////////////
// Overloaded method to get separate contributions from Rm, Rf
void A0_4DVar_Weak::f_eval(const Real *X,
                           Real &Rm_est, Real &Rf_est,
                           Real &Rm_pred, Real &Rf_pred) {
  eval_new_X(X,true);
  Rm_est  = _Rm_est;  Rf_est  = _Rf_est;
  Rm_pred = _Rm_pred; Rf_pred = _Rf_pred;
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak::df_eval(const Real *X, Real *df, bool new_X) {
  eval_new_X(X,new_X);
  std::copy(_df.begin(),_df.end(),df);
}

////////////////////////////////////////////////////////////////
///
void A0_4DVar_Weak::d2f_sparse_eval(const Real *X, Real *d2f, bool new_X) {
  eval_new_X(X,new_X);
  std::copy(_d2f_sparse.begin(),_d2f_sparse.end(),d2f);
}

////////////////////////////////////////////////////////////////
///
/// TODO This implementation assumes that the d2f is block
///      tridiagonal, where each D*D block is dense. Sparsity
///      structure of the models is not utilized!
///
///      This should be okay for small models, and is required
///      for models with dense Jacobians and Hessians.
///      For large models, the D*D blocks will be a problem,
///      and we will need to utilize sparsity structure in the
///      model as well.
///
void A0_4DVar_Weak::d2f_sparse_idxs(IndexVec *sparse_idxs) const {
  // DynamicalModel
  const Index nD = _data->dynamical_model().num_states();
  // Time domain
  const Index nN = _data->time_domain().num_time_intervals();

  // d2L pattern
  IndexVec d2L_sparse_idxs[2];
  this->d2L_sparse_idxs(d2L_sparse_idxs);
  const Index nD2L = d2L_sparse_idxs[0].size();
  assert(nD2L == 4*nD*nD);

  // Resize arrays
  const Index nElems = nN*nD2L - nD*nD*(nN - 1);
  sparse_idxs[0].resize(nElems);
  sparse_idxs[1].resize(nElems);

  // Compute sparsity pattern for d2f
  Index idx = 0, offset = 0;
  for(Index iN = 0; iN < nN; ++iN) {
    for(Index idx1 = 0; idx1 < 3*nD*nD; ++idx1) {
      sparse_idxs[0][idx] = offset + d2L_sparse_idxs[0][idx1];
      sparse_idxs[1][idx] = offset + d2L_sparse_idxs[1][idx1];
      ++idx;
    }
    offset += nD;
  }
  // Last block
  for(Index idx1 = 0; idx1 < nD*nD; ++idx1) {
    sparse_idxs[0][idx] = offset + d2L_sparse_idxs[0][idx1];
    sparse_idxs[1][idx] = offset + d2L_sparse_idxs[1][idx1];
    ++idx;
  }
}

////////////////////////////////////////////////////////////////
///
Index A0_4DVar_Weak::d2L_idxs_to_array(Index nD, Index i, Index j) {
  Index ret = 0;
  if(i >= nD) {
    ret += 2*nD*nD; i -= nD;
  }
  if(j >= nD) {
    ret +=   nD*nD; j -= nD;
  }
  ret += i*nD + j;
  //std::cout << i << "  " << j << "  " << ret << std::endl;
  return ret;
}

////////////////////////////////////////////////////////////////
/// Hessian sparsity structure of discrete Lagrangian
///
/// TODO Model sparsity and symmetry is ignored.
///
void A0_4DVar_Weak::d2L_sparse_idxs(IndexVec *sparse_idxs) const {
  // DynamicalModel
  const Index nD = _data->dynamical_model().num_states();

  // Resize arrays
  const Index nElems = 4*nD*nD;
  sparse_idxs[0].resize(nElems);
  sparse_idxs[1].resize(nElems);

  Index idx = 0;
  // d2L(n,n) block
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
    sparse_idxs[0][idx] = iD;
    sparse_idxs[1][idx] = jD;
    ++idx;
  }}
  // d2L(n,n+1) block
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
    sparse_idxs[0][idx] = iD;
    sparse_idxs[1][idx] = jD+nD;
    ++idx;
  }}
  // d2L(n+1,n) block
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
    sparse_idxs[0][idx] = iD+nD;
    sparse_idxs[1][idx] = jD;
    ++idx;
  }}
  // d2L(n+1,n+1) block
  for(Index iD = 0; iD < nD; ++iD) {
  for(Index jD = 0; jD < nD; ++jD) {
    sparse_idxs[0][idx] = iD+nD;
    sparse_idxs[1][idx] = jD+nD;
    ++idx;
  }}
}


#endif // A0_4DVAR_WEAK_H_
