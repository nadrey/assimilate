#ifndef SPARSE_TENSOR_H_
#define SPARSE_TENSOR_H_

// My includes
#include "Utils.h"

////////////////////////////////////////////////////////////////
class SparseTensor {
public:
  // Typedefs
  typedef std::vector<IndexVec> Indices;

  // Constructors
  SparseTensor(const Index rank,
               const Index *dimensions);
  SparseTensor(const IndexVec &dimensions,
               const Indices &indices,
               const RealVec &values);
  SparseTensor(const Index rank,
               const Index *dimensions,
               const IndexVec *idxs,
               const RealVec &values);
  // Copy constructor
  SparseTensor(const SparseTensor &tensor);
  // Destructor
  ~SparseTensor();

  // Get methods
  Index rank() const
    { return _dimensions.size(); }
  const IndexVec& dimensions() const
    { return _dimensions; }

  // Operators

  // Methods

  // Output stream
  friend std::ostream& operator<<(std::ostream&, const SparseTensor&);

private:
  IndexVec _dimensions;
  Indices _indices;
  RealVec _values;
};

////////////////////////////////////////////////////////////////
// Constructors
SparseTensor::SparseTensor(const Index rank,
                           const Index *dimensions) :
  _dimensions(rank), _indices(rank) {
  std::copy(dimensions,dimensions+rank,_dimensions.begin());
}
///
SparseTensor::SparseTensor(const IndexVec &dimensions,
                           const Indices &indices,
                           const RealVec &values) :
  _dimensions(dimensions), _indices(indices), _values(values) {
  if(rank() == 0) {
    return;
  }
  const Index num_elems = _indices[0].size();
  for(Index i = 1; i < (Index)_indices.size(); ++i) {
    if((Index)_indices[i].size() != num_elems) {
      throw_exception("SparseTensor::SparseTensor:: "
              "inconsistent number of indices");
    }
  }
}
///
SparseTensor::SparseTensor(const Index rank,
                           const Index *dimensions,
                           const IndexVec *idxs,
                           const RealVec &values) :
  _dimensions(rank), _indices(rank), _values(values) {
  if(rank == 0) {
    return;
  }
  std::copy(dimensions,dimensions+rank,_dimensions.begin());
  const Index num_elems = idxs[0].size();
  for(Index i = 0; i < rank; ++i) {
    if((Index) idxs[i].size() != num_elems) {
      throw_exception("SparseTensor::SparseTensor:: "
              "inconsistent number of indices");
    }
    _indices[i].resize(num_elems);
    std::copy(idxs[i].begin(),idxs[i].end(),_indices[i].begin());
  }
}
///
SparseTensor::SparseTensor(const SparseTensor &tensor) :
  _indices (tensor._indices), _values (tensor._values) {
}
///
SparseTensor::~SparseTensor() {
}

////////////////////////////////////////////////////////////////
///
struct SparseIndexSorter {
  SparseIndexSorter() {
    _nIdxs = 0; _idxVec = NULL;
  }

  bool operator() (int i,int j) {
    if(i == j) {
      return true;
    }
    for(Index idx = 0; idx < _nIdxs; ++idx) {
      //std::cout << _idxVec[idx][i] << " " << _idxVec[idx][j] << " ";
      if(_idxVec[idx][i] < _idxVec[idx][j]) {
        //std::cout << "true" << std::endl;
        return true;
      }
      else if(_idxVec[idx][i] > _idxVec[idx][j]) {
        //std::cout << "false" << std::endl;
        return false;
      }
    }
    // Error, identical elements
    for(Index idx = 0; idx < _nIdxs; ++idx) {
      std::cout << _idxVec[idx][i] << " " << _idxVec[idx][j] << " ";
    }
    std::cout << std::endl;
    throw_exception("Identical elements");
    return false;
  }
///
  void sort(const IndexVec *idxVec, Index  nIdxs, IndexVec &order) {
    _idxVec = idxVec;
    _nIdxs = nIdxs;
    order.resize(idxVec[0].size());
    for(Index i = 0; i < (Index)order.size(); ++i) {
      order[i] = i;
    }
    std::sort(order.begin(),order.end(),*this);
  }
  const IndexVec *_idxVec;
  Index  _nIdxs;
};

////////////////////////////////////////////////////////////////
/// TODO For now assumes a matrix structure
std::ostream& operator<<(std::ostream &stream,
                         const SparseTensor &tensor) {
  SparseIndexSorter sorter;
  IndexVec order;
  sorter.sort(&tensor._indices.front(),tensor.rank(), order);
  Index idx = 0;
  bool done = false;
  for(Index i = 0; i < tensor._dimensions[0]; ++i) {
    for(Index j = 0; j < tensor._dimensions[1]; ++j) {
      stream << std::fixed
             << std::setw(10)
             << std::setprecision(6);
      if(!done &&
         tensor._indices[0][order[idx]] == i &&
         tensor._indices[1][order[idx]] == j) {
        stream << tensor._values[order[idx]];
        ++idx;
        done = !(idx < (Index)tensor._values.size());
      }
      else {
        stream << 0;
      }
      stream << " | ";
    }
    stream << std::endl;
  }
  return stream;
}

////////////////////////////////////////////////////////////////
/// Generate a list of indexes where the inner row of a sparse
/// tensor starts. This can be considered a list of pointers
/// to the first non-zero elements in each row, which is useful
/// for efficient arithmetic operations.
///
/// See e.g. https://en.wikipedia.org/wiki/Sparse_matrix#Yale
///
/// Parameters:
///  1) idxs  (in) - ptr to an array of index vectors
///  2) nIdxs (in) - number of vectors in idxs, same as
///                  rank of the tensor
///  3) num_dims (in) - number of dimensions at each level of the tensor
///  3) ptrs  (out) - an index vector whose values are the indices
///                   of the first non-zero elements in each row
///
/// Requirements:
///  1) idxs must be in sorted order
///
/// For instance if we have a 3x3x3 tensor with sparsity structure:
///    0 x 0  |  0 0 0  |  0 0 0
///    x 0 0  |  0 x x  |  0 0 0
///    0 0 0  |  0 0 0  |  0 x x
///
/// Its nonzero indices are:
///   idxs[0] = {0,0,1,1,2,2}
///   idxs[1] = {0,1,1,1,2,2}
///   idxs[2] = {1,0,1,2,1,2}
///
/// The result from inner_row_ptrs would be
///   ptrs = {0 1 2 2 2 4 4 4 4 6}
///
void inner_row_ptrs(const IndexVec *idxs, Index nIdxs,
                    const Index *num_dims, IndexVec &ptrs) {
/*
  for(Index i = 0; i < idxs[0].size(); ++i) {
    for(Index n = 0; n < nIdxs; ++n) {
      std::cout << idxs[n][i] << "  ";
    }
    std::cout << std::endl;
  }
*/
  // Allocate space for ptrs
  ptrs.clear();
  Index  num_elems = 1;
  for(Index i = 0; i < nIdxs-1; ++i) {
    num_elems *= num_dims[i];
  }
  ptrs.resize(num_elems+1,0);
  if(idxs[0].empty()) {
    return;
  }

  // Indices of the current rows
  IndexVec row_idxs(nIdxs-1);

  // Iterators to idxs
  typedef IndexVec::const_iterator CIter;
  typedef std::vector<CIter> CIterVec;
  CIterVec iters(nIdxs-1);
  for(Index iDim = 0; iDim < nIdxs-1; ++iDim) {
    iters[iDim] = idxs[iDim].begin();
  }

  // Index pointer
  Index idx_ptr = 0;
  typedef IndexVec::iterator Iter;
  Iter iPtr = ptrs.begin();

  // Count number of non0s per row
  CIter it = idxs[nIdxs-2].begin(),
        end = idxs[nIdxs-2].end();
  while(it != end) {
    // count the number of non-zeros in the current row
    bool found = true;
    Index  count = 0;
    while(found) {
      for(Index iDim = 0; iDim < nIdxs-1; ++iDim) {
        //std::cout << *iters[iDim] << "  "
        //          << row_idxs[iDim] << std::endl;
        if((*iters[iDim]) != row_idxs[iDim]) {
          found = false;
          break;
        }
      }
      //std::cout << "===========" << std::endl;
      if(found) {
        ++count;
        for(Index iDim = 0; iDim < nIdxs-1; ++iDim) {
          ++iters[iDim];
        }
        ++it;
        found = (it != end);
      }
    }
    if(it == end) {
      (*iPtr) = idx_ptr; ++iPtr;
      idx_ptr += count;
      break;
    }
    // count the number of empty rows
    // until the next non-zero element
    Index  nn0s = 0;
    for(Index iDim = 0; iDim < nIdxs-1; ++iDim) {
      //std::cout << *iters[iDim] << "  "
        //        << row_idxs[iDim] << std::endl;
      Index  offset = *iters[iDim]-row_idxs[iDim];
      nn0s *= num_dims[iDim];
      nn0s += offset;
      row_idxs[iDim] = *iters[iDim];
    }
    // add to pointers and increment
    (*iPtr) = idx_ptr; ++iPtr;
    idx_ptr += count;
    for(Index iN0 = 0; iN0 < nn0s-1; ++iN0) {
      (*iPtr) = idx_ptr; ++iPtr;
    }
    /*
    std::cout << "===========" << std::endl;
    std::cout << "Count is: " << count << std::endl;
    std::cout << "Skipping next: " << nn0s << std::endl;
    for(Index i = 0; i < num_elems+1; ++i) {
      std::cout << ptrs[i] << "  ";
    }
    std::cout << std::endl;
    std::cout << "===========" << std::endl;
    */
  }
  // Fill the final pointer slots
  while(iPtr != ptrs.end()) {
    (*iPtr) = idx_ptr; ++iPtr;
  }
/*
  for(Index i = 0; i < num_elems+1; ++i) {
    std::cout << ptrs[i] << "  ";
  }
  std::cout << std::endl;
  std::cout << "===========" << std::endl;
*/
}

#endif /* SPARSE_TENSOR_H_ */
