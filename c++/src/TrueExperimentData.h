#ifndef TRUE_EXP_DATA_H_
#define TRUE_EXP_DATA_H_

// My includes
#include "EstimationData.h"

////////////////////////////////////////////////////////////////
/// 
class TrueExperimentData : public EstimationData {
public:

  // Struct to hold initialization data
  struct InitialData : public EstimationData::InitialData {
    RealVec _observations;
  };

  // Constructors
  TrueExperimentData(const DynamicalModel &dyn_model,
                     const TimeDomain &time_domain,
                     const ObservationModel &obs_model);

  // Destructor
  virtual ~TrueExperimentData();

private:
  // Initialization methods
  virtual void initialize(EstimationData::InitialData &init);

  // Copy constructor
  TrueExperimentData(const TrueExperimentData&);
};

////////////////////////////////////////////////////////////////
/// constructor
TrueExperimentData::TrueExperimentData(const DynamicalModel &dyn_model,
                                       const TimeDomain &time_domain,
                                       const ObservationModel &obs_model) :
  EstimationData(dyn_model,time_domain,obs_model) {
}

////////////////////////////////////////////////////////////////
/// destructor
TrueExperimentData::~TrueExperimentData() {
}

////////////////////////////////////////////////////////////////
/// Initialize true experiment
void TrueExperimentData::initialize(EstimationData::InitialData &init) {
  EstimationData::initialize(init);

  // Copy observations
  TrueExperimentData::InitialData &true_data_init =
      static_cast<TrueExperimentData::InitialData&>(init);
  _observations = true_data_init._observations;
}


# endif // TRUE_EXP_DATA_H_
