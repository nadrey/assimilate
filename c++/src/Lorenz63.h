#ifndef LORENZ_63_H_
#define LORENZ_63_H_

// My includes
#include "Utils.h"
#include "DynamicalModel.h"
#include "ODEStepper.h"

////////////////////////////////////////////////////////////////
/// Lorenz 1963 model
///
///   dxdt = s*(y-x);
///   dydt = x*(r-z)-y;
///   dzdt = x*y-b*z;
///
class Lorenz63 : public DynamicalModel {
public:
  Lorenz63();

  // Get methods
  virtual Index num_states() const;
  virtual Index num_params() const;
  virtual Index num_derivs() const;
  virtual void state_bounds(Real *x_lower, Real *x_upper) const;
  virtual void param_bounds(Real *p_lower, Real *p_upper) const;

  // Vector field evaluations
  void f_eval(const Real &t, const Real *X, Real *dxdt) const;
  void df_eval(const Real &t, const Real *X, Real *df) const;
  void df_sparse_eval(const Real &t, const Real *X, Real *df) const;
  void d2f_sparse_eval(const Real &t, const Real *X, Real *d2f) const;

  // Sparsity patterns
  virtual void df_sparse_idxs(IndexVec *df_sparse_idxs) const;
  virtual void d2f_sparse_idxs(IndexVec *d2f_sparse_idxs) const;

  // Random initial conditions
  virtual void random_state(Real *x) const;
  //virtual void random_param(Real *x) const;

private:
  // Initialize model
  void initialize();

  // Parameters
  Real _params[3];
};

////////////////////////////////////////////////////////////////
/// constructor
Lorenz63::Lorenz63() {
  initialize();
}

////////////////////////////////////////////////////////////////
/// Get methods
Index Lorenz63::num_states() const {
  return 3;
}
///
Index Lorenz63::num_params() const {
  return 0;
}
///
Index Lorenz63::num_derivs() const {
  return 2;
}
///
void Lorenz63::state_bounds(Real *x_lower, Real *x_upper) const {
  for(Index iD = 0; iD < num_states(); ++iD) {
    x_lower[iD] = -1.e2;
    x_upper[iD] = +1.e2;
  }
}
///
void Lorenz63::param_bounds(Real *p_lower, Real *p_upper) const {
}

////////////////////////////////////////////////////////////////
void Lorenz63::initialize() {
  // TODO For now, hard-code parameter values
  _params[0] = 10.;
  _params[1] = 60.;
  _params[2] = 8./3.;
}

////////////////////////////////////////////////////////////////
/// generate random state on the attractor
void Lorenz63::random_state(Real *x) const {
  const Index nD = num_states();
  RealVec x_lower(nD), x_upper(nD);
  state_bounds(&x_lower.front(),&x_upper.front());
  for(Index iD = 0; iD < nD; ++iD) {
    x[iD] = (x_upper[iD]-x_lower[iD])*(rand_real()-0.5) + x_lower[iD];
  }

  // Integrate to remove transients
  Index  nNp1 = 1001; Real dt = 0.01;
  RK4Step<Lorenz63> ode(*this,this->num_states());
  for(Index iN = 0; iN < nNp1; ++iN) {
    ode.step(0.,x,dt,x);
  }
}

////////////////////////////////////////////////////////////////
/// evaluate vector field at x(t)
/// Parameters:
///  1) t (in) - time of evaluation
///  2) X (in) - point of evaluation
///  3) dxdt (out) - output vector field
void Lorenz63::f_eval(const Real &t, const Real *X, Real *dxdt) const {
  const Real *P = _params;
  const Real &s = P[0], &r = P[1], &b = P[2],
             &x = X[0], &y = X[1], &z = X[2];
  dxdt[0] = s*(y-x);
  dxdt[1] = x*(r-z)-y;
  dxdt[2] = x*y-b*z;
}

////////////////////////////////////////////////////////////////
/// evaluate Jacobian of vector field df_i/dx_j
/// Parameters:
///  1) t (in) - time of evaluation
///  2) X (in) - point of evaluation
///  3) df (out) - output all Jacobian elements
///                in row-major order
void Lorenz63::df_eval(const Real &t, const Real *X, Real *df) const {
  const Real *P = _params;
  const Real &s = P[0], &r = P[1], &b = P[2],
             &x = X[0], &y = X[1], &z = X[2];
  df[0] = -s;  df[1] = s;  df[2] = 0;
  df[3] = r-z; df[4] = -1; df[5] = -x;
  df[6] = y;   df[7] = x;  df[8] = -b;
}

////////////////////////////////////////////////////////////////
/// evaluate sparse Jacobian of vector field df_i/dx_j
/// Parameters:
///  1) t (in) - time of evaluation
///  2) X (in) - point of evaluation
///  3) df (out) - output Jacobian nonzero elements
///                in row-major order
void Lorenz63::df_sparse_eval(const Real &t, const Real *X, Real *df) const {
  const Real *P = _params;
  const Real &s = P[0], &r = P[1], &b = P[2],
             &x = X[0], &y = X[1], &z = X[2];
  df[0] = -s;  df[1] = s;  //df[2] = 0;
  df[2] = r-z; df[3] = -1; df[4] = -x;
  df[5] = y;   df[6] = x;  df[7] = -b;
}

////////////////////////////////////////////////////////////////
/// get sparsity structure of vector field Jacobian in matrix
/// form
/// Parameters:
///  1) idxs[2] - i,j indices of nonzero elements
void Lorenz63::df_sparse_idxs(IndexVec *idxs) const {
  idxs[0].resize(8);
  idxs[1].resize(8);
  idxs[0][0] = 0; idxs[1][0] = 0;
  idxs[0][1] = 0; idxs[1][1] = 1;
  idxs[0][2] = 1; idxs[1][2] = 0;
  idxs[0][3] = 1; idxs[1][3] = 1;
  idxs[0][4] = 1; idxs[1][4] = 2;
  idxs[0][5] = 2; idxs[1][5] = 0;
  idxs[0][6] = 2; idxs[1][6] = 1;
  idxs[0][7] = 2; idxs[1][7] = 2;
}

////////////////////////////////////////////////////////////////
/// evaluate sparse Hessian of vector field d2f_i / dx_j dx_k
/// Parameters:
///  1) t (in) - time of evaluation
///  2) X (in) - point of evaluation
///  3) d2f (out) - output Hessian nonzero elements
///                 in row-major order
void Lorenz63::d2f_sparse_eval(const Real &t, const Real *X, Real *d2f) const {
  d2f[0] = -1;  // d2f_1 / dx_0 dx_2
  d2f[1] = -1;  // d2f_1 / dx_2 dx_0
  d2f[2] =  1;  // d2f_2 / dx_0 dx_1
  d2f[3] =  1;  // d2f_2 / dx_1 dx_0
}

////////////////////////////////////////////////////////////////
/// get sparsity structure of vector field Hessian in 3 tensor
/// format
/// Parameters:
///  1) idxs[3]  (out) - i,j,k indices of nonzero elements
void Lorenz63::d2f_sparse_idxs(IndexVec *idxs) const {
  idxs[0].resize(4);
  idxs[1].resize(4);
  idxs[2].resize(4);
  idxs[0][0] = 1; idxs[1][0] = 0; idxs[2][0] = 2;
  idxs[0][1] = 1; idxs[1][1] = 2; idxs[2][1] = 0;
  idxs[0][2] = 2; idxs[1][2] = 0; idxs[2][2] = 1;
  idxs[0][3] = 2; idxs[1][3] = 1; idxs[2][3] = 0;
}

# endif // LORENZ_63_H_
