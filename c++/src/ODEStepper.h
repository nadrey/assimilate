#ifndef ODE_STEPPER_H_
#define ODE_STEPPER_H_

// My includes
#include "Utils.h"

////////////////////////////////////////////////////////////////
///
template <class Function>
class EulerStep {
public:
  EulerStep(const Function &f, const Index   dim) :
    _f (f), _dim (dim), _dxdt (new Real[_dim]) {
  }
  ~EulerStep() {
    delete [] _dxdt;
  }

  void step(const Real &t, const Real *x0, const Real &dt, Real *x) {
    _f(t,x0,_dxdt);
    for(Index   i = 0; i < _dim; ++i) {
      x[i] = x0[i] + dt*_dxdt[i];
    }
  }

private:
  EulerStep(const EulerStep&);
  const Function &_f;
  const Index   _dim;
  Real *_dxdt;
};

////////////////////////////////////////////////////////////////
///
template <class Function>
class RK4Step {
public:
  RK4Step(const Function &f, const Index   dim) :
    _f (f), _dim (dim),
    _k1   (new Real[_dim]),
    _k23  (new Real[_dim]),
    _k4   (new Real[_dim]),
    _xTmp (new Real[_dim]) {
  }
  ~RK4Step() {
    delete [] _k1;
    delete [] _k23;
    delete [] _k4;
    delete [] _xTmp;
  }

  void step(const Real &t, const Real *x0, const Real &dt, Real *x) {
    Index   dim = _dim;
    Real half_dt  = dt*0.5,
         sixth_dt = dt/6.;
    _f(t,x0,_k1);
    for(Index   i = 0; i < dim; ++i) {
      _xTmp[i] = x0[i] + half_dt*_k1[i];
    }
    _f(t+half_dt,_xTmp,_k4);
    for(Index   i = 0; i < dim; ++i) {
      _xTmp[i] = x0[i] + half_dt*_k4[i];
    }
    _f(t+half_dt,_xTmp,_k23);
    for(Index   i = 0; i < dim; ++i) {
      _xTmp[i] = x0[i] + dt*_k23[i];
      _k23[i] += _k4[i];
    }
    _f(t+dt,_xTmp,_k4);
    for(Index   i = 0; i < dim; ++i) {
      x[i] = x0[i] + sixth_dt*(_k1[i] + 2*_k23[i] + _k4[i]);
    }
  }

private:
  const Function &_f;
  const Index   _dim;
  Real *_k1, *_k23, *_k4, *_xTmp;
  RK4Step(const RK4Step&);
};


#endif /* ODE_STEPPER_H_ */
