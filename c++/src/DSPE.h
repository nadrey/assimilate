#ifndef DSPE_H_
#define DSPE_H_

// My includes
#include "A0_4DVar_Strong.h"

////////////////////////////////////////////////////////////////
///
template<class Model>
class DSPE : public A0_4DVar_Strong<Model> {
public:

  // Constructor
  DSPE();
  virtual ~DSPE();

  // Class name
  virtual const char* classname() const
    { return "DSPE"; }

  // Compute objective function
  virtual void f(const double *Xin, double &f);
  virtual void df(const double *Xin, double *df);
  using A0_4DVar_Strong<Model>::df_sparse;
  virtual void d2f_sparse(const double *Xin, double *d2f);
  virtual void d2f_sparse(UIntVec &sp_idxs_i, 
                      UIntVec &sp_idxs_j);

  // Compute dynamical constraints
  virtual void g(const double *Xin, double *g);
  virtual void dg_sparse(const double *Xin, double *dg);
  virtual void dg_sparse(UIntVec &sp_idxs_i, 
                     UIntVec &sp_idxs_j);
  virtual void d2g_sparse(const double *Xin, double *d2g);
  virtual void d2g_sparse(UIntVec &sp_idxs_i, 
                      UIntVec &sp_idxs_j,
                      UIntVec &sp_idxs_k);
  
  // Compute number of variables 
  virtual uint num_variables() const;
  
  // Generate random initial condition
  virtual void random_X0(DoubleVec &x0) const;

  // Set model
  virtual void set_model(const Model *model);
  
protected:
  void initialize();

  // Special class that implements ODE coupling
  class Coupling;
  friend class Coupling;
  Coupling *_coupling;
  
  // Make base data members available
  UIntVec _idxs_ctrl_sparse;
  
  // Weight of delta_x^2 term
  double _alpha;
};

////////////////////////////////////////////////////////////////
/// constructor that extracts parameters/data from object fields
template<class Model>
DSPE<Model>::DSPE() :
  A0_4DVar_Strong<Model>(),
  _coupling (new Coupling(this)) {
  // TODO Hard code these for now
  _alpha = 1.;
  // Modify df to include control terms
  _idxs_ctrl_sparse.resize(_nMeas);
  typedef UIntVec::const_iterator Iter;
  Iter iIter, jIter;
  for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
    uint idxMeas = _measIdxs[iMeas],
         idxSp = 0;
    bool found = false;
    for(iIter = _idxs_df_sparse[0].begin(), jIter = _idxs_df_sparse[1].begin();
          iIter != _idxs_df_sparse[0].end(); ++iIter, ++jIter) {
      if(*iIter == *jIter) {
        if(*iIter == idxMeas) {
          found = true;
          break;
        }
      }
      ++idxSp;
    }
    if(!found) {
      _idxs_df_sparse[0].push_back(idxMeas);
      _idxs_df_sparse[1].push_back(idxMeas);
    }
    _idxs_ctrl_sparse[iMeas] = idxSp;
  }
}

////////////////////////////////////////////////////////////////
/// destructor
template<class Model>
DSPE<Model>::~DSPE() {
  delete _coupling;
}

////////////////////////////////////////////////////////////////
/// return the number of optimization variables
/// reimplemented to include control variables
template<class Model>
uint DSPE<Model>::num_variables() const { 
 return (_nMeas + _dimX)*(_nT+1); 
}

////////////////////////////////////////////////////////////////
/// use model to generate a random initial path
/// reimplemented to include control variables
template<class Model>
void DSPE<Model>::random_X0(DoubleVec &X0) const {
  X0.resize(num_variables(),0.);
  for(uint it = 0; it <= _nT; ++it) {
    _model->random_x0(&X0.front() + it*_dimX);
  }
  uint offset = _dimX*(_nT+1);
  for(uint it = 0; it <= _nT; ++it) {
    for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
      X0[offset + it*_nMeas + iMeas] = rand_real()-0.5;
    }
  }
}

////////////////////////////////////////////////////////////////
/// compute action as objective function
/// reimplemented to include control variables
template<class Model>
void DSPE<Model>::f(const double *Xin, double &f) {
  
  // Compute measurement term
  A0_4DVar_Strong<Model>::f(Xin,f);
  
  // Add control term
  double tmp = 0;
  const double *u = Xin + (_nT+1)*_dimX;
  for(uint it = 0; it <= _nT; ++it, 
        u += _nMeas) {
    for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
      tmp += u[iMeas]*u[iMeas];
    }
  }
  
  // Apply scale factor;
  double sf = 0.5*_alpha/(_nT+1.);
  f += sf*tmp;
}

////////////////////////////////////////////////////////////////
/// compute gradient of objective function
/// reimplemented to include control variables
template<class Model>
void DSPE<Model>::df(const double *Xin, double *df) {
  // Compute measurement term
  A0_4DVar_Strong<Model>::df(Xin,df);
  
  // Add control terms
  double sf = _alpha/(_nT+1.);
  const double *u = Xin + (_nT+1)*_dimX;
  df += (_nT+1)*_dimX;
  for(uint it = 0; it <= _nT; ++it, 
        df += _nMeas, u += _nMeas) {
    for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
      df[iMeas] = sf*u[iMeas];
    }
  }
}

////////////////////////////////////////////////////////////////
///
template<class Model>
void DSPE<Model>::d2f_sparse(const double *Xin, double *d2f) {
  // Compute measurement term
  A0_4DVar_Strong<Model>::d2f_sparse(Xin,d2f);
  
  // Add control terms
  double sf = _alpha/(_nT+1.);
  d2f += (_nT+1)*_nMeas;
  for(uint it = 0; it <= _nT; ++it) {
    for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
      (*d2f) = sf; ++d2f;
    }
  }
}

////////////////////////////////////////////////////////////////
/// 
template<class Model>
void DSPE<Model>::d2f_sparse(UIntVec &sp_idxs_i,
                         UIntVec &sp_idxs_j) {
  // Base method for measurement terms
  A0_4DVar_Strong<Model>::d2f_sparse(sp_idxs_i,sp_idxs_j);
  
  // Add control terms
  uint offset = _nMeas*(_nT+1);
  sp_idxs_i.resize(2*offset);
  sp_idxs_j.resize(2*offset);
  uint *iIter = &sp_idxs_i.front() + offset,
       *jIter = &sp_idxs_j.front() + offset;
  uint idx_ij = _dimX*(_nT+1);
  for(uint it = 0; it <= _nT; ++it) {
    for(uint iMeas = 0; iMeas < _nMeas; ++iMeas,
          ++iIter, ++jIter, ++idx_ij) {
      (*iIter) = (*jIter) = idx_ij;
    }
  }
}

////////////////////////////////////////////////////////////////
/// compute nonlinear constraints
/// reimplemented to include control variables
template <class Model>
void DSPE<Model>::g(const double *Xin, double *g) {
  
  // Create ODE integrator
  Coupling &coupling = *_coupling;
  EulerStep<Coupling> my_ode(coupling);
  
  // Allocate space for computation
  DoubleVec x_tmp(_dimX), dx(_nMeas,0.);
  
  // Set coupling perturbation
  coupling._deltaX = &dx.front();
  
  // Calculate coupling perturbation
  // all non-measured components are zero
  const double *x = Xin, *y = _Y,
               *u = Xin + (_nT+1)*_dimX;
  for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
    dx[iMeas]  = y[iMeas] - x[_measIdxs[iMeas]];
    dx[iMeas] *= u[iMeas]; // Apply coupling control
  }

  // Compute constraints
  double t = 0, half_dt = 0.5*_dt;
  uint it = 0;
  while(it < _nT) {
    // Constraint evaluated at midpoint between t_{i} and t_{i+1}
    double *x_mid = g + it*_dimX; 
    // Integrate forward a half step
    my_ode.step(t,x,half_dt,x_mid);
    // Iterate to t_{i+1}
    ++it; t += _dt; x += _dimX; y += _nMeas; u += _nMeas; 
    // Compute perturbation at t_{i+1}
    for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
      dx[iMeas]  = y[iMeas] - x[_measIdxs[iMeas]];
      dx[iMeas] *= u[iMeas]; // Apply coupling control
    }
    // Integrate backward a half step
    my_ode.step(t,x,-half_dt,&x_tmp.front());
    // Compute difference at the midpoint
    for(uint ix = 0; ix < _dimX; ++ix) {
      x_mid[ix] -= x_tmp[ix]; 
    }
  }
}

////////////////////////////////////////////////////////////////
///
template <class Model>
void DSPE<Model>::dg_sparse(const double *Xin, double *dg) {
  
  // Space for vector field
  uint num_df_non0s = _idxs_df_sparse[0].size();
  DoubleVec df(num_df_non0s);
  
  // While loop avoids recomputing df/dx
  typedef UIntVec::const_iterator Iter;
  Iter itSp_i, itSp_j;  
  const double *x = Xin, *y = _Y,
               *u = Xin + (_nT+1)*_dimX;
  double t = 0, half_dt = 0.5*_dt;
  uint it = 0, idx_df;

  // Compute df/dx at x(t)
  _model->df_sparse(t,x,&df.front());
  for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
    df[_idxs_ctrl_sparse[iMeas]] -= u[iMeas];
  }
  
  while(it < _nT) {
    // Bottom/left block
    itSp_i = _idxs_df_sparse[0].begin();
    itSp_j = _idxs_df_sparse[1].begin();
    idx_df = 0;
    while(itSp_i != _idxs_df_sparse[0].end()) {
      (*dg) = half_dt*df[idx_df];
      if(*itSp_i == *itSp_j) {
        (*dg) += 1.;
      }
      ++dg; ++idx_df; ++itSp_i; ++itSp_j;
    }        
    // Iterate to t_{i+1}
    ++it; t += _dt; x += _dimX; y += _nMeas; u += _nMeas; 
    // Reset df/dx to zero
    // (b/c coupling perturbation is applied with "-=")
    std::fill(df.begin(),df.end(),0);
    // Evaluate df/dx at x(t)
    _model->df_sparse(t,x,&df.front());
    for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
      df[_idxs_ctrl_sparse[iMeas]] -= u[iMeas];
    }
    // Top/right block
    itSp_i = _idxs_df_sparse[0].begin();
    itSp_j = _idxs_df_sparse[1].begin();
    idx_df = 0;
    while(itSp_i != _idxs_df_sparse[0].end()) {
      (*dg) = half_dt*df[idx_df];
      if(*itSp_i == *itSp_j) {
        (*dg) -= 1.;
      }
      ++dg; ++idx_df; ++itSp_i; ++itSp_j;
    }
  }
  
  
  // Add control terms
  x = Xin; y = _Y;
  for(uint it = 0; it < _nT; ++it) {
    for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
      (*dg) = half_dt*(y[iMeas] - x[_measIdxs[iMeas]]); ++dg;
    }
    x += _dimX; y += _nMeas;
    for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
      (*dg) = half_dt*(y[iMeas] - x[_measIdxs[iMeas]]); ++dg;
    }
  }
}

////////////////////////////////////////////////////////////////
/// 
template<class Model>
void DSPE<Model>::dg_sparse(UIntVec &sp_idxs_i, 
                        UIntVec &sp_idxs_j) {
  A0_4DVar_Strong<Model>::dg_sparse(sp_idxs_i,sp_idxs_j);
  
  // Add control terms
  uint num_df_non0s = sp_idxs_i.size(),
       num_ctrl_terms = 2*_nMeas*_nT,
       dimXT = _dimX*(_nT+1),
       idx_sparse = num_df_non0s;
  sp_idxs_i.resize(num_df_non0s + num_ctrl_terms);
  sp_idxs_j.resize(num_df_non0s + num_ctrl_terms);

  for(uint it = 0; it < _nT; ++it) {
    for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
      uint idxMeas = _measIdxs[iMeas];
      sp_idxs_i[idx_sparse] = it*_dimX + idxMeas;
      sp_idxs_j[idx_sparse] = dimXT + it*_nMeas + iMeas;
      //std::cout << sp_idxs_i[idx_sparse] 
        //<< "  " << sp_idxs_j[idx_sparse] << std::endl;
      ++idx_sparse;
    }
    for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
      uint idxMeas = _measIdxs[iMeas];
      sp_idxs_i[idx_sparse] = it*_dimX + idxMeas;
      sp_idxs_j[idx_sparse] = dimXT + (it+1)*_nMeas + iMeas;
      //std::cout << sp_idxs_i[idx_sparse] 
        //<< "  " << sp_idxs_j[idx_sparse] << std::endl;
      ++idx_sparse;
    }
  }
}

////////////////////////////////////////////////////////////////
///
template <class Model>
void DSPE<Model>::d2g_sparse(const double *Xin, double *d2g) {
  A0_4DVar_Strong<Model>::d2g_sparse(Xin,d2g);

  // Add control terms
  double half_dt = 0.5*_dt;
  uint num_d2f_non0s = _idxs_d2f_sparse[0].size(),
       nElems = 2*_nT*num_d2f_non0s;
  d2g = d2g + nElems;
  for(uint it = 0; it < _nT; ++it) {
    for(uint iMeas = 0; iMeas < 4*_nMeas; ++iMeas) {
      (*d2g) = -half_dt; ++d2g;
    }
  }
}

////////////////////////////////////////////////////////////////
/// 
template<class Model>
void DSPE<Model>::d2g_sparse(UIntVec &sp_idxs_i, 
                         UIntVec &sp_idxs_j,
                         UIntVec &sp_idxs_k) {
  A0_4DVar_Strong<Model>::d2g_sparse(sp_idxs_i,sp_idxs_j,sp_idxs_k);

  // Add control terms
  uint num_d2f_non0s = sp_idxs_i.size(),
       num_ctrl_terms = 4*_nMeas*_nT,
       dimXT = _dimX*(_nT+1),
       idx_sparse = num_d2f_non0s;
  sp_idxs_i.resize(num_d2f_non0s + num_ctrl_terms);
  sp_idxs_j.resize(num_d2f_non0s + num_ctrl_terms);
  sp_idxs_k.resize(num_d2f_non0s + num_ctrl_terms);
  for(uint it = 0; it < _nT; ++it) {
    for(uint iMeas = 0; iMeas < _nMeas; ++iMeas) {
      uint idxMeas = _measIdxs[iMeas];
      sp_idxs_i[idx_sparse] = it*_dimX + idxMeas;
      sp_idxs_j[idx_sparse] = it*_dimX + idxMeas;
      sp_idxs_k[idx_sparse] = dimXT + it*_nMeas + iMeas;
      //std::cout << sp_idxs_i[idx_sparse] 
        //<< "  " << sp_idxs_j[idx_sparse]
        //<< "  " << sp_idxs_k[idx_sparse] << std::endl;
      ++idx_sparse;

      sp_idxs_i[idx_sparse] = it*_dimX + idxMeas;
      sp_idxs_j[idx_sparse] = (it+1)*_dimX + idxMeas;
      sp_idxs_k[idx_sparse] = dimXT + (it+1)*_nMeas + iMeas;
      //std::cout << sp_idxs_i[idx_sparse] 
        //<< "  " << sp_idxs_j[idx_sparse]
        //<< "  " << sp_idxs_k[idx_sparse] << std::endl;
      ++idx_sparse;

      sp_idxs_i[idx_sparse] = it*_dimX + idxMeas;
      sp_idxs_j[idx_sparse] = dimXT + it*_nMeas + iMeas;
      sp_idxs_k[idx_sparse] = it*_dimX + idxMeas;
      //std::cout << sp_idxs_i[idx_sparse] 
        //<< "  " << sp_idxs_j[idx_sparse]
        //<< "  " << sp_idxs_k[idx_sparse] << std::endl;
      ++idx_sparse;

      sp_idxs_i[idx_sparse] = it*_dimX + idxMeas;
      sp_idxs_j[idx_sparse] = dimXT + (it+1)*_nMeas + iMeas;
      sp_idxs_k[idx_sparse] = (it+1)*_dimX + idxMeas;
      //std::cout << sp_idxs_i[idx_sparse] 
        //<< "  " << sp_idxs_j[idx_sparse]
        //<< "  " << sp_idxs_k[idx_sparse] << std::endl;
      ++idx_sparse;
    }
  }
}

////////////////////////////////////////////////////////////////
///
template <class Model>
class DSPE<Model>::Coupling {
public:
  // Friends with DSPE
  friend class DSPE<Model>;

  // Constructor
  typedef DSPE<Model> Parent;
  Coupling(const Parent *parent) :
    _parent (parent), 
    _deltaX (NULL) {
  };

  // Dimension of model
  uint dim() const
    { return _parent->_dimX; }
  
  /// Compute model vector field with constant coupling
  void f(const double &t, const double *x, double *dxdt) const {
    // Compute vector field
    _parent->_model->f(t,x,dxdt);
    // Add perturbation
    for(uint iMeas = 0; iMeas < _parent->_nMeas; ++iMeas) {
      dxdt[_parent->_measIdxs[iMeas]] += _deltaX[iMeas]; 
    }
  }
    
private:
  Coupling(const Coupling&) {};
  const Parent *_parent;
  const double *_deltaX;
};

#endif // DSPE_H_
