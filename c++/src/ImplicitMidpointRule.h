#ifndef IMPLICIT_MIDPOINT_RULE_H_
#define IMPLICIT_MIDPOINT_RULE_H_

// My includes
#include "QuadRule.h"
#include "EstimationAction.h"
#include "ODEStepper.h"

////////////////////////////////////////////////////////////////
/// Implicit midpoint rule quadrature for approximating
/// dynamical constraints as integrals between two collocation
/// times t(n), t(n+1)
///
///  int_{t(n)}^{t(n+1)} dt*[ dx/dt - f(x) ] ~
///     x(n+1) - x(n) - dt*f[x(n+1/2)]
///
///  x(n+1/2) = 1/2*[x(n+1) + x(n)]
///
class ImplicitMidpointRule : public QuadRule {
public:
  // constructor
  ImplicitMidpointRule(const DynamicalModel &model);
  // destructor
  virtual ~ImplicitMidpointRule();
  // class name
  virtual std::string classname() const
    { return "ImplicitMidpointRule"; }

  // constraint evaluation
  virtual void g_eval(const Real &t_n, const Real &t_np1,
                      const Real *x_n, const Real *x_np1,
                      Real *g_n) const;
  virtual void dg_sparse_eval(const Real &t_n, const Real &t_np1,
                              const Real *x_n, const Real *x_np1,
                              Real *dg) const;
  virtual void d2g_sparse_eval(const Real &t_n, const Real &t_np1,
                               const Real *x_n, const Real *x_np1,
                               Real *d2g) const;
  virtual void d3g_sparse_eval(const Real &t_n, const Real &t_np1,
                               const Real *x_n, const Real *x_np1,
                               Real *d3g) const;

  // constraint sparsity pattern
  virtual void dg_sparse_idxs(IndexVec *sparse_idxs)  const;
  virtual void d2g_sparse_idxs(IndexVec *sparse_idxs) const;
  virtual void d3g_sparse_idxs(IndexVec *sparse_idxs) const;

private:
  // Model reference
  const DynamicalModel &_model;
  // Temp. data
  mutable RealVec _x_mid, _f_mid;
  // Cache model vector fields
  mutable RealVec _df_model_sparse,
                  _d2f_model_sparse,
                  _d3f_model_sparse;
  IndexVec _df_model_sparse_idxs[2],
           _d2f_model_sparse_idxs[3],
           _d3f_model_sparse_idxs[4];

  // Copy
  ImplicitMidpointRule(const ImplicitMidpointRule&);
};

////////////////////////////////////////////////////////////////
/// constructor
ImplicitMidpointRule::ImplicitMidpointRule(const DynamicalModel &model) :
  QuadRule(model),
  _model (model) {
  // Allocate temp data
  _x_mid.resize(_model.num_states());
  _f_mid.resize(_model.num_states());
  // Sparse indices of model derivatives
  _model.df_sparse_idxs(_df_model_sparse_idxs);
  _model.d2f_sparse_idxs(_d2f_model_sparse_idxs);
  _model.d3f_sparse_idxs(_d3f_model_sparse_idxs);
  // Storage space for model derivatives
  _df_model_sparse.resize(_df_model_sparse_idxs[0].size());
  _d2f_model_sparse.resize(_d2f_model_sparse_idxs[0].size());
  _d3f_model_sparse.resize(_d3f_model_sparse_idxs[0].size());
  // TODO Right now, we only support models where df
  //      diagonal entries are all non zero
  Index tmp = 0;
  for(Index idx = 0; idx < (Index)_df_model_sparse_idxs[0].size(); ++idx) {
    if(_df_model_sparse_idxs[0][idx] == _df_model_sparse_idxs[1][idx]) {
      ++tmp;
    }
  }
  if(tmp != _model.num_states()) {
    throw_exception("ImplicitMidpointRule::ImplicitMidpointRule: "
            "model df/dx diagonal elements must be non zero");
  }
}
/// destructor
ImplicitMidpointRule::~ImplicitMidpointRule() {
}

////////////////////////////////////////////////////////////////
/// Constraint evaluated at midpoint between t(n) and t(n+1)
void ImplicitMidpointRule::g_eval(const Real &t_n, const Real &t_np1,
                                  const Real *x_n, const Real *x_np1,
                                  Real *g_n) const {
  const Real dt = t_np1 - t_n, half_dt = 0.5*dt;

  // Compute x_{n+1/2}
  const Index nD = _model.num_states();
  for(Index iD = 0; iD < nD; ++iD) {
    _x_mid[iD] = 0.5*(x_n[iD] + x_np1[iD]);
  }

  // Evaluate vector field at midpoint
  _model.f_eval(t_n + half_dt,&_x_mid.front(),&_f_mid.front());

  // Compute difference at the midpoint
  for(Index iD = 0; iD < nD; ++iD) {
    g_n[iD] = x_np1[iD] - x_n[iD] - dt*_f_mid[iD];
  }
}

////////////////////////////////////////////////////////////////
///
void ImplicitMidpointRule::dg_sparse_eval(const Real &t_n, const Real &t_np1,
                                          const Real *x_n, const Real *x_np1,
                                          Real *dg) const {
  const Real dt = t_np1 - t_n, half_dt = 0.5*dt;

  // Iterators
  IndexVec::const_iterator it_sp_i, it_sp_j;
  Index idx_df = 0;

  // Compute x_{n+1/2}
  const Index nD = _model.num_states();
  for(Index iD = 0; iD < nD; ++iD) {
    _x_mid[iD] = 0.5*(x_n[iD] + x_np1[iD]);
  }

  // Evaluate model Jacobian at midpoint
  _model.df_sparse_eval(t_n + half_dt,&_x_mid.front(),&_df_model_sparse.front());

  // Fill block 1: dg/dx_n
  it_sp_i = _df_model_sparse_idxs[0].begin();
  it_sp_j = _df_model_sparse_idxs[1].begin();
  idx_df = 0;
  while(it_sp_i != _df_model_sparse_idxs[0].end()) {
    (*dg) = -half_dt*_df_model_sparse[idx_df];
    if(*it_sp_i == *it_sp_j) {
      (*dg) -= 1.;
    }
    ++dg; ++idx_df; ++it_sp_i; ++it_sp_j;
  }

  // Fill block 2: dg/dx_np1
  it_sp_i = _df_model_sparse_idxs[0].begin();
  it_sp_j = _df_model_sparse_idxs[1].begin();
  idx_df = 0;
  while(it_sp_i != _df_model_sparse_idxs[0].end()) {
    (*dg) = -half_dt*_df_model_sparse[idx_df];
    if(*it_sp_i == *it_sp_j) {
      (*dg) += 1.;
    }
    ++dg; ++idx_df; ++it_sp_i; ++it_sp_j;
  }
}

////////////////////////////////////////////////////////////////
///
void ImplicitMidpointRule::d2g_sparse_eval(const Real &t_n, const Real &t_np1,
                                           const Real *x_n, const Real *x_np1,
                                           Real *d2g) const {
  const Real dt = t_np1 - t_n, half_dt = 0.5*dt, fourth_dt = 0.25*dt;

  // Iterators
  IndexVec::const_iterator it_sp_i;
  Index idx_d2f = 0;

  // Compute x_{n+1/2}
  const Index nD = _model.num_states();
  for(Index iD = 0; iD < nD; ++iD) {
    _x_mid[iD] = 0.5*(x_n[iD] + x_np1[iD]);
  }

  // Evaluate model d2f at midpoint
  _model.d2f_sparse_eval(t_n + half_dt,&_x_mid.front(),&_d2f_model_sparse.front());

  // Fill blocks 1-4
  const Index num_blocks = 4;
  for(Index idx_block = 0; idx_block < num_blocks; ++idx_block) {
    it_sp_i = _d2f_model_sparse_idxs[0].begin();
    idx_d2f = 0;
    while(it_sp_i != _d2f_model_sparse_idxs[0].end()) {
      (*d2g) = -fourth_dt*_d2f_model_sparse[idx_d2f];
      ++d2g; ++idx_d2f; ++it_sp_i;
    }
  }
}

////////////////////////////////////////////////////////////////
///
void ImplicitMidpointRule::d3g_sparse_eval(const Real &t_n, const Real &t_np1,
                                           const Real *x_n, const Real *x_np1,
                                           Real *d3g) const {
  const Real dt = t_np1 - t_n, half_dt = 0.5*dt, eigth_dt = 0.125*dt;

  // Iterators
  IndexVec::const_iterator it_sp_i;
  Index idx_d3f = 0;

  // Compute x_{n+1/2}
  const Index nD = _model.num_states();
  for(Index iD = 0; iD < nD; ++iD) {
    _x_mid[iD] = 0.5*(x_n[iD] + x_np1[iD]);
  }

  // Evaluate model d3f at midpoint
  _model.d3f_sparse_eval(t_n + half_dt,&_x_mid.front(),&_d3f_model_sparse.front());

  // Fill blocks 1-8
  const Index num_blocks = 8;
  for(Index idx_block = 0; idx_block < num_blocks; ++idx_block) {
    it_sp_i = _d3f_model_sparse_idxs[0].begin();
    idx_d3f = 0;
    while(it_sp_i != _d3f_model_sparse_idxs[0].end()) {
      (*d3g) = -eigth_dt*_d3f_model_sparse[idx_d3f];
      ++d3g; ++idx_d3f; ++it_sp_i;
    }
  }
}

////////////////////////////////////////////////////////////////
/// Get sparsity pattern of 1st derivative.
///
/// Block structure is organized as:
///     n  n+1
///  || 1 ||  n
///  ||---||
//   || 2 || n+1
///
/// where 1 and 2 are D*D blocks containing model df sparsity
/// structure at times n, n+1 respectively.
///
void ImplicitMidpointRule::dg_sparse_idxs(IndexVec *sparse_idxs) const {
  // DynamicalModel
  const Index nD = _model.num_states();

  // Resize Index vectors
  Index num_df_non0s = _df_model_sparse_idxs[0].size();
  Index num_elems = 2*num_df_non0s;
  sparse_idxs[0].resize(num_elems);
  sparse_idxs[1].resize(num_elems);

  // Fill Index vectors
  typedef IndexVec::const_iterator Iter;
  Iter it_sp_i, it_sp_j;
  Index idx = 0;

  // Fill block 1: dg/dx_n
  it_sp_i = _df_model_sparse_idxs[0].begin();
  it_sp_j = _df_model_sparse_idxs[1].begin();
  while(it_sp_i != _df_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i ;
    sparse_idxs[1][idx] = *it_sp_j;
     ++idx; ++it_sp_i; ++it_sp_j;
  }

  // Fill block 2: dg/dx_np1
  it_sp_i = _df_model_sparse_idxs[0].begin();
  it_sp_j = _df_model_sparse_idxs[1].begin();
  while(it_sp_i != _df_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j + nD;
     ++idx; ++it_sp_i; ++it_sp_j;
  }
}

////////////////////////////////////////////////////////////////
/// Get sparsity pattern of 2rd derivative. Since there are no
/// cross terms at different times, (d_n d_n+1 g = 0), it is
/// easy to include the sparsity pattern of the model.
///
/// Block structure is organized as:
///     n  n+1
///  || 1 | 2 ||  n
///  ||---|---||
//   || 3 | 4 || n+1
///
/// where 1 - 4 are D*D blocks containing model d2f sparsity
/// structure at times n, n+1 respectively.
///
/// TODO: Utilize symmetry
///
void ImplicitMidpointRule::d2g_sparse_idxs(IndexVec *sparse_idxs) const {
  // DynamicalModel
  const Index nD = _model.num_states();

  // Resize Index vectors
  Index num_d2f_non0s = _d2f_model_sparse_idxs[0].size();
  Index num_elems = 4*num_d2f_non0s;
  sparse_idxs[0].resize(num_elems);
  sparse_idxs[1].resize(num_elems);
  sparse_idxs[2].resize(num_elems);

  // Fill Index vectors
  typedef IndexVec::const_iterator Iter;
  Iter it_sp_i, it_sp_j, it_sp_k;
  Index idx = 0;

  // Fill block 1: d2g / dx_n^2
  it_sp_i = _d2f_model_sparse_idxs[0].begin();
  it_sp_j = _d2f_model_sparse_idxs[1].begin();
  it_sp_k = _d2f_model_sparse_idxs[2].begin();
  while(it_sp_i != _d2f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j;
    sparse_idxs[2][idx] = *it_sp_k;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k;
  }

  // Fill block 2: d2g / dx_n dx_n+1
  it_sp_i = _d2f_model_sparse_idxs[0].begin();
  it_sp_j = _d2f_model_sparse_idxs[1].begin();
  it_sp_k = _d2f_model_sparse_idxs[2].begin();
  while(it_sp_i != _d2f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j;
    sparse_idxs[2][idx] = *it_sp_k + nD;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k;
  }

  // Fill block 3: d2g / dx_n+1 dx_n
  it_sp_i = _d2f_model_sparse_idxs[0].begin();
  it_sp_j = _d2f_model_sparse_idxs[1].begin();
  it_sp_k = _d2f_model_sparse_idxs[2].begin();
  while(it_sp_i != _d2f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j + nD;
    sparse_idxs[2][idx] = *it_sp_k;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k;
  }

  // Fill block 4: d2g / dx_n+1^2
  it_sp_i = _d2f_model_sparse_idxs[0].begin();
  it_sp_j = _d2f_model_sparse_idxs[1].begin();
  it_sp_k = _d2f_model_sparse_idxs[2].begin();
  while(it_sp_i != _d2f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j + nD;
    sparse_idxs[2][idx] = *it_sp_k + nD;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k;
  }
}

////////////////////////////////////////////////////////////////
/// Get sparsity pattern of 3rd derivative. Since there are no
/// cross terms at different times, (d_n d_n+1 g = 0), it is
/// easy to include the sparsity pattern of the model.
///
/// Block structure is organized as:
///       n           n+1
///  || 1 | 2 ||  || 5 | 6 ||  n
///  ||---|---||  ||---|---||
//   || 3 | 4 ||  || 7 | 8 || n+1
///
/// where 1 - 8 are D*D*D blocks containing model d3f sparsity
/// structure at times n, n+1 respectively.
///
/// TODO: Utilize symmetry
///
void ImplicitMidpointRule::d3g_sparse_idxs(IndexVec *sparse_idxs) const {
  // DynamicalModel
  const Index nD = _model.num_states();

  // Resize Index vectors
  Index num_d3f_non0s = _d3f_model_sparse_idxs[0].size();
  Index num_elems = 8*num_d3f_non0s;
  sparse_idxs[0].resize(num_elems);
  sparse_idxs[1].resize(num_elems);
  sparse_idxs[2].resize(num_elems);
  sparse_idxs[3].resize(num_elems);

  // Fill Index vectors
  typedef IndexVec::const_iterator Iter;
  Iter it_sp_i, it_sp_j, it_sp_k, it_sp_l;
  Index idx = 0;

  // Fill block 1: d3g / dx_n^3
  it_sp_i = _d3f_model_sparse_idxs[0].begin();
  it_sp_j = _d3f_model_sparse_idxs[1].begin();
  it_sp_k = _d3f_model_sparse_idxs[2].begin();
  it_sp_l = _d3f_model_sparse_idxs[3].begin();
  while(it_sp_i != _d3f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j;
    sparse_idxs[2][idx] = *it_sp_k;
    sparse_idxs[3][idx] = *it_sp_l;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k; ++ it_sp_l;
  }

  // Fill block 2: d3g / dx_n^2 dx_n+1
  it_sp_i = _d3f_model_sparse_idxs[0].begin();
  it_sp_j = _d3f_model_sparse_idxs[1].begin();
  it_sp_k = _d3f_model_sparse_idxs[2].begin();
  it_sp_l = _d3f_model_sparse_idxs[3].begin();
  while(it_sp_i != _d3f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j;
    sparse_idxs[2][idx] = *it_sp_k;
    sparse_idxs[3][idx] = *it_sp_l + nD;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k; ++ it_sp_l;
  }

  // Fill block 3: d3g / dx_n dx_n+1 dx_n
  it_sp_i = _d3f_model_sparse_idxs[0].begin();
  it_sp_j = _d3f_model_sparse_idxs[1].begin();
  it_sp_k = _d3f_model_sparse_idxs[2].begin();
  it_sp_l = _d3f_model_sparse_idxs[3].begin();
  while(it_sp_i != _d3f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j;
    sparse_idxs[2][idx] = *it_sp_k + nD;
    sparse_idxs[3][idx] = *it_sp_l;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k; ++ it_sp_l;
  }

  // Fill block 4: d3g / dx_n dx_n+1^2
  it_sp_i = _d3f_model_sparse_idxs[0].begin();
  it_sp_j = _d3f_model_sparse_idxs[1].begin();
  it_sp_k = _d3f_model_sparse_idxs[2].begin();
  it_sp_l = _d3f_model_sparse_idxs[3].begin();
  while(it_sp_i != _d3f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j;
    sparse_idxs[2][idx] = *it_sp_k + nD;
    sparse_idxs[3][idx] = *it_sp_l + nD;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k; ++it_sp_l;
  }

  // Fill block 5: d3g / dx_n+1 dx_n^2
  it_sp_i = _d3f_model_sparse_idxs[0].begin();
  it_sp_j = _d3f_model_sparse_idxs[1].begin();
  it_sp_k = _d3f_model_sparse_idxs[2].begin();
  it_sp_l = _d3f_model_sparse_idxs[3].begin();
  while(it_sp_i != _d3f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j + nD;
    sparse_idxs[2][idx] = *it_sp_k;
    sparse_idxs[3][idx] = *it_sp_l;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k; ++ it_sp_l;
  }

  // Fill block 6: d3g / dx_n+1 dx_n dx_n+1
  it_sp_i = _d3f_model_sparse_idxs[0].begin();
  it_sp_j = _d3f_model_sparse_idxs[1].begin();
  it_sp_k = _d3f_model_sparse_idxs[2].begin();
  it_sp_l = _d3f_model_sparse_idxs[3].begin();
  while(it_sp_i != _d3f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j + nD;
    sparse_idxs[2][idx] = *it_sp_k;
    sparse_idxs[3][idx] = *it_sp_l + nD;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k; ++ it_sp_l;
  }

  // Fill block 7: d3g / dx_n+1^2 dx_n
  it_sp_i = _d3f_model_sparse_idxs[0].begin();
  it_sp_j = _d3f_model_sparse_idxs[1].begin();
  it_sp_k = _d3f_model_sparse_idxs[2].begin();
  it_sp_l = _d3f_model_sparse_idxs[3].begin();
  while(it_sp_i != _d3f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j + nD;
    sparse_idxs[2][idx] = *it_sp_k + nD;
    sparse_idxs[3][idx] = *it_sp_l;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k; ++ it_sp_l;
  }

  // Fill block 8: d3g / dx_n+1^3
  it_sp_i = _d3f_model_sparse_idxs[0].begin();
  it_sp_j = _d3f_model_sparse_idxs[1].begin();
  it_sp_k = _d3f_model_sparse_idxs[2].begin();
  it_sp_l = _d3f_model_sparse_idxs[3].begin();
  while(it_sp_i != _d3f_model_sparse_idxs[0].end()) {
    sparse_idxs[0][idx] = *it_sp_i;
    sparse_idxs[1][idx] = *it_sp_j + nD;
    sparse_idxs[2][idx] = *it_sp_k + nD;
    sparse_idxs[3][idx] = *it_sp_l + nD;
    ++idx; ++it_sp_i; ++it_sp_j; ++it_sp_k; ++it_sp_l;
  }
}

#endif /* IMPLICIT_MIDPOINT_RULE_H_ */
