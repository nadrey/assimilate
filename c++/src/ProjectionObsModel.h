#ifndef PROJECTION_OBS_MODEL_H_
#define PROJECTION_OBS_MODEL_H_

// My includes
#include "Utils.h"

////////////////////////////////////////////////////////////////
/// 
class ProjectionObsModel {
public:
  // Constructors
  ProjectionObsModel();

  // Set methods
  void set_observed_state_idxs(const IndexVec &idxs);
  void set_observation_window_length(const Real &length);

  // Get methods
  const IndexVec& observed_state_idxs() const;
  Index num_observed_states() const;
  Real observation_window_length() const;

  // Initialization method
  void initialize(const DynamicalModel &model,
                  const TimeDomain &time_domain);

private:
  // Measured indices
  IndexVec _observed_state_idxs;
  // Window length
  Real _observation_window_length;
};

// TODO Create abstract observation model interface
typedef ProjectionObsModel ObservationModel;

////////////////////////////////////////////////////////////////
/// constructors
///
ProjectionObsModel::ProjectionObsModel() :
  _observed_state_idxs(0),
  _observation_window_length (0) {
}

////////////////////////////////////////////////////////////////
void ProjectionObsModel::initialize(const DynamicalModel &model,
                                    const TimeDomain &time_domain) {
  // By default, observe first index
  if(_observed_state_idxs.empty()) {
    if(_observed_state_idxs.size() == 0) {
      _observed_state_idxs.push_back(0);
    }
  }
  // Check for errors
  else {
    const Index nL = _observed_state_idxs.size();
    for(Index iL = 0; iL < nL; ++iL) {
      if(_observed_state_idxs[iL] >= model.num_states()) {
        std::string msg;
        msg = "ProjectionObsModel::ProjectionObsModel: "
              "Number of observed states cannot be larger than model dimension";
        throw_exception(msg.c_str());
      }
    }
    // TODO Check for duplicates
  }

  // By default, use full observation window
  const Index nN = time_domain.num_time_intervals();
  const Real dt = time_domain.dt();
  if(_observation_window_length == 0 ||
     _observation_window_length > nN*dt) {
    _observation_window_length = nN*dt;
  }
}

////////////////////////////////////////////////////////////////
/// set methods
void ProjectionObsModel::set_observed_state_idxs(const IndexVec &idxs) {
  _observed_state_idxs = idxs;
}
///
void ProjectionObsModel::set_observation_window_length(const Real &length) {
  _observation_window_length = length;
}

////////////////////////////////////////////////////////////////
/// get methods
const IndexVec& ProjectionObsModel::observed_state_idxs() const {
  return _observed_state_idxs;
}
///
Index ProjectionObsModel::num_observed_states() const {
  return _observed_state_idxs.size();
}
///
Real ProjectionObsModel::observation_window_length() const {
  return _observation_window_length;
}

# endif // PROJECTION_OBS_MODEL_H_
