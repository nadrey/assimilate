%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dXdt = var_eqn(t,X,D,f)
  x = X(1:D);
  if(length(X) == D)
    dXdt = f(t,x);
  elseif(length(X) == D*(D+1))
    [f,df] = f(t,x);
    phi = reshape(X(D+1:D*(D+1)),D,D);
    dphi = df*phi;
    dXdt = vertcat(f,dphi(:));
  elseif(length(X) == D*(D*(D+1)+1))
    [f,df,d2f] = f(t,x);
    phi1 = reshape(X(D+1:D*(D+1)),D,D);
    dphi1 = df*phi1;
    phi2 = reshape(X(D*(D+1)+1:D*(D*(D+1)+1)),D,D,D);
    dphi2 = zeros(size(phi2));
    for i = 1:D
    for j = 1:D
    for k = 1:D
    for l = 1:D
      dphi2(i,j,k) = dphi2(i,j,k) + df(i,l)*phi2(l,j,k);
    for m = 1:D
      dphi2(i,j,k) = dphi2(i,j,k) + d2f(i,l,m)*phi1(l,j)*phi1(m,k);
    end
    end
    end
    end
    end
    dXdt = vertcat(f,dphi1(:),dphi2(:));
  else
    error('var_eqn: improper dimension');
  end
end