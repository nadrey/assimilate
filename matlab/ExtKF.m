classdef ExtKF < DA_Filter
properties
  % static covariances
  sqrt_Ro; sqrt_Rf; sqrt_Rm;
  % covariance estimate
  sqrt_Rn; Kn;
  % options
  adaptive_obs;
  max_rank;
  do_AUS;
  do_inf_cov; is_cov;
  anneal_Rf;
  % tolerances
  pinv_tol = eps;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
function obj = ExtKF()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = [];
  end
  % base initialization
  obj = initialize@DA_Filter(obj,prob,in);
  % initialize covariances
  obj = set_prop(obj,in,'sqrt_Ro',1);
  obj = set_prop(obj,in,'sqrt_Rf',1);
  obj = set_prop(obj,in,'sqrt_Rm',1);
  % options
  obj = set_prop(obj,in,'adaptive_obs',false);
  obj = set_prop(obj,in,'max_rank',obj.nD);
  obj = set_prop(obj,in,'do_AUS',false);
  obj = set_prop(obj,in,'do_inf_cov',false);
  obj = set_prop(obj,in,'anneal_Rf',false);
  obj.is_cov = true;
  obj.init_obs();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function init_obs(obj)
  % adaptive obs requires twin experiment
  if(obj.adaptive_obs)
    if(~strcmpi(class(obj.prob.data),'TwinExpData'))
      error('ExtKF::init_coupling: adaptive obs. only allowed in twin expt');
    end
    % use full rank L
    obj.prob.obs.idxs = 1:obj.nD;
  end
  obj.sqrt_Rm = obj.sqrt_Rm(1);
  obj.sqrt_Ro = obj.sqrt_Ro(1);
  obj.sqrt_Rn = diag(obj.sqrt_Ro);
  obj.Kn = zeros(obj.nD,obj.nL);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function reset(obj)
  reset@DA_Filter(obj);
  obj.init_obs();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% reset object b/c obs operator has changed
% TODO Use events to handle changing number of observations
function reset_obs(obj)
  reset_obs@DA_Filter(obj);
  init_obs(obj);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = step(obj)
  ret = true;
  try
  % measurement update
  do_meas = obj.N_skip <= 1 || mod(obj.nN+1,obj.N_skip) == 1;
  if(do_meas)
    [x_n,obj.sqrt_Rn] = obj.meas_update();
    ret = ret && obj.accept_step(obj.t,x_n);
  end
  % time update
  [x_np1,obj.sqrt_Rn] = obj.time_update();
  ret = ret && obj.accept_step(obj.t+obj.dt,x_np1);
  % check if we are done
  ret = ret && step@DA_Filter(obj);
  catch me
    warning(me.getReport);
    obj.est.xN = NaN(size(obj.est.xN));
    ret = false;
  end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = accept_step(obj,t,x)
  ret = ~any(any(obj.sqrt_Rn > 1/eps));
  if(~ret)
    display('--done: error covariance diverged');
  end
  ret = ret && accept_step@DA_Filter(obj,t,x);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% discrete time measurement update
function [xn_n,sqrt_Rn_n] = meas_update(obj)
  xn_nm1 = obj.x;
  sqrt_Rn_nm1 = obj.sqrt_Rn;
  t = obj.t;
  y = obj.y;
  
  % compute h, dh
  [h,dh] = obj.h(t,xn_nm1);
  
  % remove NaNs
  mask = ~isnan(h);
  h = h(mask); y = y(mask); 
  dh = dh(mask,:); sqrt_Rm = obj.sqrt_Rm(mask);

  % switch representation
  if(obj.do_inf_cov)
    S = svd(sqrt_Rn_nm1);
    if(1 < min(S)*max(S))
      sqrt_Rn_nm1 = pinv(sqrt_Rn_nm1,obj.pinv_tol)';
      obj.is_cov = ~obj.is_cov;
      display([t,obj.is_cov])
    end
  end

  % covariance/information update
  if(obj.is_cov)
    [Kn,sqrt_Rn_n] = obj.meas_update_cov(sqrt_Rn_nm1,dh,sqrt_Rm);
  else
    [Kn,sqrt_Rn_n] = obj.meas_update_inf(sqrt_Rn_nm1,dh,sqrt_Rm);
  end

  % update state estimate
  if(~all(Kn == 0))
    xn_n = xn_nm1 + Kn*(y - h);
    obj.Kn = Kn;
  % set all states estimates equal to observed values
  else
    obj.Kn = zeros(obj.nD,obj.nL);
    xn_n = xn_nm1;
    [~,idxs] = find(H);
    xn_n(idxs) = y(idxs);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% covariance form of measurement update
function [Kn,inv_sqrt_Rn_n] = meas_update_cov(obj,inv_sqrt_Rn_nm1,dh,sqrt_Rm)
  % QR decomposition
  inv_sqrt_Rm = diag(sqrt_Rm.^(-1));
  [~,r] = qr([   inv_sqrt_Rm    ,   zeros(size(dh));...
              inv_sqrt_Rn_nm1*dh.',inv_sqrt_Rn_nm1],0);
            
  % covariance
  nL = obj.nL;
  tmp = r((nL+1):end,:); 
  inv_sqrt_Rn_n = tmp(:,(nL+1):end);
  
  % Kalman gain
  tmp = r(1:nL,:);
  inv_sqrt_Re = tmp(:,1:nL);
  sqrt_Re = pinv(inv_sqrt_Re,obj.pinv_tol);
  Kn = transpose(sqrt_Re*tmp(:,(nL+1):end));

  % check
  %{
  inv_Rn_nm1 = inv_sqrt_Rn_nm1.'*inv_sqrt_Rn_nm1;
  inv_Rm = inv_sqrt_Rm.'*inv_sqrt_Rm;
  inv_Re = inv_Rm + dh*inv_Rn_nm1*dh.';
  Re = pinv(inv_Re,pinv_tol);
  norm(Kn - inv_Rn_nm1*dh.'*Re)
  inv_Rn = inv_Rn_nm1 - Kn*inv_Re*Kn.';
  norm(inv_Rn - inv_sqrt_Rn_n.'*inv_sqrt_Rn_n)
  %}
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% information form of measurement update
function [Kn,sqrt_Rn_n] = meas_update_inf(obj,sqrt_Rn_nm1,dh,sqrt_Rm)
  % QR decomposition
  sqrt_Rm = diag(sqrt_Rm);
  [~,sqrt_Rn_n] = qr([sqrt_Rn_nm1;sqrt_Rm*dh],0);
  
  % Kalman gain
  inv_sqrt_Rn_nm1 = pinv(sqrt_Rn_nm1,obj.pinv_tol);
  %inv_sqrt_Re = [inv_sqrt_Rm;inv_sqrt_Rn_nm1*dh.'];
  %sqrt_Re = pinv(inv_sqrt_Re,pinv_tol);
  inv_Rn_nm1 = inv_sqrt_Rn_nm1*inv_sqrt_Rn_nm1.';
  inv_Rm = diag(obj.sqrt_Rm.^(-2));
  inv_Re = inv_Rm + dh*inv_Rn_nm1*dh.';
  Re = pinv(inv_Re,obj.pinv_tol);
  Kn = inv_Rn_nm1*dh.'*Re;
  
  % check
  %{ 
  inv_Rm = diag(obj.sqrt_Rm.^(-2));
  inv_Rn_nm1 = pinv(sqrt_Rn_nm1.'*sqrt_Rn_nm1,pinv_tol);
  inv_Re = inv_Rm + dh*inv_Rn_nm1*dh.';
  Re = pinv(inv_Re,pinv_tol);
  norm(Kn - inv_Rn_nm1*dh.'*Re)
  Rn_nm1 = sqrt_Rn_nm1.'*sqrt_Rn_nm1;
  Rm = sqrt_Rm'*sqrt_Rm;
  Rn = Rn_nm1 + dh.'*Rm*dh;
  norm(Rn - sqrt_Rn_n.'*sqrt_Rn_n)  
  %}
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% discrete time update for the square root covariance
function [xnp1_n,sqrt_Rnp1_n] = time_update(obj)
  xn_n = obj.x;
  sqrt_Rn_n = obj.sqrt_Rn;
  tn = obj.t;
  
  % integrate variational equation to get xn_n and DFn
  [xnp1_n,dF] = obj.F(tn,xn_n);
  
  % anneal Rf
  if(obj.anneal_Rf)
    alpha = 1; beta = obj.rmsd; mean(obj.rmsd_cache);
    obj.sqrt_Rf = alpha/beta;
    %obj.sqrt_Rf = alpha^(floor(log(beta)/log(alpha)));
  end
  
  % covariance/information update
  if(obj.is_cov)
    sqrt_Rnp1_n = obj.time_update_cov(sqrt_Rn_n,dF);
  else
    sqrt_Rnp1_n = obj.time_update_inf(sqrt_Rn_n,dF);
  end
  
  % adaptive observations
  if(obj.adaptive_obs || obj.do_AUS)
    tol = -1.e-2;
    D = obj.nD; 
    r = min(D,obj.nL); 
    r = min(r,obj.max_rank);
    if(obj.adaptive_obs)
      mask = false(D,1); 
      mask(1:r) = true;
    elseif(obj.do_AUS)
      mask = true(D,1);
    end
    %
    % discrete
    [~,dF] = obj.F(tn,xnp1_n);
    [U,S,~] = svd(dF);
    [S,idxs] = sort(diag(S),'descend');
    tol = 1+tol;
    mask = and(mask,S > tol);
    idxs = idxs(mask);
    U = U(:,idxs);
    sqrt_Rnp1_n = U.'*sqrt_Rnp1_n;
    %{
    % continuous
    [~,dfn] = obj.f(t,xn_n);
    [Q,S] = eig(dfn + dfn');
    [S,idxs] = sort(diag(S),'descend');
    mask = and(mask,S > tol);
    idxs = idxs(mask);
    Q = Q(:,idxs);
    Kn = (Q*Q.')*Kn;
    sqrt_Rn_n = Q.'*sqrt_Rn_n;
    %}
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function inv_sqrt_Rnp1_n = time_update_cov(obj,inv_sqrt_Rn_n,dF)
  inv_sqrt_Rf = obj.sqrt_Rf.^(-1);
  mask = obj.sqrt_Rf < obj.pinv_tol;
  inv_sqrt_Rf(mask) = 0;
  inv_sqrt_Rf = diag(inv_sqrt_Rf);
  [~,inv_sqrt_Rnp1_n] = qr([inv_sqrt_Rn_n*dF';inv_sqrt_Rf],0);
  
  % check
  %{
  inv_Rn_n = inv_sqrt_Rn_n'*inv_sqrt_Rn_n;
  inv_Rf = inv_sqrt_Rf.^2;
  Df_term = dF*inv_Rn_n*dF';
  inv_Rnp1_n = inv_sqrt_Rnp1_n'*inv_sqrt_Rnp1_n;
  norm(Df_term + inv_Rf - inv_Rnp1_n)
  %}
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function sqrt_Rnp1_n = time_update_inf(obj,sqrt_Rn_n,dF)
  D = obj.nD;
  sqrt_Rf = diag(obj.sqrt_Rf);
  inv_dF = pinv(dF,obj.pinv_tol);
  sqrt_An = sqrt_Rn_n*inv_dF;
  [~,r] = qr([sqrt_Rf,zeros(D,D);-sqrt_An,sqrt_An],0);
  sqrt_Rnp1_n = r(D+1:end,:);
  sqrt_Rnp1_n = sqrt_Rnp1_n(:,D+1:end);
  
  % check
  %{  
  Rn = sqrt_Rn_n'*sqrt_Rn_n;
  An = inv_dF'*Rn*inv_dF;
  Rf = sqrt_Rf'*sqrt_Rf;
  tmp = An - An*pinv(Rf + An,pinv_tol)*An;
  Rnp1_n = sqrt_Rnp1_n'*sqrt_Rnp1_n;
  norm(tmp - Rnp1_n)
  %}
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% continuous time update
%{
function dzdt = cont_time_update(t,z,f,sqrt_Rf,is_cov)
  D = length(sqrt_Rf);
  dx_true = f(t,z(1:D));
  [dx_n,df_n] = f(t,z(D+1:2*D));
  inv_sqrt_Rf = diag(sqrt_Rf.^(-1));
  if(is_cov)
    inv_sqrt_Rn = reshape(z(2*D+1:end),D,D);
    sqrt_Rn = pinv(inv_sqrt_Rn,obj.pinv_tol);
    Df_term = inv_sqrt_Rn*df_n'*sqrt_Rn;
    Rf_term = inv_sqrt_Rf*sqrt_Rn;
    U = triu(Df_term + Df_term' + (Rf_term'*Rf_term));
    U = U - 0.5*diag(diag(U));
    inv_sqrt_dR = U*inv_sqrt_Rn;
    dzdt = vertcat(dx_true,dx_n,inv_sqrt_dR(:));
  else
    sqrt_Rn = reshape(z(2*D+1:end),D,D);
    inv_sqrt_Rn = pinv(sqrt_Rn,obj.pinv_tol);
    Df_term = inv_sqrt_Rn'*df_n'*sqrt_Rn';
    Rf_term = inv_sqrt_Rf*sqrt_Rn';
    U = triu(Df_term + Df_term' + (Rf_term'*Rf_term));
    U = U - 0.5*diag(diag(U));
    sqrt_dR = U*sqrt_Rn;
    dzdt = vertcat(dx_true,dx_n,sqrt_dR(:));
  end
  
  % check
  %{
  if(is_cov)
    inv_Rn = inv_sqrt_Rn'*inv_sqrt_Rn;
    Rf_term = diag(sqrt_Rf.^(-2));
    Df_term = df_n*inv_Rn + inv_Rn*df_n';
    dR_inv = Df_term + Rf_term;
    norm(inv_sqrt_dR'*inv_sqrt_Rn + inv_sqrt_Rn'*inv_sqrt_dR - dR_inv)
  else
    Rn = sqrt_Rn'*sqrt_Rn;
    Rf_term = Rn*diag(sqrt_Rf.^(-2))*Rn;
    Df_term = Rn*df_n + df_n'*Rn;
    dR_inv = Df_term + Rf_term;
    norm(sqrt_dR'*sqrt_Rn + sqrt_Rn'*sqrt_dR - dR_inv)
  end
  %}
end
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set/get methods
%%%
function set.sqrt_Ro(obj,val)
  if(isscalar(val))
    obj.sqrt_Ro = val*(ones(obj.nD,1));
  elseif(isvector(val))
    if(length(val) ~= obj.nD)
      obj.sqrt_Ro = val;
    else
      error('ExtKF::set.sqrt_Ro: improper length');
    end
  else
    error('ExtKF::set.sqrt_Ro: only diagonal covariances are supported');
  end
end
%%%
function set.sqrt_Rf(obj,val)
  if(isscalar(val))
    obj.sqrt_Rf = val*(ones(obj.nD,1));  
  elseif(isvector(val))
    if(length(val) == obj.nD)
      obj.sqrt_Rf = val;
    else
      error('ExtKF::set.sqrt_Rf: improper length');
    end
  else
    error('ExtKF::set.sqrt_Rf: only diagonal covariances are supported');
  end
end
%%%
function set.sqrt_Rm(obj,val)
  if(isscalar(val))
    obj.sqrt_Rm = val*(ones(obj.nL,1));  
  elseif(isvector(val))
    if(length(val) ~= obj.nL)
      obj.sqrt_Rm = val;
    else
      error('ExtKF::set.sqrt_Rm: improper length');
    end
  else
    error('ExtKF::set.sqrt_Rm: only diagonal covariances are supported');
  end
end
end
end