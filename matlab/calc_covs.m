function ret = calc_covs(in)
  in
  [mu,sqrt_C] = global_covs(in.nD,in.T,1);
  ret.mu = mu;
  ret.sqrt_C = sqrt_C;
end

% compute climatological error covariance between two trajectories
function [mu,sqrt_C] = error_covs(D,T,nIC)
  if(nargin < 2)
    nIC = 1;
  end
  if(nargin < 1)
    D = 10;
  end
  
  % initialize model
  init.nD = D;
  model = L96Model;
  model.initialize([],init);
  
  % initial conditions
  rng(nIC);
  x0 = model.rand_x0();
  x1 = model.rand_x0();
% compute long trajectory

  dt = 0.01; T = T/model.gle_max; N = round(T/dt);
  my_ode = @(t,x)rk4step(@model.f,t,x,dt);
  N_samp = 10; N = round(N/N_samp)*N_samp;
  X0 = zeros(N/N_samp,D);
  X1 = X0;
  for n = 1:N
    if(mod(n,N_samp) == 1)
      X0(ceil(n/N_samp),:) = x0.';
      X1(ceil(n/N_samp),:) = x1.';
    end
    if(mod(n,round(N/100)) == 1)
      fprintf(1,'%d%%\n',round(n/N*100)); 
    end
    t = n*dt;
    x0 = my_ode(t,x0);
    x1 = my_ode(t,x1);
  end
   
  % compute sample mean and covariance
  Xe = X0-X1;
  mu = mean(Xe);
  sqrt_C = qr(Xe,0)/sqrt(size(Xe,1));
end

% compute approximate climatological covariance
% using a single trajectory
function [mu,sqrt_C] = global_covs(D,T,nIC)
  if(nargin < 2)
    nIC = 1;
  end
  if(nargin < 1)
    D = 10;
  end
  
  % initialize model
  init.nD = D;
  model = L96Model;
  model.initialize([],init);
  
  % initial conditions
  rng(nIC);
  x = model.rand_x0();

  % compute long trajectory
  dt = 0.01; T = T/model.gle_max; N = round(T/dt);
  my_ode = @(t,x)rk4step(@model.f,t,x,dt);
  N_samp = 10; N = round(N/N_samp)*N_samp;
  X = zeros(N/N_samp,D);
  for n = 1:N
    if(mod(n,N_samp) == 1)
      X(ceil(n/N_samp),:) = x.';
    end
    if(mod(n,round(N/100)) == 1)
      fprintf(1,'%d%%\n',round(n/N*100));
    end
    t = n*dt;
    x = my_ode(t,x);
  end
   
  % compute sample mean and covariance
  mu = mean(X);
  sqrt_C = qr(X,0)/sqrt(size(X,1));
end

% compute approximate climatological covariance
function [mu,sqrt_C] = local_covs(D,T,nIC)
  if(nargin < 2)
    nIC = 1;
  end
  if(nargin < 1)
    D = 10;
  end
  
  % initialize model
  init.nD = D;
  model = L96Model;
  model.initialize([],init);
  
  % initial conditions
  rng(nIC);
  xC = model.rand_x0();
  Id = eye(D);
  xC = [xC;Id(:)];

  % compute long trajectory
  dt = 0.01; T = T/model.gle_max; N = round(T/dt);
  f = @(t,xC)cov_eqn(t,xC,model);
  my_ode = @(t,x)rk4step(f,t,x,dt);
  N_samp = 10; N = round(N/N_samp)*N_samp;
  xs = zeros(N/N_samp,D);
  Cs = zeros(N/N_samp,D,D);
  for n = 1:N
    if(mod(n,N_samp) == 1)
      xs(ceil(n/N_samp),:) = xC(1:D).';
      Cs(ceil(n/N_samp),:,:) = reshape(xC(D+1:end),D,D);
    end
    if(mod(n,round(N/100)) == 1)
      fprintf(1,'%d%%\n',round(n/N*100));
    end
    t = n*dt;
    xC = my_ode(t,xC);
  end
   
  % compute sample mean and covariance
  mu = mean(xs,1);
  C  = squeeze(mean(Cs,1));
  sqrt_C = chol(C);
  keyboard;
end

function dxC = cov_eqn(t,xC,model)
  D = model.nD;
  x = xC(1:D);
  C = reshape(xC(D+1:end),D,D);
  [dx,df] = model.f(t,x);
  dC = dx*x.' + x*dx.';
  %dC = df*C + C*df.';
  dxC = [dx;dC(:)];
end
