%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% check for success by comparing predictions
function ret = test_pred_error(prob)
  gle_max = prob.model.gle_max;
  %err_tol = prob.alg.rmse_tol;
  F_true = prob.data.F;
  F_est  = @(t,x)prob.alg.F(t,x);
  dF_est = prob.alg.var_eqn;
  H = prob.alg.H;
  D = prob.alg.nD;
  dt = prob.alg.dt;
  N_min = prob.alg.N_min;
  N_max = prob.alg.N_max;
  sigma_m = prob.data.sigma_m;
  ret = zeros(3,1);
  
  % tolerance calculated for L96
  pred_rms_tol = 5.2; 
  
  % check for divergence
  if(~isfinite(prob.est.xN))
    display('filter divergence');
    ret(1:3) = inf;
    return;
  end
  
  % compute true error of last N_min
  X_est = prob.est.X;
  X_data = prob.data.X;
  Np1 = prob.est.td.nNp1;
  X_est = X_est(Np1-N_min+1:Np1,:);
  X_data = X_data(Np1-N_min+1:Np1,:);
  true_rmse = rms(X_est - X_data,2);
  
  % make sure truth and estimate are not identical
  if(any(true_rmse < eps))
    ret(1:3) = 0;
    display(sprintf('Results are indistinghishable\n'));
    return;
  end
  
  % find minimum true error
  [true_rmse,tmp] = min(true_rmse);
  Np1 = tmp + Np1 - N_min;
  N = Np1-1;
  
  % prediction rms deviation
  rmse = zeros(N_max+1,1);
  rmsd = zeros(N_max+1,1);
  x_true = prob.data.X(Np1,:).'; 
  x_est  = prob.est.X(Np1,:).';
  n = 0;
  while(n < N_max)
    % check for  convergence using rms deviation
    e_n = x_true - x_est;
    d_n = H*(e_n + sigma_m*randn(D,1));
    rmse(n+1) = rms(e_n);
    rmsd(n+1) = rms(d_n);
    if(n > N_min)
      if(rmsd(n) > pred_rms_tol)
        break;
      end
    end
    
    % plot for debugging
    %{
    ts = dt*(0:n);
    semilogy(ts,rmse(1:n+1),'k'); 
    hold on;
    semilogy(ts,rmsd(1:n+1),'b');
    hold off;
    drawnow();
    %}
    
    % monitor LLEs
    %
    if(~exist('Rs','var'))
      Rs = []; Q = eye(D);
      x_phi = vertcat(x_est,Q(:));
    end
    t = (N + n)*dt;
    x_phi = dF_est(t,x_phi);
    phi = reshape(x_phi(D+1:end),[D,D]);
    [Q,Rs(:,:,n+1)] = qr(phi);
    x_phi(D+1:end) = Q(:);
    %}
    
    % update
    t = (N + n)*dt;
    x_true = F_true(t,x_true);
    x_est  = F_est(t,x_est);
    n = n+1;
  end
  if(n >= N_max)
    warning('test_pred_error: predictions did not converge');
  end
  rmse = rmse(1:n,1);
  rmsd = rmsd(1:n,1);
  
  % determine where threshold was crossed
  nn = find(rmsd > pred_rms_tol,1);
  if(isempty(nn))
    nn = n;
  end
  
  % compute LLEs
  lle_max = max(calc_LLEs(Q,Rs,dt));
  %lle_max = max_LLE(Rs,dt);
  
  % minimum true error
  display(sprintf('true error: %1.4e',true_rmse));
  ret(1) = true_rmse;
  % estimate using global pred error
  global_pred_rmse = rmsd(nn)*exp(-gle_max*dt*nn);
  display(sprintf('global pred. error: %1.4e',global_pred_rmse));
  ret(2) = global_pred_rmse;
  % estimate using local pred error
  local_pred_rmse = rmsd(nn)*exp(-lle_max*dt*nn);
  display(sprintf('local pred. error: %1.4e\n',local_pred_rmse));
  ret(3) = local_pred_rmse;
  
  %{
  hold on;
  %T = n*dt;
  %semilogy(ts,rmsd_n*exp((ts-T)*gle_max),'g');
  %semilogy(ts,rmsd_n*exp((ts-T)*lle_max),'c');
  B = [ones(nn,1),ts(1:nn).'];
  b = B\log(rmsd(1:nn));
  B = [ones(n,1),ts(1:n).'];
  semilogy(ts(1:n),exp(B*b),'r');
  b0 = exp(mean(log(rmsd(1:nn)) - gle_max*ts(1:nn).'));
  semilogy(ts,b0*exp(ts*gle_max),'g');
  b0 = exp(mean(log(rmsd(1:nn)) - lle_max*ts(1:nn).'));
  semilogy(ts,b0*exp(ts*lle_max),'c');
  hold off;
  keyboard;
  %}
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% estimate max LLE from random perturbations
function ret = max_LLE(Rs,dt)
  mu = 0;
  var = 0;
  i = 1; i_max = 1000; i_min = 10;
  delta = 1.e-9;
  tol = 1.e-3;
  [D,~,N] = size(Rs);
  while(true)
    dx = delta*rand(D,1);
    for n = 1:N
      dx = Rs(:,:,n)*dx;
    end
    lle = (log(norm(dx)) - log(delta))/N/dt;
    if(i > 1)
      var = (i-2)/(i-1)*var + (lle - mu).^2/i;
    end
    mu =(lle + (i-1)*mu)/i;
    if(i > i_max)
      warning('test_pred_error::max_LLE: did not converge');
      break;
    elseif(i > i_min && var/sqrt(i) < tol)
      break;
    end
    i = i+1;
  end
  ret = mu;
end