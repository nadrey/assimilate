classdef BlockTri < WC4DVar
properties
end

methods
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  % base initialization
  obj = initialize@WC4DVar(obj,prob,in); 
  % measurement error inverse covariance
  obj = set_prop(obj,in,'sqrt_R0',0);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = step(obj)
  dX = obj.f_blk_tri(obj.X);
  next_X = obj.X + dX;
  
  % reevaluate objective
  next_psi = obj.direct_eval(next_X);
  
  % step accepted
  if(obj.accept_step(obj.psi,next_psi))
    obj.psi = next_psi;
    obj.est.X = next_X;
    ret = step@DA_Smoother(obj);
  else
    ret = false;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function initialize_run(obj)
  % Do not call WC4DVar method
  initialize_run@DA_Smoother(obj);
  % Initial objective value
  obj.psi = obj.direct_eval(obj.X);
end 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dX = f_blk_tri(obj,X)
  Y = obj.Y; H = obj.H; [L,D] = size(H);
  ts = obj.td.ts; N = obj.nN; Np1 = N+1; 
  eval_F = obj.quad_rule.eval;
  sqrt_Rf = diag(obj.sqrt_Rf)/sqrt(2*Np1*D);
  sqrt_Rm = diag(obj.sqrt_Rm_vec)/sqrt(2*Np1*L);
  sqrt_R1_0 = diag(obj.sqrt_R0_vec);
  Id = eye(D);

  % allocate
  dX = zeros(Np1,D);
  sqrt_Cs = zeros(Np1,D,D);
  B_sqrt_inv_Cs = zeros(N,D,D);
  sqrt_Rs = zeros(Np1,D,D);
  dA = zeros(Np1,D);
  
  % initial covariance
  sqrt_Rm_H = sqrt_Rm*H;
  [~,sqrt_Rn_n] = qr([sqrt_Rm_H; ...
                      sqrt_R1_0],0);
  sqrt_Rs(1,:,:) = sqrt_Rn_n;
                    
  % initial gradient
  mu_0 = X(1,:).';
  dA_n = (sqrt_R1_0'*sqrt_R1_0)*(X(1,:).' - mu_0);
  
  % forward pass
  dX_n = 0;
  Bn_sqrt_inv_Cn = 0;
  for n = 1:N
    t_n = ts(n); 
    x_n = X(n,:).';
    y_n = Y(n,:).';
    t_np1 = ts(n+1);
    x_np1 = X(n+1,:).';
    [Fn,dnFn,dnp1Fn] = eval_F(t_n,x_n,t_np1,x_np1);
    % Compute gradient
    dA_n = dA_n - sqrt_Rm_H.'*sqrt_Rm*(y_n - H*x_n);
    dA_n = dA_n - dnFn.'*(sqrt_Rf.'*sqrt_Rf)*(x_np1 - Fn);
    dA(n,:) = dA_n.';
    % Update with previous Bn_sqrt_inv_Cn
    dX_n = dA_n - Bn_sqrt_inv_Cn*dX_n;
    % Compute covariance
    [~,r] = qr([ sqrt_Rn_n    , zeros(D)           ; ...
                -sqrt_Rf*dnFn , sqrt_Rf*(Id-dnp1Fn); ...
                 zeros(L,D)   ,-sqrt_Rm_H          ],0);
    sqrt_Cn = r(1:D,:); 
    sqrt_Cn = sqrt_Cn(:,1:D);
    sqrt_Cs(n,:,:) = sqrt_Cn;
    Bn_sqrt_inv_Cn = r(1:D,:); 
    Bn_sqrt_inv_Cn = Bn_sqrt_inv_Cn(:,D+1:end).';
    B_sqrt_inv_Cs(n,:,:) = Bn_sqrt_inv_Cn;
    sqrt_Rn_n = r(D+1:end,:); 
    sqrt_Rn_n = sqrt_Rn_n(:,D+1:end);
    sqrt_Rs(n+1,:,:) = sqrt_Rn_n;
    % Compute update with current covariance
    dX_n = sqrt_Cn.'\dX_n;
    dX(n,:) = dX_n.';
    % Initialize next gradient
    dA_n = (Id - dnp1Fn).'*(sqrt_Rf.'*sqrt_Rf)*(x_np1 - Fn);
  end
  n = Np1;
  x_n = X(n,:).';
  y_n = Y(n,:).';
  dA_n = dA_n - sqrt_Rm_H.'*sqrt_Rm*(y_n - H*x_n);
  dA(n,:) = dA_n.';
  sqrt_Cn = sqrt_Rn_n;
  sqrt_Cs(n,:,:) = sqrt_Cn;
  dX_n = sqrt_Cn.'\(dA_n - Bn_sqrt_inv_Cn*dX_n);
  dX(n,:) = dX_n.';
  
  % backward pass
  n = Np1;
  sqrt_Cn = squeeze(sqrt_Cs(end,:,:));
  dX_n = sqrt_Cn\dX_n;
  dX(n,:) = dX_n.';
  for n = N:-1:1
    sqrt_Cn = squeeze(sqrt_Cs(n,:,:));
    Bn_sqrt_inv_Cn = squeeze(B_sqrt_inv_Cs(n,:,:));
    dX_n = sqrt_Cn\(dX(n,:).' - Bn_sqrt_inv_Cn.'*dX_n);
    dX(n,:) = dX_n.';
  end
  
  % check
  %[psi,dpsi] = obj.direct_eval(X);
  %[~,r] = qr(full(dpsi),0);
  %dX2 = reshape(r\(r.'\(dpsi.'*psi)),D,[]).';
  %norm(dX - dX2)
  
  %  negative gradient
  dX = -dX;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set/Get
%%%
end
end