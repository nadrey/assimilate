classdef (Abstract) DA_Algorithm < DA_Base
properties
  prob; data;
  rmse_tol;
end

properties(Constant)
  ode = @rk4step;
  %ode = @(f,t,x0,dt)(x0 + dt*f(t,x0));
end

properties(Dependent)
  model; td; obs;
  nL; nD; 
  dt, t, ts; 
end

properties(Abstract)
%   is_filter;
end

methods(Abstract)
  step(obj);
  reset(obj);
  % reset object b/c obs operator has changed
  reset_obs(obj);
end

methods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Constructor
function obj = DA_Algorithm()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = struct;
  end
  if(nargin < 2)
    error('DA_Algorithm::initialize: invalid inputs');
  end
  obj.prob = prob;
  % Set error tol
  dflt_rmse_tol = max(1.e-3,obj.prob.data.sigma_m);
  set_prop(obj,in,'rmse_tol',dflt_rmse_tol); 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = est(obj)
  ret = obj.data;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = run(obj)
  % Initialize
  obj.initialize_run();
  % Iterate
  while(obj.step())
  end
  % Finalize run
  obj.finalize_run();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function initialize_run(obj)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function finalize_run(obj)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% continuous model
function [f,df,d2f] = f(obj,t,x)
  if(nargout == 1)
    f = obj.model.f(t,x);
  elseif(nargout == 2)
    [f,df] = obj.model.f(t,x);
  elseif(nargout == 3)
    [f,df,d2f] = obj.model.df(t,x);
  end
end
%%% discrete model and Jacobian
function [F,dF] = F(obj,t,x,f,dt)
  D = obj.nD;
  if(nargin < 5)
    dt = obj.dt;
  end
  if(nargin < 4)
    f = @(t,x)obj.f(t,x);
  end
  if(nargout == 1)
    F = obj.ode(f,t,x(:),dt);
  else
    Id = eye(obj.nD);
    z = [x(:); Id(:)];
    z = obj.ode(@(t,z)var_eqn(t,z,D,f),t,z,dt);
    F = z(1:D);
    dF = reshape(z(D+1:end),D,D);
  end
end
%%% variational equation
function ret = var_eqn(obj)
  f = @(t,x)obj.f(t,x);
  ret = @(t,z)obj.ode(@(t,z)var_eqn(t,z,obj.nD,f),t,z,obj.dt);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% These cannot be get methods
function ret = x(obj,t)
  if(nargin < 2)
    t = obj.t;
  end
  ret = obj.est.x(t);
end
%%%
function ret = X(obj)
  ret = obj.est.X;
end
%%%
%%% TODO Distinguish between Y est and Y data?
%%%
function ret = y(obj,t)
  if(nargin < 2)
    t = obj.t;
  end
  ret = obj.prob.data.y(t);
end
%%%
function ret = Y(obj)
  ret = obj.prob.data.Y;
end
%%%
function [h,dh] = h(obj,t,x)
  if(nargin < 3)
    t = obj.t;
    x = obj.x;
  end
  if(nargout == 1)
    h = obj.obs.h_est(t,x);
  elseif(nargout == 2)
   [h,dh] = obj.obs.h_est(t,x);
  end
end
%%%
function ret = nN(obj)
  ret = obj.td.nN; 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set/Get methods
%%%
function ret = get.model(obj)
  ret = obj.prob.model;
end
%%%
function ret = get.td(obj)
  ret = obj.est.td;
end
%%%
function ret = get.obs(obj)
  ret = obj.prob.obs;
end
%%%
function ret = get.nL(obj)
  ret = obj.obs.nL;
end
%%%
function ret = get.nD(obj)
  ret = obj.model.nD;
end
%%% 
function ret = get.dt(obj)
  ret = obj.td.dt;
end
%%%
function ret = get.t(obj)
  ret = obj.td.T;
end
%%%
function ret = get.ts(obj)
  ret = obj.td.ts;
end
end
end