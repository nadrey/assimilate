classdef SmootherData < DA_EstData
properties
  X, X0, X_opt;
  U, U0, U_opt;
end

properties(Dependent)
  x0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
function obj = SmootherData()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,alg,in)
  if(nargin < 2)
    in = [];
  end
  if(nargin < 1)
    error('SmootherData::initialize: invalid input');
  end
  % Base initialization
  obj = initialize@DA_EstData(obj,alg,in);
  % Algorithm initializes data
  alg.init_data(in);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Abstract methods
function ret = x(obj,t)
  if(nargin < 2)
    ret = obj.X(end,:).';
  else
    np1 = obj.td.t_to_np1(t);
    ret = obj.X(np1,:).';
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function reset(obj)
  obj.X = obj.X0; 
  obj.U = obj.U0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get/Set
%%%
function set.U0(obj,val)
  obj.U0 = val;
  obj.reset();
end
%%%
function set.X0(obj,val)
  obj.X0 = val;
  obj.reset();
end
%%%
function set.x0(obj,x0)
  X0(1,:) = x0(:).';
  ts = obj.td.ts;
  for n = 1:obj.td.nN
    X0(n+1,:) = obj.alg.F(ts(n),X0(n,:).').';
  end
  obj.X0 = X0;
end
%%%
function ret = get.x0(obj)
  ret = obj.x(obj.td.ts(1));
end
end
end