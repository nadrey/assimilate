% Weak 4DVar action as a function of x_0 and U = {u_n}|_n=0^N
classdef WC4DVarU < DA_Smoother
methods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = WC4DVarU()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = [];
  end
  % base initialization
  obj = initialize@DA_Smoother(obj,prob,in);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Callback to initialize SmootherData
% Note: use N+1 controls, the last will always be zero
function init_data(obj,in)
  data = obj.data;
  nD = obj.nD; 
  
  % call base method
  init_data@DA_Smoother(obj,in);
  nNp1 = obj.nN+1;
  
  % default X0, U0
  if(~isfield(in,'X0'))
    X0 = 'global';
  else
    X0 = in.X0;
    if(isempty(X0) || ischar(X0))
    elseif(isvector(X0))
      X0 = X0(:);
    elseif(ismatrix(X0))
      X0 = X0(1,:).';
    end
  end
  if(~isfield(in,'U0') || ischar(X0))
    U0 = zeros(nNp1,nD);
  else
    U0 = in.U0;
    if(size(U0,2) < nD)
      U0 = zeros(1,nD);
    end
    if(size(U0,1) < nNp1)
      U0 = padarray(U0,nNp1-size(U0,1),0,'post');
    end
  end
  
  % compute X0
  data.X0 = []; 
  data.U0 = [];
  if(isempty(X0))
    return;
  elseif(isnumeric(X0))
    data.U0 = U0;
    data.x0 = X0;
  elseif(strcmpi(X0,'global'))
    init_rand(data.x0_est_seed);
    data.U0 = U0;
    data.x0 = obj.model.rand_x0;
  elseif(strcmpi(X0,'local'))
    init_rand(data.x0_est_seed);
    dx0 = randn(nD,1); 
    dx0 = dx0/rms(dx0(:));
    %dU  = randn(nNp1,nD);
    %dU  = dU/rms(dU(:));
    r = obj.rmse_tol;
    dx0 = dx0*r;
    %dU  = dU*r;
    [data.X_opt,data.U_opt] = obj.X_opt();
    U0 = data.U_opt;% + dU;
    data.U0 = U0;
    x0_opt = data.X_opt(1,:).';
    data.x0 = x0_opt + dx0;
  else
    error('WC4DVarU::init_data: invalid input');
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% reimplemented to include controls
% careful!!! doing this in the continuous model f leads introduces
% additional terms into the jacobian
function [F,dF] = F(obj,t,x,f,U)
  if(nargin < 5)
    U = obj.est.U;
  end
  if(nargin < 4)
    f = @(t,x)obj.model.f(t,x);
  end
  
  if(nargout == 1)
    F = F@DA_Smoother(obj,t,x,f);
  elseif(nargout == 2)
    [F,dF] = F@DA_Smoother(obj,t,x,f);
  end
  np1 = obj.td.t_to_np1(t);
  u_n = U(np1,:).';
  F = F + u_n;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = step(obj)
  D = obj.nD; Np1 = obj.nN+1;
  
  % compute direct gradient
  if(~obj.do_adjoint)
    dA = obj.dpsi.'*obj.psi;
  % use adjoint
  else
    [~,dA] = obj.adjoint_eval(obj.est.x0,obj.est.U);
  end
  
  % full gauss-newton step
  if(~obj.do_gradient)
    %dx = -pinv(obj.dpsi,obj.pinv_tol)*obj.psi;
    [~,r] = qr(obj.dpsi,0);
    dxU = -r\(r.'\dA);
  % gradient descent
  else
    dxU = -dA;
  end
  
  % compute next X
  alpha = obj.alpha;
  dx = dxU(1:D);
  next_x0 = obj.est.x0 + alpha*dx;
  dU = reshape(dxU(D+1:end),D,Np1).';
  next_U = obj.est.U + alpha*dU;
  
  % reevaluate objective
  [next_X,next_psi,next_dpsi] = obj.direct_eval(next_x0,next_U);
  
  % step accepted
  if(obj.accept_step(obj.psi,next_psi))
    %display(0.5*next_psi.'*next_psi);
    obj.psi = next_psi;
    obj.dpsi = next_dpsi;
    obj.est.X = next_X;
    obj.est.U = next_U;
    ret = step@DA_Smoother(obj);
  else
    ret = false;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [X,dA] = adjoint_eval(obj,x0,U)
  error('WC4DVarU::adjoint_eval: not implemented yet');
  L = obj.nL; D = obj.nD; 
  N = obj.nN; Np1 = N+1; ts = obj.est.td.ts;
  sqrt_Rm = diag(obj.sqrt_Rm)/sqrt(2*Np1*L);
  Y = obj.Y; X = zeros(Np1,D); 
  F = @(t,x)obj.F(t,x,U);
  % TODO Support nonlinear obs
  H = obj.obs.H;
  % TODO Add background term
  
  % forward pass
  X(1,:) = x0.';
  for n = 1:N
    t_n = ts(n);
    x_n = X(n,:).';
    X(n+1,:) = F(t_n,x_n);
  end
  % backward pass
  dA = zeros(D,1);
  for n = Np1:-1:1
    t_n = ts(n);
    x_n = X(n,:).';
    y_n = Y(n,:).';
    [~,dF_n] = F(t_n,x_n);
    dA = dF_n.'*dA + H.'*(sqrt_Rm.'*sqrt_Rm)*(y_n-H*x_n);
  end
  % flip sign
  dA = -dA;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute derivatives directly using variational equation
function [X,psi,dpsi] = direct_eval(obj,x0,U,xb)
  if(nargin < 2)
    x0 = obj.est.x0(:);
  end
  if(nargin < 3)
    U = obj.est.U;
  end
  if(nargin < 4)
    xb = x0;
  end
  L = obj.nL; D = obj.nD; 
  N = obj.nN; Np1 = N+1; ts = obj.est.td.ts;
  sqrt_Rm = obj.sqrt_Rm/sqrt(2*Np1*L);
  sqrt_Rf = obj.sqrt_Rf/sqrt(2*Np1*D);
  sqrt_Rb = obj.sqrt_Rb/sqrt(2*D); % always assume x_0 = x_b
  Y = obj.Y;
  X = zeros(Np1,D);
  dFs = zeros(Np1,D,D);
  % TODO Support nonlinear obs
  H = obj.obs.H;
  
  % Model with controls
  f = @(t,x)obj.f(t,x);
  F = @(t,x)obj.F(t,x,f,U);
  
  % compute psi
  psi = zeros(Np1*(L+D)+D,1);
  % prior
  psi(1:D) = sqrt_Rb*(x0 - xb);
  % model error
  psi(D+1:D+D*Np1) = reshape(transpose(U*transpose(sqrt_Rf)),[],1);
  % meas. error
  x_n = x0;
  for n = 1:Np1
    n_idxs = (n-1)*L+(1:L);
    psi(D+D*Np1+n_idxs) = sqrt_Rm*(Y(n,:).' - H*x_n);
    X(n,:) = x_n.';
    if(n == Np1)
      break;
    end
    t_n = ts(n);
    if(nargout > 2)
      [x_n,dFs(n,:,:)] = F(t_n,x_n);
    else
      x_n = F(t_n,x_n);
    end
  end
    
  % compute dpsi
  if(nargout > 2)
    dpsi = zeros(Np1*(Np1+1)/2*L*D+Np1*D+D,3);
    % prior
    bl_idxs = 1;
    bl_idxs = bl_idxs:bl_idxs+D-1;
    dpsi(bl_idxs,1) = 1:D;
    dpsi(bl_idxs,2) = 1:D;
    dpsi(bl_idxs,3) = sqrt_Rb;
    bl_idxs = bl_idxs+D;
    % model error
    bl_idxs = bl_idxs(1);
    bl_idxs = bl_idxs:bl_idxs+Np1*D-1;
    dpsi(bl_idxs,1) = D+(1:Np1*D);
    dpsi(bl_idxs,2) = D+(1:Np1*D);
    dpsi(bl_idxs,3) = sqrt_Rf;
    bl_idxs = bl_idxs+Np1*D;
    % meas. error
    bl_idxs = bl_idxs(1);
    bl_idxs = bl_idxs:bl_idxs+L*D-1;
    [sp_idxs(:,1),sp_idxs(:,2)] = find(ones(L,D));
    for n1 = 1:Np1
      phi_n = eye(D);
      for n2 = n1:Np1
        dpsi(bl_idxs,1) = D+Np1*D+L*(n2-1)+sp_idxs(:,1);
        dpsi(bl_idxs,2) = D*(n1-1)+sp_idxs(:,2);
        dpsi(bl_idxs,3) = reshape(-sqrt_Rm*H*phi_n,[],1);
        bl_idxs = bl_idxs+L*D;
        phi_n = squeeze(dFs(n2,:,:))*phi_n;
        %{
        mask = dpsi(:,1) == 0;
        tmp = dpsi(~mask,:);
        spy(sparse(tmp(:,1),tmp(:,2),tmp(:,3)));
        keyboard;
        %}
      end
    end
    dpsi = sparse(dpsi(:,1),dpsi(:,2),dpsi(:,3));
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TODO not fixing x_b or setting R_b = 0 reduces the accuracy of the
% derivative test
function test_derivs(obj)
  D = obj.nD; Np1 = obj.nN+1;
  delta = 1.e-6;
  Xi = randn(Np1+1,D);   
  dXi = randn(Np1+1,D); 
  dXi = delta*dXi/norm(dXi(:));
  x0  = Xi(1,:).'; 
  dx0 = dXi(1,:).';
  U = Xi(2:end,:);
  dU = dXi(2:end,:);
  xb = x0;
  % 1st derivatives
  [~,~,dpsi] = obj.direct_eval(x0,U);
  spy(dpsi); %keyboard;
  [~,psi_p] = obj.direct_eval(x0+0.5*dx0,U+0.5*dU,xb);
  [~,psi_m] = obj.direct_eval(x0-0.5*dx0,U-0.5*dU,xb);
  display('1st derivatives (direct)');
  dXivec = reshape(transpose(dXi),[],1);
  display(norm((psi_p - psi_m) - dpsi*dXivec))
  %{
  % Full jacobian
  dpsi2 = zeros(size(dpsi));
  for i = 1:size(Xi,1)
  for j = 1:size(Xi,2)
    dx0 = zeros(size(x0));
    dU  = zeros(size(U));
    if(i == 1)
      dx0(j) = delta;
    else
      dU(i-1,j) = delta;
    end
    [~,psi_p] = obj.direct_eval(x0+0.5*dx0,U+0.5*dU,xb);
    [~,psi_m] = obj.direct_eval(x0-0.5*dx0,U-0.5*dU,xb);
    dpsi2(:,(i-1)*D+j) = (psi_p - psi_m)/delta;
  end
  end
  full(dpsi) - dpsi2
  %}
  % adjoint eval
  %[~,dA] = obj.adjoint_eval(x0);
  %display('1st derivatives (adjoint)');
  %display(norm(dA - dpsi.'*psi));
  %delta_A = 0.5*(psi_p.'*psi_p - psi_m.'*psi_m);
  %display(norm(delta_A - dA.'*dx));
  %display(norm(delta_A - (dpsi.'*psi).'*dx));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = rho_c(obj)
  ret = [];
  [X_opt,U_opt] = obj.X_opt;
  if(isempty(X_opt))
    return;
  else
    x0_opt = X_opt(1,:).';
  end
  [~,psi,dpsi] = obj.direct_eval(x0_opt,U_opt);
  ret = rho_c@DA_Smoother(obj,psi,dpsi);
end

end
end