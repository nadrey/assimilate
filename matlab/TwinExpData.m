%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef TwinExpData < DA_ExpData
properties
  X_twin;
  y_twin_seed; x0_twin_seed;
  ode = @rk4step;
  is_twin_exp = true;
end

properties(Dependent)
  F, x0, Np1; 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Public methods
methods
function obj = TwinExpData()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = struct;
  end
  if(nargin < 1)
    error('TwinExpData::TwinExpData: invalid input');
  end
  % Base initialization
  initialize@DA_ExpData(obj,prob,in);
  % Initialize random seeds
  set_prop(obj,in,'y_twin_seed',1);
  set_prop(obj,in,'x0_twin_seed',1);
  % Initialize twin data
  model = prob.model;
  init_rand(obj.x0_twin_seed);
  obj = set_prop(obj,in,'x0',@model.rand_x0);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = is_true_exp(~)
  ret = false;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = gen_twin_data(obj,Np1,x0)
  nD = obj.prob.model.nD;
  ts = obj.td.ts;
  
  % TODO Only store most recent data if algorithm is a filter?
  
  % extend or regenerate data
  if(nargin == 3)
    obj.X_twin = zeros(1,nD);
    obj.X_twin(1,:) = x0.';
    nn = 1;
  elseif(nargin == 2)
    nn = size(obj.X_twin,1);
  elseif(nargin == 1)
    error('TwinExpData::gen_twin_data: improper number of arguments');
  end
  
  % generate twin data
  nN = min(obj.td.nN,Np1-1);
  for n = nn:nN
    obj.X_twin(n+1,:) = obj.F(ts(n),obj.X_twin(n,:).').';
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
% TODO See comment in ProjectionObs.h
% TODO Support nonlinear obs. models
function ret = Y(obj,Np1)
  if(nargin < 2)
    Np1 = obj.td.nNp1;
  end
  obj.gen_twin_data(Np1);
  H = obj.prob.obs.H;
  ret = obj.X_twin*transpose(H);
  init_rand(obj.y_twin_seed);
  dY = obj.sigma_m*randn(size(ret));
  ret = ret + dY;
end
%%%
function ret = y(obj,t)
  if(nargin < 2)
    ret = obj.Y;
    if(~isempty(ret))
      ret = ret(end,:).';
    end
  else
    %np1 = obj.td.t_to_np1(t);
    %if(np1 > obj.Np1)
    %  obj.gen_twin_data(np1);
    %end
    %ret = obj.Y(np1,:).';
    ret = obj.prob.obs.h_data(t);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = X(obj,Np1)
  if(nargin < 2)
    Np1 = obj.td.nNp1;
  end
  obj.gen_twin_data(Np1);
  ret = obj.X_twin;
end

function ret = x(obj,t)
  if(nargin < 2)
    ret = obj.X_twin;
    if(~isempty(ret))
      ret = ret(end,:).';
    end
  else
    np1 = obj.td.t_to_np1(t);
    if(np1 > obj.Np1)
      obj.gen_twin_data(np1);
    end
    ret = obj.X_twin(np1,:).';
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set/Get methods
%%%
function set.x0(obj,val)
  obj.gen_twin_data(1,val);
end
%%%
function ret = get.F(obj)
  dt = obj.td.dt;
  model = obj.prob.model;
  f = @model.f;
  ret = @(t,x)obj.ode(f,t,x,dt);
end
%%%
function ret = get.x0(obj)
  ret = obj.X_twin(1,:).';
end
%%%
function ret = get.Np1(obj)
  ret = size(obj.X_twin,1);
end
end
end