classdef WC4DVarX < DA_Smoother
properties
  quad_rule;
  % anneal Rf
  idx_Rf;
end

properties(Dependent)
  anneal_Rf;
end

methods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = WC4DVarX()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  % quadrature rule
  obj.quad_rule = QuadRule(prob.model,in);
  % base initialization 
  obj = initialize@DA_Smoother(obj,prob,in); 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function initialize_run(obj)
  if(obj.anneal_Rf && (isempty(obj.idx_Rf) || ~obj.initializing_Xopt))
    obj.idx_Rf = 1;
    display(sprintf('sqrt Rf = %d',obj.sqrt_Rf{obj.idx_Rf}));
  end
  initialize_run@DA_Smoother(obj);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Callback to initialize SmootherData
function init_data(obj,in)
  data = obj.data;
  nD = obj.nD;
  
  % call base method
  init_data@DA_Smoother(obj,in);
  nNp1 = obj.nN+1;
  
  % default X0
  if(~isfield(in,'X0'))
    X0 = 'uniform';
  else
    X0 = in.X0;
    if(isempty(X0) || ischar(X0))
    elseif(size(X0,1) < nNp1)
      X0 = padarray(X0,nNp1-size(X0,1),0,'post');
    end
  end

  % compute X0
  data.X0 = [];
  if(isempty(X0))
    return;
  elseif(isnumeric(X0))
    data.X0 = X0;
  elseif(strcmpi(X0,'uniform'))
    bnds = obj.model.x_bounds;
    init_rand(data.x0_est_seed);
    data.X0 = rand(nNp1,nD)*(bnds(2)-bnds(1))+bnds(1);
  elseif(strcmpi(X0,'global'))
    init_rand(data.x0_est_seed);
    data.x0 = obj.model.rand_x0;
  elseif(strcmpi(X0,'local'))
    init_rand(data.x0_est_seed);
    dX0 = randn(nNp1,nD); 
    dX0 = dX0/rms(dX0(:));
    r = obj.rmse_tol;
    dX0 = dX0*r;
    data.X_opt = obj.X_opt;
    X_opt = data.X_opt;
    data.X0 = X_opt + dX0;
  else
    X0
    error('WC4DVarX::init_data: invalid input');
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = step(obj)
  D = obj.nD; Np1 = obj.nN+1;
  
  % compute direct gradient
  if(~obj.do_adjoint)
    dA = obj.dpsi.'*obj.psi;
  % use adjoint
  else
    [~,dA] = obj.adjoint_eval(obj.est.X);
  end
  
  % full gauss-newton step
  if(~obj.do_gradient)
    %dx = -pinv(obj.dpsi,obj.pinv_tol)*obj.psi;
    [~,r] = qr(obj.dpsi,0);
    dX = -r\(r.'\dA);
    %dX = -(dpsi.'*dpsi)\dpsi.'*psi;
    %dX = -minres(dpsi.'*dpsi,dpsi.'*psi);
    %dX = -symmlq(dpsi.'*dpsi,dpsi.'*psi);
    % full pseudoinverse
    %dX = -pinv(full(obj.dpsi),obj.pinv_tol)*obj.psi;
  % gradient descent
  else
    dX = -dA;
  end
  
  % compute next X
  alpha = obj.alpha;
  dX = transpose(reshape(dX,D,Np1));
  next_X = obj.X + alpha*dX;
  
  % reevaluate objective
  [next_X,next_psi,next_dpsi] = obj.direct_eval(next_X);
  
  % step accepted
  step_accepted = obj.accept_step(obj.psi,next_psi);
  if(step_accepted)
    obj.psi = next_psi;
    obj.dpsi = next_dpsi;
    obj.est.X = next_X;
    ret = step@DA_Smoother(obj);
  else
    ret = false;
  end
  
  % anneal Rf under these conditions
  if(obj.anneal_Rf && ~ret && ~obj.initializing_Xopt)
  % TODO Need better way of checking `done' status
  if(obj.opt_rmse < obj.rmse_tol || ~step_accepted || obj.i > obj.max_iters)
  % check if more annealing values exist
  if(obj.idx_Rf ~= length(obj.sqrt_Rf))
    ret = true;
    obj.idx_Rf = obj.idx_Rf + 1;
    obj.i = 1;
    display(sprintf('sqrt Rf = %d',obj.sqrt_Rf{obj.idx_Rf}));
    obj.data.X_opt = []; % reset X_opt
  end
  end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function evaluation for weak constraint 4DVar
function [X,psi,dpsi,d2psi] = direct_eval(obj,X,xb) 
  if(nargin < 2)
    X = obj.est.X;
  end
  if(nargin < 3)
    xb = X(1,:).';
  end
  if(nargout == 2)
    psi = obj.eval_psi(X,xb);
  elseif(nargout == 3)
    [psi,dpsi] = obj.eval_dpsi(X,xb);
  elseif(nargout == 4)
    [psi,dpsi,d2psi] = obj.eval_d2psi(X,xb);
  else
    error('WC4DVarX::direct_eval: Invalid number of outputs');
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function psi = eval_psi(obj,X,xb)
  if(nargin < 3)
    xb = X(1,:).';
  end
  x0 = X(1,:).';
  Y = obj.Y; L = obj.nL; D = obj.nD;
  N = obj.nN; Np1 = N+1; dt = obj.dt;
  eval_F = obj.quad_rule.eval;
  % TODO Support vector Rb/Rm/Rf
  sqrt_Rb = obj.sqrt_Rb(1);
  sqrt_Rm = obj.sqrt_Rm(1);
  if(~obj.anneal_Rf)
    sqrt_Rf = obj.sqrt_Rf(1);
  else
    sqrt_Rf = obj.sqrt_Rf{obj.idx_Rf};
  end
  % TODO Support nonlinear obs
  H = obj.obs.H;
  % scale factors
  sqrt_Rb = sqrt_Rb/sqrt(2*D);
  sqrt_Rm = sqrt_Rm/sqrt(2*Np1*L);
  sqrt_Rf = sqrt_Rf/sqrt(2*Np1*D);
  
  % allocate 
  psi = zeros(D + Np1*L + N*D,1);
  
  % prior
  idxs = 1:D;
  psi(idxs) = sqrt_Rb*(x0 - xb);
  
  % measurement block
  for n = 1:Np1
    idxs = D + (n-1)*L + (1:L);
    psi(idxs) = sqrt_Rm*(Y(n,:)' - H*X(n,:)');
  end

  % model block
  for n = 1:N
    x_n = X(n,:)';
    x_np1 = X(n+1,:)';
    t_n = (n-1)*dt;
    t_np1 = n*dt;
    F = eval_F(t_n,x_n,t_np1,x_np1);
    % set psi
    idxs = D + Np1*L + (n-1)*D + (1:D);
    psi(idxs) = sqrt_Rf*(x_np1 - F);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [psi,dpsi] = eval_dpsi(obj,X,xb)
  if(nargin < 3)
    xb = X(1,:).';
  end
  x0 = X(1,:).';
  Y = obj.Y; D = obj.nD; L = obj.nL;
  N = obj.nN; Np1 = N+1; dt = obj.dt;
  eval_F = obj.quad_rule.eval;
  % TODO Support vector Rb/Rm/Rf
  sqrt_Rb = obj.sqrt_Rb(1);
  sqrt_Rm = obj.sqrt_Rm(1);
  if(~obj.anneal_Rf)
    sqrt_Rf = obj.sqrt_Rf(1);
  else
    sqrt_Rf = obj.sqrt_Rf{obj.idx_Rf};
  end
  Id = eye(D);
  % TODO Support nonlinear obs
  H = obj.obs.H;
  % scale factors
  sqrt_Rb = sqrt_Rb/sqrt(2*D);
  sqrt_Rm = sqrt_Rm/sqrt(2*Np1*L);
  sqrt_Rf = sqrt_Rf/sqrt(2*Np1*D);
  
  % sparsity pattern of observation operator
  nnz_H = nnz(sparse(H));
  H_sp = zeros(nnz_H,3);
  [H_sp(:,1),H_sp(:,2),H_sp(:,3)] = find(sparse(H));
  
  % sparsity pattern template
  % TODO this could fail
  [~,d0F_sp,d1F_sp] = eval_F(0,rand(D,1),dt,rand(D,1));
  
  % sparsity pattern of dF
  d0F_sp = sparse(d0F_sp + Id);
  d1F_sp = sparse(d1F_sp + Id);
  d0F_sp_nnz = nnz(d0F_sp);
  d1F_sp_nnz = nnz(d1F_sp);
  d0F_sp_ind = find(d0F_sp);
  d1F_sp_ind = find(d1F_sp);
  d0F_sp_sub = zeros(d0F_sp_nnz,2);
  d1F_sp_sub = zeros(d1F_sp_nnz,2);
  [d0F_sp_sub(:,1),d0F_sp_sub(:,2)] = ind2sub(size(d0F_sp),d0F_sp_ind);
  [d1F_sp_sub(:,1),d1F_sp_sub(:,2)] = ind2sub(size(d1F_sp),d1F_sp_ind);
  
  % allocate 
  psi = zeros(D + Np1*L + N*D,1);
  dpsi = zeros(D + Np1*nnz_H + N*(d0F_sp_nnz + d1F_sp_nnz),3);
  
  % prior
  idxs = 1:D;
  psi(idxs) = sqrt_Rb*(x0 - xb);
  dpsi(idxs,1) = idxs;
  dpsi(idxs,2) = idxs;
  dpsi(idxs,3) = sqrt_Rb;
  
  % measurement block
  for n = 1:Np1
    idxs = D + (n-1)*L + (1:L);
    psi(idxs) = sqrt_Rm*(Y(n,:)' - H*X(n,:)');
    idx_block = D + (n-1)*nnz_H;
    dpsi(idx_block + (1:nnz_H),1) = (n-1)*L + H_sp(:,1) + D;
    dpsi(idx_block + (1:nnz_H),2) = (n-1)*D + H_sp(:,2);
    dpsi(idx_block + (1:nnz_H),3) = -sqrt_Rm*H_sp(:,3);
  end

  % model block
  for n = 1:N
    x_n = X(n,:)';
    x_np1 = X(n+1,:).';
    t_n = (n-1)*dt;
    t_np1 = n*dt;
    [F,dnF,dnp1F] = eval_F(t_n,x_n,t_np1,x_np1);
    % set psi
    idxs = D + Np1*L + (n-1)*D + (1:D);
    psi(idxs) = sqrt_Rf*(x_np1 - F);
   % dpsi_n/dx_n
    idx_block = D + Np1*nnz_H + (n-1)*(d0F_sp_nnz + d1F_sp_nnz);
    dpsi(idx_block + (1:d0F_sp_nnz),1) = (n-1)*D + d0F_sp_sub(:,1) + Np1*L + D;
    dpsi(idx_block + (1:d0F_sp_nnz),2) = (n-1)*D + d0F_sp_sub(:,2);
    dpsi(idx_block + (1:d0F_sp_nnz),3) = -sqrt_Rf*dnF(d0F_sp_ind);
    % dpsi_n/dx_np1
    idx_block = idx_block + d0F_sp_nnz;
    dpsi(idx_block + (1:d1F_sp_nnz),1) = (n-1)*D + d1F_sp_sub(:,1) + Np1*L + D;
    dpsi(idx_block + (1:d1F_sp_nnz),2) =     n*D + d1F_sp_sub(:,2);
    dnp1F = Id - dnp1F;
    dpsi(idx_block + (1:d1F_sp_nnz),3) = sqrt_Rf*dnp1F(d1F_sp_ind); 
  end

  % convert dpsi to sparse matrix
  dpsi = sparse(dpsi(:,1),dpsi(:,2),dpsi(:,3));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [psi,dpsi,d2psi] = eval_d2psi(obj,X,xb)
  if(nargin < 3)
    xb = X(1,:).';
  end
  x0 = X(1,:).';
  Y = obj.Y; D = obj.nD; L = obj.nL;
  N = obj.nN; Np1 = N+1; dt = obj.dt;
  eval_F = obj.quad_rule.eval;
  % TODO Support vector Rb/Rm/Rf
  sqrt_Rb = obs.sqrt_Rb(1);
  sqrt_Rm = obj.sqrt_Rm(1);
  if(~obj.anneal_Rf)
    sqrt_Rf = obj.sqrt_Rf(1);
  else
    sqrt_Rf = obj.sqrt_Rf{obj.idx_Rf};
  end
  Id = eye(D);
  % TODO Support nonlinear obs
  H = obj.obs.H;
  % scale factors
  sqrt_Rb = sqrt_Rb/sqrt(2*D);
  sqrt_Rm = sqrt_Rm/sqrt(2*Np1*L);
  sqrt_Rf = sqrt_Rf/sqrt(2*Np1*D);
  
  % sparsity pattern of observation operator
  nnz_H = nnz(sparse(H));
  H_sp = zeros(nnz_H,3);
  [H_sp(:,1),H_sp(:,2),H_sp(:,3)] = find(sparse(H));
  
  % sparsity pattern template
  % TODO this could fail
  [~,d0F_sp,d1F_sp,d00F_sp,d01F_sp,d11F_sp] = eval_F(0,rand(D,1),dt,rand(D,1));
  
  % sparsity pattern of dF
  d0F_sp = sparse(d0F_sp + Id);
  d1F_sp = sparse(d1F_sp + Id);
  d0F_sp_nnz = nnz(d0F_sp);
  d1F_sp_nnz = nnz(d1F_sp);
  d0F_sp_ind = find(d0F_sp);
  d1F_sp_ind = find(d1F_sp);
  d0F_sp_sub = zeros(d0F_sp_nnz,2);
  d1F_sp_sub = zeros(d1F_sp_nnz,2);
  [d0F_sp_sub(:,1),d0F_sp_sub(:,2)] = ind2sub(size(d0F_sp),d0F_sp_ind);
  [d1F_sp_sub(:,1),d1F_sp_sub(:,2)] = ind2sub(size(d1F_sp),d1F_sp_ind);
  
  % sparsity pattern of d2F
  mask = d00F_sp ~= 0; d00F_sp_ind = find(mask);
  mask = d01F_sp ~= 0; d01F_sp_ind = find(mask);
  mask = d11F_sp ~= 0; d11F_sp_ind = find(mask);
  d00F_sp_nnz = length(d00F_sp_ind);
  d01F_sp_nnz = length(d01F_sp_ind);
  d11F_sp_nnz = length(d11F_sp_ind);
  d00F_sp_sub = zeros(d00F_sp_nnz,3);
  d01F_sp_sub = zeros(d01F_sp_nnz,3);
  d11F_sp_sub = zeros(d11F_sp_nnz,3);
  [d00F_sp_sub(:,1),d00F_sp_sub(:,2),d00F_sp_sub(:,3)] = ind2sub(size(d00F_sp),d00F_sp_ind);
  [d01F_sp_sub(:,1),d01F_sp_sub(:,2),d01F_sp_sub(:,3)] = ind2sub(size(d01F_sp),d01F_sp_ind);
  [d11F_sp_sub(:,1),d11F_sp_sub(:,2),d11F_sp_sub(:,3)] = ind2sub(size(d11F_sp),d11F_sp_ind);
  
  % allocate 
  psi = zeros(Np1*L + N*D,1);
  dpsi = zeros(Np1*nnz_H + N*(d0F_sp_nnz + d1F_sp_nnz),3);
  d2psi = zeros( N*(d00F_sp_nnz + 2*d01F_sp_nnz + d11F_sp_nnz),4);
  
  % prior block
  idxs = 1:D;
  psi(idxs) = sqrt_Rb*(x0 - xb);
  dpsi(idxs,1) = idxs;
  dpsi(idxs,2) = idxs;
  dpsi(idxs,3) = sqrt_Rb;
  
  % measurement block
  for n = 1:Np1
    idxs = D + (n-1)*L + (1:L);
    psi(idxs) = sqrt_Rm*(Y(n,:)' - H*X(n,:)');
    idx_block = D + (n-1)*nnz_H;
    dpsi(idx_block + (1:nnz_H),1) = (n-1)*L + H_sp(:,1) + D;
    dpsi(idx_block + (1:nnz_H),2) = (n-1)*D + H_sp(:,2);
    dpsi(idx_block + (1:nnz_H),3) = -sqrt_Rm*H_sp(:,3);
  end

  % model block
  for n = 1:N
    x_n = X(n,:)';
    x_np1 = X(n+1,:)';
    t_n = (n-1)*dt;
    t_np1 = n*dt;
    [F,dnF,dnp1F,dnnF,dnnp1F,dnp1np1F] = eval_F(t_n,x_n,t_np1,x_np1);
    % set psi
    idxs = D + Np1*L + (n-1)*D + (1:D);
    psi(idxs) = sqrt_Rf*(x_np1 - F);
    % dpsi_n/dx_n
    idx_block = D + Np1*nnz_H + (n-1)*(d0F_sp_nnz + d1F_sp_nnz);
    dpsi(idx_block + (1:d0F_sp_nnz),1) = (n-1)*D + d0F_sp_sub(:,1) + Np1*L + D;
    dpsi(idx_block + (1:d0F_sp_nnz),2) = (n-1)*D + d0F_sp_sub(:,2);
    dpsi(idx_block + (1:d0F_sp_nnz),3) = -sqrt_Rf*dnF(d0F_sp_ind);
    % dpsi_n/dx_np1
    idx_block = idx_block + d0F_sp_nnz;
    dpsi(idx_block + (1:d1F_sp_nnz),1) = (n-1)*D + d1F_sp_sub(:,1) + Np1*L + D;
    dpsi(idx_block + (1:d1F_sp_nnz),2) =     n*D + d1F_sp_sub(:,2);
    dnp1F = Id - dnp1F;
    dpsi(idx_block + (1:d1F_sp_nnz),3) = sqrt_Rf*dnp1F(d1F_sp_ind);
    % d2psi_n/dx_n/dx_n
    idx_block = (n-1)*(d00F_sp_nnz + 2*d01F_sp_nnz + d11F_sp_nnz);
    d2psi(idx_block + (1:d00F_sp_nnz),1) = (n-1)*D + d00F_sp_sub(:,1) + Np1*L + D;
    d2psi(idx_block + (1:d00F_sp_nnz),2) = (n-1)*D + d00F_sp_sub(:,2);
    d2psi(idx_block + (1:d00F_sp_nnz),3) = (n-1)*D + d00F_sp_sub(:,3);
    d2psi(idx_block + (1:d00F_sp_nnz),4) = -sqrt_Rf*dnnF(d00F_sp_ind);
    % d2psi_n/dx_n/dx_np1
    idx_block = idx_block + d00F_sp_nnz;
    d2psi(idx_block + (1:d01F_sp_nnz),1) = (n-1)*D + d01F_sp_sub(:,1) + Np1*L + D;
    d2psi(idx_block + (1:d01F_sp_nnz),2) = (n-1)*D + d01F_sp_sub(:,2);
    d2psi(idx_block + (1:d01F_sp_nnz),3) =     n*D + d01F_sp_sub(:,3);
    d2psi(idx_block + (1:d01F_sp_nnz),4) = -sqrt_Rf*dnnp1F(d01F_sp_ind);
    % d2psi_n/dx_np1/dx_n
    idx_block = idx_block + d01F_sp_nnz;
    d2psi(idx_block + (1:d01F_sp_nnz),1) = (n-1)*D + d01F_sp_sub(:,1) + Np1*L + D;
    d2psi(idx_block + (1:d01F_sp_nnz),2) =     n*D + d01F_sp_sub(:,3);
    d2psi(idx_block + (1:d01F_sp_nnz),3) = (n-1)*D + d01F_sp_sub(:,2);
    d2psi(idx_block + (1:d01F_sp_nnz),4) = -sqrt_Rf*dnnp1F(d01F_sp_ind);
    % d2psi_n/dx_np1/dx_np1
    idx_block = idx_block + d01F_sp_nnz;
    d2psi(idx_block + (1:d11F_sp_nnz),1) = (n-1)*D + d11F_sp_sub(:,1) + Np1*L + D;
    d2psi(idx_block + (1:d11F_sp_nnz),2) =     n*D + d11F_sp_sub(:,2);
    d2psi(idx_block + (1:d11F_sp_nnz),3) =     n*D + d11F_sp_sub(:,3);
    d2psi(idx_block + (1:d11F_sp_nnz),4) = -sqrt_Rf*dnp1np1F(d11F_sp_ind);
  end

  % convert dpsi to sparse matrix
  dpsi = sparse(dpsi(:,1),dpsi(:,2),dpsi(:,3));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function test_derivs(obj,nDerivs)
  if(nargin < 2)
    nDerivs = 1;
  end
  if(nDerivs > 2)
    error('WC4DVar::test_derivs: derivatives only up to 2nd order');
  end
  D = obj.nD; Np1 = obj.nN+1;
  delta = 1.e-3;
  X0 = randn(Np1,D); 
  dX = randn(Np1,D); 
  dX = delta*dX/norm(dX(:));
  xb = X0(1,:).';
  % only compute 2nd derivatives if needed
  if(nDerivs == 1)
    [~,psi,dpsi] = obj.direct_eval(X0,xb);
  else
    [~,psi,dpsi,d2psi] = obj.direct_eval(X0,xb);
  end
  % 1st derivatives
  [~,psi_p] = obj.direct_eval(X0 + 0.5*dX,xb);
  [~,psi_m] = obj.direct_eval(X0 - 0.5*dX,xb);
  dXvec = reshape(transpose(dX),[],1);
  norm((psi_p - psi_m) - dpsi*dXvec)
  % 2nd derivatives
  if(nDerivs > 1)
    [~,dpsi_p] = obj.direct_eval(X0 + 0.5*dX,xb);
    [~,dpsi_m] = obj.direct_eval(X0 - 0.5*dX,xb);
    d2psi_dx = zeros(size(dpsi));
    for n = 1:size(d2psi,1)
      i = d2psi(n,1);
      j = d2psi(n,2);
      k = d2psi(n,3);
      d2psi_dx(i,j) = d2psi_dx(i,j) + d2psi(n,4)*dX(k);
    end
    norm((dpsi_p(:) - dpsi_m(:)) - d2psi_dx(:))
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% estimate critical radius based on (inverse) condition number
function ret = rho_c(obj)
  ret = [];
  X_opt = obj.X_opt;
  if(isempty(X_opt))
    return;
  end
  [~,psi,dpsi] = obj.direct_eval(X_opt);
  ret = rho_c@DA_Smoother(obj,psi,dpsi);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% set/get methods
function ret = get.anneal_Rf(obj)
  ret = iscell(obj.sqrt_Rf);
end
end
end
