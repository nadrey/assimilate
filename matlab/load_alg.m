% initialize algorithm
function [alg,obs] = load_alg(in)
  obs = ProjectionObs;
  if(strcmpi(in.alg,'Var3D'))
    alg = Var3D;
  elseif(strcmpi(in.alg,'TDVar'))
    alg = Var3D;
    obs = TimeDelayObs;
  elseif(strcmpi(in.alg,'ExtKF'))
    alg = ExtKF;
  elseif(strcmpi(in.alg,'TDExtKF'))
    alg = Var3D;
    obs = TimeDelayObs;
  elseif(strcmpi(in.alg,'SC4DVar'))
    alg = SC4DVar;
  elseif(strcmpi(in.alg,'WC4DVar') || ...
         strcmpi(in.alg,'WC4DVarX'))
    alg = WC4DVarX;
  elseif(strcmpi(in.alg,'WC4DVarU'))
    alg = WC4DVarU;
  elseif(strcmpi(in.alg(1:3),'MHE'))
    alg = MovingHorizonEst;
  else
    error('load_alg:: unknown algorithm');
  end
end