classdef SC4DVar < DA_Smoother
methods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = SC4DVar()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = [];
  end
  % base initialization
  obj = initialize@DA_Smoother(obj,prob,in);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Callback to initialize SmootherData
function init_data(obj,in)
  data = obj.data;
  nD = obj.nD; 
  
  % call base method
  init_data@DA_Smoother(obj,in);
  
  % default X0
  if(~isfield(in,'X0'))
    X0 = 'global';
  else
    X0 = in.X0;
    if(isempty(X0) || ischar(X0))
    elseif(isvector(X0))
      X0 = X0(:);
    elseif(ismatrix(X0))
      X0 = X0(1,:).';
    end
  end
  
  % compute X0
  data.X0 = [];
  if(isempty(X0))
    return;
  elseif(isnumeric(X0))
    data.x0 = X0;
  elseif(strcmpi(X0,'global'))
    init_rand(data.x0_est_seed);
    data.x0 = obj.model.rand_x0;
  elseif(strcmpi(X0,'local'))
    init_rand(data.x0_est_seed);
    dx0 = randn(nD,1); 
    dx0 = dx0/rms(dx0(:));
    r = obj.rmse_tol;
    dx0 = dx0*r;
    data.X_opt = obj.X_opt;
    x0_opt = data.X_opt(1,:).';
    data.x0 = x0_opt + dx0;
  else
    error('SC4DVar::init_data: invalid input');
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = step(obj)
  
  
  % compute direct gradient
  if(~obj.do_adjoint)
    dA = obj.dpsi'*obj.psi;
  % compute adjoint
  else
    [~,dA] = obj.adjoint_eval(obj.est.x0);
  end
  
  % full gauss-newton step
  if(~do_gradient)
    %dx = -pinv(obj.dpsi,obj.pinv_tol)*obj.psi;
    [~,r] = qr(obj.dpsi,0);
    dx = -r\(r'\dA);
  else
    dx = -dA;
  end
  
  % compute next x0
  alpha = obj.alpha;
  next_x0 = obj.est.x0 + alpha*dx;
  
  % reevaluate objective
  [next_X,next_psi,next_dpsi] = obj.direct_eval(next_x0);
  
  % step accepted
  if(obj.accept_step(obj.psi,next_psi))
    obj.psi = next_psi;
    obj.dpsi = next_dpsi;
    obj.est.X = next_X;
    ret = step@DA_Smoother(obj);
  else
    ret = false;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TODO Add background term
function [X,dA] = adjoint_eval(obj,x0)
  L = obj.nL; D = obj.nD; 
  N = obj.nN; Np1 = N+1; ts = obj.est.td.ts;
  sqrt_Rm = diag(obj.sqrt_Rm)/sqrt(2*Np1*L);
  Y = obj.Y; X = zeros(Np1,D);
  % TODO Support nonlinear obs
  H = obj.obs.H;
  
  % forward pass
  X(1,:) = x0.';
  for n = 1:N
    t_n = ts(n);
    x_n = X(n,:).';
    X(n+1,:) = obj.F(t_n,x_n);
  end
  % backward pass
  dA = zeros(D,1);
  for n = Np1:-1:1
    t_n = ts(n);
    x_n = X(n,:).';
    y_n = Y(n,:).';
    [~,dF_n] = obj.dF(t_n,x_n);
    dA = dF_n.'*dA + H.'*(sqrt_Rm.'*sqrt_Rm)*(y_n-H*x_n);
  end
  % flip sign
  dA = -dA;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute derivatives directly using variational equation
function [X,psi,dpsi,d2psi] = direct_eval(obj,x0,xb)
  if(nargin < 2)
    x0 = obj.est.x0(:);
  end
  if(nargin < 3)
    xb = x0;
  end
  L = obj.nL; D = obj.nD; 
  N = obj.nN; Np1 = N+1; ts = obj.est.td.ts;
  sqrt_Rm = diag(obj.sqrt_Rm)/sqrt(2*Np1*L);
  if(isscalar(sqrt_Rm))
    sqrt_Rm = sqrt_Rm*eye(L);
  end
  sqrt_Rb = diag(obj.sqrt_Rb)/sqrt(2*D);
  if(isscalar(sqrt_Rb))
    sqrt_Rb = sqrt_Rb*eye(D);
  end
  Y = obj.Y; X = zeros(Np1,D);
  % TODO Support nonlinear obs
  H = obj.obs.H;
  
  % compute derivatives as needed
  if(nargout == 2)
    psi = zeros(D+Np1*L,1);
    % prior
    psi(1:D) = sqrt_Rb*(x0 - xb);
    % measurements
    x_n = x0;
    for n = 1:Np1
      t_n = ts(n);
      n_idxs = D+(n-1)*L+(1:L);
      psi(n_idxs) = sqrt_Rm*(Y(n,:).' - H*x_n);
      X(n,:) = x_n.';
      x_n = obj.F(t_n,x_n);
    end
    
  elseif(nargout == 3)
    psi = zeros(D+Np1*L,1);
    dpsi = zeros(D+Np1*L,D);
    % prior
    psi(1:D) = sqrt_Rb*(x0 - xb);
    dpsi(1:D,:) = sqrt_Rb;
    % measurements
    dF = obj.var_eqn;
    phi1_0 = eye(D);
    z = vertcat(x0(:),phi1_0(:));
    for n = 1:Np1
      t_n = ts(n);
      x_n = z(1:D);
      phi_n = reshape(z(D+1:D*(D+1)),D,D);
      n_idxs = D+(n-1)*L+(1:L);
      psi(n_idxs) = sqrt_Rm*(Y(n,:)' - H*x_n);
      dpsi(n_idxs,:) = -sqrt_Rm*H*phi_n;
      X(n,:) = z(1:D).';
      z = dF(t_n,z);
    end
    
  elseif(nargout == 4)
    psi = zeros(Np1*L+D,1);
    dpsi = zeros(Np1*L+D,D);
    d2psi = zeros(Np1*L+D,D,D);
    % prior
    psi(1:D) = sqrt_Rb*(x0 - xb);
    dpsi(1:D,:) = sqrt_Rb;
    % measurements
    dF = obj.var_eqn;
    phi1_0 = eye(D);
    phi2_0 = zeros(D,D,D);
    z = vertcat(x0(:),phi1_0(:),phi2_0(:));
    for n = 1:Np1
      t_n = ts(n);
      x_n = z(1:D);
      phi1_n = reshape(z(D+1:D*(D+1)),D,D);
      phi2_n = reshape(z(D*(D+1)+1:D*(D*(D+1)+1)),D,D,D);
      n_idxs = D+(n-1)*L+(1:L);
      psi(n_idxs) = sqrt_Rm*(Y(n,:)' - H*x_n);
      dpsi(n_idxs,:) = -sqrt_Rm*H*phi1_n;
      tmp = zeros(L,D,D);
      for i = 1:L
      for j = 1:D
      for k = 1:D
      for l = 1:D
        % assumes sqrt_Rm is diagonal
        tmp(i,j,k) = tmp(i,j,k) - sqrt_Rm(i,i)*H(i,l)*squeeze(phi2_n(l,j,k));
      end
      end
      end
      end
      d2psi(n_idxs,:,:) = tmp;
      X(n,:) = z(1:D).';
      z = dF(t_n,z);
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function test_derivs(obj,nDerivs)
  if(nargin < 2)
    nDerivs = 1;
  end
  if(nDerivs > 2)
    error('SC4DVar::test_derivs: derivatives only up to 2nd order');
  end
  D = obj.nD;
  delta = 1.e-6;
  x0 = randn(D,1); 
  dx = randn(D,1); 
  dx = delta*dx/norm(dx);
  xb = x0;
  % only compute 2nd derivatives if needed
  if(nDerivs == 1)
    [~,psi,dpsi] = obj.direct_eval(x0,xb);
  else
    [~,psi,dpsi,d2psi] = obj.direct_eval(x0,xb);
  end
  % 1st derivatives
  [~,psi_p] = obj.direct_eval(x0 + 0.5*dx,xb);
  [~,psi_m] = obj.direct_eval(x0 - 0.5*dx,xb);
  display('1st derivatives (direct)');
  display(norm((psi_p - psi_m) - dpsi*dx))
  % adjoint eval
  %{
  [~,dA] = obj.adjoint_eval(x0);
  display('1st derivatives (adjoint)');
  display(norm(dA - dpsi.'*psi));
  delta_A = 0.5*(psi_p.'*psi_p - psi_m.'*psi_m);
  display(norm(delta_A - dA.'*dx));
  display(norm(delta_A - (dpsi.'*psi).'*dx));
  %}  
  % 2nd derivatives
  %{
  if(nDerivs > 1)
    [~,psi_p,dpsi_p] = obj.direct_eval(x0 + 0.5*dx,xb);
    [~,psi_m,dpsi_m] = obj.direct_eval(x0 - 0.5*dx,xb);
    norm((psi_p - psi_m) - dpsi*dx)
    d2psi_dx = zeros(size(dpsi));
    for i = 1:D
      d2psi_dx = d2psi_dx + squeeze(d2psi(:,:,i))*dx(i);
    end
    display('2nd derivatives (direct-direct)');
    display(norm((dpsi_p - dpsi_m) - d2psi_dx))
  end
  %}
  %{
  % Full jacobian
  dpsi2 = zeros(size(dpsi));
  for i = 1:D
    dx0 = zeros(size(x0));
    dx0(i) = delta;
    [~,psi_p] = obj.direct_eval(x0+0.5*dx0,xb);
    [~,psi_m] = obj.direct_eval(x0-0.5*dx0,xb);
    dpsi2(:,i) = (psi_p - psi_m)/delta;
  end
  full(dpsi) - dpsi2
  %}
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% estimate critical radius based on (inverse) condition number
function ret = rho_c(obj)
  ret = [];
  X_opt = obj.X_opt;
  if(isempty(X_opt))
    return;
  else
    x0_opt = X_opt(1,:).';
  end
  [~,psi,dpsi] = obj.direct_eval(x0_opt);
  ret = rho_c@DA_Smoother(obj,psi,dpsi);
end
end
end