%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef TDExtKF < handle
properties
  % model
  model = [];
  % time delay obs. operator
  tdObs = TimeDelayObs();
  % data
  data = [];
  % model error inverse covariance
  Rf;
  % observation error inverse covariance
  Rm = [];
  % initial estimated error covariance
  P0inv = [];
  % pinv tolerance
  pinv_tol = eps;
  % max iterations of inner loop
  max_iters = 1;
  % convergence tolerance for inner loop
  iter_conv_tol = 1.e-2;
  % check bounds
  check_bounds = false;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
  % constructor
  function obj = TDExtKF(model,varargin)
    if(nargin == 0)
      error('TDExtKF::TDExtKF: no model specified');
    end
    in = struct;
    data = [];
    for iarg = 1:nargin-1
      arg = varargin{iarg};
      if(isa(arg,'struct'))
        in = arg;
      elseif(isa(arg,'TrueExpData') || isa(arg,'TwinExpData'))
        data = arg;
      end
    end
    
    obj.initialize(model,data,in);
    obj.main(in);
  end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function obj = initialize(obj,model,data,in)
    function set_prop(key,val)
      if(isfield(in,key)); 
        obj.(key) = in.(key); 
      elseif(nargin == 2)
        obj.(key) = val;
      end
    end
    
    % initialize dynamical model
    obj.model = model;
    obj.model.initialize(in);
    
    % initialize twin data
    if(~isempty(data))
      obj.data = data;
    else
      obj.data = TwinExpData();
      obj.data.initialize(model,in);
    end
    
    % initialize time delay observations
    obj.tdObs.initialize(model,obj.data,in);
    
    % dimensions
    nD = obj.model.nD;
    nP = obj.model.nP;
    nL = obj.tdObs.nL;
    
    % model error covariance
    key = 'Rf'; val = eye(nD+nP);
    set_prop(key,val);
    % observation error covariance
    key = 'Rm'; val = eye(nL);
    set_prop(key,val);
    % initial estimated error covariance
    key = 'P0inv'; val = eye(nD+nP);
    set_prop(key,val);
    % pinv tolerance
    key = 'pinv_tol';
    set_prop(key);
    % max iterations of inner loop
    key = 'max_iters'; 
    set_prop(key);
    % convergence tolerance for inner loop
    key = 'iter_conv_tol';
    set_prop(key);
    % enforce bounds
    key = 'check_bounds';
    set_prop(key);
  end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % run TDExtKF
  function main(obj,in)
    % initialize GUI
    gui = FilterGUI();
    gui.initialize(in);
    
    % internal structures
    model = obj.model;
    data = obj.data;
    tdObs = obj.tdObs;
    
    % dimensions
    nD = model.nD;
    nP = model.nP;
    
    % time domain
    nN = data.t_domain.nN;
    t_vals = data.t_domain.t_vals;
    dt = data.t_domain.dt;

    % pseudoinverse parameters
    pinv_tol = obj.pinv_tol;

    % noise covariances
    Rf = obj.Rf; % model error 
    Rm = obj.Rm; % observation error

    % initial states
    x = transpose(data.x_est(1,:));
    p = [];
    if(nP > 0)
      p = transpose(data.p_est(1,:));
    end
    
    % initial estimation error
    Pinv = obj.P0inv;

    % run time-delay ExtKF
    for n = 1:nN
      % current estimate time
      t = t_vals(n);

      % iterated measurement update
      Xhat = vertcat(x,p);
      for i = 1:obj.max_iters
        % time-delay measurements
        [S,dHdx,dHdp] = tdObs.h(t,x,p);
        [Y,mask] = tdObs.y(t);

        % apply mask
        tmpRm = Rm(mask,mask);
        Y = Y(mask);
        S = S(mask);
        dHdx = dHdx(mask,:);
        dHdp = dHdp(mask,:);

        % construct Kalman gain
        dH = horzcat(dHdx,dHdp);
        K = dH*Pinv*transpose(dH) + tmpRm;
        try
          Kinv = pinv(K,pinv_tol);
        catch me
          throw(me)
        end
        K = Pinv*transpose(dH)*Kinv;

        % update states/parameters simultaneously
        X = vertcat(x,p); Xlast = X;
        X = Xhat + K*(Y - S - dH*(Xhat - X));

        % check bounds
        x = X(1:nD);
        x = obj.enforce_bounds(x,model.x_bounds);
        if(nP > 0)
          p = X(nD+1:nD+nP);
          p = obj.enforce_bounds(p,model.p_bounds);
        end

        % check for convergence
        r = norm(Xlast-X)/norm(X)/length(X);
        if(r < obj.iter_conv_tol)
          break;
        end
      end

      % update covariance after last iteration
      Pinv = Pinv - K*dH*Pinv;
      % difference of positive definite 
      % matrices may have numerical issues
      %tmp = eye(nD+nP) - K*dH;
      %Pinv = tmp*Pinv*transpose(tmp) + K*Rm*transpose(K);

      % time update
      X = vertcat(x,Pinv(:));
      fcov = @(t,x)TDExtKF.cov_eqn(t,x,p,model,Rf);
      X = data.ode(fcov,t,X,dt);
      x = X(1:nD);
      Pinv = reshape(X(nD+1:end),nD+nP,nD+nP);

      % save estimates
      x = obj.enforce_bounds(x,model.x_bounds);
      data.x_est(n+1,:) = x;
      if(nP > 0)
        p = obj.enforce_bounds(p,model.p_bounds);
        data.p_est(n+1,:) = p;
      end

      % GUI update
      gui.refresh(n,data);
    end
  end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function [x,modified] = enforce_bounds(obj,x,bounds)
    if(~obj.check_bounds)
      return;
    end
    if(size(bounds,1) == 1)
      bounds = repmat(bounds,length(x));
    end
    mask = x < bounds(:,1);
    x(mask) = bounds(mask,1);
    modified = any(mask);
    mask = x > bounds(:,2);
    x(mask) = bounds(mask,2);
    modified = or(modified,any(mask));
  end
end

methods (Static)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function dxdt = cov_eqn(t,X,p,model,Rf)
    nD = model.nD; nP = model.nP;
    x = X(1:nD);
    Pinv = reshape(X(nD+1:end),nD+nP,nD+nP);

    % propagate covariance
    [f,df] = model.f(t,x,p); 
    df = vertcat(df,zeros(nP,nD));
    df = horzcat(df,vertcat(model.dfdp(t,x,p),zeros(nP,nP)));
    dPinv = df*Pinv + Pinv*transpose(df) + Rf;

    % propagate state
    dxdt = vertcat(f(:),dPinv(:));
  end

end
end