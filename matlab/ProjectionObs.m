%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef ProjectionObs < DA_ObsModel
properties
  mode; idxs; 
end

properties(Dependent)
  nL; H; is_static;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Public methods
methods
% Constructor
function obj = ProjectionObs()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = struct;
  end
  % Base initialization
  obj = initialize@DA_ObsModel(obj,prob,in);
  % Set observation mode
  if(~isfield(in,'mode'))
    in.mode = 'uniform';
  end
  if(strcmpi(in.mode,'uniform'))
    obj.mode = @uniform;
  elseif(strcmpi(in.mode,'lumped'))
    obj.mode = @lumped;
  else
    error('ProjectionObs::initialize: unknown mode');
  end
  if(isfield(in,'idxs'))
    obj.idxs = in.idxs;
  else
    nD = obj.prob.model.nD;
    obj = set_prop(obj,in,'nL',nD/2);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function idxs = uniform(obj,L)
  nD = obj.prob.model.nD;
  idxs = round(linspace(1,nD,round(L)));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function idxs = lumped(obj,L)
  idxs = 1:round(L);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TODO It seems awkward to have the observation model also generate
% the data. But for time delay measurements, this is needed to distinguish 
% between time-delay `data' generated from sampling the time series, 
% and time-delay model output, which requires running the forecast model 
% forward/backward from a given x and t.
function ys = h_data(obj,ts)
  data = obj.prob.data;
  td = obj.prob.data.td;
  np1s = td.t_to_np1(ts);
  mask = np1s > 0 && np1s <= td.nNp1;
  np1s = np1s(mask);
  ys = data.Y(max(np1s));
  ys = ys(np1s,:);
  if(length(ts) == 1)
    ys = ys(:);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Measurement operator
function [hs,dhdxs] = h_est(obj,ts,xs)
  if(isvector(xs))
    hs = xs(obj.idxs);
    if(nargout == 2)
      dhdxs = obj.H;
    end
  else
    hs = xs(:,obj.idxs);
    if(nargout == 2)
      dhdxs = repmat(obj.H,size(xs,1),1);
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set/Get methods
function set.nL(obj,val)
  % allow L to be a function of D
  if(isa(val,'function_handle'))
    val = val(obj.prob.model.nD);
  end
  obj.idxs = obj.mode(obj,val);
end
%%%
function ret = get.nL(obj)
  ret = length(obj.idxs);
end
%%%
function ret = get.H(obj)
  nD = obj.prob.model.nD;
  ret = zeros(obj.nL,nD);
  ret(sub2ind(size(ret),1:obj.nL,obj.idxs)) = 1;
end
%%% 
function ret = get.is_static(obj)
  ret = true;
end
end
end