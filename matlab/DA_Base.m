classdef (Abstract) DA_Base < handle
methods
function obj = set_prop(obj,in,key,default)
  if(nargin < 4)
    default = obj.(key);
  end
  if(isfield(in,key))
    obj.(key) = in.(key);
  else
    if(isa(default,'function_handle'))
      obj.(key) = default();
    else
      obj.(key) = default;
    end
  end
end
end
end