% run parallel batches
function batch_par()
  global num_cores;
  num_cores = 4;
  global results_dir;
  results_dir = './';
  
  Lc_Dcut_EKF();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Lc_Dcut_EKF()
  global results_dir;
  name = [results_dir,'Lc_Dcut_EKF.mat'];
  
  % generate job array
  nICs = 10; Ds = 5:5:100; sigma_m = [0.e0,1.e0]; 
  alg = 'Var3D'; X0 = {'Local','Global'};
  sqrt_Ro = 1; sqrt_Rm = 1; sqrt_Rf = [0,1]; anneal_Rf = [0,1];
  keys = {'alg','x0_est_seed','x0_twin_seed','y_twin_seed','nD','sigma_m','X0',...
          'rmse_tol','sqrt_Ro','sqrt_Rm','sqrt_Rf','anneal_Rf'};
  vals = {alg,1:nICs,0,0,Ds,sigma_m,X0,1,sqrt_Ro,sqrt_Rm,sqrt_Rf,anneal_Rf};
  args = array_job(keys,vals);
  
  % run jobs
  tic; batch_job(name,@calc_Lc,args); toc;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Lc_Dcut_3DVar()
  global results_dir;
  name = [results_dir,'Lc_Dcut_3DVar.mat'];
  
  % generate job array
  nICs = 10; Ds = 5:5:100; sigma_m = [0.e0,1.e0]; 
  alg = 'Var3D'; X0 = {'Local','Global'};
  keys = {'alg','x0_est_seed','x0_twin_seed','y_twin_seed','nD','sigma_m','X0','rmse_tol'};
  vals = {alg,1:nICs,0,0,Ds,sigma_m,X0,1};
  args1 = array_job(keys,vals);
  
  % add sqrt_Rm, sqrt_Rb
  args1 = cellfun(@(x)setfield(x,'sqrt_Rm',inf),args1,'UniformOutput', false);
  args1 = cellfun(@(x)setfield(x,'sqrt_Rb',1),args1,'UniformOutput', false);
  args2 = args1;
  args2 = cellfun(@(x)setfield(x,'sqrt_Rm',1),args2,'UniformOutput', false);
  args3 = args2;
  args3 = cellfun(@(x)setfield(x,'sqrt_Rb',0),args3,'UniformOutput', false);
  args4 = args3;
  args4 = cellfun(@(x)setfield(x,'sqrt_Rb','error'),args4,'UniformOutput', false);
  args5 = args4;
  args5 = cellfun(@(x)setfield(x,'sqrt_Rb','global'),args5,'UniformOutput', false);
  
  % run jobs
  args = vertcat(args1,args2,args3,args4,args5);
  tic; batch_job(name,@calc_Lc,args); toc;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Nskip_cut()
  global results_dir;
  nICs = 1; Ds = 20; sigma_m = 0;[0,1];
  Ms = [1,5]; tau = 0.1; dt = 0.01; 
  N_skip = 0:20; gamma = @(N_skip,dt)10*dt*N_skip;

  % Filtering methods
  %alg = 'TDVar'; sqrt_Rm = inf; sqrt_Rb = 0; sqrt_Rf = NaN;
  alg = 'ExtKF'; sqrt_Rm = 1;  sqrt_Rb = 1; sqrt_Rf = 1; 
  keys = {'alg','x0_est_seed','x0_twin_seed','y_twin_seed',...
          'nD','sigma_m','sqrt_Rm','sqrt_Rb','sqrt_Rf','X0',...
          'nM','tau','N_skip','dt','gamma'};
  vals = {alg,1:nICs,0,0,...
          Ds,sigma_m,sqrt_Rm,sqrt_Rb,sqrt_Rf,'global',...
          Ms,tau,N_skip,dt,gamma};
  name = sprintf([results_dir,'Lc_Nskip_%s_Global.mat'],alg);
  tic; array_job(name,@calc_Lc,keys,vals); toc;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mcut()
  global results_dir;
  nICs = 10; D = 20; sigma_m = [0.e0,1.e0];
  L = 1; M = 1:D; tau = 0.1; max_rank = 1:D;
  alg = 'Var3D'; sqrt_Rm = inf; sqrt_Rf = 0; sqrt_Rb = 0;
  %alg = 'ExtKF'; sqrt_Rm = 1; sqrt_Rf = 1; sqrt_Rb = 1;
  keys = {'alg','x0_est_seed','x0_twin_seed','y_twin_seed',...
          'nD','sigma_m','sqrt_Rm','sqrt_Rf','sqrt_Rb','X0',...
          'nL','nM','tau','max_rank','Mcut'};
  vals = {alg,1:nICs,0,0,D,sigma_m,sqrt_Rm,sqrt_Rf,sqrt_Rb,'local',...
          L,M,tau,max_rank,1};
  name = [results_dir,'Mcut_TDVarFB_D=20_Local.mat'];
  tic; array_job(name,@calc_Mc,keys,vals); toc;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mc_Dcut()
  global results_dir;
  nICs = 10; Ds = 5:5:100; sigma_m = [0.e0,1.e0];
  %nICs = 2; Ds = [5,10]; sigma_m = 0;
  tau = 0.1; max_rank = @(nD)nD; N_skip = @(nM,tau,dt)(nM-1)*round(tau/dt);
  alg = 'Var3D'; sqrt_Rm = inf; sqrt_Rf = 0; sqrt_Rb = 0;
  %alg = 'ExtKF'; sqrt_Rm = 1; sqrt_Rf = 1; sqrt_Rb = 1;
  keys = {'alg','x0_est_seed','x0_twin_seed','y_twin_seed',...
          'nD','sigma_m','sqrt_Rm','sqrt_Rf','sqrt_Rb',...
          'X0','tau','rank_func','N_skip'};
  vals = {alg,1:nICs,0,0,Ds,sigma_m,sqrt_Rm,sqrt_Rf,sqrt_Rb,...
          'local',tau,max_rank,N_skip};
  name = [results_dir,'Mc_Dcut_TDVarFB_Local_wLag.mat'];
  tic; array_job(name,@calc_Mc,keys,vals); toc;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Lc_Dcut_old()
  global results_dir;
  nICs = 10; Ds = 5:5:100; sigma_m = [0.e0,1.e0]; X0 = 'Global';
  %nICs = 2; Ds = [5,10]; sigma_m = 0;
  %{
  % Filtering methods
  %alg = 'Var3D'; sqrt_Rm = inf; sqrt_Rb = 0;
  alg = 'ExtKF'; sqrt_Rm = 1; sqrt_Rf = 1; sqrt_Rb = 1;
  keys = {'alg','x0_est_seed','x0_twin_seed','y_twin_seed',...
          'nD','sigma_m','sqrt_Rm','sqrt_Rf','sqrt_Rb','X0'};
  vals = {alg,1:nICs,0,0,Ds,sigma_m,sqrt_Rm,sqrt_Rf,sqrt_Rb,X0};
  name = sprintf([results_dir,'Lc_Dcut_%s_%s.mat'],alg,X0);
  %}
  %
  % Fixed interval smoothing
  alg = 'WC4DVarX';
  sqrt_Rm = 1; sqrt_Rf = 1; sqrt_Rb = 0; T = [0.25,0.5,0.75,1];
  %sqrt_Rf = {num2cell(logspace(-1,1,5))}; % anneal Rf
  keys = {'alg','x0_est_seed','x0_twin_seed','y_twin_seed',...
          'nD','sigma_m','sqrt_Rm','sqrt_Rf','sqrt_Rb','T','X0'};
  vals = {alg,1:nICs,0,0,Ds,sigma_m,sqrt_Rm,sqrt_Rf,sqrt_Rb,T,X0};
  name = sprintf([results_dir,'Lc_Dcut_%s_%s.mat'],alg,X0);
  %}
  tic; array_job(name,@calc_Lc,keys,vals); toc;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Lc_Ncut()
  global results_dir;
  alg = 'SC4DVar'; X0 = 'Global';
  nICs = 10; Ds = 20; sigma_m = [0.e0,1.e0];
  sqrt_Rm = 1; sqrt_Rf = 1; sqrt_Rb = 0; Ts = 0:0.1:2.; 
  keys = {'alg','x0_est_seed','x0_twin_seed','y_twin_seed',...
          'nD','sigma_m','sqrt_Rm','sqrt_Rf','sqrt_Rb','T','X0'};
  vals = {alg,1:nICs,0,0,Ds,sigma_m,sqrt_Rm,sqrt_Rf,sqrt_Rb,Ts,X0};
  name = sprintf([results_dir,'Lc_Ncut_%s_%s.mat'],alg,X0);
  tic; array_job(name,@calc_Lc,keys,vals); toc;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Rc_Ncut()
  global results_dir;
  alg = 'WC4DVarU'; D = 20; Ls = round([D,D/2,1]);
  nICs = 10; Ts = [[0:0.05:0.45],[0.5:0.5:5]]; sigma_m = [0.e0,1.e0]; 
  %nICs = 2; Ts = 0:0.05:0.1; sigma_m = 0;
  sqrt_Rf = 1.; sqrt_Rm = 1.; sqrt_Rb = 0.;
  name = sprintf([results_dir,'Rc_Ncut_%s.mat'],alg);
  keys = {'alg','x0_est_seed','x0_twin_seed','y_twin_seed',...
          'sqrt_Rf','sqrt_Rm','sqrt_Rb','nD','nL','sigma_m','T'};
  vals = {alg,1:nICs,0,0,sqrt_Rf,sqrt_Rm,sqrt_Rb,D,Ls,sigma_m,Ts};
  tic; array_job(name,@calc_Rc,keys,vals); toc; 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rc smoother L cut
function Rc_Lcut()
  global results_dir;
  alg = 'SC4DVar'; D = 20; Ls = 1:D;
  nICs = 10; Ts = 0:0.05:2; sigma_m = [0.e0,1.e0]; 
  %nICs = 2; Ts = 0:0.05:0.1; sigma_m = 0;
  name = sprintf([results_dir,'Rc_Lcut_%s.mat'],alg);
  keys = {'alg','x0_est_seed','nD','nL','sigma_m','T'};
  vals = {alg,1:nICs,D,Ls,sigma_m,Ts};
  tic; array_job(name,@calc_Rc,keys,vals); toc;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rc smoother D cut
function Rc_Dcut()
  global results_dir;
  alg = 'SC4DVar'; D = 20; Ls = {@(D)D,@(D)floor(D/2),@(D)1};
  %nICs = 10; Ts = 0:0.05:2; sigma_m = [0.e0,1.e0]; 
  nICs = 2; Ts = 0:0.05:0.1; sigma_m = 0;
  name = sprintf([results_dir,'Rc_Dcut_%s.mat'],alg);
  keys = {'alg','x0_est_seed','nD','nL','sigma_m','T'};
  vals = {alg,1:nICs,D,Ls,sigma_m,Ts};
  tic; array_job(name,@calc_Rc,keys,vals); toc;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = my_test(varargin)
  celldisp(varargin);
  ret = rand(1,3);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% run an array of batch jobs
function args = array_job(keys,vals)
  if(length(keys) ~= length(vals) || size(keys,1) ~= 1 || size(vals,1) ~= 1)
    error('batch_par::array_job: invalid input');
  end
  num_keys = length(keys);
  num_vals = zeros(1,num_keys);
  for i = 1:num_keys
    if(size(vals{i},1) ~= 1)
      error('batch_par::array_job: invalid input');
    end
    if(iscell(vals{i}))
      num_vals(i) = size(vals{i},2);
    elseif(~isnumeric(vals{i}))
      num_vals(i) = 1;
      vals{i} = vals(i);
    elseif(isvector(vals{i}))
      num_vals(i) = length(vals{i});
      vals{i} = num2cell(vals{i});
    else
      error('batch_par::array_job: unknown input type');
    end
    % convert function handles to string
    tmp = vals{i};
    for j = 1:length(tmp)
      if(isa(tmp{j},'function_handle'))
        tmp{j} = func2str(tmp{j});
      end
    end
    vals{i} = tmp;
  end
  num_jobs = prod(num_vals);
  
  % generate argument subscripts
  subs = cell(1,num_keys);
  [subs{:}] = ind2sub(num_vals,1:num_jobs);
  subs = cell2mat(subs')';
  
  % generate inputs
  args = cell(num_jobs,1);
  for i = 1:num_jobs
    in = struct;
    for j = 1:num_keys
      in.(keys{j}) = vals{j}{subs(i,j)};
    end
    args{i} = in;
  end
  
  % run
  %batch_job(file_path,func,args);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% run batch of independent jobs on cluster across multiple nodes
% without distributed computing server
function batch_job(file_path,func,args)
  if(size(args,1) == 1)
    args = transpose(args);
  elseif(size(args,2) ~= 1)
    error('batch_par::my_batch: improper dimension of input args');
  end
  
  % display node PID
  node_id = feature('getpid');
  my_display = @(msg)display(sprintf('[%d] %s',node_id,msg));
  
  % semaphore lock file
  [~,lock_file,~] = fileparts(file_path);
  lock_file = ['./',lock_file,'.lock'];
  function lock()
    %return;
    % better
    system(sprintf('lockfile -5 -r -1 %s',lock_file));
    % bad idea, not thread safe
    %{
    while(exist(lock_file,'file'))
      pause(5);
    end
    fid = fopen(lock_file,'w+');
    if(fid == -1)
      error('batch_par::sweep: error opening lock file');
    end
    fclose(fid);
    %}
    my_display('locked');
  end
  function unlock()
    %return;
    my_display('unlocked');
    system(sprintf('rm -f %s',lock_file));
    %delete(lock_file);
  end

  % initializations
  num_jobs = size(args,1);
  try
    lock();
    % create/load output data structure
    if(~exist(file_path,'file'))
      data = horzcat(args,cell(num_jobs,1));
      save(file_path,'data');
    else
      load(file_path);
      if(any(cellfun(@(x1,x2)~isequal(x1,x2),args,data(:,1))))
        error('batch_par::sweep: input args do not match existing file');
      end
      % TODO figure out how to restart job by clearing -1 entries
      % without affecting other nodes that might have already started
    end
    % create cluster
    cluster = parcluster();
    tmp_dir = '/home/drey/.matlab/local_cluster_jobs/R2015a';
    if(exist(tmp_dir,'dir'))
      cluster.JobStorageLocation = tmp_dir;
    end
    global num_cores;
    cluster.NumWorkers = num_cores;
    my_display(sprintf('Initialized with %d cores',num_cores));
    display('... done');
    unlock();
  catch me
    unlock();
    rethrow(me);
  end
  
  % loop variables
  job_idx = 1;
  done = false;

  % create new job with tasks = num_cores
  function [job,idxs] = create_job()
    job = []; idxs = [];
    while(~done && numel(idxs) < num_cores)
      if(isempty(data{job_idx,2}))
        data{job_idx,2} = -1;
        idxs = horzcat(idxs,job_idx);
      end
      job_idx = job_idx + 1;
      done = job_idx > num_jobs;
    end
    % create job
    if(~isempty(idxs))
      job = createJob(cluster);
      display(sprintf('Creating job %d ...',job.ID));
      for idx = idxs
        in = eval_funcs(args{idx});
        in.ID = idx;
        in.file_path = file_path; 
        task = createTask(job,func,1,{in},'CaptureDiary',true);
        msg = 'Adding task %d for item %d of %d';
        display(sprintf(msg,task.ID,idx,num_jobs));
      end
      display(sprintf('Submitting job %d ...',job.ID));
      submit(job);
      display('... done');
    % should be done...
    elseif(~done)
      error('batch_par::sweep: could not find job');
    % we are done
    else
      my_display('No more jobs to add');
    end
  end

  % process outputs
  function process_job(job,idxs)
    display(sprintf('Processing job %d ...',job.ID));
    for idx = 1:numel(idxs)
      task = job.Tasks(idx);
      msg = 'Processing task %d for index %d';
      display(sprintf(msg,task.ID,idxs(idx)));
      if(data{idxs(idx),2} == -1)
      else
        warning('batch_par::sweep: data already exists');
      end
      %
      % allow some time for results to mirror
      % see http://www.mathworks.com/matlabcentral/answers/196965
      % pause(60);
      %
      if(~isempty(task.OutputArguments))
        data{idxs(idx),2} = task.OutputArguments{1};
      else
        data{idxs(idx),2} = [];
        if(strcmpi(task.State,'failed'))
          display(sprintf('Task failed'));
          display(task.ErrorMessage);
          display(task.InputArguments)
        elseif(strcmpi(task.State,'finished'));
          display(task.InputArguments)
          warning('batch_par::sweep: results are empty for finished task');
        end
      end
    end
  end
  
  % run jobs
  while(~done)
    % submit jobs
    try
      lock();
      load(file_path);
      [job,idxs] = create_job();
      save(file_path,'data');
      unlock();
    catch me
      unlock();
      rethrow(me);
    end
    % should be done if no job
    if(isempty(job))
      continue;
    end
    % wait for job to finish
    wait(job);
    my_display(sprintf('Job %d is done',job.ID));
    % process outputs separately
    try
      lock();
      load(file_path);
      process_job(job,idxs);
      save(file_path,'data');
      display('... done');
      unlock();
    catch me
      unlock();
      rethrow(me);
    end
  end
end

% function to evaluate function handles
function in = eval_funcs(in)
  mask = structfun(@ischar,in);
  idxs = find(mask);
  names = fieldnames(in);
  for i = idxs(:).'
    key = names{i};
    val = in.(key);
    if(val(1)~='@')
      continue;
    end
    i0 = 3; i1 = strfind(val,')')-1;
    [j0,j1] = regexp(val(i0:i1),'\w*');
    vars = cell(1,length(j0));
    for j = 1:length(j0)
      vars{j} = in.(val((j0(j):j1(j))+i0-1));
    end
    func = str2func(val);
    in.(key) = func(vars{:});
  end
end
