%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef FilterGUI < handle
properties
  n_plot = 10;
  handle
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
  % constructor 
  function obj = FilterGUI()
  end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % initialize
  function obj = initialize(obj,in)
    function set_prop(key)
      if(isfield(in,key)); 
        obj.(key) = in.(key); 
      end
    end
    set_prop('n_plot');
    
    % only set handle when plotting
    if(obj.n_plot > 0)
      obj.handle = gca;
    end
  end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % refresh plots
  function refresh(obj,n,data)
    if(mod(n+1,obj.n_plot) ~= 1)
      return;
    end
    
    % compute SE
    [SEx,SEp] = data.SE();
    
    % plot SEx
    t_vals = data.t_domain.t_vals;
    h = obj.handle;
    semilogy(h,t_vals(1:n+1),SEx(1:n+1),'b');
    
    % plot SEp
    if(~isempty(SEp))
      hold on;
      semilogy(h,t_vals(1:n+1),SEp(1:n+1),'r');
      hold off;
    end
    
    drawnow();
  end
end
end