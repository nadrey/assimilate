classdef MovingHorizonEst < ExtKF
properties
  alg;
  N_lag;
end

methods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = struct;
  end
  % base initialization
  obj = initialize@ExtKF(obj,prob,in);
  % initialize internal smoother
  if(~isfield(in,'alg'))
    in.alg = 'MHE_SC4DVar';
  end
  in.alg = in.alg(5:end);
  obj.alg = load_alg(in);
  if(obj.alg.is_filter)
    error('MovingHorizonEst::initialize: invalid algorithm');
  end
  % initialize window length
  % TODO Would prefer to use DA_Smoother initialization
  if(isfield(in,'N_lag'))
    obj.N_lag = in.N_lag;
  elseif(isfield(in,'T'))
    obj.N_lag = round(in.T/obj.dt/prob.model.gle_max)+1;
  else
    obj.N_lag = round(0.5/obj.dt/prob.model.gle_max)+1;
  end
  % initialize smoothing algorithm
  in.X0 = []; in.Y_idxs = 1:obj.N_lag; in.check_rmse = false;
  obj.alg.initialize(obj.prob,in);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function initialize_run(obj)
  initialize_run@ExtKF(obj);
  in.Y_idxs = (1:obj.N_lag)+obj.nN;
  in.X0 = zeros(obj.N_lag,obj.nD);
  in.U0 = zeros(obj.N_lag,obj.nD);
  mask = in.Y_idxs <= obj.N_max;
  in.Y_idxs = in.Y_idxs(mask);
  in.X0 = in.X0(mask,:);
  in.U0 = in.U0(mask,:);
  obj.alg.init_data(in);
  obj.alg.initialize_run();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [xn_n,sqrt_Rn_n] = meas_update(obj)
  % initialize from previous result
  in.Y_idxs = (1:obj.N_lag)+obj.nN;
  in.X0 = obj.alg.data.X;
  mask = ~isfinite(in.X0);
  in.X0(mask) = 0;
  in.U0 = obj.alg.data.U;
  mask = ~isfinite(in.U0);
  in.U0(mask) = 0;
  % truncate for end of window
  mask = in.Y_idxs <= obj.N_max;
  in.Y_idxs = in.Y_idxs(mask);
  in.X0 = in.X0(mask,:);
  if(~isempty(in.U0))
    in.U0 = in.U0(mask,:);
  end
  % initialize and run
  in.X0(1,:) = obj.x.';
  obj.alg.init_data(in);
  obj.alg.initialize_run();
  obj.alg.run();
  % process output
  xn_n = obj.alg.X;
  if(~any(isnan(xn_n)))
    xn_n = xn_n(1,:).';
  else
    xn_n = obj.x;
  end
  % covariance
  sqrt_Rn_n = obj.sqrt_Rn;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [xnp1_n,sqrt_Rnp1_n] = time_update(obj)
  xn_n = obj.x;
  tn = obj.t;
  [xnp1_n] = obj.F(tn,xn_n);
  % covariance
  sqrt_Rnp1_n = obj.sqrt_Rn;
  % update alg data
  X = obj.alg.data.X;
  X(1,:) = 0;
  X = circshift(X,[-1,0]);
  obj.alg.data.X = X;
  U = obj.alg.data.U;
  if(~isempty(U))
    U(1,:) = 0;
    U = circshift(U,[-1,0]);
    obj.alg.data.U = U;
  end
end
end
end