classdef FilterData < DA_EstData
properties
  X; x0;
end

properties(Dependent)
  xN, xNp1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
function obj = FilterData()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,alg,in)
  if(nargin < 2)
    in = [];
  end
  if(nargin < 1)
    error('FilterData::initialize: invalid input');
  end
  % Base initialization
  obj = initialize@DA_EstData(obj,alg,in);
  % Algorithm initializes data
  alg.init_data(obj,in);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = x(obj,t)
  if(nargin < 2)
    ret = obj.xN;
  else
    n = round(t/obj.td.dt);
    if(n == obj.td.nN)
      ret = obj.xN;
    else
     ret = obj.X(n+1,:).';
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function reset(obj)
  obj.td.nN = 0;
  obj.X = obj.x0.';
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get/Set
%%%
function set.x0(obj,val)
  obj.x0 = val(:);
  obj.reset();
end
%%%
function set.xN(obj,val)
  obj.X(obj.td.nNp1,:) = val.';
end
%%%
function ret = get.xN(obj)
  ret = obj.X(obj.td.nNp1,:).';
end
end
end