% estimate Lc for L96 using exponential search
function out = calc_Lc(in)
  if(nargin < 1)
    in = struct;
  end
  if(~isfield(in,'alg'))
    in.alg = 'Var3D';
  end
  display(in);
  
  % initialize Rb from file
  if(isfield(in,'sqrt_Rb') && ischar(in.sqrt_Rb))
    if(strcmpi(in.sqrt_Rb,'error'))
      load('L96_error_covs_T=1.e5.mat');
    elseif(strcmpi(in.sqrt_Rb,'global'))
      load('L96_global_covs_T=1.e5.mat');
    else
      error('calc_Lc:: unknown value for sqrt_Rb');
    end
    Ds = cellfun(@(x)x.nD,data(:,1));
    idx = find(Ds == in.nD);
    in.sqrt_Rb = inv(data{idx,2}.sqrt_C);
  end
  
  % initialize problem
  model = L96Model;
  [alg,obs] = load_alg(in);
  prob = DA_Problem(model,obs,alg,in);
  rmse_tol = prob.alg.rmse_tol;
  
  % outputs
  D = model.nD;
  if(alg.is_filter)
    out.ret  = -1*ones(1,D);
    out.rmse = -1*ones(1,D);
    out.rho = [];
  else
    out.ret  = -1*ones(1,D);
    out.rmse = -1*ones(1,D);
    out.rho  = -1*ones(1,D);
  end
  
  % check for existing results
  tmp_file = [];
  if(isfield(in,'file_path') && isfield(in,'ID'))
    [~,tmp_file,~] = fileparts(in.file_path);
    tmp_file = ['./tmp/',tmp_file,sprintf('_ID=%d',in.ID),'.mat'];
    if(exist(tmp_file,'file'))
      load(tmp_file);
      dSize = D - size(out.rmse,2);
      if(any(dSize > 0))
        out.rmse = padarray(out.rmse,dSize,-1,'post');
        out.ret  = padarray(out.ret ,dSize,-1,'post');
      end
    else
      save(tmp_file,'out');
    end
  end
  
  % run algorithm
  function run_alg(L)
    display(sprintf('\nrunning L = %d',L));
    if(out.ret(1,L) ~= -1)
      display('result exists');
      display(sprintf('rmse = %g',out.rmse(1,L)));
      return;
    end
    obs.nL = L;
    % reinitialize algorithm
    alg.reset_obs();
    % run filter
    alg.run();
    % record both true and predicted success 
    out.rmse(:,L) = calc_rmse(prob);
    out.ret(:,L) = out.rmse(:,L) <= rmse_tol;
    if(~isempty(out.rho))
      out.rho(:,L) = alg.rho_c;
    end
    display(sprintf('rmse = %g',out.rmse(1,L)));
    % save temporary results
    if(exist(tmp_file,'file'))
      save(tmp_file,'out');
    end
  end

  % TODO for now, use shorter estimation window
  %alg.N_max = ceil(alg.N_max/2);

  % exponential search
  L_max = D; L_min = 1; L = L_max;
  while(L_max > L_min)
    run_alg(L);
    if(out.ret(1,L))
      L_max = L;
      L = max(ceil(L/2),L_min);
    else
      L_min = L;
      break;
    end
  end
  % binary search
  while(L_max - L_min > 1)
    L = ceil(0.5*(L_max + L_min)); display(L);
    run_alg(L);
    if(out.ret(1,L))
      L_max = L;
    else
      L_min = L;
    end
  end
  % result
  display(Lc(out.ret(1,:)));
  
  %{
  for i = 1:size(out.ret,1)
    L_max = find(out.ret(i,:) == 1,1,'first');
    L_min = find(out.ret(i,:) == 0,1,'last');
    if(isempty(L_max))
      L_max = 2*D;
    end
    while(isempty(L_min) && L_max > 1)
      L = ceil(L_max/2); display(L);
      run_alg(L);
      L_max = find(out.ret(i,:) == 1,1,'first');
      L_min = find(out.ret(i,:) == 0,1,'last');
    end
  end
  
  % binary search
  for i = 1:size(out.ret,1)
    L_max = find(out.ret(i,:) == 1,1,'first');
    L_min = find(out.ret(i,:) == 0,1,'last');
    while(~isempty(L_max) && ~isempty(L_min) && L_max - L_min > 1)
      L = ceil(0.5*(L_max + L_min)); display(L);
      run_alg(L);
      L_max = find(out.ret(i,:) == 1,1,'first');
      L_min = find(out.ret(i,:) == 0,1,'last');
    end
  end
  
  % display Lc
  Lc_true = Lc(out.ret(1,:));
  if(size(out.ret,1) == 1)
    display(Lc_true);
  else
    Lc_global = Lc(out.ret(2,:));
    Lc_local =  Lc(out.ret(3,:));
    display([Lc_true,Lc_global,Lc_local]);
  end
  %}
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = Lc(val)
  ret = find(val == 0,1,'last');
  if(isempty(ret))
    ret = 1;
  else
    ret = ret + 1;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = calc_rmse(prob)
  if(prob.alg.is_filter)
    %out.ret = test_pred_error(prob);
    ret = prob.alg.rmse; 
  else
    X_true = prob.data.X;
    X_opt  = prob.alg.X_opt;
    X_est  = prob.est.X;
    X0_est = prob.est.X0;
    display('true error')
    true_err = rms(X_true(:) - X0_est(:));
    display(sprintf('  initial: %1.4e',true_err));
    true_err = rms(X_true(:) - X_est(:));
    display(sprintf('  final:   %1.4e',true_err));
    display('smoothed error');
    opt_err = rms(X_opt(:) - X0_est(:));
    display(sprintf('  initial: %1.4e',opt_err));
    opt_err = rms(X_opt(:) - X_est(:));
    display(sprintf('  final:   %1.4e\n',opt_err));
    ret(1) = opt_err;
    %ret(2) = 0;
  end
end

