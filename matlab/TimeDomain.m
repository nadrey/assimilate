%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef TimeDomain < DA_Base
properties
  nN, dt;
end
  
properties (Dependent)
  T; nNp1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
function obj = TimeDomain()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = struct;
  end
  if(nargin < 2)
    error('TimeDomain::initialize: invalid input');
  end
  
  % initialize time step
  obj = set_prop(obj,in,'dt',1.e-2);
  
  % initialize window length
  if(prob.alg.is_filter)
    dflt_nN = 0;
  else
    dflt_nN = 0.5/obj.dt/prob.model.gle_max;
  end
  obj = set_prop(obj,in,'nN',dflt_nN);

  % alternative initialization with T
  if(isfield(in,'T') && ~isfield(in,'nN'))
    obj.nN = floor(in.T/obj.dt);
    % scale by 1/gle_max
    obj.nN = obj.nN/prob.model.gle_max;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = ts(obj,n)
  if(nargin == 2)
    ret = obj.dt*(n-1);
  else
    ret = transpose(0:obj.nN)*obj.dt;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = t_to_np1(obj,t)
  ret = round(t./obj.dt)+1;
end
function ret = np1_to_t(obj,np1)
  ret = (np1-1)*obj.dt;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set/get
%%%
function set.nN(obj,val)
  obj.nN = round(val);
end
%%%
function set.nNp1(obj,val)
  obj.nN = val-1;
end
%%%
function ret = get.T(obj)
  ret = obj.dt*obj.nN;
end
%%%
function ret = get.nNp1(obj)
  ret = obj.nN+1;
end
end
end