classdef DA_Problem < DA_Base
properties
  model; 
  obs;
  data; 
  alg;
end

properties(Dependent)
  est;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
function obj = DA_Problem(model,obs,alg,in)
  if(nargin < 4)
    in = struct;
  end
  obj = obj.initialize(model,obs,alg,in);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,model,obs,alg,in)
  if(nargin < 1)
    in = struct;
  end
  % set components
  obj.model = model;
  obj.obs = obs;
  obj.alg = alg;
  % initialize models
  model.initialize(obj,in);
  obs.initialize(obj,in);
  % create data structures
  if(isfield(in,'data_file_name'))
    obj.data = TrueExpData();
    obj.data = obj.data.initialize(obj,in);
  else
    obj.data = TwinExpData();
    obj.data = obj.data.initialize(obj,in);
  end
  % initialize algorithm
  alg.initialize(obj,in);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set/Get methods
function ret = get.est(obj)
  ret = obj.alg.est;
end
end
end