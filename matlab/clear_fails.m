function clear_unfinished_runs(fname)
  load(fname);
  for i = 1:size(data,1)
    if(isnumeric(data{i,2}))
    if(data{i,2} == -1)
      data{i,2} = [];
    end
    end
  end
  save(fname,'data');
end
