classdef (Abstract) DA_EstData < DA_Base
properties
  alg, td; 
  x0_est_seed;
end

properties(Abstract)
  X;
end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods (Abstract)
  x(obj,t); reset(obj);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
function obj = DA_EstData()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,alg,in)
  if(nargin < 2)
    in = [];
  end
  if(nargin < 1)
    error('DA_EstData::initialize: invalid input');
  end
  obj.alg = alg;
  % set x0 seed
  dflt = 1;
  if(obj.alg.prob.data.is_twin_exp)
    x0_twin_seed = obj.alg.prob.data.x0_twin_seed;
    if(isfield(in,'x0_est_seed'))
      if(in.x0_est_seed == x0_twin_seed)
        msg = ' data and estimate x0 seeds are equal';
        warning(['DA_EstData::initialize: ',msg]);
      end
    else
      dflt = x0_twin_seed + 1;
    end
  end
  set_prop(obj,in,'x0_est_seed',dflt);
  % initialize time domain
  obj.td = TimeDomain;
  obj.td = obj.td.initialize(alg.prob,in);
end
end
end