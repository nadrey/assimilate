function ret = calc_GLEs(nIC,D)
  display([D,nIC]);
  % initialize model
  init = [];
  init.nD = D;
  model = L96Model;
  model.initialize(init);
  
  % initial conditions
  rng(nIC);
  x_true = model.rand_x0();
  
  % calculate GLEs
  Id = eye(D);
  x_phi = vertcat(x_true,Id(:));
  dt = 0.01;
  my_ode = @(t,x)rk4step(@(t,x)var_eqn(t,x,D,@model.f),t,x,dt);
  n = 0;
  N_min = 1.e5;
  N_max = 1.e7;
  mu_n = 0;
  var_n = 0;
  done = false;
  while(~done && n < N_max)
    % iterate dynamics
    t = n*dt;
    x_phi = my_ode(t,x_phi);
    n = n+1;
    
    % extract var. matrix and perform QR decomposition
    phi = reshape(x_phi(D+1:end),[D,D]);
    [Q,R] = qr(phi);
    x_phi(D+1:end) = Q(:);
    
    % incremental update for variance and mean
    q_n = log(abs(diag(R)))/dt;
    if(n > 1)
      var_n = (n-2)/(n-1)*var_n + (q_n - mu_n).^2/n;
    end
    mu_n =(q_n + (n-1)*mu_n)/n;  
    
    % debugging
    %if(~exist('cache','var'))
    %  cache = [];
    %end
    %cache = [cache,q_n];
    %norm(mean(cache,2) - mu_n)
    %norm(var(cache,0,2) - var_n)
    
    % check convergence
    if(n > N_min)
      % use relative std errr in the mean for largest GLE
      tol = 1.e-3;
      sem = sqrt(var_n)/sqrt(n);
      rel_sem = sem(1)/abs(mu_n(1));
      if(rel_sem < tol)
        done = true;
      end
      if(mod(n,N_min) == 0)
        display([n*dt,rel_sem,mu_n(1)]);
      end
      % for autonomous systems,
      % check convergence on 0 eigenvalue
      %tol = 1.e-3;
      %if(min(abs(mu_n)) < tol)
        %done = true;
      %end
    end
  end
  
  display(sprintf('%d',n*dt));
  ret = mu_n;

end
