function xout = ode4(ode,tspan,x0,options,varargin)
  if(length(tspan) < 2)
    error('ode4: tspan requires at least two values');
  end
  max_dt = 1.e-1; dt = max_dt;
  xout = zeros(length(tspan),length(x0));
  t = tspan(1); idx_tspan = 1;
  x = x0;
  xout(idx_tspan,:) = transpose(x);
  output_next = false;
  while(true)
    delta_t = tspan(idx_tspan+1)-t;
    if(dt >= delta_t)
      dt = delta_t;
      output_next = true;
    end
    x = rk4step(ode,t,x,dt,varargin);
    t = t + dt;
    if(output_next)
      output_next = false;
      idx_tspan = idx_tspan+1;
      xout(idx_tspan,:) = transpose(x);
      dt = max_dt;
      if(idx_tspan == length(tspan))
        break;
      end
    end
  end
end

function x = rk4step(f,t,x0,dt,f_args)
  k1 = dt*feval(f,t,x0,f_args{:});
  k2 = dt*feval(f,t+0.5*dt,x0 + k1/2,f_args{:});
  k3 = dt*feval(f,t+0.5*dt,x0 + k2/2,f_args{:});
  k4 = dt*feval(f,t+dt,x0 + k3,f_args{:});
  x = x0 + (k1 + 2*k2 + 2*k3 + k4)/6;
end