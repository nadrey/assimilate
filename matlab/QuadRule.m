classdef QuadRule < handle
properties
  model;
  eval;
end

methods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = QuadRule(model,in)
  if(nargin < 2 || ~isfield(in,'quad_rule'))
    in.quad_rule = 'explicit_rk4';
  end
  if(nargin < 1)
    error('QuadRule::QuadRule: no model');
  end
  obj.model = model;
  f = @model.f;
  if(strcmpi(in.quad_rule,'hermite_simpson_rule'))
    obj.eval = @(t0,x0,t1,x1)obj.hermite_simpson_rule(f,t0,x0,t1,x1);
  elseif(strcmpi(in.quad_rule,'trapezoidal_rule'))
    obj.eval = @(t0,x0,t1,x1)obj.trapezoidal_rule(f,t0,x0,t1,x1,0.5);
  elseif(strcmpi(in.quad_rule,'implicit_midpoint_rule'))
    obj.eval = @(t0,x0,t1,x1)obj.implicit_midpoint_rule(f,t0,x0,t1,x1);
  elseif(strcmpi(in.quad_rule,'explicit_rk4'))
    obj.eval = @(t0,x0,t1,x1)obj.explicit_rk4(f,t0,x0,t1,x1);
  else
    error('QuadRule::QuadRule: unknown quad type');
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function test_derivs(obj)
  % Test derivatives
  my_quad = @obj.eval;
  D = obj.model.nD;
  x0 = randn(D,1); x1 = randn(D,1); dx = randn(D,1);
  dx = 1.e-3*dx/norm(dx); dt = 1;
  % first derivatives
  [~,d0F,d1F] = my_quad(0,x0,dt,x1);
  F_p0 = my_quad(0,x0 + 0.5*dx,dt,x1);
  F_m0 = my_quad(0,x0 - 0.5*dx,dt,x1);
  F_p1 = my_quad(0,x0,dt,x1 + 0.5*dx);
  F_m1 = my_quad(0,x0,dt,x1 - 0.5*dx);
  norm((F_p0 - F_m0) - d0F*dx)
  norm((F_p1 - F_m1) - d1F*dx)
  % second derivatives
  [~,~,~,d00F,d01F,d11F] = my_quad(0,x0,dt,x1);
  [~,d0F_p0,d1F_p0] = my_quad(0,x0 + 0.5*dx,dt,x1);
  [~,d0F_m0,d1F_m0] = my_quad(0,x0 - 0.5*dx,dt,x1);
  [~,d0F_p1,d1F_p1] = my_quad(0,x0,dt,x1 + 0.5*dx);
  [~,d0F_m1,d1F_m1] = my_quad(0,x0,dt,x1 - 0.5*dx);
  d00F_dx = zeros(D,D);
  d11F_dx = zeros(D,D);
  d01F_dx = zeros(D,D);
  d10F_dx = zeros(D,D);
  for i = 1:D
  for j = 1:D
  for k = 1:D
    d00F_dx(i,j) = d00F_dx(i,j) + d00F(i,j,k)*dx(k);
    d11F_dx(i,j) = d11F_dx(i,j) + d11F(i,j,k)*dx(k);
    d01F_dx(i,j) = d01F_dx(i,j) + d01F(i,j,k)*dx(k);
    d10F_dx(i,j) = d10F_dx(i,j) + d01F(i,k,j)*dx(k);
  end
  end
  end
  norm((d0F_p0 - d0F_m0) - d00F_dx)
  norm((d1F_p1 - d1F_m1) - d11F_dx)
  norm((d0F_p1 - d0F_m1) - d01F_dx)
  norm((d1F_p0 - d1F_m0) - d10F_dx)
end
end

methods(Static)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% a generalized trapezoidal rule
%   x_1 = F(x_0,x_1) ~ x_0 + dt*(alpha*f(x_1) + (1-alpha)*f(x_0))
%
% Note that:
%   alpha = 0 -> forward euler
%   alpha = 1 -> backward euler
%   alpha = 1/2 -> trapezoidal rule
%
function varargout = trapezoidal_rule(f,t0,x0,t1,x1,alpha)
  D = length(x0); Id = eye(D);
  h = t1-t0;
  % F
  if(nargout <= 1)
    f0 = f(t0,x0);
    f1 = f(t1,x1);
    varargout{1} = x0 + h*(alpha*f1 + (1-alpha)*f0);
  % F, d0F, d1F
  elseif(nargout == 3)
    [f0,df0] = f(t0,x0);
    [f1,df1] = f(t1,x1);
    varargout{1} = x0 + h*(alpha*f1 + (1-alpha)*f0);
    varargout{2} = Id + h*(1-alpha)*df0;
    varargout{3} = h*alpha*df1;
  % F, d0F, d1F, d00F, d01F, d11F
  elseif(nargout == 6)
    [f0,df0,d2f0] = f(t0,x0);
    [f1,df1,d2f1] = f(t1,x1);
    varargout{1} = x0 + h*(alpha*f1 + (1-alpha)*f0);
    varargout{2} = Id + h*(1-alpha)*df0;
    varargout{3} = h*alpha*df1;
    varargout{4} = h*(1-alpha)*d2f0;
    varargout{5} = zeros(size(d2f0));
    varargout{6} = h*alpha*d2f1;
  else
    error('trapezoid_rule: unexpected number of outputs');
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% implicit midpoint rule
function varargout = implicit_midpoint_rule(f,t0,x0,t1,x1)
  D = length(x0); Id = eye(D);
  h = t1-t0;
  % F
  if(nargout <= 1)
    fm = f(0.5*(t0+t1),0.5*(x0+x1));
    varargout{1} = x0 + h*fm;
  % F, d0F, d1F
  elseif(nargout == 3)
    [fm,dfm] = f(0.5*(t0+t1),0.5*(x0+x1));
    varargout{1} = x0 + h*fm;
    varargout{2} = Id + 0.5*h*dfm;
    varargout{3} = 0.5*h*dfm;
  % F, d0F, d1F, d00F, d01F, d11F
  elseif(nargout == 6)
    [fm,dfm,d2fm] = f(0.5*(t0+t1),0.5*(x0+x1));
    varargout{1} = x0 + h*fm;
    varargout{2} = Id + 0.5*h*dfm;
    varargout{3} = 0.5*h*dfm;
    varargout{4} = 0.25*h*d2fm;
    varargout{5} = 0.25*h*d2fm;
    varargout{6} = 0.25*h*d2fm;
  else
    error('implicit_midpoint_rule: unexpected number of outputs');
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hermite simpson rule
function varargout = hermite_simpson_rule(f,t0,x0,t1,x1)
  D = length(x0); Id = eye(D);
  h = t1-t0;
  % F
  if(nargout <= 1)
    f0 = f(t0,x0);
    f1 = f(t1,x1);
    xm = 0.5*(x1 + x0) + 0.125*h*(f0 - f1);
    fm = f(0.5*(t0+t1),xm);
    varargout{1} = x0 + h/6*(f0 + 4*fm + f1);
  % F, d0F, d1F
  elseif(nargout == 3)
    [f0,df0] = f(t0,x0);
    [f1,df1] = f(t1,x1);
    xm = 0.5*(x1 + x0) + 0.125*h*(f0 - f1);
    [fm,dfm] = f(0.5*(t0+t1),xm);
    d0xm = 0.5*eye(D) + 0.125*h*df0;
    d1xm = 0.5*eye(D) - 0.125*h*df1;
    varargout{1} = x0 + h/6*(f0 + 4*fm + f1);
    varargout{2} = Id + h/6*(df0 + 4*dfm*d0xm);
    varargout{3} = h/6*(4*dfm*d1xm + df1);
  % F, d0F, d1F, d00F, d01F, d11F
  elseif(nargout == 6)
    [f0,df0,d2f0] = f(t0,x0);
    [f1,df1,d2f1] = f(t1,x1);
    xm = 0.5*(x1 + x0) + 0.125*h*(f0 - f1);
    [fm,dfm,d2fm] = f(0.5*(t0+t1),xm);
    d0xm = 0.5*eye(D) + 0.125*h*df0;
    d1xm = 0.5*eye(D) - 0.125*h*df1;
    % 2nd derivatives
    d00xm =  0.125*h*d2f0;
    % d01xm = 0
    d11xm = -0.125*h*d2f1;
    varargout{1} = x0 + h/6*(f0 + 4*fm + f1);
    varargout{2} = Id + h/6*(df0 + 4*dfm*d0xm);
    varargout{3} = h/6*(4*dfm*d1xm + df1);
    % tensor multiplication
    varargout{4} = d2f0;
    varargout{5} = zeros(size(d2f0));
    varargout{6} = d2f1;
    for i = 1:D
    for j = 1:D
    for k = 1:D
    for l = 1:D
    for m = 1:D
      varargout{4}(i,j,k) = varargout{4}(i,j,k) + 4*d2fm(i,l,m)*d0xm(l,j)*d0xm(m,k);
      varargout{5}(i,j,k) = varargout{5}(i,j,k) + 4*d2fm(i,l,m)*d0xm(l,j)*d1xm(m,k);
      varargout{6}(i,j,k) = varargout{6}(i,j,k) + 4*d2fm(i,l,m)*d1xm(l,j)*d1xm(m,k);
    end
    varargout{4}(i,j,k) = varargout{4}(i,j,k) + 4*dfm(i,l)*d00xm(l,j,k);
   %varargout{5}(i,j,k) = varargout{5}(i,j,k) + 4*dfm(i,l)*d01xm(l,j,k);
    varargout{6}(i,j,k) = varargout{6}(i,j,k) + 4*dfm(i,l)*d11xm(l,j,k);
    end
    end
    end
    end
    varargout{4} = h/6*varargout{4};
    varargout{5} = h/6*varargout{5}; 
    varargout{6} = h/6*varargout{6};
  else
    error('hermite_simpson_rule: unexpected number of outputs');
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% explicit rk4
%   x_1 = F(x_0)
%
%%% discrete model and Jacobian
function varargout = explicit_rk4(f,t0,x0,t1,x1)
  D = length(x0); Id = eye(D);
  h = t1-t0;
  % F
  if(nargout <= 1)
    varargout{1} = rk4step(f,t0,x0,h);
  % F, d0F, d1F
  elseif(nargout == 3)
    z0 = [x0(:); Id(:)];
    z0 = rk4step(@(t,z)var_eqn(t,z,D,f),t0,z0,h);
    varargout{1} = z0(1:D);
    varargout{2} = reshape(z0(D+1:end),D,D);
    varargout{3} = zeros(D,D);
  % F, d0F, d1F, d00F, d01F, d11F
  elseif(nargout == 6)
    error('explicit_rk4: not yet implemented');
    Id2 = repmat(Id,[1,1,D]);
    z0 = [x0(:); Id(:); Id2(:)];
    z0 = rk4step(@(t,z)var_eqn(t,z,D,f),t0,z0,h);
    varargout{1} = z0(1:D);
    varargout{2} = reshape(z0(D+1:D*(D+1)),D,D);
    varargout{3} = zeros(D,D);
    varargout{4} = reshape(z0(D*(D+1)+1:end),D,D,D);
    varargout{5} = zeros(D,D,D); 
    varargout{6} = zeros(D,D,D); 
  else
    error('explicit_rk4: unexpected number of outputs');
  end
end
end
end