classdef DA_Model < DA_Base
properties
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
function obj = DA_Model()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [obj,prob,in] = initialize(obj,prob,in)
  if(nargin < 3)
    in = struct;
  end
  if(nargin < 2)
    in = struct; prob = [];
  end
  % TODO Want to be able to use model independently
  % but like having 'in' as last argument
  if(isstruct(prob))
    in = prob;
    prob = [];
  end
end
end
end