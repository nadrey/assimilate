function test_derivs 
  % test derivatives
  D = 10; Np1 = 10; L = 5;
  dt = 1.e-1; sqrt_Rm = 2.e0; sqrt_Rf = 3.e0;
  Y = randn(Np1,L);
  H = randn(L,D);
  delta = 1.e-3;
  
  % initialize model
  init.nD = D;
  model = L96Model;
  model.initialize(init);
  
  % strong constraint 4Dvar
  my_func = @(x0)eval_sc4D(x0,Y,H,@model.f,dt,sqrt_Rm);
  x0 = randn(D,1); 
  dx = randn(D,1); 
  dx = delta*dx/norm(dx);
  % 1st derivatives
  [~,dpsi] = my_func(x0);
  [psi_p] = my_func(x0 + 0.5*dx);
  [psi_m] = my_func(x0 - 0.5*dx);
  norm((psi_p - psi_m) - dpsi*dx)
  % 1st and 2nd derivatives
  [~,dpsi,d2psi] = my_func(x0);
  [psi_p,dpsi_p] = my_func(x0 + 0.5*dx);
  [psi_m,dpsi_m] = my_func(x0 - 0.5*dx);
  norm((psi_p - psi_m) - dpsi*dx)
  d2psi_dx = zeros(size(dpsi));
  for i = 1:D
    d2psi_dx = d2psi_dx + squeeze(d2psi(:,:,i))*dx(i);
  end
  norm((dpsi_p - dpsi_m) - d2psi_dx)
  
  % weak constraint 4Dvar
  my_func = @(x0)eval_4Dwc(x0,Y,H,@model.f,dt,sqrt_Rm,sqrt_Rf);
  X0 = randn(Np1*D,1); 
  dX = randn(Np1*D,1); 
  dX = delta*dX/norm(dX);
  % 1st derivatives
  [~,dpsi] = my_func(X0);
  [psi_p] = my_func(X0 + 0.5*dX);
  [psi_m] = my_func(X0 - 0.5*dX);
  norm((psi_p - psi_m) - dpsi*dX)
  % 1st and 2nd derivatives
  [~,dpsi,d2psi] = my_func(X0);
  [psi_p,dpsi_p] = my_func(X0 + 0.5*dX);
  [psi_m,dpsi_m] = my_func(X0 - 0.5*dX);
  norm((psi_p - psi_m) - dpsi*dX)
  d2psi_dx = zeros(size(dpsi));
  for n = 1:size(d2psi,1)
    i = d2psi(n,1);
    j = d2psi(n,2);
    k = d2psi(n,3);
    d2psi_dx(i,j) = d2psi_dx(i,j) + d2psi(n,4)*dX(k);
  end
  norm((dpsi_p - dpsi_m) - d2psi_dx)

end
  
  
  