classdef Var3D < DA_Filter
properties
  % step function
  my_step;
  % measurement error covariance
  sqrt_Rm;
  % background mean/covariance
  b; sqrt_Rb;
  sqrt_Rb_inv; sqrt_Rm_inv;
  finite_Rb_inv, finite_Rm;
  % options
  gamma;
  adaptive_obs;
  do_AUS;
  max_rank;
  % tolerances
  pinv_tol = eps;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
function obj = Var3D()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% reset object b/c obs operator has changed
function reset_obs(obj)
  reset_obs@DA_Filter(obj);
  init_coupling(obj);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = struct;
  end
  % base initialization
  obj = initialize@DA_Filter(obj,prob,in);
  % initialize covariances
  obj = set_prop(obj,in,'b',[]);
  obj = set_prop(obj,in,'sqrt_Rb',1.);
  obj = set_prop(obj,in,'sqrt_Rm',inf);
  % initialize other parameters
  obj = set_prop(obj,in,'gamma',1);
  obj = set_prop(obj,in,'adaptive_obs',false);
  obj = set_prop(obj,in,'do_AUS',false);
  obj = set_prop(obj,in,'max_rank',obj.nD);
  obj = set_prop(obj,in,'pinv_tol',eps);
  % determine scatic coupling
  init_coupling(obj);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function init_coupling(obj)
  % adaptive obs requires twin experiment
  if(obj.adaptive_obs)
    if(~strcmpi(class(obj.prob.data),'TwinExpData'))
      error('Var3D::init_coupling: adaptive obs. only allowed in twin expt');
    end
    obj.prob.obs.idxs = 1:obj.nD;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Two step filtering
function ret = step(obj)
  ret = true;
  try
  % measurement update
  do_meas = obj.N_skip <= 1 || mod(obj.nN+1,obj.N_skip) == 1;
  if(do_meas)
    x_n = obj.meas_update();
    ret = ret && obj.accept_step(obj.t,x_n);
  end
  % time update
  x_np1 = obj.time_update();
  ret = ret && obj.accept_step(obj.t+obj.dt,x_np1);
  % check if we are done
  ret = ret && step@DA_Filter(obj);
  catch me
    warning(me.getReport);
    obj.est.xN = NaN(size(obj.est.xN));
    ret = false;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function x_np1 = time_update(obj)
  t = obj.t; 
  x = obj.x;
  x_np1 = obj.F(t,x);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
function x_n = meas_update(obj)
  t = obj.t;
  x = obj.x;
  y = obj.y;
  b = obj.b(:);
  
  % infinite Rb^{-1} ==> pecora/carroll synch
  if(~obj.finite_Rb_inv)
    x_n = x;
    x_n(obj.obs.idxs) = y;
    return;
  end

  % compute h, dh
  [h,dh] = obj.h(t,x);
  
  % remove NaNs from h, dh
  mask = and(~isnan(h),~any(isnan(dh),2));
  h = h(mask); y = y(mask);
  if(isempty(h) || isempty(y))
    x_n = NaN;
    return;
  end
  
  % compute K
  K = obj.K(dh,mask);
  
  % adaptive obs to control unstable subspace
  tol = -1.e-3;
  if(obj.adaptive_obs)
    % rank
    D = obj.nD; 
    r = min(obj.nD,obj.nL); 
    r = min(r,obj.max_rank);
    mask = false(D,1); mask(1:r) = true;
    % discrete
    [~,dF] = obj.F(t,x);
    [U,S,V] = svd(dF);
    [S,idxs] = sort(diag(S),'descend');
    tol = 1+tol;
    mask = and(mask,S > tol);
    idxs = idxs(mask);
    U = U(:,idxs);
    V = V(:,idxs);
    S = S(idxs)-tol;
    K = U*diag(S)*V.';
    %{
    % continuous
    [~,dfn] = obj.f(t,x);
    [Q,S] = eig(dfn + dfn');
    [S,idxs] = sort(diag(S),'descend');
    mask = and(mask,S > tol);
    idxs = idxs(mask);
    Q = Q(:,idxs);
    S = S(idxs)-tol;
    K = obj.dt*0.5*Q*diag(S)*Q';
    %}
  end
  
  % project dx onto unstable subspace
  tol = -1.e-3;
  if(obj.do_AUS)
    % rank
    D = obj.nD; 
    r = min(obj.nD,obj.nL); 
    r = min(r,obj.max_rank);
    mask = false(D,1); mask(1:r) = true;
    % discrete
    [~,dF] = obj.F(t,x);
    [U,S,~] = svd(dF);
    [S,idxs] = sort(diag(S),'descend');
    tol = 1+tol;
    mask = and(mask,S > tol);
    idxs = idxs(mask);
    U = U(:,idxs);
    K = (U*U.')*K;
    %{
    % continuous
    [~,dfn] = obj.f(t,x);
    [Q,S] = eig(dfn + dfn');
    [S,idxs] = sort(diag(S),'descend');
    mask = and(mask,S > tol);
    idxs = idxs(mask);
    Q = Q(:,idxs);
    K = Q*Q'*K;
    %}
  end
  
  % scale factor
  K = K*obj.gamma;
  
  % meas. update 
  if(isempty(b)) % x = b
    x_n = x + K*(y-h);
  else % x != b
    x_n = b + K*(y-h+dh*(x-b)); 
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function K = K(obj,dh,mask)
  % remove NaNs
  dh = dh(mask,:);
  
  % infinite Rm ==> synchronization
  if(~obj.finite_Rm)
    if(strcmpi(class(obj.prob.obs),'ProjectionObs'))
      K = dh.';
    else
      K = obj.my_pinv(dh);
    end
  % finite Rm, Rb ==> 3DVar
  elseif(obj.finite_Rb_inv)
    idxs = find(mask);
    sqrt_Rm_inv = obj.sqrt_Rm_inv(idxs,:);
    sqrt_Rm_inv = sqrt_Rm_inv(:,idxs);
    sqrt_Rb_inv_dh_tr = obj.sqrt_Rb_inv*(dh.');
    %K = sqrt_Rm_inv.'*sqrt_Rm_inv + sqrt_Rb_inv_dh_tr.'*sqrt_Rb_inv_dh_tr;
    K = vertcat(sqrt_Rm_inv,sqrt_Rb_inv_dh_tr);
    K = obj.my_pinv(K);
    K = obj.sqrt_Rb_inv.'*sqrt_Rb_inv_dh_tr*(K*K.');
  end
end
%%%
function M = my_pinv(obj,M)
  [U,S,V] = svd(M);
  S = diag(S);
  r = min(obj.nL,obj.nD);
  r = min(r,obj.max_rank);
  r = min(r,sum(S > obj.pinv_tol));
  Sinv = 1./S(1:r);
  U = U(:,1:r);
  V = V(:,1:r);
  M = bsxfun(@times,V,Sinv(:).')*U.';
  %M = pinv(M,obj.pinv_tol);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set/get methods
%%%
function set.sqrt_Rm(obj,val)
  if(isscalar(val))
    obj.sqrt_Rm = val*eye(obj.nL);
    obj.sqrt_Rm_inv = eye(obj.nL)/val;
    obj.finite_Rm = isfinite(val);
  elseif(isvector(val))
    if(length(val) == obj.nL)
      obj.sqrt_Rm = diag(val);
      obj.sqrt_Rm_inv = diag(1./val);
      obj.finite_Rm = all(isfinite(val));
    else
      error('Var3D::set.sqrt_Rm: improper length');
    end
  elseif(ismatrix(val))
    if(all(size(val) == [obj.nL,obj.nL]))
      obj.sqrt_Rm = val;
      obj.sqrt_Rm_inv = inv(val);
      obj.finite_Rm = all(all(isfinite(val)));
    else
      error('Var3D::set.sqrt_Rm: improper size');
    end
  else
    error('Var3D::set.sqrt_Rm: only diagonal covariances are supported');
  end
end
%%%
function set.sqrt_Rb(obj,val)
  if(isscalar(val))
    obj.sqrt_Rb = val*eye(obj.nD);
    obj.sqrt_Rb_inv = eye(obj.nD)/val;
    obj.finite_Rb_inv = isfinite(1./val);
  elseif(isvector(val))
    if(length(val) == obj.nD)
      obj.sqrt_Rb = diag(val);
      obj.sqrt_Rb_inv = diag(1./val);
      obj.finite_Rb_inv = all(isfinite(1./val));
    else
      error('Var3D::set.sqrt_Rb: improper length');
    end
  elseif(ismatrix(val))
    if(all(size(val) == [obj.nD,obj.nD]))
      obj.sqrt_Rb = val;
      obj.sqrt_Rb_inv = inv(val);
      obj.finite_Rb_inv = all(all(isfinite(obj.sqrt_Rb_inv)));
    else
      error('Var3D::set.sqrt_Rb: improper size');
    end
  else
    error('Var3D::set.sqrt_Rb: only diagonal covariances are supported');
  end
end
end
end