%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Use recursive QR algorithm to compute LLEs
function ret = calc_LLEs(Q,Rs,dt)
  D = size(Rs,1);
  N = size(Rs,3);
  i = 1;
  
  % This has convergence issues
  % 
  sf = 1;
  while(~is_done(Q,D,i))
    for n = 1:N
      [Q,Rs(:,:,n)] = qr(Rs(:,:,n)*Q);
    end
    i = i+1;
  end
  %}
  
  % This also has convergence issues
  %{
  sf = 0.5;
  Ns = [1:N,fliplr(1:N)];
  Rs = Rs(:,:,Ns);
  while(~is_done(Q,D,i))
    tmp = Rs;
    for n = 1:2*N
      [Q,Rs(:,:,n)] = qr(tmp(:,:,n)*Q);
    end
    i = i+1;
  end
  %}
  
  % compute final result
  ret = zeros(D,1);
  for i = 1:D
    tmp = squeeze(Rs(i,i,:));
    ret(i) = sum(log(abs(tmp)));
  end
  ret = sf*ret/N/dt;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% test whether Q is identity matrix
function [done,res] = is_done(Q,D,i)
  max_iter = 10;
  if(i > max_iter)
    %warning('calc_LLEs: did not converge');
    done = true;
    return;
  end
  tol = 1.e-6;
  res = abs(Q)-eye(D);
  res = norm(res(:))/sqrt(D);
  done = res < tol;
end