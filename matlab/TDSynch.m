%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef TDSynch < handle
properties
  % model
  model = [];
  % time delay obs. operator
  tdObs = TimeDelayObs();
  % data
  data = [];
  % x-space coupling
  Kx;
  % S-space coupling
  Ks;
  % pinv tolerance
  pinv_tol = eps;
  % max pinv rank
  max_pinv_rank;
  % enforce bounds
  check_bounds = false;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
  % constructor
  function obj = TDSynch(model,varargin)
    if(nargin == 0)
      error('TDSynch::TDSynch: no model specified');
    end
    in = struct;
    data = [];
    for iarg = 1:nargin-1
      arg = varargin{iarg};
      if(isa(arg,'struct'))
        in = arg;
      elseif(isa(arg,'TrueExpData') || isa(arg,'TwinExpData'))
        data = arg;
      end
    end
    
    obj.initialize(model,data,in);
    obj.main(in);
  end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function obj = initialize(obj,model,data,in)
    function set_prop(key,val)
      if(isfield(in,key)); 
        obj.(key) = in.(key); 
      elseif(nargin == 2)
        obj.(key) = val;
      end
    end
    
    % initialize dynamical model
    obj.model = model;
    obj.model.initialize(in);
    
    % initialize twin data
    if(~isempty(data))
      obj.data = data;
    else
      obj.data = TwinExpData();
      obj.data.initialize(model,in);
    end
    
    % initialize time delay observations
    obj.tdObs.initialize(model,obj.data,in);
    
    % dimensions
    nD = obj.model.nD;
    nP = obj.model.nP;
    nL = obj.tdObs.nL;
    
    % x-space coupling
    key = 'Kx'; val = 1.e1*eye(nD+nP);
    set_prop(key,val);
    % S-space coupling
    key = 'Ks'; val = 1.e0*eye(nL);
    set_prop(key,val);
    % pinv tolerance
    key = 'pinv_tol';
    set_prop(key);
    % max pinv rank
    key = 'max_pinv_rank'; val = nD+nP;
    set_prop(key,val);
    % enforce bounds
    key = 'check_bounds';
    set_prop(key,val);
  end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % run TDSynch
  function main(obj,in)
    % initialize GUI
    gui = FilterGUI();
    gui.initialize(in);
    
    % internal structures
    model = obj.model;
    data = obj.data;
    tdObs = obj.tdObs;
    
    % time domain
    nN = data.t_domain.nN;
    t_vals = data.t_domain.t_vals;
    dt = data.t_domain.dt;
    
    % dimensions
    nD = model.nD;
    nP = model.nP;

    % constant coupling
    Kx = obj.Kx; % x-space coupling
    Ks = obj.Ks; % S-space coupling

    % run time-delay synchronization
    x = transpose(data.x_est(1,:));
    p = [];
    if(nP > 0)
      p = transpose(data.p_est(1,:));
    end
    for n = 1:nN
      % current estimate time
      t = t_vals(n);

      % time-delayed measurements
      [S,dSdx,dSdp] = tdObs.h(t,x,p);
      [Y,mask] = tdObs.y(t);
      
      % check length of embedding
      if(any(mask))
        tmpKs = Ks(mask,mask);
      end

      % compute dxdS
      dS = horzcat(dSdx,dSdp);
      [U,G,V] = svd(dS);
      G = diag(G);
      mask = G >= obj.pinv_tol;
      r = min(obj.max_pinv_rank,sum(mask));
      Ginv = G(1:r).^(-1);
      dSinv = V(:,1:r)*diag(Ginv)*transpose(U(:,1:r));

      % compute coupling perturbation
      try
      dx = Kx*dSinv*tmpKs*(Y-S);
      catch
        keyboard
      end
      % update state estimate
      fc = @(t,x)model.f(t,x,p) + dx(1:nD);
      x = data.ode(fc,t,x,dt);
      x = obj.enforce_bounds(x,model.x_bounds);
      data.x_est(n+1,:) = x;

      % update parameter estimate
      if(nP > 0)
        fc = @(t,p) dx(nD+1:end);
        p = data.ode(fc,t,p,data.dt);
        p = obj.enforce_bounds(p,model.p_bounds);
        data.p_est(n+1,:) = p;
      end

      % GUI update
      gui.refresh(n,data);
    end
  end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function [x,modified] = enforce_bounds(obj,x,bounds)
    if(~obj.check_bounds)
      return;
    end
    if(size(bounds,1) == 1)
      bounds = repmat(bounds,length(x));
    end
    mask = x < bounds(:,1);
    x(mask) = bounds(mask,1);
    modified = any(mask);
    mask = x > bounds(:,2);
    x(mask) = bounds(mask,2);
    modified = or(modified,any(mask));
  end
  
end
end