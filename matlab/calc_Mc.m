% estimate Mc for L96 using exponential search
function out = calc_Mc(in)
  if(nargin < 1)
    in = struct;
  end
  if(~isfield(in,'alg'))
    in.alg = 'Var3D';
  end
  if(~isfield(in,'tau'))
    in.tau = 0.1;
  end
  % input function for max rank
  if(~isfield(in,'rank_func'))
    rank_func = @(D)D;
  elseif(isa(in.rank_func,'function_handle'))
    rank_func = in.rank_func;
  elseif(ischar(in.rank_func))
    rank_func = str2func(in.rank_func);
  else
    error('calc_Mc:: improper input');
  end
  % input function for lag
  if(~isfield(in,'lag_func'))
    lag_func = @(M,tau,dt)1;
  elseif(isnumeric(in.lag_func))
    lag_func = @(M,tau,dt)in.lag_func;
  elseif(isa(in.lag_func,'function_handle'))
    lag_func = in.lag_func;
  elseif(ischar(in.lag_func))
    lag_func = str2func(in.lag_func);
  else
    error('calc_Mc:: improper input');
  end
  display(in);
  
  % initialize problem
  model = L96Model;
  obs = TimeDelayObs;
  alg = load_alg(in);
  prob = DA_Problem(model,obs,alg,in);
  rmse_tol = prob.alg.rmse_tol;
  
  % run alg and return sampled rsme
  if(isfield(in,'Mcut') && in.Mcut == true)
    alg.run();
    out.ts = unique([alg.ts(1:10:end);alg.ts(end)]);
    out.rmse = alg.rmse(out.ts);
    out.rmsd = alg.rmsd(out.ts);
    return;
  end
  
  % parameter bounds
  tau = in.tau;
  D = model.nD;
  L_max = ceil(D/2);
  M_max = min(40,2*D); dM = 5;
  Ms = unique([1,dM:dM:M_max,M_max]);
  out.rmse = -ones(L_max,M_max);
  out.ret  = -ones(L_max,M_max);
  
  % check for existing results
  tmp_file = [];
  if(isfield(in,'file_path') && isfield(in,'ID'))
    [~,tmp_file,~] = fileparts(in.file_path);
    tmp_file = ['./tmp/',tmp_file,sprintf('_ID=%d',in.ID),'.mat'];
    if(exist(tmp_file,'file'))
      load(tmp_file);
      dSize = [L_max,M_max] - size(out.rmse);
      if(any(dSize > 0))
        out.rmse = padarray(out.rmse,dSize,-1,'post');
        out.ret  = padarray(out.ret ,dSize,-1,'post');
      end
    else
      save(tmp_file,'out');
    end
  end
  
  % TODO for now, use shorter estimation window
  %alg.N_max = alg.N_min;
  
  % run algorithm
  function ret = run_alg(L,M)
    display(sprintf('\nrunning L = %d, M = %d',L,M));
    if(out.ret(L,M) ~= -1)
      ret = out.ret(L,M);
      display('result exists');
      display(sprintf('rmse = %g',out.rmse(L,M)));
      return;
    end
    obs.nL = L;
    obs.set_uniform(tau,M); 
    alg.max_rank = in.max_rank;
    alg.N_skip = in.N_skip;
    alg.reset_obs();
    alg.run();
    rmse = alg.rmse;
    ret  = rmse < rmse_tol;
    out.rmse(L,M) = alg.rmse;
    out.ret(L,M)  = ret;
    display(sprintf('rmse = %g',rmse));
    % save temporary results
    if(exist(tmp_file,'file'))
      save(tmp_file,'out');
    end
  end
  
  % grid search on L,M
  done = false;
  for L = 1:L_max
  for M = Ms
    ret = run_alg(L,M);
    % stop at first failure after success
    idx = find(out.ret(L,1:M) == 1,1,'last');
    if(~isempty(idx) && idx < M)
      done = true;
      break;
    end
    % stop if converged at highest value of M
    if(ret && M == max(Ms))
      done = true;
      break;
    end
  end
  if(done)
    break;
  end
  end
  
  % remove any unneeded rows
  %mask = all(out.ret == -1,2);
  %out.ret = out.ret(~mask,:);
  %out.rmse = out.rmse(~mask,:);
  
  % binary search for lower bound
  [L,M_max] = find(out.ret == 1,1,'first');
  M_min = max(M_max-dM,1);
  while(M_max-M_min > 1)
    M = floor((M_max+M_min)/2);
    ret = run_alg(L,M);
    if(ret)
      M_max = M;
    else
      M_min = M;
    end
  end
  
  % binary search for upper bound
  [L,M_min] = find(out.ret == 1,1,'last');
  M_max = min(M_min+dM,max(Ms));
  while(M_max-M_min > 1)
    M = ceil((M_max+M_min)/2);
    ret = run_alg(L,M);
    if(ret)
      M_min = M;
    else
      M_max = M;
    end
  end
  
  % output
  [L,M] = find(out.ret == 1,1,'first');
  display(sprintf('\nLower bound: L=%d, M=%d',L,M));
  [L,M] = find(out.ret == 1,1,'last');
  display(sprintf('Upper bound: L=%d, M=%d',L,M));
  % delete tmp file
  %if(exist(tmp_file,'file'))
  %  delete(tmp_file);
  %end
end

%{
  % bounds
  function [L,M] = bounds(L,M)
    tmp = reshape(permute(out.ret,[2,1]),[],1);
    idx = M*M_max + L;
    idx0 = find(tmp(1:idx-1) ~= -1,1,'last');
    idx1 = find(tmp(idx+1:end) ~= -1,1,'first');
    [M(1),L(1)] = ind2sub(idx0,[M_max,L_max]);
    [M(2),L(2)] = ind2sub(idx1,[M_max,L_max]);
  end

  L = L_max; M = 1;
  while(true)
    run_alg(L,M);
    [L_bnd,M_bnd] = bounds(L,M);
    if(out.ret(L,M))
      if(M > 1)
        M = ceil(M/2);
      else
        L = ceil(L/2);
        M = 1;
      end
    else
      if(M < M_max)
        M = min(2*M,M_max);
      else
        L = min(2*L,L_max);
        M = 1;
      end
    end
  end
%}
