function ret = calc_Du()
  Ds = 5:5:100;
  nICs = 5;
  ret = zeros(length(Ds),nICs);
  for iD = 1:length(Ds)
    for iIC = 1:nICs
      ret(iD,iIC) = calc_L96_Du(Ds(iD),iIC);  
    end
  end
end

% compute average dimension of unstable subspace from 0.5(df + df^tr)
function ret = calc_L96_Du(D,nIC)
  if(nargin < 2)
    nIC = 1;
  end
  if(nargin < 1)
    D = 10;
  end
  
  % initialize model
  init.nD = D;
  model = L96Model;
  model.initialize([],init);
  
  % initial conditions
  rng(nIC);
  x_true = model.rand_x0();
  x = x_true;

  % estimate Lc
  dt = 0.01;
  my_ode = @(t,x)rk4step(@model.f,t,x,dt);
  n = 0;
  mu_n = 0;
  var_n = 0;
  tol = 1.e-2;
  N_min = 100;
  N_max = N_min*100;
  done = false;
  while(~done)
    % model jacobian
    t = n*dt;
    [~,df] = model.f(t,x);
    
    % iterate dynamics
    x = my_ode(t,x);
    n = n+1;
    
    % average unstable subspace
    q_n = sum(eig(df + df') > 0.);
    
    % incremental update for variance and mean
    if(n > 1)
      var_n = (n-2)/(n-1)*var_n + (q_n - mu_n).^2/n;
    end
    mu_n =(q_n + (n-1)*mu_n)/n;  
    
    % debugging
    %if(~exist('cache','var'))
    %  cache = [];
    %end
    %cache = [cache,q_n];
    %norm(mean(cache,2) - mu_n)
    %norm(var(cache,0,2) - var_n)
    
    % check relative standard error in the mean for largest GLE
    if(n > N_min)
      sem = sqrt(var_n)/sqrt(n);
      rel_sem = sem(1)/abs(mu_n(1));
      if(n >= N_max || rel_sem < tol)
        done = true;
      end
      if(mod(log2(n),1) == 0)
        display([rel_sem,mu_n(1)]);
      end
    end
  end
  
  ret = mu_n;
  
end
  