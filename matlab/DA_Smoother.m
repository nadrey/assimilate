classdef (Abstract) DA_Smoother < DA_Algorithm
properties
  is_filter = false;
  i;
  psi; dpsi;
  sqrt_Rb; sqrt_Rm; sqrt_Rf;
  Y_idxs;
  max_iters = 10;
  pinv_tol = eps;
  step_tol = 1.e-3;
  do_adjoint  = false;
  do_gradient = false;
  check_rmse = false;
  verbose = false;
  alpha = 1;
end

properties(Dependent)
  initializing_Xopt;
end

properties(Abstract)
end

methods(Abstract)
  direct_eval(obj);
end

methods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = DA_Smoother()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  % base initialization
  obj = initialize@DA_Algorithm(obj,prob,in);
  % measurement error inverse covariance
  set_prop(obj,in,'sqrt_Rb',0);
  set_prop(obj,in,'sqrt_Rm',1);
  set_prop(obj,in,'sqrt_Rf',1);
  % options
  set_prop(obj,in,'max_iters',10);
  set_prop(obj,in,'pinv_tol',eps);
  set_prop(obj,in,'step_tol',1.e-3);
  set_prop(obj,in,'do_adjoint',false);
  set_prop(obj,in,'do_gradient',false);
  set_prop(obj,in,'check_rmse',true);
  set_prop(obj,in,'verbose',false);
  set_prop(obj,in,'alpha',1);
  % create smoother data
  obj.data = SmootherData;
  obj.data = obj.data.initialize(obj,in);
  % disable warning
  warning('off','MATLAB:illConditionedMatrix');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Reimplemented for MHE to use a subset of the data
%%% TODO Give sliding analysis window its own class?
function ret = Y(obj)
  ret = obj.prob.data.Y;
  ret = ret(obj.Y_idxs,:);  
end
%%%
function ret = nN(obj)
  ret = obj.Y_idxs(end) - obj.Y_idxs(1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Function to initialize smoother data
function init_data(obj,in)
  % Y time indices
  if(~isfield(in,'Y_idxs'))
    obj.Y_idxs = 1:obj.prob.data.td.nNp1;
  else
    obj.Y_idxs = in.Y_idxs;
    obj.data.td.nN = max(obj.Y_idxs)-min(obj.Y_idxs);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = step(obj)
  obj.i = obj.i + 1;
  ret = ~is_done(obj);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function reset(obj)
  obj.data.reset();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% reset object b/c obs operator has changed
function reset_obs(obj)
  obj.sqrt_Rm = obj.sqrt_Rm(1);
  % reinitialize X_opt
  obj.data.X_opt = [];
  obj.data.U_opt = [];
  obj.X_opt();
  obj.reset();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function my_display(obj,in)
  if(obj.verbose)
    disp(in);
  end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = is_done(obj)
  ret = true;
  if(obj.verbose)
    msg = sprintf('iteration %d, opt. rmse %1.4e, action level %1.4e',...
                    obj.i,obj.opt_rmse,0.5*obj.psi.'*obj.psi);
    obj.my_display(msg)
  end
  if(obj.check_rmse && ~obj.initializing_Xopt && obj.opt_rmse < obj.rmse_tol)
    obj.my_display('-- done: RMSE converged');
  elseif(any(~isfinite(obj.X)))
    obj.my_display('-- done: unbounded solution');
  elseif(obj.i > obj.max_iters)
    obj.my_display('-- done: max iterations exceeded');
  else
    ret = false;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function initialize_run(obj)
  obj.i = 1;
  obj.data.reset();
  % message
  if(obj.verbose)
    if(~obj.initializing_Xopt)
      obj.my_display(sprintf('running...'));
    else
      obj.my_display(sprintf('computing optimal solution...'));
    end
  end
  % initial objective eval
  [obj.est.X,obj.psi,obj.dpsi] = obj.direct_eval();
  % action level
  A_i = 0.5*obj.psi'*obj.psi;
  if(obj.verbose)
    msg = sprintf('iteration %d, opt. rmse %1.4e, action level %1.4e',...
                    obj.i,obj.opt_rmse,0.5*obj.psi.'*obj.psi);
    obj.my_display(msg)
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function finalize_run(obj)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% check for error growth
function ret = accept_step(obj,psi_i,psi_ip1)
  A_i = 0.5*(psi_i'*psi_i);
  A_ip1 = 0.5*(psi_ip1'*psi_ip1);
  ret = false;
  if(A_ip1 >= A_i)
    obj.my_display('-- done: objective increased');
  elseif(A_i - A_ip1 <= obj.step_tol*A_i)
    obj.my_display('-- done: step size too small');
  else
    ret = true;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
function [X_opt,U_opt] = X_opt(obj)
  X_opt = [];
  if(~obj.prob.data.is_twin_exp)
    return;
  elseif(isempty(obj.data.X_opt))
    tmp.X0 = obj.est.X0;
    tmp.X  = obj.est.X;
    tmp.U0 = obj.est.U0;
    tmp.U  = obj.est.U;
    tmp.psi = obj.psi;
    tmp.dpsi = obj.dpsi;
    obj.est.X0 = obj.prob.data.X;
    obj.est.U0 = zeros(obj.nN+1,obj.nD);
    obj.data.X_opt = -1; % prevent infinite loop in opt_rmse 
    obj.run();
    obj.data.X_opt = obj.est.X;
    obj.data.U_opt = obj.est.U;
    obj.est.X0 = tmp.X0;
    obj.est.U0 = tmp.U0;
    obj.est.X = tmp.X;
    obj.est.U = tmp.U;
    obj.psi = tmp.psi;
    obj.dpsi = tmp.dpsi;
  end
  X_opt = obj.data.X_opt;
  if(nargout > 1)
    U_opt = obj.data.U_opt;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
function ret = true_rmse(obj)
  if(obj.prob.data.is_twin_exp)
    ret = rms(rms(obj.prob.data.X - obj.est.X));
  else
    warning('DA_Smoother::true_rmse: rms error only defined in twin exp.');
    ret = obj.rmsd();
  end
end
%%% 
function ret = opt_rmse(obj)
  % careful not to call this while generating X_opt
  if(obj.initializing_Xopt)
    ret = [];
    return;
  end
  if(obj.prob.data.is_twin_exp)
    ret = rms(rms(obj.X_opt - obj.est.X));
  else
    warning('DA_Smoother::opt_rmse: rms error only defined in twin exp.');
    ret = obj.rmsd();
  end
end
%%%
function ret = obs_rmsd(obj)
  ret = rms(rms(obj.prob.data.Y - obj.obs.h(obj.est.td.ts,obj.est.X)));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% estimate critical radius based on (inverse) condition number
function ret = rho_c(~,psi,dpsi)
  ret = [];
  if(nargin < 3)
    return;
  end
  if(any(~isfinite(reshape(full(dpsi),[],1))) || any(~isfinite(psi)))
    ret = inf;
    return;
  end
  ver = version;
  if(false && issparse(dpsi))
    S_max = svds(dpsi,1,'largest');
    S_min = svds(dpsi,1,'smallest');
    %r = qr(dpsi,0); 
    %beta = svds(r\(r'\dpsi'),1,'largest');
  else
    S = svd(full(dpsi));
    S_max = S(1);
    S_min = S(end);%S(find(S > tol,1,'last'));
    %beta = norm(pinv(dpsi,obj.pinv_tol));
  end
  %beta_0 = 1/S_min^2;
  %lambda = nan;%norm(d2psi(:,3));
  %c = norm(psi);
  inv_kappa = S_min/S_max;
  %rc_est1 = 2/beta*(1/lambda - c*beta_0);
  %rc_est2 = 2/S_max*(S_min^2/lambda - c);
  ret = inv_kappa;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set/get methods
function set.sqrt_Rb(obj,val)
  if(isscalar(val))
    obj.sqrt_Rb = val;%val*ones(obj.nD,1);
  else
    error('DA_Smoother::set.sqrt_Rb: only scalar covariances are supported');
  end
  %{
  elseif(isvector(val))
    if(length(val) == obj.nD)
      obj.sqrt_Rb = val;
    else
      error('DA_Smoother::set.sqrt_Rb: improper length');
    end
  else
    error('DA_Smoother::set.sqrt_Rb: only diagonal covariances are supported');
  end
  %}
end
%%%
function set.sqrt_Rm(obj,val)
  if(isscalar(val))
    obj.sqrt_Rm = val;%val*ones(obj.nL,1);
  else
    error('DA_Smoother::set.sqrt_Rm: only scalar covariances are supported');
  end
  %{
  elseif(isvector(val))
    if(length(val) == obj.nL)
      obj.sqrt_Rm = val;
    else
      error('DA_Smoother::set.sqrt_Rm: improper length');
    end
  else
    error('DA_Smoother::set.sqrt_Rm: only diagonal covariances are supported');
  end
  %}
end
%%%
function set.sqrt_Rf(obj,val)
  if(isscalar(val))
    obj.sqrt_Rf = val;%val*ones(obj.nD,1);
  % use cell array for Rf annealing
  elseif(iscell(val))
    obj.sqrt_Rf = val;
  else
    error('DA_Smoother::set.sqrt_Rf: only scalar covariances are supported');
  end
  %{
  elseif(isvector(val))
    if(length(val) == obj.nD)
      obj.sqrt_Rf = val;
    else
      error('DA_Smoother::set.sqrt_Rf: improper length');
    end
  else
    error('DA_Smoother::set.sqrt_Rf: only diagonal covariances are supported');
  end
  %}
end
%%%
function ret = get.initializing_Xopt(obj)
  ret = isscalar(obj.data.X_opt) && obj.data.X_opt == -1;
end
end
end