%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef L96Model < DA_Model
properties
  % number of states/parameters/inputs
  nD, nP = 0, nU = 0;
  % state/parameter bounds
  x_bounds = [-25,25];
  p_bounds = [5,10];
  % cache d2f, since it is constant
  d2f;
  % parameter modes
  estimate_params = false;
  input_mode = 'global';
  param_mode = 'static';
  p = []; u = [];
  % maximum global Lyapunov exponent
  gle_max;
end

properties (Access = private)
  p_vals = [];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Public methods
methods
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Constructor
function obj = L96Model()
  obj.nD = 10;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = struct;
  end
  if(nargin < 2)
    in = struct; prob = [];
  end
  % base initialization
  [obj,prob,in] = initialize@DA_Model(obj,prob,in);
  % number of states
  obj = set_prop(obj,in,'nD',obj.nD);
  % state bounds
  obj = set_prop(obj,in,'x_bounds',obj.x_bounds);
  % parameter bounds
  obj = set_prop(obj,in,'p_bounds',obj.p_bounds);
  % parameter mode
  obj = set_prop(obj,in,'param_mode',obj.param_mode);
  % input mode
  obj = set_prop(obj,in,'input_mode',obj.input_mode);
  % estimate parameters
  obj = set_prop(obj,in,'estimate_params',obj.estimate_params);

  % initialize parameters and inputs
  obj.p = []; obj.nP = 0;
  obj.u = []; obj.nU = 0;
  if(strcmp(obj.param_mode,'static'))
    if(strcmp(obj.input_mode,'global'))
      obj.p_vals = 8.17;
      if(obj.estimate_params)
        obj.nP = 1;
        obj.p = @obj.static;
        obj.nU = 0;
        obj.u = @obj.null;
      else
        obj.nP = 0;
        obj.p = @obj.null;
        obj.nU = 1;
        obj.u = @obj.static;
      end
    elseif(strcmp(obj.input_mode,'local'))
      obj.p_vals = 8.17*ones(obj.nD,1);
      if(obj.estimate_params)
        obj.nP = obj.nD;
        obj.p = @obj.static;
        obj.nU = 0;
        obj.u = @obj.null;
      else
        obj.nP = 0;
        obj.p = @obj.null;
        obj.nU = obj.nD;
        obj.u = @obj.static;
      end
    end
  end
  if(isempty(obj.p) || isempty(obj.u))
    error(['L96Model::initialize:unknown parameter mode ',in.p_mode]);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Model-dependent initializations of time domain
function init_time_domain(obj,data,in)
  function set_prop(key,val)
    if(~isfield(in,key)); 
      in.(key) = val; 
    end
  end
  set_prop('dt',0.01);
  set_prop('T',10);
  td = TimeDomain;
  data.t_domain = td.initialize(in);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Model-dependent initializations of twin data
function init_twin_data(obj,data,in)
  function set_prop(key,val)
    if(~isfield(in,key)); 
      data.(key) = val; 
    else
      data.(key) = in.(key);
    end
  end

  % initialize time domain
  obj.init_time_domain(data,in);
  dt = data.t_domain.dt;

  % initalize ODE solver
  set_prop('ode',@rk4step);
  if(isa(data.ode,'char'))
    data.ode = eval(data.ode);
  end

  % true initial state
  if(isfield(in,'x_true'))
    x0 = in.x_true;
  else
    x0 = obj.rand_x0(in);
    % integrate to remove transient
    for n = 1:1000
      t = 0;
      x0 = data.ode(@obj.f,t,x0,dt);
    end
  end
  data.x_true = x0;

  % initial state estimate
  [x0,p0] = obj.rand_x0(in);
  set_prop('x_est',x0);

  % initial parameters
  if(obj.nP > 0)
    set_prop('p_true',obj.p_vals);
    set_prop('p_est',p0); 
  end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Model-dependent initializations of true data
function init_true_data(obj,data,in)
  function set_prop(key,val)
    if(~isfield(in,key)); 
      data.(key) = val; 
    else
      data.(key) = in.(key);
    end
  end

  % initialize time domain
  obj.init_time_domain(data,in);

  % ODE solver
  set_prop('ode',@rk4step);
  if(isa(data.ode,'char'))
    data.ode = eval(data.ode);
  end

  % initial state estimate
  [x0,p0] = obj.rand_x0(in);
  set_prop('x_est',x0);

  % initial parameter estimate
  if(obj.nP > 0)
    set_prop('p_est',p0); 
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Random initial conditions
function [x0,p0] = rand_x0(obj,in)
  x0 = rand(obj.nD,1)*diff(obj.x_bounds,1,2) + obj.x_bounds(1);
  p0 = rand(obj.nP,1)*diff(obj.p_bounds,1,2) + obj.p_bounds(1);

  % on attractor
  dt = 0.01; N = 1000;
  for n = 1:N
    x0 = rk4step(@obj.f,0,x0,dt);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Continuous model evaluation
function [f,df,d2f] = f(obj,t,x,p,u)
  if(nargin < 5 || isempty(u))
    u = obj.u(t);
  end
  if(nargin < 4 || isempty(p))
    p = obj.p(t);
  end

  % dx/dt
  x_r1 = circshift(x,-1);
  x_l1 = circshift(x,1);
  x_l2 = circshift(x,2);
  f = -x + x_l1.*(x_r1 - x_l2);
  f = f + p + u;
  if(nargout == 1)
    return;
  end
  D = length(x);
  df = -eye(D);
  df = df + circshift(diag(x_l1),[0,1]);
  df = df - circshift(diag(x_l1),[0,-2]);
  df = df + circshift(diag(x_r1 - x_l2),[0,-1]);
  if(nargout == 2)
    return;
  end
  if(isempty(obj.d2f))
    obj.d2f = zeros(D,D,D);
    for i = 1:D
      il1 = mod(i-2,D)+1;
      il2 = mod(i-3,D)+1;
      ir1 = mod(i,D)+1;
      for j = 1:D
      for k = 1:D
        if(j == il1 && k == ir1)
          obj.d2f(i,j,k) = 1;
        elseif(j == ir1 && k == il1)
          obj.d2f(i,j,k) = 1;
        elseif(j == il1 && k == il2)
          obj.d2f(i,j,k) = -1;
        elseif(j == il2 && k == il1)
          obj.d2f(i,j,k) = -1;
        end
      end
      end
    end
  end
  d2f = obj.d2f;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter sensitivities
function [dfdp,varargout] = dfdp(obj,t,x,p,u)
  if(obj.nP == 0)
    dfdp = zeros(obj.nD,obj.nP); 
  elseif(obj.nP == 1)
    dfdp = ones(obj.nD,1);
  elseif(obj.nP == obj.nD)
    dfdp = eye(obj.nD);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter modes
function p = null(obj,t)
  p = 0;
end
function p = static(obj,t)
  p = obj.p_vals;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set/get methods
function set.nD(obj,val)
  obj.nD = val;
  if(val >= 5 && val <= 100)
    load('./L96_gles.mat');
    obj.gle_max = mean(GLEs{val}(:,1));
  else
    obj.gle_max = 0;
  end
  if(obj.nD == 3)
    error('L96Model::set.nD: D = 3 is broken');
  end
end

end
end
