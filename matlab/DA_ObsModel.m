classdef DA_ObsModel < DA_Base
properties
  prob;
end

properties(Abstract,Dependent)
  is_static;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
function obj = DA_ObsModel()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = struct;
  end
  if(nargin < 2)
    error('DA_ObsModel::initialize: invalid input');
  end
  obj.prob = prob;
end
end
end