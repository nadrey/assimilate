% estimate critical radius for L96 using exponential search
function ret = calc_Rc(in)
  if(nargin < 1)
    in = struct;
  end
  if(~isfield(in,'alg'))
    in.alg = 'SC4DVar';
  end
  in.X0 = 'local';
  display(in);
  
  % initialize problem
  model = L96Model;
  obs = ProjectionObs;
  alg = load_alg(in);
  prob = DA_Problem(model,obs,alg,in);
  
 % TODO support filters
  if(alg.is_filter)
    error('calc_Rc:: filters not supported yet');
  end
  
  % search limits
  delta_r = 10; 
  r_max = 1.e1; r_min = 1.e-6;
  
  % initialize search direction
  X_opt = alg.X_opt;
  r_hat = alg.est.X0 - X_opt;
  
  % exponential search
  r = r_min;
  while(r <= r_max)
    display(sprintf('r = %g',r));
    alg.est.X0 = X_opt + r_hat*r;
    alg.run();
    if(success(prob,X_opt))
      r_min = r;
      r = delta_r*r_min;
    else
      r_max = r;
      break;
    end
  end
  
  % binary search
  r_tol = 1.e-1;
  while(r_max - r_min > r_tol*r_max)
    r = 0.5*(r_max + r_min);
    display(sprintf('r = %g',r));
    alg.est.X0 = X_opt + r_hat*r;
    alg.run();
    if(success(prob,X_opt))
      r_min = r;
    else
      r_max = r;
    end
  end
  %ret = [r_min,alg.rho_c];
  ret = r_min;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = success(prob,X_opt)
  X_true = prob.data.X;
  X_est  = prob.est.X;
  X0_est = prob.est.X0;
  display('true error')
  true_err = rms(X_true(:) - X0_est(:));
  display(sprintf('  initial: %1.4e',true_err));
  true_err = rms(X_true(:) - X_est(:));
  display(sprintf('  final:   %1.4e',true_err));
  display('smoothed error');
  opt_err = rms(X_opt(:) - X0_est(:));
  display(sprintf('  initial: %1.4e',opt_err));
  opt_err = rms(X_opt(:) - X_est(:));
  display(sprintf('  final:   %1.4e\n',opt_err));
  err_tol = max(0.5*opt_err,1.e-6);
  ret = opt_err < err_tol;
end
