classdef (Abstract) DA_Filter < DA_Algorithm
properties
  is_filter = true;
  N_skip;
  auto_cutoff;
  N_min, N_max; 
  rmse_cache;
  rmsd_cache;
  plot_rmse;
end

methods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Constructor
function obj = DA_Filter()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = struct;
  end
  if(nargin < 2)
    error('DA_Filter::initialize: invalid input');
  end
  % base initialization
  obj = initialize@DA_Algorithm(obj,prob,in);
  % create filter data
  obj.data = FilterData;
  obj.data = obj.data.initialize(obj,in);
  % lag between analyses
  set_prop(obj,in,'N_skip',1); 
  % adaptive cutoff
  sf = 1/obj.dt/obj.model.gle_max;
  set_prop(obj,in,'auto_cutoff',true);
  set_prop(obj,in,'N_min',round(1.e1*sf)); 
  set_prop(obj,in,'N_max',round(1.e2*sf));
  set_prop(obj,in,'plot_rmse',false);
  % TODO Handle this better
  prob.data.td.nN = obj.N_max;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Callback to initialize FilterData
function init_data(obj,data,in)
  % default X0
  if(~isfield(in,'X0'))
    x0 = 'global';
  else
    x0 = in.X0;
  end
  
  data.x0 = [];
  if(isnumeric(x0))
    data.x0 = x0;
  elseif(strcmpi(x0,'global'))
    init_rand(data.x0_est_seed);
    data.x0 = obj.model.rand_x0;
  elseif(strcmpi(x0,'local'))
    init_rand(data.x0_est_seed);
    nD = obj.nD;
    dx0 = rand(nD,1);
    dx0 = dx0/rms(dx0(:));
    r = obj.rmse_tol;
    dx0 = dx0*r;
    x0_opt = obj.prob.data.x0;
    data.x0 = x0_opt + dx0;
  else
    error('DA_Filter::init_data: invalid input');
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = est(obj)
  ret = obj.data;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = step(obj)
  ret = ~obj.is_done();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function reset(obj)
  obj.data.reset();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% reset object b/c obs operator has changed
function reset_obs(obj)
  obj.sqrt_Rm = obj.sqrt_Rm(1);
  obj.reset();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function initialize_run(obj)
  obj.reset();
  
  % use sliding window to check for early convergence
  obj.rmse_cache = zeros(obj.N_min,1);
  obj.rmse_cache(end) = obj.rmse;
  % make similar rmsd cache available for use
  obj.rmsd_cache = zeros(obj.N_min,1);
  obj.rmsd_cache(end) = obj.rmsd;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ret = accept_step(obj,t,x)
  ret = true;
  
  % save step
  np1 = obj.td.t_to_np1(t);
  obj.td.nNp1 = np1;
  obj.est.X(np1,:) = x(:).';
  
  % check if estimate has become unbounded
  if(any(~isfinite(x)))
    display('-- done: unbounded solution');
    ret = false;
    return;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function for early termination
function ret = is_done(obj)
  ret = false;
  n = obj.nN; 
  
  % progress output
  delta = 0.1;
  if(n == 1 || floor(n/obj.N_max/delta) > floor((n-1)/obj.N_max/delta))
    fprintf(1,'%0.2f done at %s\n',n/obj.N_max,datestr(now,'HH:MM:SS'));
  end
  
  % check number of interations
  if(n == obj.N_max)
    display('-- done: max iterations exceeded');
    ret = true;
    return;
  end
  
  % update cache
  obj.rmse_cache = circshift(obj.rmse_cache,-1);
  obj.rmse_cache(end) = obj.rmse;
  %obj.rmsd_cache = circshift(obj.rmsd_cache,-1);
  %obj.rmsd_cache(end) = obj.rmsd;
  
  % check for termination
  if(obj.auto_cutoff)
    if(n > obj.N_min && obj.rmse_cache(end) < obj.rmse_tol)
      display('-- done: RMSE converged');
      ret = true; 
      return;
    end
  end

  % sliding window plot for debugging
  if(obj.plot_rmse)
    semilogy(obj.dt*(((n-obj.N_min+1):n)+1),obj.rmse_cache); drawnow();
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute RMS errors
function ret = rmse(obj,ts)
  if(nargin < 2)
    ts = obj.t;
  end
  if(~obj.prob.data.is_twin_exp)
    error('DA_Filter::get.rmse: RMS error only defined in twin exp.');
  end
  ret = zeros(size(ts));
  for it = 1:length(ts)
    t = ts(it);
    ret(it) = rms(obj.prob.data.x(t) - obj.x(t));
  end
end
%%%
% compute RMS deviations
function ret = rmsd(obj,ts)
  if(nargin < 2)
    ts = obj.t;
  end
  ret = zeros(size(ts));
  for it = 1:length(ts)
    t = ts(it);
    x = obj.x(t);
    h = obj.h(t,x);
    y = obj.y(t);
    mask = ~isnan(h);
    ret(it) = rms(h(mask)-y(mask));
  end
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set/get methods
end