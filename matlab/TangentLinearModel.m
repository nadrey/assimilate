%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef TangentLinearModel < handle
properties
  order = 1;
  model = [];
  f = [];
end

properties (Dependent = true)
  nD, nP;
end

methods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % constructor
  function obj = TangentLinearModel()
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % get methods
  function ret = get.nD(obj)
    ret = obj.model.nD;
  end
  function ret = get.nP(obj)
    ret = obj.model.nP;
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % initialize
  function obj = initialize(obj,model,in)
    if(nargin < 3)
      in = struct;
    end
    obj.model = model;
    obj.f = @(t,x,p)obj.var_eqn_o1(t,x,p);
  end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % evaluate the variational equation and model equation in tandem
  function dxdt = var_eqn_o1(obj,t,X,p)
    if(nargin < 4 || isempty(p))
      p = obj.model.p(t);
    end
    nD = obj.nD; nP = obj.nP;
    x = X(1:nD);
    Phi_x = reshape(X(nD+1:nD*(nD+1)),nD,nD);
    Phi_p = reshape(X(nD*(nD+1)+1:nD*(nD+nP+1)),nD,nP);

    % propagate state var. eqn.
    [fx,df] = obj.model.f(t,x,p);
    dPhi_x = df*Phi_x;

    % propagate parameter var. eqn
    dPhi_p = df*Phi_p + obj.model.dfdp(t,x,p);

    % propagate variational equation
    dxdt = vertcat(fx(:),dPhi_x(:),dPhi_p(:));
  end
end
end