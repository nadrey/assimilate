function x = rk4step(f,t,x0,dt)
  k1 = dt*feval(f,t,x0);
  k2 = dt*feval(f,t+0.5*dt,x0 + k1/2);
  k3 = dt*feval(f,t+0.5*dt,x0 + k2/2);
  k4 = dt*feval(f,t+dt,x0 + k3);
  x = x0 + (k1 + 2*k2 + 2*k3 + k4)/6;
end