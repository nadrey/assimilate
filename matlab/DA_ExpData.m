classdef (Abstract) DA_ExpData < DA_Base
properties
  prob; td; sigma_m;
end

properties (Abstract)
  is_twin_exp;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods (Abstract)
  Y(obj,Np1), y(obj,t);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
function obj = DA_ExpData()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = struct;
  end
  if(nargin < 2)
    error('DA_ExpData::initialize: invalid input');
  end
  obj.prob = prob;
  % initialize time domain
  obj.td = TimeDomain;
  obj.td = obj.td.initialize(prob,in);
  % initialize noise level
  obj = set_prop(obj,in,'sigma_m',0);
end
end
end