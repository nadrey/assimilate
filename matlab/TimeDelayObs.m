%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef TimeDelayObs < DA_ObsModel
properties
  % list of time delays
  tau = [];
  % internal observation operator
  obs;
end

properties(Dependent)
  % total number of `measurements', including time delays
  nL;
  % embedding dimension
  nM;
  % static observation operator
  H; is_static;
  % is uniform embedding
  is_uniform;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Constructor
function obj = TimeDelayObs()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function obj = initialize(obj,prob,in)
  if(nargin < 3)
    in = struct;
  end
  % base initialization
  obj = initialize@DA_ObsModel(obj,prob,in);
  % initialize internal obs model
  obj.obs = ProjectionObs();
  obj.obs.initialize(prob,in);
  % default is no time delays
  set_prop(obj,in,'tau',0);
  % nM sets uniform tau [0:(nM-1)]*tau
  set_prop(obj,in,'nM',1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is needed to distinguish between time-delay `data'
% generated from sampling the time series, and time-delay model output,
% which requires running the forecast model forward/backward from a given 
% x and t. 
function ys = h_data(obj,ts)
  data = obj.prob.data;
  td = data.td;
  Np1 = td.nNp1;
  nM = obj.nM;
  nL = obj.obs.nL;
  tau = obj.tau;
  tau_np1s = td.t_to_np1(tau);
  np1s = td.t_to_np1(ts);
  ys = NaN(length(ts),nM*nL);
  
  % get data
  Y = data.Y(max(tau_np1s)+max(np1s)-1);
  
  % extract time delay data
  for it = 1:length(ts)
    idxs = tau_np1s+np1s(it)-1;
    mask = idxs >= 1 & idxs <= Np1;
    y = Y(idxs(mask),:);
    y = reshape(y.',1,[]);
    idxs = reshape(1:nM*nL,nL,nM);
    idxs = idxs(:,mask);
    idxs = reshape(idxs,1,[]);
    ys(it,idxs) = y;
  end
  
  % convert to vector
  if(length(ts) == 1)
    ys = ys(:);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TODO support nonlinear obs. operators and parameter estimation
function [hs,dhdxs] = h_est(obj,ts,xs)
  ts = ts(:);
  if(isvector(xs))
    xs = reshape(xs,1,[]);
  end
  Np1 = obj.prob.data.td.nNp1;
  td = obj.prob.est.td;
  dt = td.dt;
  np1s = td.t_to_np1(ts);
  nM = obj.nM;
  nL = obj.obs.nL;
  tau = obj.tau;
  hs = zeros(length(ts),nM*nL);
  nD = obj.prob.model.nD;
  f = @(t,x)obj.prob.model.f(t,x);
  Ff = @(t,x)obj.prob.alg.F(t,x,f,+dt);
  Fb = @(t,x)obj.prob.alg.F(t,x,f,-dt);
  % TODO support nonlinear observations
  H = obj.obs.H;

  % split backwards and forwards
  mask = tau < 0;
  nTaus = round(tau/dt);
  nTaus_b = nTaus(mask);
  nTaus_f = nTaus(~mask);

  % construct embedding
  hs = NaN(length(ts),nM*nL,1);
  dhdxs = NaN(length(ts),nM*nL,nD);
  
  for in = 1:length(np1s)
    % backward
    n = 0; np1 = np1s(in);
    x = xs(in,:).'; phi = eye(nD);
    for iM = 1:length(nTaus_b)
      nTau = nTaus_b(end-iM+1);
      if(np1 + nTau < 1)
        break;
      end
      while(n > nTau)
        t = ts(in) + n*dt;
        [x,dF] = Fb(t,x);
        phi = dF*phi;
        n = n-1;
      end
      t = ts(in) + n*dt;
      idxs = (1:nL) + nL*(length(nTaus_b)-iM);
      hs(in,idxs) = transpose(H*x);
      dhdxs(in,idxs,:) = H*phi;
    end

    % forwards
    n = 0; np1 = np1s(in); 
    x = xs(in,:).'; phi = eye(nD);
    for iM = 1:length(nTaus_f)
      nTau = nTaus_f(iM);
      if(np1 + nTau > Np1)
        break;
      end
      while(n < nTau)
        t = ts(in) + n*dt;
        [x,dF] = Ff(t,x);
        phi = dF*phi;
        n = n+1;
      end
      t = ts(in) + n*dt;
      idxs = (1:nL) + nL*(length(nTaus_b)+iM-1);
      hs(in,idxs) = transpose(H*x);
      dhdxs(in,idxs,:) = H*phi;
    end
  end
  
  % convert to vector
  if(length(ts) == 1)
    hs = hs(:);
    dhdxs = squeeze(dhdxs);
    if(size(dhdxs,2) ~= nD)
      dhdxs = dhdxs.';
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% set uniform tau 
function set_uniform(obj,tau,nM)
  obj.tau = (0:(nM-1))*tau;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get methods
%%%
function ret = get.nL(obj)
  ret = obj.nM*obj.obs.nL;
end
%%%
function ret = get.nM(obj)
  ret = length(obj.tau);
end
%%%
%%% TODO Figure out how to support nonlinear obs.
function ret = get.H(obj)
  ret = obj.obs.H;
end
%%%
function set.tau(obj,tau)
  obj.tau = unique(tau);
end
%%%
function set.nL(obj,nL)
  obj.obs.nL = nL;
end
%%%
function set.nM(obj,nM)
  obj.set_uniform(obj.tau,nM);
end
%%%
function ret = get.is_uniform(obj)
  if(obj.nM <= 2)
    ret = true;
  else
    ret = all(diff(diff(obj.tau)) == 0);
  end
end
%%%
function ret = get.is_static(obj)
  ret = any(obj.tau ~= 0);  
end
end
end
