% initialize random numbers
function init_rand(seed)
  % this doesn't work on cluster
  %rng(seed);
  % 
  s = RandStream('mt19937ar','Seed',seed);
  RandStream.setGlobalStream(s);
end