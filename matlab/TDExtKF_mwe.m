function data = TDExtKF_mwe()
  % time domain
  T = 100; % length of assimilation window
  dt = 1.e-2; % fixed time step
  N = T/dt; % number of time steps 
  
  % ODE stepper
  my_ode = @(f,x,t)rk4step(f,x,t,dt);
  
  % time-delay embedding parameters
  M = 5;
  tau = 0.1;
  nTau = tau/dt;
  
  % L96 model definition
  D = 50; % number of states
  p = 8.17; % forcing parameter
  f = @(t,x)eval_L96(t,x,p);
  
  % linear projection observation operator
  L = 3;
  idx_meas = round(linspace(1,D,L)); % measured indices
  h = zeros(L,D);
  for i = 1:length(idx_meas)
    h(i,idx_meas(i)) = 1;
  end
  dh = h; 
  
  % noise covariances
  Q = 1.e0*eye(D);   % model error 
  R = 1.e0*eye(M*L); % observation error
  
  % initial conditions
  x0_true = rand(D,1); % true initial condition
  % integrate initial condition so that it is on attractor
  nInit = 100;
  for n = 1:nInit
    x0_true = my_ode(f,0,x0_true);
  end
  dx0 = (rand(D,1)-0.5);
  dx0 = 1.e-3*dx0/rms(dx0); % initial perturbation
  x0_est = x0_true + dx0; % initial estimate
  P0 = eye(D); % initial covariance
  
  % generate true data trajectory for twin experiment
  nTD = N + (M-1)*nTau;
  x = x0_true;
  data.t = transpose(0:nTD)*dt;
  data.x_true = zeros(nTD+1,D);
  data.x_true(1,:) = x;
  for n = 1:nTD
    t = data.t(n);
    x = my_ode(f,t,x);
    data.x_true(n+1,:) = x;
  end
  y = data.x_true*transpose(h); % measured data
  data.x_true = data.x_true(1:(N+1),:);
  data.t = data.t(1:(N+1),:);
  
  % run tTDExtKF
  x = x0_est;
  data.x_est = zeros(N+1,D);
  data.x_est(1,:) = x;
  S = zeros(L*M,1);
  Y = zeros(L*M,1);
  dS = zeros(L*M,D);
  P = P0;
  Id = eye(D);
  for n = 1:N
    % current estimate time
    t = (n-1)*dt;
    
    % construct embedding
    S(1:L,:) = transpose(h*x);
    Y(1:L,:) = y(n,:);
    dS(1:L,:) = dh;
    X = vertcat(x,Id(:)); % combined state/variational matrix
    for m = 2:M
      for i = 1:nTau
        tt = t + dt*(i-1+(m-1)*nTau);
        X = my_ode(@(t,x)var_eqn_o1(t,x,f,D),tt,X);
      end
      idxs = L*(m-1) + (1:L); 
      S(idxs,:) = transpose(h*X(1:D));
      Y(idxs,:) = y(n+(m-1)*nTau,:);
      dS(idxs,:) = dh*reshape(X(D+1:end),D,D);
    end
    
    % construct Kalman gain
    K = dS*P*transpose(dS) + R;
    K = P*transpose(dS)*pinv(K);
    
    % measurement update
    x = x + K*(Y-S);
    P = (Id-K*dS)*P;
    
    % time update
    t = (n-1)*dt;
    X = vertcat(x,P(:)); % combined state/covariance
    X = my_ode(@(t,x)cov_eqn(t,x,f,D,Q),t,X);
    x = X(1:D); P = reshape(X(D+1:end),D,D);
    
    % save
    data.x_est(n+1,:) = transpose(x);
    
    % running plot of instantaneous synch. error
    if(mod(n+1,10) == 1)
      SE = data.x_true(1:n+1,:) - data.x_est(1:n+1,:);
      SE = rms(SE,2);
      semilogy(data.t(1:n+1),SE);
      drawnow;
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dxdt = var_eqn_o1(t,x,ff,D)
  Phi = reshape(x((D+1):end),D,D);
  x = x(1:D);
  
  % evaluate model
  [f,df] = ff(t,x);
  
  % propagate var. eqn.
  dPhidt = df*Phi;
  
  % propagate state
  dxdt = vertcat(f,dPhidt(:));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dxdt = cov_eqn(t,x,ff,D,Q)
  P = reshape(x((D+1):end),D,D);
  x = x(1:D);
  
  % evaluate model
  [f,df] = ff(t,x);
  
  % propagate covariance using tangent linear approx.
  dPdt = df*P + P*transpose(df) + Q;
  
  % combine state/covariance
  dxdt = vertcat(f,dPdt(:));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% perform a single step of explicit 4th order Runge-Kutta
function x = rk4step(f,t,x0,dt)
  k1 = dt*feval(f,t,x0);
  k2 = dt*feval(f,t+0.5*dt,x0 + k1/2);
  k3 = dt*feval(f,t+0.5*dt,x0 + k2/2);
  k4 = dt*feval(f,t+dt,x0 + k3);
  x = x0 + (k1 + 2*k2 + 2*k3 + k4)/6;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% evaluate vector field dxdt = f(t,x) (and Jacobian df/dx) for Lorenz 96
% model with fixed forcing parameter p
function [f,df] = eval_L96(t,x,p)
  x_r1 = circshift(x,-1);
  x_l1 = circshift(x,1);
  x_l2 = circshift(x,2);
  f = -x + x_l1.*(x_r1 - x_l2) + p;
  if(nargout > 1)
    df = -eye(length(x));
    df = df + circshift(diag(x_l1),[0,1]);
    df = df - circshift(diag(x_l1),[0,-2]);
    df = df + circshift(diag(x_r1 - x_l2),[0,-1]);
  end
end